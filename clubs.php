<?php

include "admin.php";
include "common/tableTools.php";
switch ($action) {
  case "edit":
    include "common/tableRowEdit.php";
    include "head.php";
    echo "<title>Edition club</title>" . nl;

    $col = array("num"      => array(6, "Numéro AWBB"),
        "name"     => array(50, "Nom club"),
        "address"  => array(200, "Adresse principale"),
        "address2" => array(200, "Autre adresse"),
        "jersey"   => array(150, "Couleur équipement")
    );

    $obj = new TableRowEdit;
    $obj->title = "Edition de club";
    $obj->colDef = &$col;
    $obj->editRow();
    exit();

  case "save":
  case "insert":
    $request["_val_updated"] = 1;
}
selectAction();

$col = array("num"      => array(5, "N°", "width:15", "text-align:right"),
    "name"     => array(15, "Nom", "width:100", "text-align:right"),
    "address"  => array(50, "Adresse 1", "width:200"),
    "address2" => array(50, "Adresse 2", "width:200"),
    "jersey"   => array(10, "Couleur équipement", "width:150")
);

$visible = array("name", "num", "address", "address2", "jersey");
include "head.php";
//table itself
echo "<title>Clubs";
echo '</title></head><body onload="buildOpt()"></head><body>';

include "common/tableEdit.php";

class Clubs extends TableEdit
{

//---------------------------------------------------------------------------------------------------
  function buildKey(&$row)
  {
    return "clubs where num=" . $row["num"];
  }

}

$club = new Clubs;
$club->colDef = &$col;
$club->visible = &$visible;
$club->tableName = "clubs";
$club->tableId = "members";
if (!$club->order)
  $club->order = "num";
$club->bHeader = true;
$club->build();




