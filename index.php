<?php

include "common/common.php";

//dump($request);
session_start();
if ($request["remcnx"]) {
  $_SESSION = array();
  if (ini_get("session.use_cookies")) {
    $params = session_get_cookie_params();
    setcookie(session_name(), '', time() - 42000, $params["path"], $params["domain"], $params["secure"], $params["httponly"]);
  }
  session_destroy();
}

if ($_SESSION["bRoot"]) {
  header("Location:menuRoot.php");
  exit();
}

if (!$request["login"]) {
  echo "<body><img src=__NEW__logo_6x6.png>";
  echo "<h1 align=middle>Login administrateur</h1>";
  echo "<form action=index.php method=post><table style=margin:auto>";
  echo "<tr><td>Nom utilisateur : </td><td><input type=text autocomplete=off name=login></td></tr>";
  echo "<tr><td>Mot de passe : </td><td><input type=password name=pwd></td></tr>";
  echo "<tr><td></td><td style=text-align:center><input type=submit value=Ok></td></tr>";
  exit();
}

$res = jmysql_query("select id from admin,members where login='" . addslashes($request["login"]) . "' and pwd='" . addslashes($request["pwd"]) . "' and members.num=admin.num");
if (jmysql_num_rows($res) != 1) {
  echo "<body><img src=__NEW__logo_6x6.png>";
  echo "<h1 align=middle>Nom d'utilisateur ou mot de passe invalide!</h1>";
  exit();
}

$_SESSION["id"] = jmysql_result($res, 0);
header("Location:menuTreasurer.php");
exit();
