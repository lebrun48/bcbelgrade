<?php

include "common/common.php";
echo file_get_contents($_SERVER["DOCUMENT_ROOT"] . "/members/report/hd.html");

$dir = scandir("tmp/report");
foreach ($dir as $v)
  if ($v == "all.html") {
    $all = file_get_contents("tmp/report/all.html");
    unlink("tmp/report/all.html");
    extractTable($all);
    $all = "<h2 style=width:100%;text-align:center>Saison</h2>$all";
    file_put_contents($_SERVER["DOCUMENT_ROOT"] . "/members/report/all.html", $all);
  } else if (substr($v, -5) == ".html") {
    $file = file_get_contents("tmp/report/$v");
    unlink("tmp/report/$v");
    $dt = substr($v, 0, -5);
    $dt = "20" . substr($dt, -2) . "-" . substr($dt, 2, 2) . "-" . substr($dt, 0, 2);
    $tup = jmysql_fetch_row(jmysql_query("select home, away, results from calendar where substr(date,1,10)='$dt' and team=100"));
    extractTable($file, $dt);
    $t = $dt;
    $dt = new DateTime($dt);
    $file = "<h2 style=width:100%;text-align:center>$tup[0] - $tup[1] du " . $weekDays[$dt->format("w")] . $dt->format(" d ") . $months[$dt->format("n")] . $dt->format(" Y") . ($tup[2] ? "  ($tup[2])" : '') . "</h2>$file";
    file_put_contents($_SERVER["DOCUMENT_ROOT"] . "/members/report/$t.html", $file);
  }

foreach ($dir as $v)
  if (strpos($v, '.') === false)
    rename("tmp/report/" . $v, $_SERVER["DOCUMENT_ROOT"] . "/members/report/${t}_$v.png");

echo $file . $all;
echo file_get_contents($_SERVER["DOCUMENT_ROOT"] . "/members/report/expl.html");

function extractTable(&$file, $dt = null)
{
  $b = stripos($file, "<table");
  $e = stripos($file, "</table>", $i);
  $file = substr($file, $b, $e - $b + 8);
  $file = str_replace("\"><b>Player", " all\"><b>Player", $file);
  $file = str_replace("<TD>#", "<td class=all>#", $file);
  $file = str_replace("<TR bgcolor=\"#dddddd\"><TR>", "<TR bgcolor=\"#dddddd\">", $file);
  $file = str_replace("<TR bgcolor=\"#eeeeee\"><TR>", "<TR>", $file);
  $file = str_replace("HOME: Belgrade", "Belgrade", $file);
  $t = 0;
  if ($dt) {
    $file = str_replace("GAMES", "SHOOTS", $file);
    while ($n = strpos($file, '>#', $t)) {
      //dump(htmlspecialchars(substr($file,$n,20)));
      $nb = substr($file, $n + 2, strpos($file, ' ', $n) - $n - 2);
      $t = stripos($file, '<TD>', $n);
      echo "n=$n, t=$t, nb=$nb, ' '=" . strpos($file, ' ', $n) . NL;
      $file = substr($file, 0, $t) . "<td style=text-align:center><img style=text-align:center;cursor:pointer;width:15px;height:15px src=field.jpg onclick=shootClick('${dt}_$nb')></td>" . substr($file, $t + 10);
    }
  }
}
