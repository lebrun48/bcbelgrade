<?php

//coût salle
//$costFields=array("BELGRADE, terrain 1","BELGRADE, terrain 2","ST-SERVAIS, Beau-Vallon","NAMUR, St Louis", "MALONNE, St Berthuin");
$costFields = array(array(5.19, 6.06, 7.80, 6.5, 6.5, 20.23), //"BELGRADE, terrain 1"
    array(5.19, 6.06, 7.80), //"BELGRADE, terrain 2"
    array(3, 3), //"ST-SERVAIS Beau-Vallon"
    array(15, 15), //"NAMUR, St Louis", 
    array(10, 10), //"MALONNE"
);
//0:Ref,1:KM,2:FedeYear,   3:FedePlayer,4:Gift,5:Ext,6:cost,7:team 
$costTeam = array(1   => array(60 + 2, 50, 36000, 13.2 + 6.05, 15), //N1
    5   => array(55 + 2, 50, 1045.9 + 15000, 13.2 + 6.05, 15), //N2
    10  => array(44 + 2, 50, 399.9 + 7500, 13.2 + 6.05, 15), //N3
    15  => array(30 + 2, 25, 261.5 + 4000, 4.2 + 6.05, 15, 0, 0, 20), //SEN_R1
    20  => array(28 + 2, 25, 131.1 + 3000, 4.2 + 6.05, 15), //SEN_R2
    25  => array(22 + 2, 10, 32.7 + 2000, 4.2 + 6.05, 15, 0, 0, 30), //SEN_P1
    30  => array(22 + 2, 10, 22.9 + 1000, 4.2 + 6.05, 15, 0, 0, 35), //SEN_P2
    35  => array(22 + 2, 10, 22.9 + 500, 4.2 + 6.05, 15), //SEN_P3
    40  => array(18 + 2, 15, 22.9, 4.2 + 6.05, 15), //JUN_REG
    45  => array(18 + 2, 15, 22.9, 4.2 + 6.05, 15), //CAD_REG
    50  => array(18 + 2, 15, 22.9, 4.2 + 6.05, 15), //MIN_REG
    55  => array(18 + 2, 15, 22.9, 4.2 + 6.05, 15), //PUP_REG
    60  => array(13 + 2, 10, 22.9, 4.2 + 6.05, 15), //JUN_PROV
    65  => array(13 + 2, 10, 22.9, 4.2 + 6.05, 15), //CAD_PROV
    70  => array(13 + 2, 10, 22.9, 4.2 + 6.05, 15), //MIN_PROV
    75  => array(13 + 2, 10, 22.9, 4.2 + 6.05, 15), //PUP_PROV
    80  => array(9 + 2, 0, 22.9, 4.2 + 6.05, 15), //BENJ
    85  => array(9 + 2, 0, 22.9, 4.2 + 6.05, 15), //POUS
    90  => array(9 + 2, 0, 22.9, 4.2 + 6.05, 15), //PRE_POUS
    95  => array(0, 0, 0, 0, 0), //BABIES	Babies
    115 => array(17 + 2, 10, 22.9 + 500, 4.2 + 6.05, 0, 1500, 300), //P1 DAMES
    120 => array(17 + 2, 10, 22.9 + 300, 4.2 + 6.05, 0, 1500, 300)   //P2 DAMES
);
define("COST_KM", 0.25);
/*
  1037=--- Stage rentrée
  1038=--- Stage Christmas
  1025=--- Stage Pâques
  1026=--- BBQ
  1027=--- Gibier
  1028=--- Choucroute
  1029=--- lasagnes
  1030=--- St Nicolas
  1031=--- VTT
  1039=--- Marche ADEPS
  1032=--- Tournois
  1033=--- Tombola
  1034=--- Gaufres
  1035=--- Vins Alsavins
  1036=--- Vêtements NBCB
  1040=--- Autre */
//compute manifestations
$manif = array(array(1026, 3),
    array(1027, 4),
    array(1028, 2),
    array(1029, 1),
    array(1030, 0),
    array(1034, 5),
    array(1037, -1, array(450, 909)),
    array(1038, -1, array(450, 909)),
    array(1025, -1, array(450, 909)),
    array(1032, -1, array(600, 909))
);

$buvette = array(151  => 15,
    252  => 10,
    951  => 0,
    952  => 0,
    1200 => 5
);

$subsAm = array(array(0, 0),
    array(20, 0, 0),
    array(35, 0, 0),
    array(45, 0, 0)
);
$subsTeam = array(0  => 0,
    40 => 3,
    50 => 2,
    60 => 3,
    70 => 2,
    80 => 1,
    95 => 0
);
