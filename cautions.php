<?php
include "admin.php";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">
    <link rel="shortcut icon" href="<?php echo LOGO_LITTLE_CLUB ?>"__NEW__logo_3x3.jpg" type="image/x-icon" />
          <title>Cautions</title>
  </head>
  <body>
    <?php
    $r = jmysql_query("select cotisations.previous,cotisations.num,members.name,members.firstName,members.category from cotisations,members,category where members.num=cotisations.num and members.type<>0 and (cotisations.caution&0x25)<>0 order by members.category,members.name");
//echo jmysql_error();
    while ($row = jmysql_fetch_assoc($r)) {
      $row["catName"] = getCategory($row["category"], true);
      unset($fam);
      $i = 0;
      $fam[$i] = $row;
      while (true) {
        $r1 = jmysql_query("select cotisations.previous,cotisations.num,members.name,members.firstName,members.category from cotisations,members,category where cotisations.previous=" . $fam[$i]["num"] . " and members.type<>0 and members.num=cotisations.num");
        if (jmysql_num_rows($r1) == 1) {
          $i++;
          $fam[$i] = jmysql_fetch_assoc($r1);
          $fam[$i]["catName"] = getCategory($row["category"], true);
          continue;
        }
        break;
      }
      //dump($fam);
      ?>
      <br>
      <table style="font-family:'trebuchet ms', Arial, Helvetica, sans-serif; height:<?php echo 850 - (sizeof($fam) - 1) * 180 ?>;width: 704px;" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td style=vertical-align:top>
            <img style="width: 170px; height: 170px;position:relative; left:-20px;" alt="" src="<?php echo LOGO_CLUB ?>">
          </td>
          <td  style=vertical-align:top>
            <div style="text-align: right"><?php echo CITY_CLUB ?> le <?php echo $now->format("d/m/Y") ?></div>
            <h3 style=text-align:center><?php echo $fam[0]["name"] . " " . $fam[0]["firstName"] ?></h3>
            <p style=margin-top:50>Madame, Monsieur,</p>
            <p>Si vous désirez récupérer votre caution de 50€ pour l'équipement, prière de compléter le coupon ci-dessous avec <b><u>le N° de compte</b></u> sur lequel votre caution vous sera remboursée. Remettez ensuite ce coupon à votre entraineur en même temps que l'équipement <u>complet et en bon état</u>.</p>
            <?php
            if (sizeof($fam) > 1)
              echo "<p><b>Attention:</b> La caution ne sera remboursée qu'après remise des " . sizeof($fam) . " équipements et coupons de la famille.</p>";
            ?>
            <p>Je vous rappelle que si vous comptez quitter le club, n'oubliez pas de le signaler au secrétaire ou à votre entraineur <b><u>avant le 01 juin</b></u> sinon une retenue sur la caution sera effectuée en vue de récupérer les frais accasionnés par une réinscription inutile à la fédération. Pour cette raison, la caution ne sera remboursée qu'en septembre <?php echo $yearSeason + 1 ?>.</p>  
            <p><b><u>Cependant</b></u>, si vous restez la saison prochaine au <?php echo NAME_CLUB ?> et préférez ne pas récupérer la caution afin qu'elle serve pour l'équipement de la saison prochaine, vous pouvez ignorer ce document et remettre uniquement votre équipement à l'entraineur.</p>
            <br><p style="position:relative; left:300px">Bien à vous.<br><br>Pour le comité, le trésorier.<br>Jacques Meirlaen.</p>

          </td>
        </tr>
        <tr>
      </table>
      <?php
      echo str_repeat("-", 200) . nl;
      foreach ($fam as $i => $val) {
        if ($i <> 0) {
          if ($i == 1)
            echo "<br><br>";
          echo str_repeat("-", 200) . NL;
        }
        echo "<div style='width:704; margin-left:150'>";
        echo "<h3>Demande de remboursement de caution</h3>" . NL;
        echo "<b>" . $val["name"] . " " . $val["firstName"] . "</b> catégorie <b>" . $val["catName"] . NL . NL;
        if ($i == 0)
          echo "N° de compte sur lequel la caution sera versée: ..................................................</b>";
        echo "</div>" . nl;
      }
      echo "<span style=page-break-after:always></span>" . nl;
    }
    ?>

  </body>
</html>
