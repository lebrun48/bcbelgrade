<?php
include"head.php";
include"payOutData.php";
?>
<title>Encodage des présences          
</title>     
</head>     
<body><h1>Suivi de pr&eacute;sence des joueurs</h1>
  <?php
  include "presences.php";

  function buildSanction($var, &$sanction)
  {
    if (empty($var))
      return;

    $n = 0;
    $ns = 0;
    for ($i = 0; $i < strlen($var); $i++) {
      $c = ord($var[$i]);
      if ($c >= 128) {
        $ns = 0;
        $n = $c - 128;
      } else {
        $sanction[$n][$ns] = $c;
        $ns++;
      }
    }
  }

  $team = $request["team"];
  echo "<p>" . $coach->prenom . ', ';
  $bUpdate = $right == UPDATE || $right == ROOT;
  if ($right == DISPLAY || !$bUpdate)
    echo "voici l'encodage de l'équipe</p>";
  else {
    ?>indique dans le tableau ci-dessous les pr&eacute;sences des joueurs en cliquant sur la case correspondante au jour.
    <br>            N'oublie pas d'entrer le n&deg; du maillot du joueur s'il n'est pas encore indiqu&eacute; ou s'il y a eu un changement.
  </p>                
  <p>Indique &eacute;galement le(s) nouveau(x) joueur(s) &agrave; la fin de la liste dans le tableau.
  </p>                
  <p>Si un joueur <u>quitte le club</u> (pas un arrêt temporaire), clique sur la dernière case dans la colonne <b>'X'</b> et indique éventuellement une raison.            
  </p>           
  <p>Ton encodage terminé, clique sur le bouton <b>'Enregistrer'</b>.            
  </p>           
  <?php
}
if ($coach->assemble) {
  $teamFull = '(';
  $t = strtok($coach->team, '-');
  $bd = " style=border-bottom:0";
  while ($t) {
    $t |= CAT_PLAYER;
    $t1 = getCategory($t);
    $aTeams[$t] = $t1["short"];
    $teamFull .= ($t) . ",";
    $t = strtok('-');
  }
  $teamFull = substr($teamFull, 0, -1) . ')';
} else
  $teamFull = '(' . ($team | CAT_PLAYER) . ')';
echo "<h2>Equipe $teamName semaine du " . prDate(0) . " au " . prDate(6) . ".</h2>";
echo "<form method=post action=savePresences.php$basePath&team=$team" . ($right == ROOT ? "&user=root" : "") . ($bList ? "&begin=$begin" : "") . ">";
echo "<table class=main><tr><th$bd>NOM Pr&eacute;nom</th><th$bd" . ($aTeams ? " colspan=" . sizeof($aTeams) : '') . ">N&deg;</th>";
echo "<th$bd width=25px>Lu</th><th$bd width=25px>Ma</th><th$bd width=25px>Me</th><th$bd width=25>Je</th><th$bd width=25px>Ve</th><th$bd width=25px>Sa</th><th$bd width=25px>Di</th><th$bd>Raison absence si connu</th><th$bd>X</th></tr>";
if ($aTeams) {
  echo "<tr><th style=border-top:0></th>";
  foreach ($aTeams as $k => $v)
    echo "<th style=border-top:0;font-size:x-small>$v</th>";
  echo str_repeat("<th style=border-top:0></th>", 8) . "</tr>";
}
$n = 0;
$player = jmysql_query("select num, name, firstName, status, numEquip as numEquip1,numEquip2,numEquip3,category as category1,category2,category3 from members where (category in $teamFull or category2 in $teamFull or category3 in $teamFull) and type<>0 order by " . ($coach->order ? " numEquip," : '') . "name");
if (!$player)
  stop(__FILE__, __LINE__, 'Invalid query: ' . jmysql_error());
$par = 0;
while ($row = jmysql_fetch_assoc($player)) {
  echo "<tr  class=parity$par>";
  $par = 1 - $par;
  echo "<td><input name=Player$n value=" . $row["num"] . " type=hidden>"
  . "<input name=CotiState$n value=" . getCotiState($row) . " type=hidden>"
  . getImgCotiState($row) . "<input readonly=readonly value=\"" . htmlspecialchars($row["name"]) . ' ' . htmlspecialchars($row["firstName"]) . "\" maxlength=45 size=45 type=text></td>";

  if ($aTeams)
    foreach ($aTeams as $k => $v) {
      for ($i = 1; $i <= 3; $i++)
        if ($k == $row["category$i"]) {
          $nPlayer++;
          echo "<td><input name=Num${n}_$i value='" . $row["numEquip$i"] . "' maxlength=2 size=2 type=text></td>";
          $nNumEquip += !empty($row["numEquip$i"]);
          break;
        }
      if ($k != $row["category$i"])
        echo "<td></td>";
    } else
    for ($i = 1; $i <= 3; $i++)
      if ($row["category$i"] == ($team | CAT_PLAYER)) {
        $nPlayer += $i == 1;
        //echo "name=".$row["name"].", row[category$i]=".$row["category$i"]." num=".$row["numEquip$i"].", team=".($team|PLAYERS).NL;
        echo "<td><input name=Num${n}_$i value='" . $row["numEquip$i"] . "' maxlength=2 size=2 type=text></td>";
        $nNumEquip += !empty($row["numEquip$i"]);
        break;
      }
  $pres = "0000000";
  $sanction = array();
  $reason = '';
  $bPaid = false;
  //if ($i==1)
  //$bPaid=jmysql_result(jmysql_query("select count(*) from pay_out where num=".$row["num"]. " and month=0 and (type=".PLAYER_MONTHLY." or type=".PLAYER_BONUS.")"),0);
  if ($bExist) {
    $res = jmysql_query("select pres, sanction, reason from presences where date='" . $date . "' and num=" . $row["num"] . " and team=$team");
    if (!$res)
      stop(__FILE__, __LINE__, 'Invalid query: ' . jmysql_error());
    if (jmysql_num_rows($res) == 1) {
      $pres = jmysql_result($res, 0, "pres");
      $reason = jmysql_result($res, 0, "reason");
      buildSanction(jmysql_result($res, 0, 1), $sanction);
    }
  }
  for ($i = 0; $i < 7; $i++) {
    echo '<td><input ' . ($pres[$i] ? "checked " : "") . 'name="Presence' . $n . '_' . $i . '" type="checkbox">';
    if ($bPaid) {
      echo '<div title="Appliquer une sanction" id="div' . $n . '_' . $i . '" onclick="createSanction(' . "'$n" . '_' . "$i'" . ')" style="color:red; text-align:center; cursor:pointer; border:1px solid black; font-size:75%;">';
      for ($j = 0; $j < strlen($sanction[$i]); $j++)
        if ($sanction[$i][$j])
          echo $sanction[$i][$j] . ",";
      echo '&nbsp;</div>';
      echo '<input type="hidden" id="sanction' . $n . '_' . $i . '" name="Sanction' . $n . '_' . $i . '" value="';
      for ($j = 0; $j < strlen($sanction[$i]); $j++)
        if ($sanction[$i][$j])
          echo $sanction[$i][$j] . ",";
      echo '">';
    }
    echo "</td>";
  }
  echo '<td><input value="' . $reason . '" name="Raison' . $n . '" maxlength="55" size="55" type="text"></td>';
  echo '<td><input name="End' . $n . '" type="checkbox"></td>';
  echo '</tr>' . nl;
  $n++;
}
$r1 = jmysql_query("select num,concat(name,' ',substr(firstName,1,1)) as name,category,category2,category3 from members where type<>0 and substr(birth,1,4)<=$ageMin and substr(birth,1,4)>=$ageMax and category&" . CAT_PLAYER . " and category not in $teamFull and (category2 is null or category2&" . CAT_PLAYER . " and category2 not in $teamFull) and (category3 is null or category3&" . CAT_PLAYER . " and category3 not in $teamFull) order by name");
for ($k = 0; $k < 3; $k++) {
  echo "<tr class=parity$par>";
  $par = 1 - $par;
  echo "<td><select name=newPlayer$k><option></option>";
  for ($i = 0; $i < jmysql_num_rows($r1); $i++) {
    $cat = getCategory(jmysql_result($r1, $i, 2));
    echo "<option value=" . jmysql_result($r1, $i, 0) . ">" . jmysql_result($r1, $i, 1) . " (." . $cat["short"] . ")</option>";
  }
  echo "</select></td>";
  if ($aTeams)
    foreach ($aTeams as $k1 => $v)
      echo "<td><input name=Num$n_$k1 maxlength=2 size=2 type=text></td>";
  else
    echo "<td><input name=Num$n maxlength=2 size=2 type=text></td>";
  for ($i = 0; $i < 7; $i++)
    echo '<td><input name="Presence' . $n . '_' . $i . '" type="checkbox"></td>';
  echo '<td><input name="Raison' . $n . '" maxlength="55" size="55" type="text"></td>';
  echo '</tr>' . nl;
  $n++;
}
?>                          
</table>                  
<p>Indique un &eacute;ventuel commentaire ci-dessous</p>    
<textarea wrap="soft" cols="70" rows="3" name="Com" value="<?php echo $com ?>"><?php echo $com ?></textarea>
<p>
  <?php
  include "tools.php";
  if ($bUpdate) {
    echo '<input id="dummySave" type="submit" style="visibility:hidden">';
    createAction(300, "Enregistrer", "save()");
  }
  if ($right == ROOT || $right == DISPLAY || $bList)
    createAction(450, "Annuler", "window.history.back()", 120);
  ?>                        
</p><br><br>               
</form>     
</body>
</html>
<script type="text/javascript">
  var win = null;
  var bFirst = true;
  function save()
  {
    i = 0;
    $("[name^='Num']").each(function () {
      if (this.value)
        i++;
    });
    if (<?php echo $bRoot || $bNoCaution ? "true||" : '' ?>i /<?php echo $nPlayer ?> > 0.5)
      document.getElementById('dummySave').click();
    else
    if (bFirst)
    {
      alert("Quand tu auras le N° des maillots des joueurs, peux-tu le mettre dans la colonne 'N°'?");
      bFirst = false;
      return;
    }
    document.getElementById('dummySave').click();
  }

  function createSanction(p)
  {
    var loc = "sanction.php?n=" + p;
    if (win && !win.closed)
      win.location.replace(loc);
    else
      win = window.open(loc, "_blank", "scrollbars=yes,left=800,width=700,height=" + screen.availHeight + ",titlebar=no,menubar=no,status=no,location=no");
    win.focus();
  }
</script>