<?php

include "common/myFPDF.php";

//------------------------------------------------------------------------------------------------------------------------
class HeaderClub extends myFPDF
{

  //------------------------------------------------------------------------------------------------------------------------
  function Template()
  {
    $this->SetFont('Times');
    $this->SetMargins(0, 0, 0);
    $this->SetAutoPageBreak(false);
    $this->Image(LOGO_CLUB, 5, 5, 45);

    $this->Image("logo_Alsavin.jpg", 8, 68, 35);
    $this->Image("Mazuin_1.jpg", 10, 105, 30);
    $this->Image("fintro.jpg", 10, 130, 30);
    $this->Image("carrefour.jpg", 10, 200, 30);
    $this->Image("adeps.jpg", 10, 217, 30);
    $this->Image("logo_ville.jpg", 10, 250, 15);
    $this->SetFontSize(10);
    $this->SetLineWidth(0.4);
    $this->Line(5, 283, 203, 283);
    $this->SetXY(5, 285);
    $this->Write(4, NAME_CLUB . " | " . ADDRESS_CLUB . " | ");
    $this->Image("mobile_phone.jpeg", null, null, 4);
    $this->SetXY(100, 285);
    $this->Write(4, PHONE_CLUB . " | ");
    $this->Image("email.jpg", null, null, 4);
    $this->SetXY(127, 285);
    $this->Write(4, EMAIL_CLUB . " |  ");
    $this->Image("website.jpg", null, null, 4);
    $this->SetXY(165, 285);
    $this->Write(4, URI_CLUB);
    $this->Ln();
    $this->SetX(59.3);
    $this->Write(4, "TVA: " . TVA_CLUB . " | Compte " . ACCOUNT_CLUB);
  }

}
