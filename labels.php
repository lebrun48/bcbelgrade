<?php

function printLabel()
{
  global $pdf;
  $pdf->Write(5, "Prix r�duit pour les stagiaires\ndu New BC Alsavin-Belgrade:\n");
  $pdf->setFontSize(15);
  $pdf->setTextColor(0xDC, 0x14, 0x3C);
  $pdf->setX($pdf->getX() + 7, "95�");
  $pdf->Write(10, "95�");
  $pdf->setFontSize(10);
  $pdf->setTextColor(0x0);
  $pdf->Write(10, "   au lieu de 110�");
}

include "common/common.php";
if (!$_GET["cols"]) {
  include "head.php";
  echo "<form action=labels.php method=get target=_blank>";
  $l = "Nombre d'�tiquettes par lignes: <input name=cols type=text size=1 maxlength=2 value=4><br>";
  $l .= "Nombre d'�tiquettes � imprimer: <input name=number type=text size=3 maxlength=3><br>";
  $l .= "A partir de la position <input name=start size=1 type=text value=0><br>";
  $l .= "<input type=submit value=Ex�cuter>";
  echo utf8_encode($l);
  exit();
}

include_once BASE_PATH . "/tools/fpdf/fpdf.php";
define('FPDF_FONTPATH', BASE_PATH . "/tools/fpdf/font");

$pdf = new FPDF;
$pdf->SetAutoPageBreak(false, 0);
$pdf->SetMargins(0, 0, 0);
$pdf->SetFont("Arial", '', 10);

function printLine()
{
  global $pdf, $i, $cols, $bAddr;
  if (!($p = $i % (11 * $cols))) {
    $pdf->AddPage();
  }
  $pdf->SetLeftMargin($x = $cols == 3 ? 10 + 200 / $cols * ($p % $cols) : 6 + ($p % $cols) * 50);
  $pdf->SetXY($x, 12 + 280 / 11 * intval($p / $cols));
  printLabel();
  $i++;
}

if ($i = $request["start"])
  $pdf->AddPage();
$cols = $_GET["cols"];
while ($i < $request["number"] + $request["start"])
  printLine();

$pdf->Output("labels.pdf", 'I');
