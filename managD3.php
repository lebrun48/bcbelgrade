<?php

include "common/common.php";
include "memberData.php";
include "activitySel.php";

include "head.php";
echo "<title>Activité</title>"
?>
<style>
  table.activities
  {
    font-family: Calibri;
    border-collapse:collapse;
  }
  table.activities td 
  {
    font-size:0.9em;
    padding:0px 2px 0px 2px;
    border:3px solid white;
  }

</style>
<?php

echo "</head><body>";
includeTabs();
$r = jmysql_query("select ri,date from activities where date>='" . $now->format("Y-m-d") . "' order by date");
$tab = 0;
while ($tup = jmysql_fetch_row($r)) {
  $dt = new DateTime($tup[1]);
  $dt = $dt->format("d/m/Y");
  echo "<li><a href=#view$tab>$dt</a></li>";
  $ri[$tab] = $tup[0];
  if (++$tab == 10)
    break;
}
echo "</ul>";
echo "<div class=tabcontents>";
for ($kk = 0; $kk < $tab; $kk++) {
  echo "<div id=view$kk>";
  buildActivity($ri[$kk]);
  echo "</div>";
}
echo "</div>";

shareMsg("D3");

