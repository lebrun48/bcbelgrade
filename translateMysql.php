<?php

function TreatDir($dir)
{
  var_dump($dir);
  echo "<br>In directory $dir->path<br>";

  while ($file = $dir->read()) {
    if ($file[0] != '.' && is_dir($file)) {
      echo "va traiter $dir->path/$file<br>";
      TreatDir(dir("$dir->path/$file"));
      continue;
    }

    if (substr($file, -4) != ".php" || $file == "translateMysql.php" || $file == "mysql.php")
      continue;
    $content = file_get_contents("$dir->path/$file");

    global $mysql;
    echo "treat file $dir->path/$file<br>";
    $newContent = str_replace("jmysql_", "jmysql_", $content);
    $newContent = str_replace("jjmysql_", "jmysql_", $newContent);
    if (strlen($newContent) != strlen($content)) {
      file_put_contents("$dir->path/$file", $newContent);
      $offset = 0;
    }
    while ($offset = strpos($newContent, "jmysql_", $offset)) {
      if (substr($newContent, $offset + 7, 5) == "query") {
        $offset += 7;
        continue;
      }
      $mysql[strtok(substr($newContent, $offset + 7, 20), " (")] = 1;
      $offset += 20;
    }
  }
}

TreatDir(dir("."));

$hd = fopen("common/mysql.php", "w");
foreach ($mysql as $k => $v)
  fwrite($hd, "function jmysql_$k() {global \$currentDBLink; return \$currentDBLink->$k();}\n");


