<?php
include "common/common.php";
include "common/tableTools.php";

switch ($action) {
//-----------------------------------------------------------------------------------------------
  case "move":
    //dump($request);
    //$seeRequest=3;
    $o = substr($request["orig"], 4);
    $d = substr($request["dest"], 4);
    if (jmysql_result(jmysql_query("select count(distinct(catOrder)) from cashRegister where ri=$o or ri=$d"), 0) > 1)
      exit();
    $sav = jmysql_fetch_assoc(jmysql_query("select * from cashRegister where ri=$o"));
    $r = jmysql_query("select * from cashRegister where catOrder=" . $sav["catOrder"] . " and ri" . ($o > $d ? "<$o and ri>=$d order by ri desc" : ">$o and ri<=$d order by ri asc"));
    $prev = $o;
    while ($tup = jmysql_fetch_assoc($r)) {
      jmysql_query("update cashRegister set picture=" . (($t = $tup["picture"]) ? "'$t'" : "null") . ", price=" . $tup["price"] . ", product='" . addslashes($tup["product"]) . "' where ri=$prev");
      $prev = $tup["ri"];
    }
    jmysql_query("update cashRegister set picture=" . (($t = $sav["picture"]) ? "'$t'" : "null") . ", price=" . $sav["price"] . ", product='" . addslashes($sav["product"]) . "' where ri=$d");
    exit();

//-----------------------------------------------------------------------------------------------
  case "saveOrder":
    $t = strtok($request["order"], ";");
    $i = 0;
    while ($t) {
      jmysql_query("update cashRegister set catOrder=" . ($i++) . " where category='" . utf8_decode($t) . "'", 1);
      $t = strtok(";");
    }
    exit();

//-----------------------------------------------------------------------------------------------
  case "newCategory";
    include "head.php";
    include "tools.php";
    $seeRequest = 1;
    $n = jmysql_result(jmysql_query("select max(catOrder)+1 from cashRegister"), 0);
    jmysql_query("insert into cashRegister set category='" . addslashes(utf8_decode($request["category"])) . "', catOrder=$n");
  case "categories":
    include_once "head.php";
    include_once "tools.php";
    echo "<title>Products</title></head><body>";
    echo "<h2 style=text-align:center;width:100%>Gestion des catégories</h2>";
    $res = jmysql_query("select category from cashRegister group by category order by catOrder");
    echo "<table class=main style=width:20%;margin:auto><tr><th>Nom ";
    createAction(10, "New", "newCategory()");
    echo "</th></tr>";
    $i = 0;
    while ($tup = jmysql_fetch_row($res))
      echo "<tr draggable=true id=drag" . ($i++) . " ondragstart=drag(event) ondrop=drop(event) ondragover=allowDrop(event)><td>$tup[0]</td></tr>";
    echo "</table>";
    echo "<div style=margin-top:30px;width:100%;text-align:center>";
    createAction(-1, "Enregistrer", "saveOrder()");
    echo "</div>";
    ?>
    <script>
      function saveOrder() {
        var t = '';
        $("td").each(function () {
          t += this.innerHTML + ';';
        });
        $.get("cashRegister.php?action=saveOrder&order=" + encodeURIComponent(t), function (data, status) {
          //alert(data);
          parent.location.reload();
          //me.close(); 
        });
      }
      function allowDrop(ev) {
        ev.preventDefault();
      }
      function drag(ev) {
        ev.dataTransfer.setData("text", ev.target.id);
      }
      function drop(ev) {
        ev.preventDefault();
        var data = ev.dataTransfer.getData("text");
        $("#" + ev.target.parentElement.id).before(document.getElementById(data));
      }
      function newCategory()
      {
        t = prompt("Entre la nouvelle catégorie");
        if (t)
          location.replace("cashRegister.php?action=newCategory&category=" + encodeURIComponent(t));
      }
    </script>
    <?php
    exit();
//-----------------------------------------------------------------------------------------------
  case "edit":
    include "common/tableRowEdit.php";
    include "head.php";

    $col = array("category" => array(20, "Catégorie", "[Mand][Select(#cat)]"),
        "product"  => array(50, "Produit", "[Mand]"),
        "picture"  => array(0, "Image"),
        "price"    => array(10, "Prix")
    );

    class Table extends TableRowEdit
    {

      function ApplyValue($key, $val, $row)
      {
        switch ($key) {
          case "category":
            echo parent::ApplyValue($key, $val, $row);
            include "tools.php";
            createAction(10, "Gérer catégorie", "categories()");
            return;

          case "picture":
            echo "<img src=pictures/$row[$key] onlick=changeImage()>";
        }
        parent::ApplyValue($key, $val, $row);
      }

    }

    $ed = new Table;
    $ed->title = "Produits en vente";
    $ed->colDef = &$col;
    $ed->cat = jmysql_query("select category from cashRegister group by category");
    $ed->editRow();
    ?>
    <script>
      function categories() {
        window.open("cashRegister.php?action=categories", "_blank");
      }
      function changeImage()
      {
      }
    </script>
    <?php
    exit();

//-----------------------------------------------------------------------------------------------
  case "cash":
    ?>
    <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//FR">  
    <head>            
      <meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">            
      <link rel="shortcut icon" href="__NEW__logo_3x3.jpg" type="image/x-icon" />
      <?php
      echo "<title>Caisse</title></head><body style=font-family:Arial>"; // onload=getPCNum()>";
      echo "<table style=width:100%;height:100%><tr><td style=vertical-align:top;width:80%>";
      $res = jmysql_query("select * from cashRegister order by catOrder,ri");
      $j = "products=[";
      $p = 0;
      while ($tup = jmysql_fetch_assoc($res)) {
        if ($cat !== $tup["catOrder"]) {
          if (isset($cat))
            echo "</tr></table></fieldset>";
          $cat = $tup["catOrder"];
          echo nl . "<fieldset style=border-radius:10px;background:#E0F3E0><legend style=color:#009900;font-weight:bold>" . $tup["category"] . "</legend><table style=width:100%><tr>";
          $n = 0;
        }
        if (!(($n) % 5) && $n)
          echo "</tr><tr style=height:10px></tr><tr>";
        $n++;
        $j .= "{name:'" . $tup["product"] . "',price:" . $tup["price"] . ",category:" . $tup["catOrder"] . "},";
        echo "<td style=width:20%><b>" . $tup["product"] . sprintf(" (%0.2f€)", $tup["price"]) . "</b><br>";
        for ($i = 0; $i <= 5; $i++) {
          if ($i == 3)
            echo NL;
          echo "<input onclick=radio(this) " . (!$i ? "checked " : '') . "type=radio value=$i name=prod" . $tup["catOrder"] . "_$p id=${p}_$i><label for=${p}_$i>$i&nbsp;</label>";
        }
        echo NL . "<input onmouseup=input(this) onkeypress=inputKey(event,this) id=$p size=5 maxlength=2 style=width:50px type=number min=6>&lt;Autre&gt;";
        echo "</td>";
        $p++;
      }
      echo "</tr></table>\n</fieldset></td><td style='vertical-align:top;border:solid black 2px'><h3 style=width:100%;text-align:center>Ticket</h3><div id=ticket></div></td></tr></table>";
      echo "<div id=ticketPrint><div style=width:100%;text-align:center><img style=vertical-align:middle src=./__NEW__logo_3x3.jpg></div></div>";
      $j = substr($j, 0, -1) . "];";
      ?>
      <script>
        PCNum =<?php echo $_GET["num"] ?>;
        function fullScreen() {
          var docElm = document.body;
          if (docElm.requestFullscreen)
            docElm.requestFullscreen();
          else if (docElm.mozRequestFullScreen)
            docElm.mozRequestFullScreen();
          else if (docElm.webkitRequestFullScreen)
            docElm.webkitRequestFullScreen();
        }
        //---------------------------------------------------------------------------------------
    <?php echo $j ?>
        var lines = [];
        var tot;
        function pTicket(prod, val) {
          var idx = -1;
          for (i in lines)
            if (lines[i].prod == prod) {
              idx = i;
              break;
            }
          if (val == 0) {
            if (idx >= 0)
              lines.splice(idx, 1);
          } else
            lines[idx < 0 ? lines.length : idx] = {nb: val, prod: prod};
          ticket = "<table>";
          tot = 0;
          for (i in lines) {
            price = products[lines[i].prod].price * lines[i].nb;
            ticket += "<tr><td><b>" + lines[i].nb + "</b></td><td>" + products[lines[i].prod].name + "</td><td>=</td><td style=text-align:right>" + price.toFixed(2) + "€</td></tr>";
            tot += price;
          }
          ticket += "</table><h3>Total : " + tot.toFixed(2) + "€</h3><div style=width:100%;text-align:center><input type=button value=Imprimer onclick=printTicket()></div>";
          document.getElementById("ticket").innerHTML = ticket;
        }
        //---------------------------------------------------------------------------------------
        function radio(obj) {
          prod = obj.id.substr(0, obj.id.indexOf('_'));
          document.getElementById(prod).value = null;
          pTicket(prod, obj.value);
        }
        //---------------------------------------------------------------------------------------
        function input(obj) {
          prod = obj.id;
          for (i = 0; i <= 5; i++)
            document.getElementById(prod + "_" + i).checked = false;
          pTicket(prod, obj.value);
        }
        //---------------------------------------------------------------------------------------
        function inputKey(e, obj) {
          prod = obj.id;
          for (i = 0; i <= 5; i++)
            document.getElementById(prod + "_" + i).checked = false;
          if (window.event) // IE
            keynum = event.keyCode;
          else if (e) // Netscape/Firefox/Opera
            keynum = e.keycode;
          c = String.fromCharCode(keynum);
          s = "0123456789";
          if (s.indexOf(c) == -1)
            return false;
          pTicket(prod, obj.value + c);
        }

        //---------------------------------------------------------------------------------------
        function printTicket() {
          t = document.getElementById('ticketPrint');
          t.style.display = 'inline';
          base = t.innerHTML;
          bLast = false;
          ticket = '';
          for (i in lines)
          {
            p = lines[i].prod;
            if (products[p].category == 4)
            {
              bLast = true;
              continue;
            }
            ticket += "<tr><td><b>" + lines[i].nb + "</b></td><td>" + products[p].name + "</tr>";
          }
          if (ticket.length) {
            document.body.innerHTML = '';
            ticket = base + "<div style='text-align:center;width:100%;20px;margin-bottom:10px'>Caisse " + PCNum + "</div><div style='text-align:center;width:100%;border:2px solid black;font-size:30px;padding:5px;margin-bottom:10px'><b>X-Mas</b></div><table style=width:100%;margin:auto;font-size:20px>" + ticket;
            ticket += "</table><div style='text-align:center;width:100%;font-size:20px;margin-bottom:5px'><span style=font-size:12px;font-weight:bold>Total :" + tot.toFixed(2) + "€</span><b style=margin-left:5px>Merci</b><div><br><br></div></div>";
            t.innerHTML = ticket;
            document.body.appendChild(t);
            print();
          }
          if (bLast) {
            document.body.innerHTML = '';
            ticket = base + "<div style='text-align:center;width:100%;20px;margin-bottom:10px'>Caisse " + PCNum + "</div><div style='text-align:center;width:100%;border:2px solid black;font-size:30px;padding:5px;margin-bottom:10px'><b>X-Mas</b></div><table style=width:100%;margin:auto;font-size:20px>";
            bLast = false;
            for (i in lines) {
              p = lines[i].prod;
              if (products[p].category == 4)
                ticket += "<tr><td><b>" + lines[i].nb + "</b></td><td>" + products[p].name + "</tr>";
            }
            ticket += "</table><div style='text-align:center;width:100%;font-size:20px'><b>Merci</b><div style=color:white>|</div></div>";
            t.innerHTML = ticket;
            document.body.appendChild(t);
            print();
          }
          document.location.reload();
        }

        var PCNum;
        function getPCNum()
        {
          PCNum = prompt("Entre la référence du PC");
        }

      </script>
      <?php
      exit();

//-----------------------------------------------------------------------------------------------
    case "save":
    case "insert":
      $request["_val_catOrder"] = jmysql_result(jmysql_query("select distinct(catOrder) from cashRegister where category='" . addslashes($request["_val_category"]) . "'"), 0);
      break;
  }
  selectAction();

  include "head.php";
  include "common/tableEdit.php";
  echo "<title>Products</title></head><body onload=defaultLoadEvent()>";


  $col = array("category" => array(-1, "Catégorie", "width:150"),
      "product"  => array(-1, "Produit", "width:150"),
      "picture"  => array(0, "Photo", "width:70"),
      "price"    => array(0, "Prix", "", '[money]'),
  );

  class Table extends TableEdit
  {

//---------------------------------------------------------------------------------------------------
    function buildKey($row)
    {
      return "cashRegister where ri=" . $row["ri"];
    }

//---------------------------------------------------------------------------------------------------
    function applyOrder()
    {
      if ($this->order == "category")
        return "order by catOrder $this->dir,ri";
      else
        parent::applyOrder();
    }

//---------------------------------------------------------------------------------------------------
    public function formatTD($key, $row)
    {
      if ($key == "product")
        return "draggable=true id=drag" . $row["ri"] . " ondragstart=drag(event) ondrop=drop(event) ondragover=allowDrop(event)";
    }

//---------------------------------------------------------------------------------------------------
    public function applyDisplay($key, $val, $row, $idxLine)
    {
      switch ($key) {
        case "picture":
          if ($t = $row[$key])
            $dispLine = "<img src=pictures/$t height=50px>";
          break;
      }
      return parent::applyDisplay($key, $val, $row, $idxLine, $dispLine);
    }

  }

//$seeRequest=1;          
  $obj = new Table;
  $obj->colDef = &$col;
  $obj->addCol = "ri";
  $obj->tableName = "cashRegister";
  $obj->tableId = "members";
  $obj->bHeader = true;
  $obj->bInsertNew = true;
  if (!$obj->order)
    $obj->order = "catOrder,ri";
  $obj->build();
  include "tools.php";
  createAction(-1, "Caisse", "cash()");
  ?>
  <script>
    function allowDrop(ev) {
      ev.preventDefault();
    }
    function drag(ev) {
      ev.dataTransfer.setData("text", ev.target.id);
    }
    function drop(ev) {
      ev.preventDefault();
      $.get("cashRegister.php?action=move&orig=" + ev.dataTransfer.getData("text") + "&dest=" + ev.target.id, function (data, result) {
        if (data.length)
          alert(data);
        location.replace("cashRegister.php");
      });
    }

    function cash() {
      PCNum = prompt("Entre le N° du PC");
      document.location.replace("cashRegister.php?action=cash&num=" + PCNum);
    }

  </script>

<?php 
 