<?php
$bAcceptNoId = true;
include "admin.php";
include "common/tableTools.php";
include "head.php";
echo "<title>Entraînements</title><body onload=loadEvent()>";

define('PROP', 0.6); //1 min= 0.5px
define('PROP120', .033); //1 min= 0.033px au-delà de 2h
//dump($request);
$colors = array
    ("FFFFFF", "D3D3D3", "808000", "BA3838", "7FFFD4", "8A2BE2", "DEB887", "5F9EA0", "7FFF00", "D2691E", "FF7F50", "6495ED", "008B8B", "008000", "FF8C00",
    "E9967A", "8FBC8F", "2F4F4F", "00CED1", "1E90FF", "FF00FF", "FFD700", "FF69B4", "F0E68C", "FF4500", "FFB6C1", "CD853F", "FFFF00", "4169E1", "00FF7F");
//$fields=array("BELGRADE, terrain 1","BELGRADE, terrain 2","ST-SERVAIS, Beau-Vallon","NAMUR, St Louis", "MALONNE St-Berthuin", "TEMPLOUX");
$fields = array(
    0 => "BELGRADE, terrain 1",
    1 => "BELGRADE, terrain 2",
    5 => "TEMPLOUX",
    6 => "LA PLANTE",
    7 => "TABORA",
    3 => "MALONNE St-Berthuin",
    2 => "Autre",
    4 => "Autre"
);
if (!($grid = $_GET["grid"]))
  $grid = 0;
$bNew = $now->format("Y") == ($yearSeason + 1) && $now->format("m-d") < "08-01";
$cat = ($grid == -1 ? 'n' : '') . "category";
$r = jmysql_query("select $cat from members where type<>0 and ${cat}&" . CAT_PLAYER . " group by $cat");
while ($tup = jmysql_fetch_row($r)) {
  $c = getCategory($tup[0]);
  $c = unserialize($c["teamSite"]);
  $team = $tup[0] & 0xFFFF;
  $teamsColor[$team] = $c[$team % 10]->color;
}

//---------------------------------------------------------------------------------------------------
function rappel($buffer)
{
  chdir(dirname($_SERVER['SCRIPT_FILENAME']));
  $f = fopen("Entrainements.xls", "wt");
  fwrite($f, $buffer);
  return $buffer;
}

switch ($action) {
  case "export":
    $bExport = true;
    ob_start("rappel");
  case "print":
    $bPrint = true;
    break;

  case "deleteGrid":
    jmysql_query("delete from training where grid=" . $_GET["grid"]);
    $grid = 0;
    break;

  case "new":
    //new grid
    if (!strlen($_GET["week"])) {
      $r = jmysql_query("select grid from training group by grid");
      while ($tup = jmysql_fetch_row($r))
        $already[$tup[0]] = true;
      $n = mktime(12, 0, 0, $now->format("n"), $now->format("j"), $now->format("Y")) - ($now->format('N') - 1) * 24 * 3600;
      $beg = new DateTime("$yearSeason-08-01 12:00");
      $beg = creDate($beg, -$beg->format('N') + 1);
      $beg = mktime(12, 0, 0, $beg->format("n"), $beg->format("j"), $beg->format("Y"));
      if ($beg < $n)
        $beg = $n;
      $end = mktime(12, 0, 0, 7, 1, $yearSeason + 1);
      echo "<b>La nouvelle grille doit être créée pour la semaine du lundi </b><select onchange=location.replace('training.php${basePath1}action=new&week=" . $_GET["grid"] . "&grid='+this.options[this.selectedIndex].value)><option></option>";
      for ($i = $beg; $i < $end; $i += 7 * 24 * 3600) {
        if ($already[$i])
          continue;
        $dt = new DateTime('@' . $i);
        echo "<option value=$i>" . $dt->format("d ") . $months[$dt->format("n")] . ' ' . $dt->format("Y") . "</option>";
      }
      if (!jmysql_result(jmysql_query("select count(*) from training where grid=-1"), 0))
        echo "<option value=-1>&lt;Prochaine saison&gt;</option>";
      echo "</select>";
      return;
    }
    $r = jmysql_query("select * from training where grid=" . $_GET["week"]);
    while ($tup = jmysql_fetch_assoc($r)) {
      $sql = "insert into training set grid=$grid";
      foreach ($tup as $k => $v) {
        switch ($k) {
          case "grid":
          case "ri":
            break;

          case "comment":
            $sql .= ",$k='" . addslashes($v) . "'";
            break;

          default:
            $sql .= ",$k=$v";
            break;
        }
      }
      jmysql_query($sql);
    }
    break;

  case "delete":
    jmysql_query("delete from training where ri=" . $request["key"]);
    break;

  case "insert":
    $sql = "insert into training set grid=$grid, ";
    $bIns = true;
  case "save":
    if (!$bIns)
      $sql = "update training set ";
    $sql .= "begin=" . ($request["weekDay"] * 10000 + $request["hour"] * 100 + $request["minute"]);
    $sql .= ", duration=" . ($request["dur_hour"] * 60 + $request["dur_minute"]);
    if ($team = $request["team"]) {
      $sql .= ", color=0";
      $c = getCategory($team | CAT_PLAYER);
      $c = unserialize($c["teamSite"]);
      if ($col = $request["color"])
        $c[$team % 10]->color = $col;
      else
        unset($c[$team % 10]->color);
      $teamsColor[$team] = $col;
      if ($c)
        $c = serialize($c);
      jmysql_query("update category set teamSite='" . addslashes($c) . "' where num=" . (($team - $team % 10) | CAT_PLAYER));
    } else
      $sql .= ", color=" . $request["color"];
    $sql .= ", field=" . $request["field"] . ", team=$team";

    if ($request["_val_comment"])
      $sql .= ", comment='" . addslashes($request["_val_comment"]) . "'";
    else if (!$bIns)
      $sql .= ", comment=null";
    if (!$bIns)
      $sql .= " where ri=" . $request["key"];
    jmysql_query($sql);
    break;

  case "edit":
    echo "<title>Edition entrainement</title></head><body>";
    include "common/tableRowEdit.php";
    $rowEdit = array("weekDay"  => array(0, "Jour de la semaine: "),
        "field"    => array(0, "Salle: "),
        "begin"    => array(0, "Heure du début: "),
        "duration" => array(0, "Durée: "),
        "team"     => array(0, "Equipe: "),
        "comment"  => array(50, "Commentaire: "),
    );

    class TrainingEd extends TableRowEdit
    {

      //---------------------------------------------------------
      function getTuple()
      {
        global $request;
        if ($request["key"]) {
          $this->addButtonOffset = 2;
          return jmysql_fetch_assoc(jmysql_query("select begin div 10000 as weekDay, begin mod 10000 as begin,duration,field,team,comment,color,grid from training where ri=" . $request["key"]));
        }
        $this->bSave = false;
        return $request;
      }

      //---------------------------------------------------------
      function addButton(&$offset)
      {
        createAction($offset, "Supprimer", "saveClick('delete')", 100);
      }

      //---------------------------------------------------------
      function applyValue(&$key, &$val, &$row)
      {
        switch ($key) {
          case "weekDay":
            //dump($row);
            global $weekDays;
            echo "<select name=weekDay>";
            for ($i = 1; $i < 6; $i++)
              echo "<option value=$i" . ($row[$key] == $i ? " selected=selected" : '') . ">$weekDays[$i]</option>";
            echo "</select>";
            return;

          case "begin":
            echo "<select name=hour>";
            $hr = substr($row[$key], 0, 2);
            if (!$hr)
              $hr = 15;
            $mn = substr($row[$key], 2);
            for ($i = 9; $i < 22; $i++)
              echo "<option value=$i" . (strlen($hr) && $hr == $i ? " selected=selected>" : '>') . sprintf("%02d", $i) . "</option>";
            echo "</select>&nbsp;Heures ";
            echo "<select name=minute>";
            for ($i = 0; $i < 60; $i += 5)
              echo "<option value=$i" . (strlen($hr) && $mn == $i ? " selected=selected" : '') . ">" . sprintf("%02d", $i) . "</option>";
            echo "</select>&nbsp;minutes";
            return;

          case "duration":
            echo "<select name=dur_hour>";
            $hr = intval($row[$key] / 60);
            $mn = $row[$key] % 60;
            for ($i = 0; $i < 8; $i++)
              echo "<option value=$i" . (strlen($row[$key]) && $hr == $i ? " selected=selected" : '') . ">" . sprintf("%02d", $i) . "</option>";
            echo "</select>&nbsp;Heures ";
            echo "<select name=dur_minute>";
            for ($i = 0; $i < 60; $i += 5)
              echo "<option value=$i" . (strlen($row[$key]) && $mn == $i ? " selected=selected" : '') . ">" . sprintf("%02d", $i) . "</option>";
            echo "</select>&nbsp;minutes";
            return;

          case "field":
            global $fields;
            echo "<select name=field>";
            foreach ($fields as $k => $v)
              echo "<option value=$k" . (strlen($row[$key]) && $row[$key] == $k ? " selected=selected" : '') . ">$v</option>";
            echo "</select>";
            return;

          case "team":
            global $colors, $teamsColor, $bNew;
            $t = getExistingCategories("category&" . CAT_PLAYER . " and type<>0 and", false, $row["grid"] == -1);
            echo "<table><tr><td>";
            echo "<select onchange=changeTeam(this) name=team><option value=0></option>";
            foreach ($t as $k => $v) {
              $k &= 0xFFFF;
              $already[$teamsColor[$k]] = $k;
              echo "<option value=$k" . ($row[$key] == $k ? " selected=selected" : '') . ">$v</option>";
            }
            echo "</select>";
            $color = ($team = $row[$key]) ? $teamsColor[$team] : $row["color"];
            echo "&nbsp;<input type=hidden name=color value=" . intval($color) . "><span id=color" . ($color ? " style=background-color:#$colors[$color]" : '') . ">Couleur (click pour le choix): </span></span>";
            echo "</td><td>";
            echo "<table>";
            global $colors;
            for ($i = 0; $i < 5; $i++) {
              echo "<tr style=height:.5cm>";
              for ($j = 0; $j < 6; $j++) {
                $n = $i * 6 + $j;
                echo "<td" . ($already[$n] ? " title='Déjà assigné à " . getCategory($already[$n] | CAT_PLAYER, true) . "'" : '') . " onclick=setColor(" . ($n) . ",'" . $colors[$n] . "') style=text-align:center;width:.5cm;background-color:#" . $colors[$n] . ">" . ($already[$n] ? 'X' : '') . "</td>";
              }
              echo "</tr>";
            }
            echo "</table>";
            echo "</td></tr></table>";
            return;
        }
        TableRowEdit::applyValue($key, $val, $row);
      }

      function applySave()
      {
        ?>
        if (document.getElementsByName("dur_hour")[0].selectedIndex==0 && document.getElementsByName("dur_minute")[0].selectedIndex<6)
        {
        alert("Un créneau doit être au minimum de 30 minutes")
        return;
        }
        <?php
      }

    }

    $ed = new TrainingEd;
    $ed->title = "Grille horaire entrainements";
    $ed->colDef = &$rowEdit;
    $ed->editRow();
    ?>
    <script type="text/javaScript">
      function setColor(i,col)
      {
      document.getElementById('color').style.backgroundColor='#'+col;
      document.getElementsByName('color')[0].value=i;
      }

      var teamsColor=new Object();
      var color=new Object();
      <?php
      foreach ($teamsColor as $k => $v)
        echo "teamsColor[$k]='$colors[$v]';";
      echo nl;
      foreach ($colors as $k => $v)
        echo "color['$v']=$k;";
      ?>

      function changeTeam(obj)
      {
      i=obj.options[obj.selectedIndex].value;
      if (col=teamsColor[i])
      {
      document.getElementById('color').style.backgroundColor='#'+col;
      document.getElementsByName('color')[0].value=color[col];
      }
      else
      {
      document.getElementById('color').style.backgroundColor=null;
      document.getElementsByName('color')[0].value=0;
      }
      }
    </script>
    <?php
    exit();
}
$bRoot = $admin && $admin["rights"] == 0;
if ($bEd = !$bPrint && $id && $bRoot)
  $point = "cursor:pointer;";
$nDt = mktime(10, 0, 0, $now->format("n"), $now->format("j"), $now->format("Y")) - (($t = $now->format('N')) < 6 ? $t - 1 : $t - 8) * 24 * 3600;


echo "<h2 style=width:100%;text-align:center>Horaire Entrainements";
if ($grid > 0) {
  $dt = new DateTime("@$grid");
  echo " de la semaine du " . $dt->format("d/m/Y");
}
echo "</h2>" . nl;
if (!$bPrint) {
  $r = jmysql_query("select grid from training " . ($bRoot ? '' : " where grid=0 or grid>=$nDt") . " group by grid");
  if (($n = jmysql_num_rows($r)) || $grid != 0) {
    echo "<b>Grille de la </b>";
    echo "<select style=color:darkgreen;font-weight:bold onchange=\"location.replace('training.php${basePath1}grid='+this.options[this.selectedIndex].value)\">";
    if ($n == 0)
      echo "<option value=0>saison</option>";
    else {
      if ($n == 1)
        echo "<option></option>";
      while ($tup = jmysql_fetch_row($r)) {
        if ($tup[0] == -1)
          continue;
        if (!isset($request["grid"]) && intval($nDt / 72000) == intval($tup[0] / 72000))
          $grid = $tup[0];
        $dt = new DateTime("@" . $tup[0]);
        echo "<option " . ($grid == $tup[0] ? " selected=selected" : '') . " value=$tup[0]>" . ($tup[0] > 0 ? "semaine du lundi " . $dt->format("d ") . $months[$dt->format("n")] . ' ' . $dt->format("Y") : "saison") . "</option>";
      }
      if ($bNew && ($id || $now->format("Ymd") > ($yearSeason + 1) . "0615"))
        echo "<option " . ($grid == -1 ? " selected=selected" : '') . " value=-1>saison " . ($yearSeason + 1) . '-' . ($yearSeason + 2) . "</option>";
    }
    echo "</select>";
  }
  if (jmysql_result(jmysql_query("select count(*) from training where grid=$grid"), 0)) {
    echo "<b>&nbsp;   Mettre en évidence </b><select style=color:darkgreen;font-weight:bold onchange=\"location.replace('training.php${basePath1}grid=$grid&view='+this.options[this.selectedIndex].value)\">";
    echo "<option value=''>&lt;Toutes&gt;</option>";
    $r = jmysql_query("select team from training where grid=$grid group by team");
    while ($tup = jmysql_fetch_row($r))
      echo "<option" . ($_GET["view"] == $tup[0] ? " selected=selected" : '') . " value=$tup[0]>" . ($tup[0] ? getCategory($tup[0] | CAT_PLAYER, true) : "&lt;Autre&gt;") . "</option>";
    echo "</select>";
    if ($bEd) {
      echo "<input style=cursor:pointer type=button value='Copier cette grille' onclick=location.assign('training.php${basePath1}action=new&grid=$grid')>";
      echo "<input style=cursor:pointer type=button value='Supprimer cette grille' onclick=deleteGrid($grid)>";
      echo "<input style=cursor:pointer type=button value='Imprimer' onclick=\"window.open('training.php${basePath1}action=print&grid=$grid')\">";
      echo "<input style=cursor:pointer type=button value='Export Excel' onclick=\"location.assign('training.php${basePath1}action=export&grid=$grid')\">";
    }
  }
}
?>
<style type="text/css">.sel{border:1px solid black}<?php echo $bEd ? ".sel:hover{border:1px solid red;font-style:italic}" : '' ?></style>
<?php
echo "<table  class=training style='border:2px solid darkgreen;width:100%;color:black;font-size:small'>";
echo "<tr><th></th>";
foreach ($fields as $v)
  echo "<th style=background-color:PapayaWhip;width:" . intval(100 / sizeof($fields)) . "%>$v</th>";
echo "</tr>";

for ($i = 1; $i < 6; $i++) {
  $first = "9999";
  $field = $max = $ht = 0;
  unset($aGrid);
  $r = jmysql_query("select * from training where grid=$grid and begin div 10000=$i order by field,begin");
  while ($tup = jmysql_fetch_assoc($r)) {
    $tup["begin"] = substr($tup["begin"], 1);
    $aGrid[$tup["field"]][] = $tup;
    if ($tup["begin"] < $first)
      $first = $tup["begin"];
    if ($tup["field"] != $field) {
      if ($ht > $max)
        $max = intval($ht);
      $field = $tup["field"];
      $ht = 0;
    }
    $ht += $tup["duration"] > 120 ? 120 * PROP : $tup["duration"] * PROP;
  }
  if ($ht > $max)
    $max = intval($ht);
  //dump($aGrid);
  //echo "max=$max, first=$first".NL;
  $first = intval($first / 100) * 60 + $first % 100;
  if (!$max)
    $max = 15;
  echo "<tr style='page-break-before:auto;margin-bottom:5px;border:2px solid darkgreen'><td style=font-weight:bold;color:red;font-size:medium>$weekDays[$i]";
  if ($grid > 0) {
    $dt = new DateTime("@" . ($grid + ($i - 1) * 24 * 3600));
    echo "<br>" . $dt->format("d/m");
  }
  echo "</td>";
  foreach ($fields as $j => $dumy) {
    $par = "training.php${basePath1}action=edit&grid=$grid&field=$j&weekDay=$i";
    echo "<td style='padding:5px 5px 5px 3px;vertical-align:top'>";
    $prev = $first;
    $n = 1;

    if (sizeof($aGrid[$j])) {
      foreach ($aGrid[$j] as $tup) {
        //dump($tup,"tup");
        $h = $tup["begin"];
        $h = intval($h / 100) * 60 + $h % 100;
        $v = $_GET["view"];
        $bk = ($team = $tup["team"]) && ($color = $teamsColor[$team]) || ($color = $tup["color"]) ? ";background-color:#$colors[$color]" : '';
        if ($bk && strlen($v) && $v != $team)
          unset($bk);
        $bBlink = false;
        if ($prev)
          if ($h > $prev) {
            echo "<div" . ($bEd ? " class=sel onclick=location.assign('$par&begin=" . intval($prev / 60) . ($prev % 60) . "&duration=" . ($h - $prev) . "')" : '') . " style='${point}" . ($n > 1 ? "border-top:1px solid black;" : '') . "height:" . height($prev, $h - $prev) . "px;width:100%'></div>";
          } else {
            //$bBlink=$h<$prev;
            $bErr += $bBlink;
          }
        $t = CAT_COACH | $team;
        $t1 = ($grid == -1 ? 'nC' : 'c') . "ategory";
        $r = jmysql_query("select concat(substr(firstName,1,1), '. ', name) from members where ($t1=$t or ${t1}2=$t or ${t1}3=$t) and type<>0");
        if ($r && jmysql_num_rows($r))
          $trainer = "<div style=font-size:x-small;font-style:italic>" . jmysql_result($r, 0) . "</div>";
        else
          unset($trainer);
        echo "<div class=sel" . ($bEd ? " onclick=location.assign('$par&key=" . $tup["ri"] . "')" : '') . " style='${point}font-weight:bold" . ($n++ < sizeof($aGrid[$j]) ? ";border-bottom:0px" : '') . ";height:" . height($h, $tup["duration"]) . "px;width:100%$bk'>";
        echo ($bBlink ? "<span name=blinked><img src=attention.jpeg height=15 width=15>&nbsp;" : '') . "De " . substr($tup["begin"], 0, 2) . ":" . substr($tup["begin"], 2, 2) . " à ";
        $prev = $h + $tup["duration"];
        echo sprintf("%02d:%02d", intval($prev / 60), $prev % 60) . ($bBlink ? "</span>" : '') . NL;
        echo "<div style='margin-left:1px;text-align:center;width:99%$bk'>" . getCategory($tup["team"] | CAT_PLAYER, true) . ' ' . $tup["comment"] . "</div>$trainer</div>";
      }
      if ($bEd && $prev / 60 < 21)
        echo "<div class=sel onclick=location.assign('$par&begin=" . intval($prev / 60) . ($prev % 60) . "&duration=90') style='${point}background-color:lightgray;height:14px;width:100%'>Créneau suivant</div>";
    } else if ($bEd)
      echo "<div class=sel onclick=location.assign('$par&duration=90') style='${point}width:100%;height:${max}px'></div>";
    echo "</td>";
  }
  echo "</tr>";
}
echo "</table>";
if ($bErr)
  echo "<span name=blinked><img src=attention.jpeg width=50 width=50></span>&nbsp;<b style=color:red;font-weight:bold>Il y a chevauchement d'heure " . ($bErr > 1 ? "aux éléments qui clignotent." : "à l'élément qui clignote");

if ($bExport)
  ob_end_flush();

//------------------------------------------------------------------------------    
function height($beg, $dur)
{
  global $j, $aGrid, $first;
  if ($dur < 120)
    return intval($dur * PROP);
  $max = 120 * PROP + intval(($dur - 120) * PROP120);
  $end = $beg + $dur;
  foreach ($aGrid as $k => $v)
    if ($k <> $j) {
      $prev = $first;
      $ht = 0;
      foreach ($v as $tup) {
        $h = $tup["begin"];
        //echo NL."h=$h, prev=".sprintf("%02d%02d",intval($prev/60),$prev%60).", beg=".sprintf("%02d%02d",intval($beg/60),$beg%60).", ";
        if (($h = intval($h / 100) * 60 + $h % 100) < $beg) {
          $prev = $h + $tup["duration"];
          continue;
        }
        $t = (($bStop = $h >= $end) ? $end : $h + 2) - $prev;
        $ht += $t > 120 ? 120 * PROP + intval(($t - 120) * PROP120) : intval($t * PROP);
        //echo "t=$t, bstop=$bStop, ht=$ht, ";
        if ($bStop)
          break;
        $t = $h + $tup["duration"] >= $end ? $end - $h : $tup["duration"];
        $ht += $t > 120 ? 120 * PROP + intval(($t - 120) * PROP120) : intval($t * PROP);
        //echo "ht=$ht";
        $prev = $h + $t;
      }
      if ($max < $ht)
        $max = $ht;
    }
  //echo NL."max=$max";
  return $max;
}
?>
<script type="text/javaScript">
  var elmts=new Array();
  var bShow=1;
  function deleteGrid(grid)
  {
  if (confirm ("Es-tu sûr de vouloir supprimer cette grille?"))
  location.assign('training.php<?php echo $basePath1 ?>action=deleteGrid&grid='+grid);
  }

  function loadEvent()
  {
  <?php
  if ($bExport)
    echo "location.assign('Entrainements.xls')" . nl;
  else if ($bPrint)
    echo "window.print();close()\n";

  if ($bMSIE) {
    ?>
    spans=document.getElementsByTagName('span');
    for (i=0; i<spans.length; i++)
    if (spans[i].name=='blinked')
    elmts.push(spans[i]);
    <?php
  } else
    echo "elmts=document.getElementsByName('blinked');" . nl
    ?>
  if (elmts)
  setTimeout("blink()",200+300*bShow);
  }

  function blink()
  {
  bShow=1-bShow;
  for (i=0; i<elmts.length; i++)
  {
  if (bShow)
  elmts[i].style.visibility='visible';
  else
  elmts[i].style.visibility='hidden';
  }
  setTimeout("blink()", 200+300*bShow);
  }
</script>