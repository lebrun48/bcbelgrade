<?php

include "admin.php";
include "calendarData.php";
include "head.php";
?>
<title>Update calendar          
</title>    
</head>   
<body>
  <?php

  $r = jmysql_query("select category from members where category&" . CAT_PLAYER . " and type<>0 and category<>0 group by category");
  while ($tup = jmysql_fetch_row($r)) {
    $n = getCategory($tup[0]);
    //dump($n); 
    if ($n["disabled"] || !$n["teamSite"])
      continue;
    $t = unserialize($n["teamSite"]);
    $i = ($tup[0] & 0xFFFF) % 10;
    if (!$t[$i])
      continue;
    $category[$t[$i]->div] = $tup[0] & 0xFFFF;
  }
//dump($category);
//---------------------------------------------------------------------------------------------------------
  $bDisplay = true;
  $min = new DateTime("$yearSeason-08-20");

  function dispEl(&$t, $bCont = false, $bRec = false)
  {
    global $bDisplay;
    if (!$bDisplay)
      return;
    static $level = 0;
    $tab = str_repeat(" - ", $level);
    echo $tab . $t->length . NL;
    for ($i = 0; $i < $t->length; $i++) {
      echo $tab . $t->item($i)->nodeName . "[$i](" . $t->item($i)->nodeType . ")=" . $t->item($i)->nodeValue . NL;
      if ($bRec && $t->item($i)->childNodes->length) {
        $level++;
        dispEl($t->item($i)->childNodes, $bCont, true);
      }
    }

    if ($level) {
      $level--;
      return;
    }
    if (!$bCont)
      exit();
  }

//---------------------------------------------------------------------------------------------------------
  function removeOld($type)
  {
    global $now, $listNum, $calRem;
    if ($bRoot)
      echo "Supprimer ceux qui n'existent plus..." . NL;
    if ($listNum)
      $res = jmysql_query("select num from calendar where num is not null and type=$type and date>='" . $now->format("Y-m-d") . "' and (updated&0xC0=0) and num not in (" . substr($listNum, 1) . ") and not (home='bye' or away='bye')");
    if ($res && jmysql_num_rows($res))
      while ($row = jmysql_fetch_row($res)) {
        $calRem[] = $row[0];
        //dump(current($calRem),"rem"); 
      }
    $listNum = null;
  }

//---------------------------------------------------------------------------------------------------------
  function updCalendar(&$cal, $bPrint = false)
  {
    global $now, $yearSeason, $listNum, $calUpd, $calIns, $min, $bRoot;
    if ($bPrint)
      dump($cal, "cal");
    if ($cal->date)
      $dt = new DateTime($cal->date);
    else {
      $d = strtok($cal->day, "-/");
      $m = strtok("-/");
      $y = strtok("-/");
      if (!$y)
        $y = $yearSeason + ($m < 7 ? 1 : 0);
      $h = strtok($cal->hr, ".:;");
      $mm = strtok(".:;");
      $dt = new DateTime("$y-$m-$d $h:$mm:00");
    }
    //Store in list to see if not remove
    if ($dt >= $now)
      $listNum .= ",'$cal->num'";
    else if ($dt < $min) {
      if ($bRoot)
        echo '<div style="color:red; font-weight:bold">Date officielle invalide: ' . $dt->format("d/m-Y") . "</div>" . NL;
      unset($cal);
      return;
    }
    $res = jmysql_query("select * from calendar where num='" . $cal->num . "' and not (home='bye' or away='bye')");
    if (jmysql_num_rows($res) == 0) {
      $cal->date = $dt;
      $cal->sameDate = jmysql_result(jmysql_query("select count(*) from calendar where date='" . $dt->format("Y-m-d H:i:00") . "'"), 0);
      //dump($cal);
      unset($cal->day);
      unset($cal->hr);
      unset($cal->comment);
      $calIns[] = clone $cal;
      //$sql="insert into calendar set num='".$cal->num."', team=".$cal->team.", home='".$cal->home."', away='".$cal->away."', date='".$dt->format("Y-m-d H:i:s")."', lastUpdated=NOW(), type=".$cal->type.($cal->comment ? ", comment='".$cal->comment."'" : '');
      //dump(current($calIns), "new");
      //jmysql_query($sql);
    } else {
      //check what has changed
      $row = jmysql_fetch_assoc($res);
      $upd = 0;
      $dtdb = new DateTime($row["date"]);
      if ($dtdb != $dt && ($dt > $now || $dtdb > $now)) {
        $cal->date = $dt;
        //dump($cal);
        unset($cal->day);
        unset($cal->hr);
        unset($cal->comment);
        unset($cal->home);
        unset($cal->away);
        $calUpd[] = clone $cal;
        $sql = "date='" . $dt->format("Y-m-d H:i:s") . "'";
        $upd |= 0x01;
        $com = $dtdb->format("d/m/Y H:i");
      }
      /* if ($row["home"]!=$cal->home)
        {
        $sql.=($upd ? ',': '')."home='".$cal->home."'";
        $com.=" à ".$row["home"];
        $upd|=0x02;
        }
        if ($row["away"]!=$cal->away)
        {
        $sql.=($upd ? ',': '')."away='".$cal->away."'";
        $com.=" à ".$row["away"];
        $upd|=0x04;
        } */
      if ($upd) {
        //$sql="update calendar set $sql, updated=$upd, comment='".($cal->comment ? $cal->comment.". " : '')."Auparavant: $com', lastUpdated=NOW() where num='".$cal->num."' and updated&0x80=0";
        //dump(current($calUpd), "upd");
        //jmysql_query($sql);
      }
    }
    unset($cal);
  }

//---------------------------------------------------------------------------------------------------------
  if ($bRoot)
    echo NL . "<big><u><b>Calendrier & résultats CPNAMUR</b></u></big>" . NL;
  $c = file_get_contents('http://www.cpnamur.be/Jacques/utf8/calendarGet.php?club=1702');
  $c = unserialize($c);
//dump($c);
  foreach ($c as $v) {
    $cal->home = $v["home"];
    $cal->away = $v["away"];
    $cal->num = $v["num"];
    $cal->date = $v["date"];
    $cal->type = CHAMPIONSHIP_PROV;
    $cal->team = $category[substr($v["num"], 1, 2) * 10];
    updCalendar($cal);
  }
  removeOld(CHAMPIONSHIP_PROV);

//---------------------------------------------------------------------------------------------------------
  /*
    if ($bRoot)
    {
    echo NL."<big><u><b>Résultats CPNAMUR</b></u></big>".NL;
    $r=jmysql_query("select substr(date,1,10) as dt from calendar where type=".CHAMPIONSHIP_PROV." and results is null and (team<800 or team >950) and date<'".$now->format("Y-m-d h:i:s")."' group by dt",1);
    while ($dt=jmysql_fetch_row($r))
    {
    $dt=new DateTime($dt[0]);
    $document = new DOMDocument();
    @$document->loadHTML(file_get_contents("http://www.cpnamur.be/index_r.php?jc=".$dt->format("j")."&mc=".$dt->format("n")."&ac=".$dt->format("Y")));
    $t=$document->getElementsByTagName("table")->item(0)->childNodes->item(0)->childNodes->item(2)->childNodes;
    //tables with titles
    //echo "len=".$t->length.NL;
    global $bDisplay;
    for ($i=0; $i<$t->length; $i++)
    {
    $el=$t->item($i);
    //echo "status=$status, nodeName=".$el->nodeName.", value=".$el->nodeValue.NL;
    switch($status)
    {
    case 0:
    //Get division
    if ($el->nodeName=="b" && ($team=$category[$el->nodeValue]))
    $status=1;
    break;

    case 1:
    //Get table
    if ($el->nodeName=="table")
    {
    getResults($el, $team, $dt);
    $status=0;
    }
    break;
    }
    }
    }
    }


    function getResults(&$table, $team, $dt)
    {
    global $now;
    for ($i=0; $i<$table->childNodes->length; $i++)
    {
    $li=$table->childNodes->item($i)->childNodes;
    //dispEl($li,true);
    $home=trim($li->item(0)->nodeValue);
    $away=trim($li->item(1)->nodeValue);
    if (stripos($home, "BELGRADE")!==false || stripos($away, "BELGRADE")!==false)
    {
    $results=$li->item(2)->nodeValue." - ".$li->item(3)->nodeValue;
    jmysql_query("update calendar set results='$results' where team=$team and results is null and date like '".$dt->format("Y-m-d")."%'",1);
    }
    }
    } */


//---------------------------------------------------------------------------------------------------------

  if ($bRoot) {
    echo NL . "<big><u><b>Championat Régionaux</b></u></big>" . NL;
    $state = 0;
    while ($state < 2) {
      $document = new DOMDocument();
      switch ($state) {
        case 0:
          @$document->loadHTML(file_get_contents('http://www.cpliege.be/calennaj.htm'));
          break;

        case 1:
          @$document->loadHTML(file_get_contents('http://www.cpliege.be/calennas.htm'));
          break;
      }
      $t = $document->getElementsByTagName("p");
      for ($i = 0; $i < $t->length && stripos($t->item($i)->nodeValue, ($state ? "Seniors" : "Jeunes") . " - Le calendrier") === false; $i++)
        ;
      if ($i == $t->length) {
        echo '<div style="color:red; font-weight:bold">En-tête " - Le calendrier" pas trouvée!</div>' . NL;
        exit();
      }
      //dispEl($t->item($i)->parentNode->childNodes);
      $t = $t->item($i)->parentNode->childNodes->item(9)->childNodes;
      //dispEl($t,true);
      for ($i = 0; $i < $t->length; $i++) {
        $el = $t->item($i);
        global $bDisplay;
        $bDisplay = $state == 1;
        //dispEl($el->childNodes,true);
        if (($p = stripos($el->nodeValue, "JOURNEE DU ")) !== false) {
          if ($el->nodeName == "table") {
            $i = 0;
            $t = $el->childNodes;
            //dispEl($t,true);
            continue;
          }
          $d = strtok(substr($el->nodeValue, $p + 11), " ");
          $m = array_search(strtok(" "), $uMonths);
          $y = strtok(" ");
          if ($y < $yearSeason)
            $y = $yearSeason;
          $tim = mktime(12, 0, 0, $m, $d, $y);
          if ($state)
            $tim -= 24 * 3600;
        } else if ($el->childNodes->length == ($state ? 9 : 8)) {
          $teamName = trim($el->childNodes->item($state ? 5 : 4)->nodeValue);
          $team = $category[$teamName];
        } else if ($el->childNodes->length == ($state ? 14 : 12) && stripos($el->nodeValue, "BELGRADE")) {
          if (!$team) {
            if ($bRoot)
              echo '<div style="font-weight:bold; color:red">La catégorie ' . $teamName . " n'existe pas!!</div>" . NL;
            continue;
          }
          //dispEl($el->childNodes,true);
          $el1 = $el->childNodes;
          if ($cal->hr = trim($el1->item(4)->nodeValue)) {
            $cal->num = trim($el1->item(0)->nodeValue);
            if ($p = trim($el1->item($state ? 12 : 10)->nodeValue))
              $cal->day = strtok($p, "()");
            else {
              $d = trim($el1->item(2)->nodeValue);
              switch ($d) {
                case 'D': $dt = new DateTime('@' . ($tim + 24 * 3600));
                  break;
                case 'V': $dt = new DateTime('@' . ($tim - 24 * 3600));
                  break;
                default: $dt = new DateTime('@' . $tim);
                  break;
              }
              $cal->day = $dt->format("d/m/Y");
            }
            $cal->home = trim($el1->item($state ? 8 : 6)->nodeValue);
            $cal->away = trim($el1->item($state ? 10 : 8)->nodeValue);
            $cal->type = CHAMPIONSHIP_REG;
            $cal->team = $team;
            updCAlendar($cal);
          }
        }
      }
      $state++;
    }
    removeOld(CHAMPIONSHIP_REG);
  }

//---------------------------------------------------------------------------------------------------------
  if ($bRoot) {
    echo NL . "<big><u><b>Championat Régionaux résultats</b></u></big>" . NL;
    $t = time() - 24 * 3600 * ($now->format("N") + 1);
    $sat = new DateTime('@' . $t);
    $we = new DateTime('@' . ($t + 24 * 3600));
    $we = "(date like '" . $sat->format("Y-m-d") . "%' or date like '" . $we->format("Y-m-d") . "%')";
    $document = new DOMDocument();
    @$document->loadHTML(file_get_contents('http://www.cpliege.be/natresul.asp'));
    $t = $document->getElementsByTagName("body")->item(0)->childNodes;
    //dispEl($t);
    $cal->team = null;
    for ($i = 0; $i < $t->length; $i++) {
      $el = $t->item($i);
      //dispEl($el->childNodes,true);
      if (!$cal->team) {
        if ($el->childNodes->length == 1) {
          $cal->team = $category[trim(substr($el->childNodes->item(0)->nodeValue, 2))];
          //echo substr($el->childNodes->item(0)->nodeValue,2).", team=".$cal->team.NL;
        }
        continue;
      }

      $el = $el->childNodes;
      //dispEl($el, true);
      for ($v = 1; $v < $el->length;) {
        if (stripos($el->item($v)->nodeValue, "BELGRADE") !== false) {
          if (jmysql_result(jmysql_query("select count(*) from calendar where team=" . $cal->team . " and $we"), 0) > 1) {
            echo '<div style="font-weight:bold; color:red">La catégorie ' . array_search($cal->team, $championshipReg) . " joue plus d'une fois le W-E du " . $sat->format("d/m/Y") . "!!</div>" . NL;
            break;
          }
          if ($v == 1) {
            $el = $el->item($v)->childNodes;
            //dispEl($el,true);
            if ($el->length == 10 && $el->item(4)->nodeValue && is_numeric($el->item(4)->nodeValue)) {
              $cal->home = $el->item(1)->nodeValue;
              $cal->away = $el->item(3)->nodeValue;
              $cal->results = $el->item(4)->nodeValue . " - " . $el->item(6)->nodeValue;
              $wh = "where team=" . $cal->team . " and $we and results is null";
              //echo "wh=$wh".NL;
              switch (jmysql_result(jmysql_query("select count(*) from calendar $wh"), 0)) {
                case 1:
                  $sql = "update calendar set results='" . $cal->results . "' $wh";
                  echo $sql . NL;
                  jmysql_query($sql);
                case 0:
                  break;

                default:
                  echo '<div style="color:red; font-weight:bold">il y a plus d\'un match pour ce critère: ' . $wh . "</div>" . NL;
              }
            }
          } else {
            $base = intval($v / 10) * 10;
            //echo "base=$base, ".$el->item($base+6)->nodeValue.NL;
            if ($el->item($base + 6)->nodeValue && is_numeric($el->item($base + 6)->nodeValue)) {
              $cal->home = $el->item($base + 3)->nodeValue;
              $cal->away = $el->item($base + 5)->nodeValue;
              $cal->results = $el->item($base + 6)->nodeValue . " - " . $el->item($base + 8)->nodeValue;
              $wh = "where team=" . $cal->team . " and $we and results is null";
              switch (jmysql_result(jmysql_query("select count(*) from calendar $wh"), 0)) {
                case 1:
                  $sql = "update calendar set results='" . $cal->results . "' $wh";
                  echo $sql . NL;
                  jmysql_query($sql);
                case 0:
                  break;

                default:
                  echo '<div style="color:red; font-weight:bold">il ya plus d\'un match pour ce critère: ' . $wh . "</div>" . NL;
              }
            }
          }
          //only one result a week;
          //echo "reset $v".NL;
          break;
        }
        if ($v == 1)
          $v = 3;
        else if (($v % 10) == 3)
          $v += 2;
        else
          $v += 8;
        //echo "v$=$v".NL;
      }
      $cal->team = null;
    }
  }


//------------------------------------------------------------------------------------------------------------------------

  if ($bRoot) {
    //Mettre n° club
    $seeRequest = 1;
    echo NL . "<big><u><b>Get TeamNb</b></u></big>" . NL;
    $res = jmysql_query("select home,away,rowid from calendar where type<=4 and clubNb is null");
    while ($row = jmysql_fetch_row($res)) {
      $sql = "select num,name from clubs where name='" . (stripos($row[0], "belgrade") === false ? $row[0] : $row[1]) . "'";
      if (($r = jmysql_query($sql)) && jmysql_num_rows($r) == 1) {
        $r = jmysql_fetch_row($r);
        $sql = "update calendar set clubNb=$r[0] where rowid=$row[2]";
        echo $sql . ". Club=$r[1]" . NL;
        jmysql_query($sql);
        continue;
      }
      for ($j = 0; $j < 2; $j++) {
        for ($lim = 2; $lim < 5; $lim++) {
          $t = stripos($row[0], "belgrade") === false ? $row[0] : $row[1];
          if ($j) {
            $t = str_replace('.', '', $t);
            $t = str_replace('-', '', $t);
            $t = str_replace(')', '', $t);
            $t = str_replace('(', '', $t);
          }
          $t = strtok($t, " .-()");
          unset($s);
          while ($t) {
            if (!is_numeric($t) && strlen($t) >= $lim && (!$j || strcasecmp($t, "basket")))
              $s .= "%$t";
            $t = strtok(" .-()");
          }
          $sql = "select num,name from clubs where name like '$s%'";
          if (($r = jmysql_query($sql)) && jmysql_num_rows($r) == 1) {
            $r = jmysql_fetch_row($r);
            $sql = "update calendar set clubNb=$r[0] where rowid=$row[2]";
            echo $sql . ". Club=$r[1]" . NL;
            jmysql_query($sql);
            break;
          }
          $p1 = strtok($s, "% ");
          unset($s1);
          unset($bReq);
          while ($p1) {
            if (strlen($p1) <= 4) {
              $bReq = true;
              for ($i = 0; $i < strlen($p1); $i++)
                $s1 .= "%$p1[$i]";
            } else
              $s1 .= "%$p1";
            $p1 = strtok(' %');
          }
          if ($bReq) {
            $sql = "select num,name from clubs where name like '$s1%'";
            if (($r = jmysql_query($sql)) && jmysql_num_rows($r) == 1) {
              $r = jmysql_fetch_row($r);
              $sql = "update calendar set clubNb=$r[0] where rowid=$row[2]";
              echo $sql . ". Club=$r[1]" . NL;
              jmysql_query($sql);
              break;
            }
          }
        }
      }
      if ($lim == 5)
        echo '<div style="color:red; font-weight:bold">Pas de club trouvé avec ce critère: ' . $s . ". (ri=$row[2])</div>" . NL;
    }
  }

//---------------------------------------------------------------------------------------------------------
//dump($calRem, "rem");
//dump($calIns, "ins");
//dump($calUpd, "upd");
  if ($calIns) {
    echo "<h1>Matches à insérer (présents dans l'officiel mais pas chez nous) ou numéro de match incorrect </h1>" . nl;
    echo "<table class=main><tr><th>Calendrier</th><th>Numéro</th><th>Date</th><th>Equipe</th><th>Visité</th><th>Visiteur</th><tr>" . nl;
    foreach ($calIns as $v) {
      echo "<tr><td style=font-weight:bold;color:blue>Officiel</td><td>";
      if ($v->sameDate)
        echo "<a target=_blank href=calendar.php${basePath1}sel=3&_sel_date=" . urlencode("=" . $v->date->format("Y-m-d H:i:00")) . ">$v->num</a>";
      else
        echo "$v->num";
      echo "</td><td>" . $v->date->format("d/m/Y H:i") . "</td><td>" . getCategory($v->team | CAT_PLAYER, true) . "</td><td>$v->home</td><td>$v->away</td></tr>" . nl;
    }
    echo "</table>" . nl;
  }

  if ($calRem) {
    echo "<h1>Matches à supprimer (pas ou plus présents sur l'officiel)</h1>" . nl;
    echo "<table class=main><tr><th>Calendrier</th><th>Numéro</th><th>Date</th><th>Equipe</th><th>Visité</th><th>Visiteur</th><tr>" . nl;
    foreach ($calRem as $v) {
      $cal = jmysql_fetch_assoc(jmysql_query("select * from calendar where calendar.num='$v'"));
      $dt = new DateTime($cal["date"]);
      echo "<tr><td style=font-weight:bold;color:blue>NBCB</td><td class=tabLink><a target=_blank href=calendar.php${basePath1}sel=3&_sel_num=%3D$v>$v</td><td>" . $dt->format("d/m/Y H:i") . "</td><td>" . getCategory($cal["team"] | CAT_PLAYER, true) . "</td><td>" . $cal["home"] . "</td><td>" . $cal["away"] . "</td></tr>" . nl;
    }
    echo "</table>" . nl;
  }

  if ($calUpd) {
    echo "<h1>Matches dont la date diffère</h1>" . nl;
    echo "<table class=main><tr><th>Calendrier</th><th>Numéro</th><th>Date</th><th>Equipe</th><th>Visité</th><th>Visiteur</th><tr>" . nl;
    foreach ($calUpd as $v) {
      $cal = jmysql_fetch_assoc(jmysql_query("select * from calendar where num='$v->num'"));
      $dt = new DateTime($cal["date"]);
      echo "<tr><td style=font-weight:bold;color:blue>NBCB</td><td class=tabLink><a target=_blank href=calendar.php${basePath1}sel=3&_sel_num=%3D$v->num>$v->num</td><td style=color:red>" . $dt->format("d/m/Y H:i") . "</td><td>" . getCategory($cal["team"] | CAT_PLAYER, true) . "</td><td>" . $cal["home"] . "</td><td>" . $cal["away"] . "</td></tr>" . nl;
      echo "<tr style='border-bottom:4 double black'><td style=font-weight:bold;color:blue>Officiel</td><td></td><td style=color:red>" . $v->date->format("d/m/Y H:i") . "</td><td></td><td></td><td></td></tr>" . nl;
    }
    echo "</table>" . nl;
  }

  echo "Comparaison terminée.</body></html>" . nl;

  