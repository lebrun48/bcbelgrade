<?php

$bOnlyRoot = true;
include "../tools/common/common.php";
include "head.php";
echo "<title>Participation</title></head>";

echo "<h2 style=width:100%;text-align:center>Liste des \"absents\" aux activités</h2>";
echo "<table style=margin:auto class=main><tr><th>Nom Prénom</th><th>Catégorie</th></tr>";
$res = jmysql_query("select concat(name,' ',firstName), category from members where category<0x4FFFF and type<>0 and num not in (select distinct num from events) order by category");
while ($tup = jmysql_fetch_row($res))
  echo "<tr><td>$tup[0]</td><td>" . getCategory($tup[1], true) . "</td></tr>";

echo "</table>";
