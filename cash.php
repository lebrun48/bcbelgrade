<?php
$bOnlyRoot = true;
include "admin.php";
switch ($_GET["action"]) {
  case "save":
    //dump($request);
    unset($res);
    foreach ($request as $k => $v) {
      if ($k == "comment")
        $com = $v;
      else if (substr($k, 0, 5) == "cash_")
        $res[substr($k, 5, 1)][substr($k, 7)] = $v;
    }
    include "head.php";
    include "billerNBCBConf.php";
    echo "<title>Entrée facturier</title></head><h2>Entrée dans facturier</h2>";
    echo "<form method=post action=cash.php$basePath&action=save_db><input id=submit type=submit style=display:none><table id=edit>";
    echo "<input type=hidden name=res3 value=\"" . htmlspecialchars("insert into cash set cash='" . serialize($res[3]) . "', date=now()") . "\">";
    echo "<input type=hidden name=res4 value=\"" . htmlspecialchars("insert into cash set cash='" . serialize($res[4]) . "', date=null") . "\"><input type=hidden name=amount value=" . $request["amount"] . ">";
    echo "<tr><th>Montant</th><td>" . number_format($request["amount"], 2, ',', '.') . " €</td></tr><tr><th>Date</th><td><input name=date type=date value=" . $now->format('Y-m-d') . "></td></tr>";
    echo "<tr><th>Caisse</th><td><select name=cash>";
    foreach ($account as $k => $v)
      if (stripos($v, "caisse") !== false)
        echo "<option value=$k>$v</option>";
    echo "</select></td></tr><tr><th>Catégorie</th><td><select name=category onchange=statusChange()>";
    foreach ($catSerial as $k => $v)
      echo "<option value=$k>$v</option>";
    echo "</select>";
    if (isset($resultCategory)) {
      echo "<b>  Catégorie Bilan : </b><select name=status>";
      echo "<option></option>";
      $step = -1;
      foreach ($resultCategory as $k => $v) {
        if ((int) ($k / 100) != $step) {
          if ($step != -1)
            echo "</optgroup>";
          $step = (int) ($k / 100);
          echo "<optgroup label='" . $resultCatName[$step] . "'>";
        }
        echo "<option value=$k>$v</option>";
      }
      echo "</select>";
    }

    echo "</td></tr><tr><th>Commentaire</th><td><input name=comment value=\"" . htmlspecialchars($request["comment"]) . "\" type=text size=100></td></tr></table></form>";
    include "tools.php";
    createAction(-1, "Enregistrer", "document.getElementById('submit').click()");
    createAction(-1, "Annuler", "window.close()");
    if (isset($resultCategory)) {
      ?>
      <script type="text/javaScript">
      <?php
      echo "allCat=[";
      foreach ($category as $k => $v)
        foreach ($v as $kk => $vv) {
          echo $sep . ($k * 256 + $kk);
          $sep = ',';
        }
      echo "];\nconvPos=[";
      unset($sep);
      foreach ($category as $k => $v)
        foreach ($v as $kk => $vv) {
          echo $sep . $vv[1];
          $sep = ',';
        }
      echo "];\nconvNeg=[";
      unset($sep);
      foreach ($category as $k => $v)
        foreach ($v as $kk => $vv) {
          echo $sep . $vv[2];
          $sep = ',';
        }
      echo "];" . nl;
      ?>
        function statusChange()
        {
        obj=document.getElementsByName('category')[0];
        cat=obj.options[obj.selectedIndex].value;
        obj=document.getElementsByName('status')[0];
        am=document.getElementsByName('amount')[0].value;
        for (i in allCat)
        if (allCat[i]==cat)
        {
        v=am<0 ? convNeg[i]:convPos[i];
        if (v==-1)
        obj.selectedIndex=0;  
        for (j=0; j<obj.length; j++)
        if (obj.options[j].value.length && obj.options[j].value==v)
        { 
        obj.selectedIndex=j;
        break;
        }
        }
        }
      </script>
      <?php
    }
    exit();

  case "save_db":
    echo "<body onload=opener.location.assign('cash.php$basePath');window.close()></body>";
    jmysql_query("insert into billerNBCB set amount=" . $request["amount"] . ", date='" . $request["date"] . "', orig='" . addslashes($request["comment"]) . "', season=" . ($yearSeason - 2000) . ", category=" . $request["category"] . (($t = $request["status"]) ? ", status=$t" : '') . ", hidden=0, account=" . $request["cash"]);
    jmysql_query($request["res3"] . ", comment='" . addslashes($request["comment"]) . "'");
    jmysql_query($request["res4"]);
    break;

  case "save_only":
    unset($res);
    foreach ($request as $k => $v) {
      if ($k == "comment")
        $com = $v;
      else if (substr($k, 0, 5) == "cash_")
        $res[substr($k, 5, 1)][substr($k, 7)] = $v;
    }
    jmysql_query("insert into cash set cash='" . serialize($res[3]) . "', comment='" . addslashes($com) . "', date=now()");
    jmysql_query("insert into cash set cash='" . serialize($res[4]) . "', date=null");
}
?>  
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//FR">  
<head>            
  <meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">            
  <link rel="shortcut icon" href="__NEW__logo_3x3.jpg" type="image/x-icon" />
</head><title>Caisses</title>
<body onload=loadEvent()>
  <?php
  include "tools.php";

  $bills = array(500, 200, 100, 50, 20, 10, 5, 2, 1, .5, .2, .1, .05, .02, .01);

  function displayCash(&$content = null, $bEnabled = true, $content2 = null)
  {
    global $bills, $history, $histPrev, $cur3, $basePath;
    static $cashNb = 1;
    if ($history && $cashNb == 1) {
      $dt = new DateTime($cur3[0]);
      echo "<span style=color:blue;font-weight:bold>Le " . $dt->format("Y/m/d H:m") . " $cur3[2]</span>";
    } else
      echo "<span style=color:blue;font-weight:bold>Total : <span id=total_$cashNb></span></span>";
    echo "<table><tr>";
    if ($cashNb == 4) {
      for ($i = 0; $i < sizeof($bills); $i++)
        echo "<th><img src=up.jpg height=50 width=20 title='Transfert réserve->caisse' onclick=transferCash($i)></th>";
      echo "</tr><tr>";
    }
    foreach ($bills as $i => $v)
      echo "<th>$v</th>";
    echo "</tr><tr>";
    for ($i = 0; $i < sizeof($bills); $i++)
      echo "<td align=right><input id=cur_${cashNb}_$i " . (!$bEnabled ? "disabled=disabled style=color:black " : "onkeyup=compute(this,$cashNb,$i) ") . "value='" . round($content[$i]) . "' type=text size=5 maxlength=100></td>";
    if ($cashNb >= 3) {
      echo "</tr><tr>";
      for ($i = 0; $i < sizeof($bills); $i++)
        echo "<td align=right><input id=next_${cashNb}_$i readonly=readonly style=background-color:gray;font-weight:bold value='" . round($content2 ? $content2[$i] : $content[$i]) . "' name=cash_${cashNb}_$i type=text size=4 maxlength=100></td>";
    } else
      switch ($cashNb) {
        case 1:
          echo "<td><img style=cursor:pointer src=historyDown.jpg height=40 width=30 onclick=location.assign('cash.php$basePath&history=%2D" . ($histPrev ? $histPrev : '1') . "')>";
          if ($history)
            echo "<img style=cursor:pointer;margin-left:0.5cm src=historyUp.jpg height=40 width=30 onclick=location.assign('cash.php$basePath&history=%2B$history')>";
          echo "</td>";
          break;

        case 2:
          if (!$history) {
            echo "<td>";
            createAction(-1, "Enregistrer", "save(false)");
            echo "<br>";
            createAction(-1, "+ dans facturier", "save(true)");
            echo "</td>";
          }
      }
    echo "</tr><tr style=font-size:small;color:darkgreen>";
    for ($i = 0; $i < sizeof($bills); $i++)
      echo "<td style=text-align:right id=cash_${cashNb}_$i></td>";
    echo "</tr></table>";
    $cashNb++;
  }

  if ($history = $_GET["history"]) {
    if ($history == "-1") {
      $r = jmysql_query("select max(ri) from cash");
      if ($history = $r && jmysql_num_rows($r) ? jmysql_result($r, 0) : 0) {
        $cur4 = jmysql_fetch_row(jmysql_query("select date,cash,comment from cash where ri=$history"));
        $cur3 = jmysql_fetch_row(jmysql_query("select date,cash,comment from cash where ri=" . ($history - 1)));
        $history = -$history;
      }
    }
    if ($history < 0) {
      $history = -$history;
      $cur4 = jmysql_fetch_row(jmysql_query("select date,cash,comment from cash where ri=$history"));
      $cur3 = jmysql_fetch_row(jmysql_query("select date,cash,comment from cash where ri=" . ($history - 1)));
      $c3 = unserialize($cur3[1]);
      $c4 = unserialize($cur4[1]);
      $r = jmysql_query("select max(ri) from cash where ri<" . ($history - 1));
      if ($histPrev = $r && jmysql_num_rows($r) ? jmysql_result($r, 0) : 0) {
        $p4 = unserialize(jmysql_result(jmysql_query("select cash from cash where ri=$histPrev"), 0));
        $p3 = unserialize(jmysql_result(jmysql_query("select cash from cash where ri=" . ($histPrev - 1)), 0));
      } else
        $history = 0;
    } else if ($history > 0) {
      $p4 = unserialize(jmysql_result(jmysql_query("select cash from cash where ri=$history"), 0));
      $p3 = unserialize(jmysql_result(jmysql_query("select cash from cash where ri=" . ($history - 1)), 0));
      $histPrev = substr($history, 1);
      $r = jmysql_query("select min(ri)+1 from cash where ri>$histPrev");
      if ($history = $r && jmysql_num_rows($r) ? jmysql_result($r, 0) : 0) {
        $cur4 = jmysql_fetch_row(jmysql_query("select date,cash,comment from cash where ri=$history"));
        $cur3 = jmysql_fetch_row(jmysql_query("select date,cash,comment from cash where ri=" . ($history - 1)));
        $c3 = unserialize($cur3[1]);
        $c4 = unserialize($cur4[1]);
      }
    }
  }

  echo "<h2>Contenu caisse</h2><form id=form method=post autocomplete=off><input id=submit type=submit style=display:none><input type=hidden name=comment><input id=amount type=hidden name=amount>" . nl;
  $v = null;
  displayCash($v, !$history);

  echo "<h2>Transfert caisse à réserve</h2>" . nl;
  if ($history) {
    foreach ($c4 as $i => $v)
      if ($v != $p4[$i])
        $c[$i] = $v - $p4[$i];
    foreach ($c3 as $i => $v)
      if ($v != $p3[$i])
        $c[$i] += $v - $p3[$i];
    displayCash($c, false);
  } else
    displayCash();

  echo "<h2>Caisse</h2>" . nl;
  if ($history)
    displayCash($p3, false, $c3);
  else {
    $c = unserialize(jmysql_result(jmysql_query("select cash from cash where ri=(select max(ri)-1 from cash)"), 0));
    displayCash($c, false);
  }

  echo "<h2>Réserve</h2>" . nl;
  if ($history)
    displayCash($p4, false, $c4);
  else {
    $c = unserialize(jmysql_result(jmysql_query("select cash from cash where ri=(select max(ri) from cash)"), 0));
    displayCash($c, false);
  }
  echo "<h2 id=cash style='text-align:center;width:30%;color:blue;border:2px solid black'></h2>";
  echo "</form></body>";
  ?>
  <script type="text/javaScript">

    bills=[500,200,100,50,20,10,5,2,1,.5,.2,.1,.05,.02,.01];
    var cashOrig=0;
    var reserveCash=0;
    var boxCash=0;
    function loadEvent()
    {
    for (i=0; i<bills.length; i++)
    {
    cashOrig+=changed(document.getElementById('next_3_'+i),3,i)/1;
    changed(document.getElementById('next_4_'+i),4,i);
    changed(document.getElementById('cur_2_'+i),2,i);
    }
    }

    function transferCash(i)
    {
    v=prompt("Entre le nombre de "+(i>=7 ? "pièces":"billets")+" de "+bills[i]+"€ à transférer dans la caisse");
    if (!v)
    return;
    v=eval(v);
    obj=document.getElementById('next_3_'+i);
    obj.value=obj.value/1+v/1;
    changed(obj,3,i);  
    obj=document.getElementById('next_4_'+i);
    obj.value-=v;
    changed(obj,4,i);  
    }


    function getCash(v)
    {
    var t1=document.getElementById(v).innerHTML;
    if (!t1.length)
    return t1;
    return t1.substr(0,t1.length-1);
    }

    function compute(obj, cash, i)
    {
    var v=changed(obj,cash,i);
    switch(cash)
    {
    case 1:
    if (!v.length)
    {
    var out=document.getElementById('next_3_'+i);
    out.value=document.getElementById('cur_3_'+i).value;
    changed(out,3,i);
    return;
    }
    var out=document.getElementById('cur_2_'+i);
    v=out.value=(Number)((v-getCash('cash_3_'+i))/bills[i]).toFixed();
    changed(out,2,i);
    out=document.getElementById('next_4_'+i);
    out.value=(Number)(v/1+document.getElementById('cur_4_'+i).value/1).toFixed();
    changed(out,4,i);
    return;

    case 2:
    var out=document.getElementById('next_4_'+i);
    out.value=(Number)(v/bills[i]+document.getElementById('cur_4_'+i).value/1).toFixed();
    changed(out,4,i);
    out=document.getElementById('next_3_'+i);
    var v1=getCash('cash_1_'+i);
    if (!v1.length)
    return;
    out.value=(Number)((v1-v)/bills[i]).toFixed();
    changed(out,3,i);
    return;

    }
    }

    function changed(obj, cash, i)
    {
    var el='cash_'+cash+'_'+i;
    var out=document.getElementById(el);
    if (!obj.value.length)
    out.innerHTML=vout='';
    else
    {
    vout=(eval('('+obj.value+')*'+bills[i])).toFixed(2);
    out.innerHTML=vout+"€";
    }
    el='cash_'+cash+'_';
    var t=0;
    var tb=0;
    for (i=0; i<bills.length; i++)
    {
    if (i<=6)
    tb+=getCash(el+i)/1;
    t+=getCash(el+i)/1;
    }
    var v=t.toFixed(2)+"€ (billets:"+tb.toFixed(2)+"€)";
    if (cash==1)
    v+="  Gain: "+(t-cashOrig).toFixed(2)+"€";
    document.getElementById("total_"+cash).innerHTML=v;
    if (cash==3)
    boxCash=t;
    if (cash==4)
    reserveCash=t;  
    document.getElementById("cash").innerHTML="Total liquidité: "+(boxCash/1+reserveCash/1).toFixed(2)+"€";
    return vout;  
    }

    function save(bBiller)
    {
    o=document.getElementById('form');
    if (bBiller)
    {
    v=document.getElementById("total_1").innerHTML;
    if ((i=v.lastIndexOf(" Gain: "))!=-1)
    document.getElementById('amount').value=v.slice(i+7,v.lastIndexOf('€'));
    else
    {
    v=document.getElementById("total_2").innerHTML;
    document.getElementById('amount').value=v.slice(0,v.indexOf('€'));
    }
    o.target='_blank';
    o.action="cash.php<?php echo $basePath ?>&action=save";
    }
    else
    {
    if (!(document.getElementsByName("comment")[0].value=prompt("Entre un commentaire pour cette enregistrement")))
    return;
    o.target='_self';
    o.action="cash.php<?php echo $basePath ?>&action=save_only";
    }
    document.getElementById("submit").click();
    }

    function history()
    {
    }

  </script>  