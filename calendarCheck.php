<?php

function checkCalendar($condition, $bUpd = false)
{
  //$condition="date like '2016-03-19%'";
  if ($bUpd)
    $res = jmysql_query("update calendar set field=null where ($condition)");
  $res = jmysql_query("select * from calendar where ($condition) and home like '%" . KEY_CLUB . "%' and updated&0x50=0 and (type<5 or type=8 or type>=11) order by date");
  while (true) {
    $row = jmysql_fetch_assoc($res);
    //dump($row, "row");
    //echo "curDate=$curDate".NL;
    if (!$row || substr($row["date"], 0, 10) != $curDate) {
      if ($bUpd && $teamTime) {
        //dump($teamTime,"teamTime");
        foreach ($teamTime as $k => $v) {
          //echo "<pre>time=".sprintf("%02d:%02d</pre>", intval($v->time/60), $v->time%60);
          //dump($v,"v[$k]");
          if ($k == sizeof($teamTime) - 1)
            break;
          $v1 = &$teamTime[$k + 1];
          if ($v1->time - $v->time >= ($v->team >= 1000 || $v->team < 800 ? 105 : ($v->team < 900 ? 90 : 60)))
            continue;

          if (!$v->field || $v->moveAllowed)
            $v->field = $v1->team < $v->team ? 2 : 1;
          $v1->field = 3 - $v->field;
          $v1->moveAllowed = $v1->time - $v->time >= ($v->team >= 1000 || $v->team < 800 ? 105 : ($v->team < 900 ? 90 : 60));
          //echo "<pre>field v=$v->field, field v1=$v1->field</pre>";
          //echo (int)($v->time/60).":".($v->time%60)." - ".(int)($v1->time/60).":".($v1->time%60)."=".abs($v1->time - $v->time).NL;dump ($v, "v");dump($v1, "v1");
        }
        foreach ($teamTime as $k => $v) {
          if (!$v->field)
            $v->field = $v->team < 400 || $v->team >= 1000 && $v->team < 1250 ? 3 : ($v->team >= 800 ? 2 : 1);
          $sql = "update calendar set field=$v->field where rowid=$v->ri";
          jmysql_query($sql);
          //echo "sql=$sql".NL;
        }
      }
      unset($teams);
      unset($times);
      unset($teamTime);
      $curDate = substr($row["date"], 0, 10);
    }
    //2 teams same day
    //dump($teams,"teams");
    //dump($row,"row");
    if (!$row)
      break;
    $ri = $row["rowid"];
    $day = substr($row["date"], 0, 10);
    if ($teams[$row["team"]]) {
      $rowids->days[$day] = true;
      $rowids->ri[$ri] |= 1;
      $rowids->ri[$teams[$row["team"]]] |= 1;
    } else
      $teams[$row["team"]] = $ri;

    //less than 1h45 between 2 teams on same field.
    //only valid at home.
    $t = strtok(substr($row["date"], 10), ": ") * 60;
    $t += strtok(": ");
    //echo "curDate=$curDate, t=$t".NL;
    unset($obj);
    $obj->time = $t;
    $obj->ri = $ri;
    $obj->team = $row["team"];
    $teamTime[] = $obj;
    //dump($teamTime, "teamTime");
    if (isset($row["field"]))
      if (!$times[$f = $row["field"] - 1] || $t >= $times[$f]->time) {
        $times[$f]->time = $t + ($obj->team >= 1000 || $obj->team < 800 ? 105 : ($obj->team < 900 ? 90 : 0));
        $times[$f]->ri = $ri;
      } else {
        $rowids->days[$day] = true;
        $rowids->ri[$ri] |= 2;
        $rowids->ri[$times[$f]->ri] |= 2;
      }
    else if (!$times[0] || $t >= $times[0]->time) {
      $times[0]->time = $t + ($obj->team >= 1000 || $obj->team < 800 ? 105 : ($obj->team < 900 ? 90 : 0));
      $times[0]->ri = $ri;
    } else if (!$times[1] || $t >= $times[1]->time) {
      $times[1]->time = $t + ($obj->team >= 1000 || $obj->team < 800 ? 105 : ($obj->team < 900 ? 90 : 0));
      $times[1]->ri = $ri;
    } else {
      $rowids->days[$day] = true;
      $rowids->ri[$ri] |= 2;
      $rowids->ri[$times[0]->ri] |= 2;
      $rowids->ri[$times[1]->ri] |= 2;
    }
    //dump($times, "times");
    //dump($rowids, "rowids");
  }
  return $rowids;
}
