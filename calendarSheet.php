<?php
include "common/common.php";
include "head.php";
?>
<style type="text/css">
  td {padding:2 2 2 2}
</style>
<?php
$nb = 0;

function match($tup, $bLeft, $span = null)
{
  global $nb;
  if (!$span)
    $b = "border-" . ($bLeft ? "left" : "right") . ":3px solid black;";
  return "<td style='" . ($bLeft ? $b : '') . "font-weight:bold;text-align:center;background-color:#F8C0C0'>" . substr($tup["date"], 11, 5) . "</td><td style=text-align:center>" . $tup["num"] . "</td><td onclick=edit(" . ( ++$nb) . ") $span style='" . (!$bLeft ? $b : '') . "'><span style=font-weight:bold;font-style:italic;background-color:#F8C0C0>" . $tup["team"] . "</span> - " . $tup["away"] . "<span id=span$nb style=font-size:small;color:blue;display:none></span><input onblur=lost($nb) id=inp$nb type=text style=display:none size=15 maxlength=50>" . ($tup["comment"] && stripos($tup["comment"], "auparavant") === false ? "<br><span style=color:#5d73a3;font-style:italic;font-weight:bold>" . $tup["comment"] . "</span>" : '') . "</td>" . nl;
}

$day = $_GET["day"];
$dt = new DateTime($day);
echo "<h1 style=width:28cm;text-align:center;text-decoration:underline;color:blue><img src=" . LOGO_LITTLE_CLUB . " style=vertical-align:middle;margin-right:15px>Journée du " . $weekDays[$dt->format("w")] . ' ' . $dt->format("d") . ' ' . $months[$dt->format('n')] . ".<img src=__NEW__logo_3x3.png style=vertical-align:middle;margin-left:15px></h1>" . nl;
if (jmysql_result(jmysql_query("select count(*) from calendar where date like '$day%' and updated&0x40=0 and (type=10 or home like '%" . KEY_CLUB . "%')"), 0)) {
  echo "<h2 style=width:28cm;text-align:center;background-color:lightgreen>Matches à domicile.</h2>" . nl;

  echo "<table border=1 style='border-collapse:collapse;width:27cm;margin-left:0.5cm'>" . nl;
  echo "<tr style=font-size:large><th colspan=3 style='border-right:3px solid black;width:50%;max-width:50%;min-width:50%'>Terrain 2 (côté entrée)</th><th colspan=3>Terrain 1 (côté buvette)</th></tr>" . nl;
  echo "<tr style='border-bottom:5px double black'><th style=width:1cm>Hr</th><th style=width:1.5cm>Match</th><th style='border-right:3px solid black'>Equipe - club</th><th style=width:1cm>Hr</th><th style=width:1.5cm>Match</th><th>Equipe - club</th></tr>";
}
/*
  updated=
  0x01 date updated
  0x02 home updated
  0x04 away updated
  0x10 extern
  0x40 Row deleted
 */

$r = jmysql_query("select * from calendar where date like '$day%' order by date");
$n = "<td colspan=3></td>";
$bFirst = true;
while ($tup = jmysql_fetch_assoc($r)) {
  if ($tup["type"] == 10) {
    echo "<tr><td style=font-weight:bold;text-align:center>" . substr($tup["date"], 11, 5) . "</td><td colspan=5 style=font-weight:bold;font-style:italic;text-align:center>" . $tup["comment"] . "</td>";
    continue;
  }
  $c = getCategory($tup["team"] | CAT_PLAYER);
  $tup["team"] = $c["short"];
  if (stripos($tup["home"], KEY_CLUB) !== false && ($tup["updated"] & 0x10) == 0) {
    switch ($tup["field"]) {
      case 1:
        echo "<tr>$n" . match($tup, true) . "</tr>" . nl;
        break;

      case 2:
        echo "<tr>" . match($tup, false) . "$n</tr>" . nl;
        break;

      default:
        echo "<tr" . ($bFirst ? " style='border-top:4px double black;'" : '') . ">" . match($tup, null, " colspan=4 align=center") . "</tr>" . nl;
        $bFirst = false;
        break;
    }
  } else
    $away[] = $tup;
}
echo "</table>";

if (!$away)
  return;
$nb = 100;
echo "<h2 style=width:28cm;text-align:center;margin-top:1cm;background-color:red>Matches en déplacement.</h2>" . nl;
echo "<table border=1 style='border-collapse:collapse;width:27cm;margin-left:0.5cm'>" . nl;
echo "<tr style='font-size:large;border-bottom:5px double black'><th style=width:1cm>Hr</th><th style=width:1.5cm>N° match</th><th style=width:3cm>Equipe</th><th style=width:6.5cm>Club</th><th>Adresse</th></tr>" . nl;
foreach ($away as $tup) {
  //dump($tup);
  $addr = $tup["clubNb"] ? jmysql_result(jmysql_query("select address from clubs where num=" . $tup["clubNb"]), 0) : null;
  echo "<tr><td  style='font-weight:bold;text-align:center'>" . substr($tup["date"], 11, 5) . "</td><td style=;text-align:center>" . $tup["num"] . "</td><td style=font-weight:bold;font-style:italic>" . $tup["team"] . "</td><td style=font-weight:bold;white-space:nowrap>" . $tup["home"] . ($tup["comment"] && stripos($tup["comment"], "auparavant") === false ? "<br><span style=color:#5d73a3;font-style:italic;font-weight:bold>" . $tup["comment"] . "</span>" : '') . "</td><td onclick=edit(" . ( ++$nb) . ")><span id=span$nb>$addr</span><textarea rows=2 cols=65 style=display:none id=inp$nb onblur=lost($nb)>$addr</textarea></td></tr>" . nl;
}
echo "</table>" . nl;
?>    

<script type="text/javaScript">

  function edit(nb)
  {
  inp=document.getElementById('inp'+nb);
  document.getElementById('span'+nb).style.display='none';
  inp.style.display='inline';
  inp.focus();
  }

  function lost(nb)
  {
  inp=document.getElementById('inp'+nb);
  span=document.getElementById('span'+nb);
  inp.style.display='none';
  if (nb>=100)
  span.innerHTML=inp.value;
  else
  span.innerHTML=' ('+inp.value+')';
  if(inp.value.length)
  span.style.display='inline';
  else
  span.style.display='none';
  }

</script>
