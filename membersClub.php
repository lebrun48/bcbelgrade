<?php
$bSession = true;
if ($_GET["action"] == "equipement") {
  include "common/common.php";
  //dump($request);
  jmysql_query("update members set " . (($t1 = $request["i"]) > 3 ? "equipement=" . (($t = $request["value"]) ? "'" . addslashes($t) . "'" : "null") : "equipSt$t1=" . $request["value"]) . " where num=" . $request["num"]);
  exit();
}
include "admin.php";
include "head.php";
//stop(__FILE__,__LINE__,null,"Site en maintenance!<br>Cette fonctionnalité ne sera disponible qu'à partir du lundi 18/12.");
if (!$admin)
  stop(__FILE__, __LINE__, "Pas de droit de lecture");

include "common/tableTools.php";
$bOnlyMain = $request["onlymain"] == 'y';
include "memberActions.php";
//dump($request,"request");
//$seeRequest=1;
include "common/tableEdit.php";
?>
<title>Membres          
</title>    
<style type="text/css">
  input {font-size:75%;}
</style>
</head>
<body onload=genericLoadEvent()>   
  <?php
  $col = array("picture"   => array(0, "Photo", "min-width:40; width:40", "text-align:center", '@', '1'),
      "sex"       => array(1, "S.", "min-width:20; width:20", "text-align:center"),
      "numEquip"  => array(2, "N°J", "min-width:40; width:40", "text-align:center;vertical-align:top"),
      "equipSt"   => array(0, "Etat équipement", '', 'vertical-align:top'),
      "num"       => array(4, "Num", "min-width:30; width:30", "text-align:right"),
      "id"        => array(0, "ID", "min-width:120; width:120"),
      "numCat"    => array(0, "N°C", "min-width:40; width:40", "text-align:right;vertical-align:top", "@"),
      "category"  => array(-1, "Catégorie", "min-width:90;width:90", "vertical-align:top"),
      "ncategory" => array(-1, "Nouvelle cat.", "min-width:100;width:100", "vertical-align:top"),
      "caution"   => array(-1, "St"),
      "remark"    => array(10, "Remarque", "min-width:100;width:100"),
      "AWBB_id"   => array(5, "AWBB", "min-width:40; width:40", "text-align:right"),
      "year"      => array(2, "An", "min-width:40; width:40", "text-align:right", "date_format(birth,'%y')"),
      "name"      => array(15, "Nom", "min-width:120; width:120"),
      "firstName" => array(15, "Prénom", "min-width:100; width:100"),
      "phP"       => array(0, "GSM", "min-width:90; width:90"),
      "emailP"    => array(10, "EMail", '', "white-space:nowrap;max-width:100; width:100;overflow:hidden"),
      "phF"       => array(0, "GSM parent 1", "min-width:85; width:85"),
      "phM"       => array(0, "GSM parent 2", "min-width:90; width:90"),
      "phFix"     => array(0, "Tél fixe.", "min-width:90; width:90"),
      "emailF"    => array(10, "EMail 1", '', "white-space:nowrap;max-width:100; width:100;overflow:hidden"),
      "emailM"    => array(10, "EMail 2", '', "white-space:nowrap;max-width:100; width:100;overflow:hidden"),
      "cp"        => array(15, "CP"),
      "address"   => array(15, "Adresse", "min-width:180; width:180", '', "concat(address,', ',town)"),
      "birth"     => array(0, "Anniv.", "min-width:70; width:70"),
      "natNum"    => array(0, "N° Nat.", "min-width:70; width:70"),
      "birthLoc"  => array(0, "Lieu naissance", "min-width:90; width:90"),
      "dateIn"    => array(15, "Entrée", "min-width:70; width:70"),
      "dateOut"   => array(15, "Sortie", "min-width:70; width:70")
  );

  class Member extends TableEdit
  {

    public $cat;
    public $idxCat;

//--------------------------------------------------------------------------------------------------
    public function applyDisplay(&$key, &$val, &$row, $idxLine)
    {
      static $bNumCat;
      static $styleCat;
      switch ($key) {
        case "picture":
          if (file_exists($_SERVER["DOCUMENT_ROOT"] . "/members/pictures/N_" . $row["num"] . ".jpg"))
            return $this->formatValue('<img src="/members/pictures/N_' . $row["num"] . '.jpg" height="50" widt="39">', $val);
          if ($row["extern"])
            return $this->formatValue('<span style="font-size:x-large">X</span>', $val);
          return $this->formatValue("&nbsp;", $val);

        case "name":
          for ($i = 0; $i <= 6; $i++)
            if ($row["category" . ($i ? $i : '')] == CAT_SPONSOR) {
              $r = jmysql_query("select society from client where num=" . $row["num"]);
              if ($r && jmysql_num_rows($r) && ($n = jmysql_result($r, 0)) && (!$row["name"] || stripos($n, $row["name"]) === false))
                $row["name"] = ($t = $row["name"]) ? $n . NL . $t : $n;
              break;
            }
          $this->nums .= $row["num"] . ',';
        case "firstName":
          global $type;
          if ($type && !$row["type"])
            return TableEdit::applyDisplay($key, $val, $row, $idxLine, "<span style=text-decoration:line-through>" . $row[$key] . "</span>");
          break;

        case "numCat":
          $bNumCat = true;
          $styleCat = $val[3];
          return;

        case "caution":
//Caution
//bit 0x01 Payée
//bit àx02 Remboursée
//bit 0x04 Payée sur versement
//bit 0x08 Pas de remboursement
//bit 0x10 Pas de caution
//bit 0x20 Caution obligatoire
//status
//0=>"Rien payé" 
//1=>"Payé 1er" 
//2=>"Payé 2ième"
//3=>"Payé 3ième"
//4=>"Payé totalité"
//5=>"Payé partie"
//9=>"Facturé"
//10=>"Nouveau"
//11=>"En attente"
//0x10=>"Only single payment;
//0x20=>"Recommandé"
//0x40=>"Count Status";
//0x80=>"Keep single price";
          if (!isset($row["status"])) {
            $dispVal = '';
            return TableEdit::applyDisplay($key, $val, $row, $idxLine, $dispVal);
          }
          switch ($row["status"] & 0x0F) {
            case 0:$dispVal = "<b>R </b>";
              break;
            case 1:$dispVal = "1 ";
              break;
            case 2:$dispVal = "2 ";
              break;
            case 3:$dispVal = "<span style=color:green;font-weight:bold>3 </span>";
              break;
            case 4:$dispVal = "<span style=color:green;font-weight:bold>T </span>";
              break;
            case 5:$dispVal = "P ";
              break;
            case 9:$dispVal = "F ";
              break;
            case 10:$dispVal = "N ";
              break;
            case 11:$dispVal = "W ";
              break;
          }
          switch ($row["caution"]) {
            case 0:$dispVal .= "F";
              break;
            case 1:$dispVal .= "P";
              break;
            case 2:$dispVal .= "<span style=color:red;font-weight:bold>R</span>";
              break;
            case 4:$dispVal .= "V";
              break;
            case 8:$dispVal .= "<span style=color:red;font-weight:bold>N</span>";
              break;
            case 16:$dispVal .= "X";
              break;
            case 32:$dispVal .= "O";
              break;
          }
          return TableEdit::applyDisplay($key, $val, $row, $idxLine, $dispVal);

        case "numEquip":
          $dispVal = $row[$key];
          if ($row["category2"])
            $dispVal .= "<br>" . $row["numEquip2"];
          if ($row["category3"])
            $dispVal .= "<br>" . $row["numEquip3"];
          return TableEdit::applyDisplay($key, $val, $row, $idxLine, $dispVal);

        case "equipSt":
          if ($row["category"] & CAT_PLAYER || $row["category2"] & CAT_PLAYER || $row["category3"] & CAT_PLAYER) {
            for ($i = 0; $i < 3; $i++) {
              $n = $i ? $i + 1 : '';
              if ($row["numEquip$n"])
                $dispVal .= "<input style=height:11px type=checkbox onchange=equipChange('$n'," . $row["num"] . ",this)" . ($row[$key . $n] ? " checked=checked" : '') . ">Ok<br>";
            }
            $dispVal .= "<input type=text onchange=equipChange('4'," . $row["num"] . ",this) value=\"" . htmlspecialchars($row["equipement"]) . "\">";
          }
          return TableEdit::applyDisplay($key, $val, $row, $idxLine, $dispVal);

        case "category":
        case "ncategory":
          global $bOnlyMain;
          $bNumCat = $bNumCat && $key[0] == 'c';
          $bExport = $this->bExport && !$bOnlyMain;
          unset($this->cat);
          if ($row[$key]) {
            $t = getCategory($row[$key]);
            $category = $t["short"];
            $numCat = $row["category"];
          }
          if ($this->bExport) {
            $this->idxCat = $idxLine;
            if ($bNumCat)
              $this->line[$idxLine] = "<td>" . $row[$key] . "</td>";
            $this->line[$idxLine] .= "<td>" . $t["name"] . "</td>";
          }
          for ($i = 2; $i <= 6; $i++) {
            if ($row["${key}$i"]) {
              $t = getCategory($row["${key}$i"]);
              if ($bNumCat)
                $numCat .= "<br>" . $row["${key}$i"];
              if ($bExport) {
                if ($bNumCat)
                  $this->cat[$i - 2] = "<td>" . $row["${key}$i"] . "</td>";
                $this->cat[$i - 2] .= "<td>" . $t["name"] . "</td>";
              }
              $category .= "<br>" . $t["short"];
            }
          }
          if ($bNumCat)
            $s = "<td style=\"$styleCat\">$numCat</td>";
          return $s . "<td style=\"$val[3]\">$category</td>";

        case "emailF":
        case "emailM":
        case "emailP":
          if ($row[$key][0] == '-')
            $row[$key] = substr($row[$key], 1);
          if ($this->bExport)
            $this->line[$idxLine] = "<td>$row[$key]</td>";
          return "<td style=\"$val[3]\">" . (is_null($row[$key]) ? "&nbsp;" : '<a title="Envoyer un mail" style="color:black" href="mailto:' . $row["name"] . " " . $row["firstName"] . " <" . $row[$key] . '>">' . $row[$key] . "</a>") . '</td>';
      }
      return TableEdit::applyDisplay($key, $val, $row, $idxLine);
    }

//--------------------------------------------------------------------------------------------------
    public function applyFilter(&$key, &$val)
    {
      global $admin;
      switch ($key) {
        case "type":
          if ($t = $admin["categories"])
            $where = "($t or " . str_replace("category", "category2", $admin["categories"]) . " or " . str_replace("category", "category3", $admin["categories"]) . " ) and ";
          return "$where " . ($val == 2 ? "(type=0 and (numEquip is not null or numEquip2 is not null or numEquip3 is not null) or type<>0)" : "type" . ($val ? "<>0" : "=0"));

        case "category":
        case "ncategory":
          $a = unserialize(substr($val, 3));
          foreach ($a as $k => $v) {
            if ($v == CAT_COACH) {
              $t = "($key&" . CAT_COACH;
              for ($i = 2; $i <= ($key[0] == 'n' ? 3 : 6); $i++)
                $t .= " or $key$i&" . CAT_COACH;
              if (sizeof($a) == 1)
                return "$t)";
              $t .= " or ";
              $isCoach = true;
            }
            if ($v == CAT_PLAYER) {
              $t .= (sizeof($t) ? '' : '(') . "$key&" . CAT_PLAYER;
              for ($i = 2; $i <= ($key[0] == 'n' ? 3 : 6); $i++)
                $t .= " or $key$i&" . CAT_PLAYER;
              if (sizeof($a) == 1 || $isCoach && sizeof($a) == 2)
                return "$t)";
              $t .= " or ";
              break;
            }
          }
          $t .= ($t ? '' : '(') . $this->buildFilter($key, $val);
          for ($i = 2; $i <= ($key[0] == 'n' ? 3 : 6); $i++)
            $t .= " or " . $this->buildFilter("${key}$i", $val);
          return $t . ')';

        case "year":
          return "date_format(birth, '%y')='$val'";
      }
      return TableEdit::applyFilter($key, $val);
    }

//--------------------------------------------------------------------------------------------------
    public function applyOrder()
    {
      switch ($this->order) {
        case "name":
          return;

        case "numEquip":
          return "order by category,numEquip " . $this->dir;

        case "year":
          return "order by birth " . $this->dir;

        default:
          return "order by $this->order " . $this->dir . ", name";
      }
    }

//--------------------------------------------------------------------------------------------------
    public function addHeaderFunction()
    {
      global $type, $admin;
      if ($admin["rights"] < 2)
        return '<img onclick="actionClick(\'viewType\', null, null)" class=imgButton src="' . ($type ? 'common/b_empty.png" title="Voir les supprimés">' : 'common/b_browse.png" title="Voir les actifs">');
    }

//--------------------------------------------------------------------------------------------------
    public function applyStyle($row)
    {
      global $color;
      return "color:" . $color[$row["type"]];
    }

//--------------------------------------------------------------------------------------------------
    public function printNumLine($num, &$row)
    {
      global $admin, $basePath;
      if ($this->bAllowEdit && $admin["rights"] == 0) {
        $r = jmysql_query("select num from cotisations where num=" . $row["num"]);
        if ($r && jmysql_num_rows($r))
          return "<a target=_blank href=cotiPDF.php$basePath&nums=" . $row["num"] . " style=color:black>$num</a>";
      }
      return TableEdit::printNumLine($num, $row);
    }

//--------------------------------------------------------------------------------------------
    public function bSelBox(&$row)
    {
      return $this->addRem;
    }

//--------------------------------------------------------------------------------------------------
    public function editImg(&$row)
    {
      global $admin, $bAdd, $request;
      if ($this->addRem = $admin["rights"] == 0 || $this->bAllowEdit && ($row["category"] & CAT_PLAYER || $row["category2"] & CAT_PLAYER || $row["category3"] & CAT_PLAYER))
        return parent::editImg($row);
    }

//--------------------------------------------------------------------------------------------------
    public function removeImg(&$row)
    {
      global $type, $admin, $bAdd;
      if (!$this->addRem)
        return;
      if ($this->bExport)
        $this->cat = array();
      if ($type)
        return TableEdit::removeImg($row);
      return '<img src="b_insrow.png" title="Remettre actif" style="border-style:none; cursor:pointer" onclick="remMember(\'' . str_replace("'", "$", $row["firstName"]) . ' ' . str_replace("'", "$", $row["name"]) . "', " . $row["num"] . ')">';
    }

//--------------------------------------------------------------------------------------------------
    public function buildKey(&$row)
    {
      return "members where num=" . $row["num"];
    }

//--------------------------------------------------------------------------------------------------
    public function deleteClick(&$row)
    {
      return "remMember('" . str_replace("'", "$", $row["firstName"]) . ' ' . str_replace("'", "$", $row["name"]) . "', " . $row["num"] . ")";
    }

//--------------------------------------------------------------------------------------------------
    public function applyExport()
    {
      if (!empty($this->cat)) {
        fwrite($this->exportFile, "<tr>");
        foreach ($this->cat as $val) {
          foreach ($this->line as $j => $v)
            if ($j == $this->idxCat)
              fwrite($this->exportFile, $val);
            else
              fwrite($this->exportFile, $this->line[$j]);
          fwrite($this->exportFile, "</tr>\n");
        }
      }
    }

//---------------------------------------------------------------------------------------------------
    function buildHelpArray(&$key, &$where)
    {
      switch ($key) {
        case "category":
        case "ncategory":
          $a = getExistingCategories("type <>0 and ", true, $key == "ncategory");
          $i = 0;
          foreach ($a as $k => $v) {
            if (!$isCoach && $k & CAT_COACH) {
              $a = array_slice($a, 0, $i, true) + array(CAT_COACH => "<span style=color:blue;font-weight:bold>(Tous les entraineurs)</span>") + array_slice($a, $i, null, true);
              $isCoach = true;
              $i++;
            }
            if ($k & CAT_PLAYER)
              return array_slice($a, 0, $i, true) + array(CAT_PLAYER => "<span style=color:blue;font-weight:bold>(Tous les joueurs)</span>") + array_slice($a, $i, null, true);
            $i++;
          }
      }
    }

//--------------------------------------------------------------------------------------------------
    public function exportClick()
    {
      global $admin, $bRoot;
      if ($bRoot || (!$admin["right"] & 0x80))
        echo "if (confirm('Seulement la catégorie principale (Ok) ?'))\n";
      ?>
      {
      parameter='onlymain';
      value='y';
      } 
      <?php
    }

  }

  $member = new Member;
  $member->colDef = &$col;
  $member->bExport = !strcmp($request["action"], "export");
  $member->tableId = "members";
  $member->addCol = "num,type,category2,category3,category4,category5,category6,numEquip2,numEquip3,extern,status,equipSt2,equipSt3";
  $bAdd = ($now->format("Y") == ($yearSeason + 1) && $now->format("m-d") < NEW_SEASON_DATE) || isset($request["season"]);
  if ($member->bAllowEdit = ($admin["rights"] == 0 || ($admin["rights"] & 1) && $bAdd)) {
    $member->bInsertNew = true;
    $member->selKey = "num";
    if ($bAdd) {
      $member->addCol .= ",ncategory2,ncategory3";
      $member->visible[] = "ncategory";
    } else
      unset($col["ncategory"]);
  }
  if (array_search("equipSt", $member->visible) !== false) {
    $member->addCol .= ",equipement";
    if ($bAdd) {
      $member->addCol .= ",ncategory2,ncategory3";
      $member->visible[] = "ncategory";
    }
  }

  $member->tableName = "members";
  $member->exportFileName = "membres" . SHORT_NAME_CLUB . ".xls";
  $member->bHideCol = true;
  $tok = strtok($admin["attributes"], ',');
  while ($tok) {
    $member->visible[] = $tok;
    $tok = strtok(",");
  }
  $tok = strtok($admin["visible"], ',');
  while ($tok) {
    $member->hideCol[] = $tok;
    $tok = strtok(",");
  }

  if ($action == "hideCol") {
    $t = "visible='";
    $key = $request["key"];
    if ($member->hideCol && ($k = array_search($key, $member->hideCol)) !== false) {
      unset($member->hideCol[$k]);
      if ($admin["sort"] == $key) {
        $t = "sort=null, visible='";
        $admin["sort"] = null;
      }
    } else {
      $member->hideCol[] = $key;
    }
    if ($id != -1) {
      foreach ($member->hideCol as $v)
        $t .= "$v,";
      jmysql_query("update admin set " . ($member->hideCol ? substr($t, 0, -1) : $t) . "' where num=$num");
    }
  }
  if ($cond && $action == "viewAll" && isset($request["viewAll"]))
    foreach ($cond as $k => $v)
      if ($k != "type" && (!in_array($k, $member->visible) || in_array($k, $member->hideCol))) {
        $isUpdated = true;
        unset($cond[$k]);
      }
  if ($isUpdated)
    jmysql_query("update admin set conditions='" . serialize($cond) . "' where num=$num");

  $member->order = $admin["sort"] ? $admin["sort"] : "name";
  $member->filter = &$cond;
  $member->defaultWhere = $admin["dftWhere"];
  $member->bHeader = true;
  $member->bPrintNumLine = true;
  if ($admin["rights"] < 2 || ($admin["rights"] & 0x80))
    $member->bExportAvailable = true;
  $member->build();

  include_once "tools.php";
  createAction(-1, "Liste mails", "openMail()", 120);
  if ($bRoot) {
    createAction(-1, "Generate mail", "generateMails()", 120);
    echo "<form style=display:inline method=post action=buildMailMember.php$basePath target=_blank><input id=submitMail type=submit style=display:none;visibility:hidden>" . nl;
    echo "<input id=nums type=hidden name=list></form>" . nl;
  }
  ?>

  <script type="text/javascript">
    var win = null;

    function equipChange(i, num, obj)
    {
      var xmlhttp;
      if (window.XMLHttpRequest)
        xmlhttp = new XMLHttpRequest();
      else
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
      xmlhttp.open("GET", "membersClub.php<?php echo $basePath ?>&action=equipement&i=" + i + "&num=" + num + "&value=" + (i > '3' ? encodeURI(obj.value) : obj.checked), false);
      xmlhttp.send();
      if (xmlhttp.responseText.length)
        alert(xmlhttp.responseText);
    }

    function openMail()
    {
      nums = listNums();
      if (!nums.length)
        nums = '<?php echo substr($member->nums, 0, -1) ?>';
      if (!nums.length)
      {
        alert("Aucun membre sélectionné");
        return;
      }
      loc = "membersMail.php<?php echo $basePath ?>&nums=" + nums;
      if (win && !win.closed)
        win.location.replace(loc);
      else
        win = window.open(loc, "_blank", "scrollbars=yes,left=800,width=300,height=" + screen.availHeight + ",titlebar=no,menubar=no,status=no,location=no");
      win.focus();
      return false;
    }

    function listNums()
    {
      els = document.getElementsByName("_select_");
      nums = '';
      for (i in els)
      {
        if (!els[i].checked)
          continue;
        nums += els[i].value + ',';
      }
      return nums.substr(0, nums.length - 1);
    }

    function remMember(name, par)
    {
      nums = listNums();
      if (confirm("Es-tu sûr de vouloir <?php echo (!$type ? "restaurer" : "supprimer") ?> " + (nums.length ? "cette liste" : "'" + name.replace('$', "'") + "'") + "?"))
      {
        if (nums)
        {
          par = nums;
          k = "list";
        } else
          k = "key";
<?php
if ($type && $bRoot) {
  ?>
          if (confirm("Supprimer définitivement (Oui=Ok, Non=Annuler)"))
            actionClick("delete", k, par);
          else
            actionClick("suppress", k, par);
  <?php
} else
  echo "actionClick('suppress', k, par);" . nl;
?>
      }
    }
<?php
if ($bRoot) {
  ?>

      function generateMails()
      {
        nums = listNums();
        if (!nums.length)
          nums = '<?php echo substr($member->nums, 0, -1) ?>';
        if (!nums.length)
        {
          alert("Aucun membre sélectionné");
          return;
        }
        document.getElementById("nums").value = nums;
        document.getElementById("submitMail").click();
      }
  <?php
}
?>

  </script>

</body>
</html>
