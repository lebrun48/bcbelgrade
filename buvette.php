<?php
include "../tools/common/common.php";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//FR">
<head>            
  <meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">            
  <link rel="shortcut icon" href="<?php echo LOGO_LITTLE_CLUB ?>" type="image/x-icon" />
</head>
<body style='font-family:Comic Sans MS'>
  <?php
  echo "</head><body>";

  function mySort($a, $b)
  {
    return $b[4] - $a[4];
  }

  $r = jmysql_query("select date,amount from tmp order by amount desc");
  while ($tup = jmysql_fetch_row($r)) {
    $dt = new DateTime($tup[0]);
    $n = jmysql_result(jmysql_query("select count(*) from calendar where (date like '" . creDate($dt, 0)->format("Y-m-d") . "%' or date like '" . creDate($dt, 1)->format("Y-m-d") . "%' or date like '" . creDate($dt, 2)->format("Y-m-d") . "%') and home like '%" . KEY_CLUB . "%'"), 0);
    $R2 = jmysql_query("select away from calendar where (date like '" . creDate($dt, 0)->format("Y-m-d") . "%' or date like '" . creDate($dt, 1)->format("Y-m-d") . "%' or date like '" . creDate($dt, 2)->format("Y-m-d") . "%') and home like '%" . KEY_CLUB . "%' and team=129");
    $R2 = jmysql_num_rows($R2) ? jmysql_result($R2, 0) : '';
    $ar[] = array($dt, $n, $R2, $tup[1], $tup[1] / $n);
    if (strlen($R2)) {
      $nb[1] ++;
      $sum[1] += $tup[1] / $n;
    } else {
      $nb[0] ++;
      $sum[0] += $tup[1] / $n;
    }

    $tot += $tup[1];
  }

  $m0 = $sum[0] / $nb[0];
  $m1 = $sum[1] / $nb[1];
  usort($ar, "mySort");
  echo "<table border=1px style=border-collapse:collapse;width:50%><tr><th>Date</th><th>Montant</th><th>nb équipes</th><th>Moyenne</th><th>R2 joue</th></tr>" . nl;
  foreach ($ar as $k => $v) {
    echo "<tr><td align=right>" . $v[0]->format("d/m/Y") . "</td><td align=right>" . number_format($v[3], 2, ',', '.') . "€</td><td align=center>$v[1]</td><td align=right>" . number_format($v[4], 2, ',', '.') . "€</td><td align=center style=white-space:nowrap>$v[2]</td></tr>" . nl;
    $sR2 += $v[2] ? $v[3] - $m0 * ($v[1] - 1) : 0;
  }

  echo "</table><div style=color:blue>Moyenne sans R2: <b>" . number_format($m0, 2, ',', '.') . "€</b>" . NL;
  echo "Moyenne avec R2: <b>" . number_format($m1, 2, ',', '.') . "€</b>" . NL;
  echo "Apport R2: <b>" . number_format($m1 - $m0, 2, ',', '.') . "€</b> par match soit <b>" . number_format(100 * ($m1 - $m0) / $m0, 2, ',', '.') . "%</b> de plus qu'une autre équipe.<br>Total R2: <b>" . number_format($sum[1], 2, ',', '.') . "€</b> soit <b>" . number_format(100 * $sum[1] / $tot, 2, ',', '.') . "%</b> du total des recettes buvettes<br>Si on suppose que les bonnes recettes ne sont dues qu'à la R2 elle aurait rapporté <b>" . number_format($sR2, 2, ',', '.') . "€</b> soit <b>" . number_format(100 * $sR2 / $tot, 2, ',', '.') . "%</b> du total de la buvette</div>" . NL;



  