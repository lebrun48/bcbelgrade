<?php

include "payOutData.php";
include "common/common.php";
if ($request["action"] == "getclosed") {
  echo jmysql_result(jmysql_query("select count(*) from pay_out where type=" . ENCLOSE_MONTH . " and month=" . $request["month"]), 0);
  exit();
}

include "admin.php";
include "head.php";
include "calendarData.php";
echo '<title>Payment build';
echo '</title></head><body>' . nl;

if (!$request["month"]) {
  $y = $now->format('Y');
  $m = $now->format("n");
  if ($m == 8)
    $m = 9;
  echo "<input type=checkbox id=add checked=checked>Ajouter les nouvelles semaines et nouveaux matches<br>";
  echo "</h2><p>Choisir le mois à construire</p>";
  echo "<select onChange=compute(this)>";
  echo "<option>&nbsp;</option>";
  for ($month = $m; $month != 8; $month++) {
    if ($month == 13)
      $month = 1;
    echo "<option value=\"$month\">$months[$month] " . ($month > 8 || $m < 8 ? $y : $y + 1) . "</option>";
  }
  echo "</select>";
  ?>
  <script>
    function compute(o) {
      if (o.selectedIndex == 0) {
        alert("Mois invalide");
        return;
      }
      $.ajax({async: false, url: 'payOutBuild.php', data: {action: 'getclosed', month: o.options[o.selectedIndex].value}, success: function (res) {
          if (res / 1) {
            if (document.getElementById('add').checked) {
              alert("Ce mois est déjà clôturé et il est dangereux d'ajouter les nouvelles semaines...");
              location.reload();
              return;
            }
            if (!confirm("Ce mois est déjà clôturé. Veux-tu vraiment recalculer ?")) {
              location.reload();
              return;
            }
          }
        }});
      location.assign(location.href + '&month=' + o.options[o.selectedIndex].value + '&new=' + (document.getElementById('add').checked ? 'y' : 'n'));
    }
  </script>
  <?php

  exit();
}

$seeRequest = 1;
$m = $request["month"];
//dump($request);
if ($bAdd = $request["new"] == 'y')
  jmysql_query("update prestations set status=$m where status=-1");
jmysql_query("delete from pay_out where month=$m and type<>" . ONE_SHOT . " AND type<>" . PLAYER_SANCTION . " AND type<>" . PLAYER_SANCTION_PERC);

//Get player
$res = jmysql_query("select category from members where category&" . CAT_PLAYER . " and category&0xFFFF<300 and type<>0 group by category");
while ($t = jmysql_fetch_row($res)) {
  $team = $t[0] & 0xffff;
  $r = jmysql_query("select distinct(presences.num),name from presences,members where members.num=presences.num and team=$team and presences.num in (select num from pay_out where type<=3 and month=0 and (team is null or team=$team))");
  while ($tup1 = jmysql_fetch_row($r)) {
    $player[$tup1[0]]->name = $tup1[1];
    $player[$tup1[0]]->totPres += substr_count(jmysql_result(jmysql_query("select group_concat(pres separator '') from presences where team=$team and num=$tup1[0]"), 0), "1");
  }
  //dump($player);
  $c = getRealCoach($team);
  $r = jmysql_query("select date from prestations where num=" . $c[0]["num"] . " and status=$m order by date", 1);
  while ($tup = jmysql_fetch_row($r)) {
    $lastDate = new DateTime($tup[0]);
    $lastDate = creDate($lastDate, 7);
    $lastDate = $lastDate->format("Y-m-d");
    dump($lastDate, "lastDate");
    $r1 = jmysql_query("select num, sanction,pres from presences where team=$team and date='$tup[0]' and num in (select num from pay_out where type<=3 and month=0 and (team is null or team=$team))");
    if ($r1 && jmysql_num_rows($r1)) {
      $t = mktime(12, 0, 0, substr($tup[0], 5, 2), substr($tup[0], 8, 2), substr($tup[0], 0, 4));
      if (!$player[0]->begin || $t < $player[0]->begin) {
        dump($tup[0], "begin");
        $player[0]->begin = $t;
      }
      if ($player[0]->last < $tup[0])
        $player[0]->last = $tup[0];
      while ($tup1 = jmysql_fetch_row($r1)) {
        //dump($tup1);
        if (strpos($player[$tup1[0]]->team, ",$team") === false)
          $player[$tup1[0]]->team .= ",$team";
        $t = ($player[$tup1[0]]->nPres += substr_count($tup1[2], "1"));
        if ($maxPres < $t)
          $maxPres = $t;
        $cur = new DateTime($tup[0]);
        if ($tup1[1]) {
          for ($i = 0; $i < strlen($tup1[1]); $i++) {
            $c = ord($tup1[1][$i]);
            if ($c >= 128)
              $d = $c - 128;
            else if ($c == 19) {
              $d = creDate($cur, $d);
              $sql = "insert into pay_out set month=$m, num=$tup1[0], type=" . PLAYER_SANCTION_PERC_AUTO . ", descr='Application régle <b>C4</b>: <i>Absence (-10%) du " . $d->format("d/m/Y") . "</i>', amount=10";
              jmysql_query($sql, 1);
            }
          }
        }
      }
    }
  }
  //dump($player,"nPres");
  $sql = "select rowid,date,home,results,payMonth from calendar where date<='$lastDate' and (" . ($bAdd ? "payMonth=0 or " : '') . "payMonth=$m) and team=$team and (type=" . PLAY_OFF . " or type<=" . REG_CUP . " or type>=" . CHAMPIONSHIP_NAT . ")";
  $r = jmysql_query($sql, 1);
  if ($r)
    while ($tup = jmysql_fetch_row($r)) {
      dump($tup);
      $dtM = new DateTime($tup[1]);
      $offset = $dtM->format("N") - 1;
      $dt = creDate($dtM, -$offset)->format("Y-m-d");
      $r1 = jmysql_query("select num, pres,sanction,reason from presences where team=$team and date='$dt' and num in (select num from pay_out where type<=3 and month=0 and (team is null or team=$team))");
      if (!$r1)
        break;
      $bWinHome = strtok($tup[3], " -") > strtok("- ");
      $points = ($i = stripos($tup[2], KEY_CLUB) !== false) && $bWinHome || $i === false & !$bWinHome ? 3 : 1;
      while ($row = jmysql_fetch_row($r1)) {
        if (!$tup[3]) {
          echo "<div style='border:1px solid black;color:red;font-size:x-large;width:100%'>Pas de résultats pour match du " . $dtM->format("Y-m-d") . "</div>";
          exit();
        }
        dump($row);
        $pres = substr($row[1], $offset, 1);
        $player[$row[0]]->nPoints += $pres * $points;
        if ($pres) {
          if (!$tup[4]) {
            $tup[4] = $m;
            jmysql_query("update calendar set payMonth=$m where rowid=$tup[0]");
          }
          if (stripos($row[3], "banc") === false && stripos($row[3], "pas repris") === false && stripos($row[3], "pas aligné") === false && stripos($row[3], "non aligné") === false && stripos($row[3], "non joueur") === false) {
            $player[$row[0]]->nWin += $points == 3;
            $player[$row[0]]->nPres--;
            $player[$row[0]]->nMatch++;
          }
          $player[$row[0]]->cal .= ',' . $tup[0];
        }
      }
    }
  //dump($player,"player");
}
dump($maxPres, "maxPres");
$d = $player[0]->last;
echo "last=" . $player[0]->last . NL;
$player[0]->last = mktime(12, 0, 0, substr($d, 5, 2), substr($d, 8, 2), substr($d, 0, 4));
dump($player, "player");

//----------------------------------------------------------------------------------------------
  $res = jmysql_query("select pay_out.* from pay_out,members where (month=0 or month=$m) and members.num=pay_out.num and members.type<>0 and pay_out.type&0x3F<" . PLAYER_DESC . " order by members.name,members.firstName,pay_out.type&0x3f,team desc");
$restMonth10 = array(1 => 10, 9, 8, 7, 6, 5, 5, 5, 4, 3, 2, 1);
$restMonth9 = array(1 => 9, 8, 7, 6, 5, 4, 4, 4, 4, 3, 2, 1);
while ($row = jmysql_fetch_assoc($res)) {
  //dump($row,"row");
  //dump($sum, "sum");
  $amount = $row["amount"];
  if ($num != $row["num"]) {
    unset($nMonth);
    $sum = $sumSanction = 0;
    $totKm = 0;
    $kmMonth = 0;
    $yearAc = 0;
    $num = $row["num"];
    $rowTot = jmysql_fetch_row(jmysql_query("select count(ndep), sum(ndep) from prestations where num=$num and " . ($m > 7 ? "status<>$m" : "status>=1 and status<$m") . " and status>0"));
    $rowMonth = jmysql_fetch_row(jmysql_query("select sum(nprest), sum(ndep), count(ndep) from prestations where num=$num and status=$m"));
    $prestMonth = 0;
    $rowPlayer = jmysql_fetch_row(jmysql_query("select name, firstName, id from members where num=$num"));
    dump($rowMonth, "rowMonth");
    //echo NL.$num.NL;
    unset($allTeam);
  }
  $typ = $row["type"] & 0x3F;
  $bCont = false;
  switch ($typ) {
    case PLAYER_SANCTION_PERC:
    case PLAYER_SANCTION_PERC_AUTO:
      $typ = PLAYER_SANCTION_PAID;
      break;

    case PLAYER_SANCTION:
    case ONE_SHOT:
      $sum += $amount;
      dump($sum, "sum1");
      $bCont = true;
      break;

    case HOME:
      $totKm += $rowTot[0] * MV_WEEK + $rowTot[1] * $amount;
      $home = $amount;
      $kmMonth = $rowMonth[1] * $amount + $rowMonth[2] * MV_WEEK;
      if (isset($player[$num])) {
        $kmMonth += ($player[$num]->nPres + $player[$num]->nMatch) * $amount * 2;
        $totKm += ($player[$num]->totPres - $player[$num]->nPres - +$player[$num]->nMatch) * $amount * 2;
      }
      dump($kmMonth, "kmMonth");
      //dump($player[$num]);
      $bCont = true;
      break;

    case PREST_FICT:
      $kmMonth = $home * 2 * $amount + 4 * MV_WEEK;
      $totKm += $kmMonth * ($m > 6 ? $m - 9 : $m - 1);
      $bCont = true;
      break;

    case YEAR_ACCOUNT:
      if ($m > 8) {
        $yearAc = $amount;
        $totKm += $row["detail"];
      }
    case NO_MAIL:
      $bCont = true;
      break;

    case PREST:
      unset($row["descr"]);
      break;
  }
  if ($bCont)
    continue;

  $sql = "insert into pay_out set month=$m, num=$num, type=$typ" . ($row["descr"] ? ", descr='" . $row["descr"] . "'" : null) . ($row["detail"] ? ", detail='" . $row["detail"] . "'" : null) . ($row["team"] ? ",team=" . $row["team"] : '');

  switch ($typ) {
    case PLAYER_SANCTION_PAID:
      $amount = -$sum * $amount / 100;
      $sql .= ", amount=$amount";
      $sumSanction += $amount;
      break;

    case PREST:
      if ($row["team"]) {
        $nPrest = 0;
        $dt = jmysql_query("select date from prestations where num=$num and status=$m");
        while ($t = jmysql_fetch_row($dt)) {
          $n = jmysql_result(jmysql_query("select CONV(BIT_OR(CONV(pres, 2, 10 )),10,2) from presences where team=" . $row["team"] . " and date='$t[0]'", 1), 0);

          $nPrest += substr_count($n, '1');
          //dump($nPrest);
        }
        $prestMonth += $nPrest;
      } else
        $nPrest = $rowMonth[0] - $prestMonth;
      $sum += $nPrest * $amount;
      $sql .= ", amount=" . ($nPrest * $amount) . ", descr='Prestation entraineur" . ($row["team"] ? " (" . getCategory($row["team"] | CAT_PLAYER, true) . ")" : '') . "', detail='$nPrest x " . sprintf("%.02f", $amount) . " € (*)'";
      $prest = $amount;
      break;

    case MOVE:
      $mv = $rowMonth[1] * $amount * PAY_KM_REAL;
      $sum += $mv;
      $totKm += $rowTot[0] * MV_WEEK + $rowTot[1] * $amount;
      $kmMonth = $rowMonth[1] * $amount + $rowMonth[2] * MV_WEEK;
      if (!$prestMonth && $rowMonth[1] == $rowMonth[0] * 2)
        $sql = "update pay_out set amount=amount+$mv, detail='$rowMonth[0] x " . sprintf("%.02f", $prest + $amount * PAY_KM_REAL * 2) . " € (*)' where month=$m and num=$num and type=" . PREST;
      else
        $sql .= ", amount=$mv, descr='Frais déplacements', detail='$rowMonth[1] déplacements'";
      break;

    case MONTHLY:
      if ($rowMonth[0] && stripos($row["descr"], "prestation") !== false)
        $sql .= ", detail='$rowMonth[0] prestations (*)'";
    case PLAYER_MONTHLY:
      $nMonth = $typ == MONTHLY ? 8 + (($row["type"] >> 6) & 0x3) : 9;
      if ($orderMonths[$m] > $nMonth) {
        $bCont = true;
        break;
      }
      $sum += $amount;
      if (!$row["descr"])
        $row["descr"] = 'Indemnité mensuelle';
      $sql .= ",amount=$amount";
      break;

    case PLAYER_BONUS:
    case PLAYER_BONUS_WIN:
      $t = ($typ == PLAYER_BONUS ? $player[$num]->nPoints : $player[$num]->nWin) * $amount;
      $sum += $t;
      $sql .= ", amount=$t, descr='" . ($row["type"] == PLAYER_BONUS ? "Bonus par point." : "Prime victoire <b>si aligné</b>.");
      if ($player[$num]->cal)
        $sql .= " (voir les <a href=calendar.php?season=" . ($yearSeason - 2000) . "&list=" . $player[$num]->cal . ">matches</a>)', detail='" . ($typ == PLAYER_BONUS ? $player[$num]->nPoints . " points" : ($n = $player[$num]->nWin) . " match" . ($n > 1 ? "es" : '')) . " x " . sprintf("%.02f", $amount) . " €'";
      else
        $sql .= "', detail='Pas de match gagné'";
      break;

    case PLAYER_ABSENT:
      if (($n = $maxPres - $player[$num]->nPres - $player[$num]->nMatch) > 0) {
        $sql .= ",descr='Déduction pour absence(s)  (voir les <a href=presPlayer.php?id=$rowPlayer[2]&season=" . ($yearSeason - 2000) . "&begin=" . $player[0]->begin . "&end=" . $player[0]->last . "&team=" . substr($player[$num]->team, 1) . ">présences</a>)', detail='$n absence" . ($n > 1 ? 's' : '') . " X ${amount}€', amount=-" . ($amount *= $n);
        $sum -= $amount;
        break;
      }
      $bCont = true;
      break;

    case PLAYER_PRESENCE:
      $t = $player[$num]->nPres * $amount;
      echo $t . NL;
      $sum += $t;
      $sql .= ", amount=$t, descr='Défraiement présences entrainements et match <b>non aligné</b> (voir les <a href=presPlayer.php?id=$rowPlayer[2]&season=" . ($yearSeason - 2000) . "&begin=" . $player[0]->begin . "&end=" . $player[0]->last . "&team=" . substr($player[$num]->team, 1) . ">présences</a>)', detail='" . $player[$num]->nPres . " présences x " . sprintf("%.02f", $amount) . " €'";
      break;

    case PLAYER_MATCH:
      $t = $player[$num]->nMatch * $amount;
      echo $t . NL;
      $sum += $t;
      $sql .= ", amount=$t, descr='Défraiement match officiel <b>aligné</b>', detail='" . ($n = $player[$num]->nMatch) . " match" . ($n > 1 ? "es" : '') . " x " . sprintf("%.02f", $amount) . " €'";
      break;

    case ACCOUNT:
      $sum += $sumSanction;
      if (!($ac = $row["amount"]) || $ac > $sum)
        $sql .= ", amount=$sum, detail=$sum";
      else {
        $liq = intval(($sum - $ac + 4) / 5) * 5;
        $ac = $sum - $liq;
        $sql .= ", amount=$ac, detail=$sum";
      }
      break;

    case MONEY:
      $sum += $sumSanction;
      $sql .= ", amount=$sum";
      break;

    case DISTR_ACCOUNT:
      $sum += $sumSanction;
      $ac = $sum;
      if (jmysql_result(jmysql_query("select count(*) from pay_out where num = $num and pay_out.type&0x3F = ".COACH_DESC), 0)) {
        $payYear =  PAY_YEAR_COACH;
        $payKm = PAY_KM_COACH;
      } else {
        $payYear =  PAY_YEAR_PLAYER;
        $payKm = PAY_KM_PLAYER;
      }
      
      if ($sum > 0) {
        if (!$nMonth)
          $nMonth = 10;
        if ($totKm > MAX_YEAR_KM)
          $totKm = MAX_YEAR_KM;
        $ac = $yearAc + jmysql_result(jmysql_query("select sum(amount) from pay_out where num=$num and " . ($m > 6 ? "month<>$m" : "month>=1 and month<$m") . " and (type=" . DISTR_ACCOUNT . " or type=" . ACCOUNT . ") and amount>0"), 0);
        $rm = "restMonth$nMonth";
        echo "sum=$sum, max account=($payYear - paid:$ac + totKm:$totKm * " . $payKm . ") /" . ${$rm}[$m] . "=";
        $ac = ($payYear - $ac + $totKm * $payKm) / ${$rm}[$m];
        echo $ac;
        if (($totKm + $kmMonth) < MAX_YEAR_KM)
          $ac += $kmMonth * $payKm;
        else if ($totKm < MAX_YEAR_KM)
          $ac += (MAX_YEAR_KM - $totKm) * $payKm;
        echo ", +kmMonth:$kmMonth=$ac" . NL;
        if ($sum - $ac <= 50)
          $ac = $sum;
        if ($ac > PAY_MONTH)
          $ac = PAY_MONTH;
        if ($ac < 5) {
          $liq = $sum;
          $ac = 0;
        } else {
          $liq = intval(($sum - $ac + 4) / 5) * 5;
          $ac = $sum - $liq;
        }
      }
      $sql .= ", amount=$ac, detail=$sum";
  }
  if ($bCont)
    continue;
  jmysql_query($sql);
  echo "Décompte type '" . $aType[$typ] . "'' pour $rowPlayer[0] $rowPlayer[1] terminé (A payer=$sum)" . NL;
}

