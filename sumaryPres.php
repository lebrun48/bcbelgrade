<?php include "head.php" ?>
<title>Résumé Présences     
</title>  
</head>  
<body>
  <?php

  include "presences.php";
  include "sumaryPresCom.php";
  echo "<h1>Résumé présences équipe '$teamName'</h1>";
  echo "<p>Lorsqu'un \"...\" apparaît dans le tableau des présences, cela signifie qu'une raison a été renseignée pour l'absence. Pour voir cette raison il suffit de déplacer la souris sur l'élément et la raison apparaîtra après 1 seconde.</p>" . nl;
  if ($right == UPDATE)
    echo "<p>Tu peux également modifier les présences des joueurs en cliquant sur le jour dans l'entête de la colonne.</p>";
  echo "<br>";
  buildSumaryPres($right == UPDATE);
  echo "<br>";
  include "tools.php";
  createAction(120, "Retour Menu", "window.history.back()");
  ?>
