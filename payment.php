<?php
$bSession = true;
include "admin.php";
include "head.php";
include "memberData.php";
include "payOutData.php";
include "tools.php";
?>
<script type="text/javaScript">
  var win=null;
  function openWindow(loc)
  {
  if (win && !win.closed)
  win.location.replace(loc);
  else
  win=window.open(loc, "_blank");
  win.focus();
  return false;
  }

  var bAll={};
  function selectAll(name)
  {
  if (!name)
  name="_select_";
  if (!(name in bAll))
  bAll[name]=true;
  els=document.getElementsByName(name);
  for (i=0;i<els.length;i++)
  els[i].checked=bAll[name];
  bAll[name]=!bAll[name];
  el=document.getElementById(name);
  el.innerHTML=bAll[name] ? 'T':'A';
  el.title=bAll[name] ? 'Sélectionner Tous':'Sélectionner Aucun';
  }

</script>
<?php
//$bOfficial=true;
echo '<title>Paiements';
echo '</title></head><body>' . nl;

if (!($month = $request["month"]))
  $month = $now->format("n");
echo "<form autocomplete=off action=payment.php$basePath method=post><input id=submit type=submit style=display:none>Mois : <select name=month onchange=document.getElementById('submit').click()>";
for ($m = 9; $m != 8; $m++) {
  if ($m == 13)
    $m = 1;
  echo "<option value=$m" . ($month == $m ? " selected=selected>" : '>') . $months[$m] . ' ' . ($m > 8 ? $yearSeason : $yearSeason + 1) . "</option>";
}
echo "</select><h1>Résumé des paiements pour $months[$month]</h1>";
$cuts = array(500, 200, 100, 50, 20, 10, 5, 2, 1, 0.5, 0.2, 0.1);
echo "<table class=main><tr><th><span id=_select_ onclick=selectAll() title='Sélectionner Tous' class=tabLink>T</span></th><th width=250>NOM Prénom</th><th width=80px>" . ($bOfficial ? "Montant" : "Compte</th><th width=80px>Liquide</th><th width=80px>Total") . "</th>";
if ($bRoot && !$bOfficial) {
  foreach ($cuts as $k => $v)
    echo "<th style=width:40>${v}€<br><input type=number style=width:40 name=cuts_$k value='" . $request["cuts_$k"] . "'</th>";
  echo "<td style=border:0;width:100;text-align:center>";
  createAction(-1, "Recalculer", "document.getElementById('submit').click()");
  echo "</td></tr>" . nl;
}
$res = jmysql_query("select pay_out.num from pay_out,members where pay_out.month=$month and pay_out.num>0 and members.num=pay_out.num group by members.name,members.firstName");
//$res=jmysql_query("begin select pay_out.num from pay_out,members where pay_out.month=$month and pay_out.num>0 and pay_out.type>=5 and members.num=pay_out.num group by members.name,members.firstName; select pay_out.num from pay_out,members where pay_out.month=$month and pay_out.num>0 and pay_out.type<5 and pay_out.num not in (select num from pay_out where month=$month and num>0 and type>=5) and members.num=pay_out.num group by members.name,members.firstName; end");
$par = 0;
$sumMoney = 0;
$sumAccount = 0;
while ($row = jmysql_fetch_row($res)) {
  //dump($row);
  $name = jmysql_fetch_row(jmysql_query("select concat(name,' ',firstname),id from members where num=$row[0]"));
  echo "<tr class=parity$par><td><input type=checkbox name=_select_ value=$row[0]></td><td><a href=# onclick=openWindow(" . ($bOfficial ? "'payOutSumary" : "'payOutPayment") . ".php?id=" . $name[1] . "&season=" . ($yearSeason - 2000) . "&month=$month&user=root')>$name[0]</td>";
  $par = 1 - $par;
  $sum = 0;
  unset($n);
  $r = jmysql_query("select type, amount from pay_out where num=$row[0] and month=$month order by type");
  while ($pay = jmysql_fetch_row($r)) {
    //echo "name=$name[0], ";dump($pay);
    switch ($pay[0]) {
      case PLAYER_SANCTION_PERC:
      case PLAYER_SANCTION_PERC_AUTO:
      case HOME:
        break;

      case DISTR_ACCOUNT:
      case ACCOUNT:
        $n = sprintf("%0.2f", $sum - $pay[1]);
        echo "<td style=text-align:right" . ($pay[1] < 0 ? ";color:red" : '') . ">" . sprintf("%.02f €", $pay[1]) . "</td>" . (!$bOfficial ? "<td style=text-align:right>" . sprintf("%.02f €", $n) . "</td><td style=text-align:right>" . sprintf("%.02f €", $n + ($pay[1] > 0 ? $pay[1] : 0)) . "</td>" : '');
        if ($pay[1] > 0)
          $sumAccount += $pay[1];
        $sumMoney += $n;
        if ($pay[1] && !jmysql_result(jmysql_query("select count(*) from pay_out where num=$row[0] and type=" . NO_MAIL), 0))
          $list .= ",$row[0]";
        break;

      /*      case ACCOUNT:
        $n=0;
        echo '<td style="text-align:right'.($pay[1]<0 ? '; color:red' : '').'">'.sprintf("%.02f €", $pay[1]).'</td><td style="text-align:right">0.00 €</td>';
        if ($pay[1]>0)
        $sumAccount+=$pay[1];
        break; */

      case MONEY:
        $n = $pay[1];
        echo '<td style="text-align:right">0.00 €</td><td style="text-align:right">' . sprintf("%.02f €", $n) . '</td>';
        $sumMoney += $n;
        if ($pay[1])
          $list .= ",$row[0]";
        break;

      default:
        $sum += $pay[1];
        break;
    }
  }
  if (!$bRoot || $bOfficial)
    continue;
  if ((float) $n) {
    $money[$row[0]][0] = $n;
    $money[$row[0]][1] = $name[0];
  }
  $sum = 0;
  foreach ($cuts as $k => $v) {
    $nCuts[$k]->n = strtok(($n - $sum) / $v, '.');
    $nCuts[$k]->sum += $nCuts[$k]->n;
    if (strlen($request["cuts_$k"]) && ($dif = $request["cuts_$k"] - $nCuts[$k]->sum) < 0) {
      $nCuts[$k]->sum += $dif;
      $nCuts[$k]->n += $dif;
    }
    $sum += $nCuts[$k]->n * $v;
    echo "<td style=text-align:center>" . $nCuts[$k]->n . "</td>";
  }
}
echo "<tr><th class=bottom></th><th class=bottom>Total</th><th class=bottom style=\"text-align:right\">" . sprintf("%.02f €", $sumAccount) . "</th>" . (!$bOfficial ? "<th class=bottom style=\"text-align:right\">" . sprintf("%.02f €", $sumMoney) . "</th><th class=bottom style=text-align:right>" . sprintf("%.02f €</th>", $sumMoney + $sumAccount) : '');
if (!$bRoot || $bOfficial)
  exit();

createAction(-1, "Envoyer mails", "sendMails()");

foreach ($nCuts as $v)
  echo "<th class=bottom>$v->sum</th>";

echo '</tr></table></form><table id="edit" style="border:0;page-break-before:always;color:black">';
$i = 0;
foreach ($money as $k => $v) {
  if (($i % 3) == 0) {
    if ($i != 0)
      echo "</tr>";
    echo "<tr>";
  }
  if ($i < 3)
    $pad = "0px ";
  else
    $pad = "20px ";
  if (($i % 3) == 2)
    $pad .= "0px ";
  else
    $pad .= "20px ";
  if ((int) ($i / 5) == 4)
    $pad .= "0px ";
  else
    $pad .= "20px ";
  if (($i % 3) == 0)
    $pad .= "0px;";
  else
    $pad .= "20px;";
  $i++;
  echo "<td style=vertical-align:text-top;font-weight:normal;width:300;padding-left:30px><p style=text-align:right><span style=float:left>$v[1]</span><span style=text-align:right;font-size:x-small;font-style:italic>" . sprintf("%.02f €", $v[0]) . "</span></p></td>";
  $team = jmysql_result(jmysql_query("select category&0xffff from members where num=$k"), 0);
  if ($team) {
    unset($hour);
    $r = jmysql_query("select begin, duration from training where team=$team and grid=0 and field<2 order by begin");
    while ($hr = jmysql_fetch_row($r)) {
      if ($hr == $hr_sav)
        continue;
      $hour .= ($hour ? ', ' : '') . $weekDays[intval($hr[0] / 10000)] . " " . substr($hr[0], 1, 2) . ':' . substr($hr[0], 3, 2);
      $hr_sav = $hr;
    }
  }
  //echo "<small><small><small><span style=\"text-align:left\"><i>$hour</span></small></small></i></td>".nl;
}
echo "</tr></table><h2 style=width:100%;page-break-before:always>Paiements de $months[$month] " . $now->format("Y") . "</h2><table style='border-collapse:collapse;border:1px solid black'>";
foreach ($money as $k => $v)
  echo "<tr style=height:50px><td style='padding-left:10px;border:1px solid black;width:300px'>$v[1]</td><td style='border:1px solid black;width:300px'></td></tr>";
?>
<script>
  function sendMails() {
    els = document.getElementsByName("_select_");
    nums = '';
    for (i = 0; i < els.length; i++)
      if (els[i].checked)
        nums += ',' + els[i].value;
    window.open("buildMailPrest.php<?php echo "$basePath&month=$month&list=" ?>" + (nums ? nums.substr(1, nums.length - 1) : "<?php echo substr($list, 1) ?>"), "_blank");
  }
</script>




