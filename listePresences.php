<?php
include "presences.php";
include "head.php";
?>
<title>Liste présences joueurs          
</title>     
</head>     
<body><h1>Liste présences équipe       
    <?php
    echo $teamName . "</h1><p>" . $coach->prenom . ", Tu peux visualiser ou modifier les encodages des semaines pr&eacute;c&eacute;dentes pour l'équipe <b>";
    echo $teamName . "</b>";
    echo "<p>Clique sur la semaine &agrave; visualiser ou modifier: </p><ul>";
    $dt = new DateTime();
    $y = $dt->format("Y");
    if ($dt->format("m") < 8)
      $y--;
    $fin = mktime(0, 0, 0, 8, 1, $y);
    for ($i = $last; $i > $fin; $i -= 7 * 24 * 3600) {
      $dt = new DateTime('@' . $i);
      $res = jmysql_query("select count(*) from com_presences where num=$team AND date='" . $dt->format('Y-m-d') . "'");
      echo "<li><a href=presencesClub.php${basePath1}begin=$i&team=$team> Semaine du " . $dt->format("d/m/Y") . " au ";
      $j = $i + 6 * 24 * 3600;
      $dt = new DateTime('@' . $j);
      echo $dt->format("d/m/Y") . "</a>";
      if (jmysql_result($res, 0) == 0)
        echo "&nbsp; (pas d'encodages)";
      echo "</li>";
    }
    echo "</ul><br>";
    include "tools.php";
    createButton(120, "Retour Menu", "menu.php${basePath1}");
    ?> 

    <br><br></body>
    </html>