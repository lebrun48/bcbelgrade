<script>
  function reload(obj)
  {
    if ((n = location.href.search('&all=')) != -1)
      loc = location.href.substr(0, n + 5) + (obj.checked ? '1' : '0');
    else
      loc = location.href + "&all=" + (obj.checked ? '1' : '0');
    location.replace(loc);
  }

  function putlist(v)
  {
    if (document.getElementById('list').value.search(',' + v + ',') == -1)
      document.getElementById('list').value += ',' + v + ',';
  }

  function reset_f(num) {
    $("#line" + num).children("td:gt(1)").css("background", "lightgray").children(":checked").attr("checked", false);
    $("[name^='_val_choice" + num + "']").val('').attr("checked", false);
    if ($("#reset").val().search("," + num + ",") == -1)
      document.getElementById('reset').value += ',' + num + ',';
  }
</script>

<?php

function sendMails($msg, $tup = null)
{
  global $query, $to, $subject, $headerLine, $bodyLine, $lastLine, $hRef, $from, $signing, $noBcc, $bNoComment, $request, $member;
  //send mail
  $ri = ($t = $request["group"]) ? jmysql_result(jmysql_query("select ri from activities where groupNm='$t' limit 1"), 0) : $request["activity"];
  if (!$tup)
    $tup = jmysql_fetch_row(jmysql_query("select title,groupNm,invited from activities where ri=$ri"));
  $inv = unserialize($tup[2]);
  foreach ($inv->nums as $k => $v)
    $l .= ",$k";
  $query = "select name,id,firstName,ifnull(emailF,emailP) as emailF from members where num in (" . substr($l, 1) . ") and (emailF is not null or emailP is not null)";
  $to = '§name§ §firstName§ <§emailF§>';
  $subject = $tup[1] ? $tup[1] : $tup[0];
  $headerLine = 'Bonjour §firstName§';
  $sender = jmysql_fetch_row(jmysql_query("select name,firstName,ifnull(emailF,emailP) as emailF from members where num=$member->num"));
  $bodyLine[0] = eval("return \"$msg\";");
  $lastLine = "<p>Bonne journée.</p>";
  $hRef = "http://" . $_SERVER["HTTP_HOST"] . "/members/" . basename($_SERVER["SCRIPT_NAME"]) . "?id=§id§&activity=$ri";
  $from = "noreply@skynet.be";
  $signing = "Message automatique du gestionnaire d'activité\nNe pas répondre!";
  $noBcc = true;
  $bNoComment = true;
  $request["sure"] = 'y';
  //$bTest=true;
  //$bDumpMail=true;
  include "common/genMailCommon.php";
  include "common/sendmails.php";
}

//------------------------------------------------------------------------------------------------------
switch ($request["action"]) {
  case "save":
    include "tools.php";
    //dump($request);
    $numAct = $request["activity"];
    $tup = jmysql_fetch_row(jmysql_query("select title,groupNm,invited,activities,owner,updInv from activities where ri=$numAct"));
    $acts = unserialize($tup[3]);
    $inv = unserialize($tup[2]);
    if (!($num = strtok($request["list"], ',')))
      $num = $member->num;
    while ($num) {
      foreach ($acts as $v2)
        $inv->nums[$num]->res[$v2->num] = strlen($t = $request["_val_choice${num}_$v2->num"]) ? $t : 0;
      $num = strtok(',');
    }
    $num = strtok($request["reset"], ',');
    while ($num) {
      unset($inv->nums[$num]->res);
      $num = strtok(',');
    }

    //dump($inv);
    unset($t);
    if (!$request["all"] && strpos($tup[5], ",$member->num,") === false) {
      $tup[5] .= (!strlen($tup[5]) ? ',' : '') . "$member->num,";
      if (substr_count($tup[5], ',') >= 5) {
        $msg = "<p>" . jmysql_result(jmysql_query("select group_concat(concat(firstName,' ',name) separator ', ') from members where num in (" . substr($tup[5], 1, -1) . ")"), 0) . " ont encodé ou modifié leurs disponibilités pour l'activité '$tup[0]'.<br> Tu peux consulter leur encodage en cliquant <a href=§hRef§>ici</a>.</p>";
        //sendMails($msg,$tup);
        $t = ",updInv=null";
      } else
        $t = ",updInv='$tup[5]'";
    }
    //dump($tup[5]);
    //alert("Tes disponibilités sont enregistrées!");
    jmysql_query("update activities set invited='" . serialize($inv) . "'$t where ri=$numAct");
    break;

  case "suppressComment":
    jmysql_query("delete from shComments where `key`='" . $request["key"] . "'");
    echo buildComments($request["key"]);
    exit();

  case "submitComment":
    //dump($request);
    jmysql_query("insert into shComments set comment='" . addslashes($request["comment"]) . "', member=$member->num, date='" . $now->format("Y-m-d H:i:s") . "', `key`='" . $request["key"] . "'");
    echo buildComments($request["key"]);
    $msg = "<p>\$sender[1] \$sender[0] a posté le commentaire sur l'activité '\$subject':<div style=font-family:courier;font-size:small>" . $request["comment"] . "</div>Pour consulter ou modifier tes disponibilités sur l'activité, clique <a href=§hRef§>ici</a>.</p>";
    //sendMails($msg);
    exit();
}

function buildActivity($numAct)
{
  global $request, $weekDays, $months, $member, $basePath, $bRoot;
  $act = jmysql_fetch_assoc(jmysql_query("select * from activities where ri=$numAct"));
  $dt = new DateTime($act["date"]);
  echo "<h2 style=width:100%;margin-top:0><img src=" . LOGO_LITTLE_CLUB . " style=vertical-align:middle;margin-right:15px>" . $act["title"] . " le " . $weekDays[$dt->format("w")] . $dt->format(" d ") . $months[$dt->format("n")] . $dt->format(" Y") . (($t = $dt->format("H:i")) != "00:00" ? " à $t" : '') . "<img src=" . LOGO_LITTLE_CLUB . " style=vertical-align:middle;margin-left:15px></h2>";

  echo "<p>" . str_replace("\x0d\x0a", "<br>", $act["descr"]) . "</p>";

  $acts = unserialize($act["activities"]);
  foreach ($acts as $k => $v) {
    $acts[$k]->categories = $v = unserialize($v->categories);
    if (count($v) == 1 && $v[0] == '')
      unset($acts[$k]->categories);
  }
  $inv = unserialize($act["invited"]);
  $tw = sizeof($acts) < 8 ? 100 - (8 - sizeof($acts)) * 10 : 100;
  $w = intval((100 - intval(17 * 100 / $tw)) / sizeof($acts));
  $w = "width:$w%;max-width:$w%";
  $bAll = $request["all"];
  echo "<form method=post action=" . $_SERVER["SCRIPT_NAME"] . "$basePath" . (($t = $act["groupNm"]) ? "&group=" . urlencode($t) : "&activity=$numAct") . "><input type=submit style=display:none id=submit$numAct><input type=hidden name=action value=save><input type=hidden name=activity value=$numAct><input type=hidden name=list id=list><input type=hidden name=reset id=reset>";
  echo "<table class=activities style=width:$tw%><tr><td colspan=" . (sizeof($acts) + 1 + $bAll) . " style=text-align:left;background:darkgreen;color:white>" . $dt->format("d ") . $months[$dt->format("n")] . $dt->format(" Y") . "<br>" . $weekDays[$dt->format("w")] . "</td></tr>";
  if ($bRoot)
    echo "<tr><td><input name=all value=1 onclick=reload(this) type=checkbox" . ($bAll ? " checked" : '') . ">Pouvoir modifier tous</td></tr>";

  echo "<tr><td style=width:" . intval(17 * 100 / $tw) . "%;height:40px></td>" . ($bAll ? "<td></td>" : '');
  //dump($acts);
  foreach ($acts as $v)
    echo "<td style=$w;text-align:center>$v->name" . ($v->max != -1 ? "<br><span style=color:blue>Max:$v->max</span>" : '') . "</td>";

  echo "</tr>";
  $b = $act["viewActiveOnly"];
  foreach ($inv->nums as $k => $v)
    if (!$b || $v->res && array_sum($v->res) || $k == $member->num)
      $l .= ",$k";
  $l = substr($l, 1);
  $r = jmysql_query("select num,concat(firstName,' ',name),sex,category,category2,category3 from members where num in ($l) order by name,firstName");
  while ($tup = jmysql_fetch_row($r)) {
    $v = $inv->nums[$tup[0]];
    foreach ($acts as $v1)
      $v1->curNb += $v->res[$v1->num];
  }

  jmysql_data_seek($r, 0);
  while ($tup = jmysql_fetch_row($r)) {
    $k = $tup[0];
    $v = $inv->nums[$k];
    //dump($v);
    $img = file_exists(BASE_PATH . ($t = "/members/pictures/N_$k.jpg")) ? $t : ($tup[2] == 'M' ? "man.png" : "woman.png");
    echo "<tr id=line$k><td><img style=width:25px;margin-right:5px;vertical-align:middle src=$img>$tup[1]</td>";
    if ($bAll)
      echo "<td><img src=undo.png width=15px title=Reset onclick=reset_f($k)></td>";
    foreach ($acts as $v1) {
      echo "<td style=text-align:center;background:" . (!$v->res ? "lightgray>" : ($v->res[$v1->num] > 0 ? "#CCFFCC>" : "#FFB2B2>"));
      $bDisabled = $v1->max > 0 && $v1->curNb >= $v1->max && !$v->res[$v1->num];
      if ($k == $member->num || $bAll) {
        $bAllow = true;
        if ($v1->categories) {
          $bAllow = false;
          foreach ($v1->categories as $cat) {
            for ($i = 3; $i <= 5; $i++)
              if (isset($tup[$i])) {
                if (($tup[$i] & 0xFF0000) == $cat * CAT_COACH) {
                  $bAllow = true;
                  break;
                }
              }
            if ($bAllow)
              break;
          }
        }
        if (!$bAllow)
          echo "<span style=color:blue>Réservé</span>";
        else if ($v1->max > 0 && $v1->curNb >= $v1->max && !$v->res[$v1->num])
          echo "<span style=color:blue>Max atteint</span>";
        else
          echo "<input value=" . ($v1->number ? "'" . $v->res[$v1->num] . "'" : "1") . " name=_val_choice${k}_$v1->num type=" . ($v1->number ? "text maxlength=2 size=2 style=width:30px" : "checkbox") . " onchange=putlist($k)" . ($v->res[$v1->num] ? " checked" : '') . ">";
      } else
        echo ($v->res ? ($v1->number ? $v->res[$v1->num] : (!$v->res[$v1->num] ? '' : "<img src=checked.png style=width:20px;height:20px>")) : '');
      echo "</td>";
    }
    echo "</tr>";
  }
  echo "</tr></table></form><br><br>";
  include_once "tools.php";
  createAction(200, "Enregistrer mes disponibilités", "document.getElementById('submit$numAct').click()");
}

function shareMsg($key)
{
  ?>
  <script>
    function submitComment(key)
    {
      line = $('#shComment').val().trim();
      $('#shComment').val('');
      line = line.replace(/\n/g, '<br>');
      if (!line.length)
        return;
      $.ajax({data: "comment=" + encodeURIComponent(line) + "&action=submitComment&key=" + key, success: function (result) {
          //alert(result);
          $("#comments").html(result);
        }
      });
      $('#shComment').val('');
    }

    function suppressComment(key)
    {
      if (confirm("Es-tu sûr de vouloir supprimer tous les commentaires?")) {
        $.ajax({data: "action=suppressComment&key=" + key, success: function (result) {
            //alert(result);
            $("#comments").html(result);
          }
        });
      }
      $('#shComment').val('');
    }
  </script>
  <?php
  global $member, $bRoot;
  echo "<br>$member->firstName, tu peux partager un commentaire ci-dessous<br>";
  echo "<textarea style=width:900px;margin-bottom:10px rows=3 id=shComment></textarea><br>";
  include_once "tools.php";
  createAction(-1, "Soumettre le commentaire", "submitComment('$key')");
  if ($bRoot)
    createAction(-1, "Supprimer tous les commentaires", "suppressComment('$key')");

  echo "<br><div style=';margin-top:10px;width:900px;height:300px;overflow-y:auto;border:1px solid black;' id=comments>" . buildComments($key) . "</div>";
}

function buildComments($key)
{
  global $now;
  $line = "<table width=100%>";
  $r = jmysql_query("select * from shComments where `key`='$key' order by date desc");
  while ($tup = jmysql_fetch_assoc($r)) {
    $img = file_exists(BASE_PATH . ($t = "/members/pictures/N_" . $tup["member"] . ".jpg")) ? $t : "man.png";
    $dt = new dateTime($tup["date"]);
    $line .= "<tr style=font-size:x-small;height:40px;vertical-align:bottom;color:darkgray><td><img style=width:20px;margin-right:5px;vertical-align:middle src=$img>" . jmysql_result(jmysql_query("select concat(firstName, ' ', name) from members where num=" . $tup["member"]), 0) . "</td><td text-align:right>" . ($dt->format("Y-m-d") == $now->format("Y-m-d") ? $dt->format("H:i") : $dt->format("d/m/Y H:i")) . "</td></tr>";
    $line .= "<tr><td colspan=2 style=padding-left:10px;font-size:small>" . $tup["comment"] . "</td></tr>";
  }
  return $line . "</table>";
}
