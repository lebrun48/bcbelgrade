<?php

$bOnlyRoot = true;
include "admin.php";


$arHome = array(array("Chrono", 1, 1),
    array("24/14 sec", 1, 1),
    array("Délégué aux arbitres", 1, 1),
    array("Délégué aux équipes", 1, 0),
    array("Frotteur(s)", -1, 0),
    array("Entrées", 1, 1),
    array("Réserve", -1, 0),
    array("Présentation équipe", 1, 0)
);
$arAway = array(array("Feuille", 1, 1),
    array("Délégué aux arbitres", 1, 1)
);

foreach ($arHome as $k => $v) {
  $actsHome[$k]->num = $k;
  $actsHome[$k]->name = $v[0];
  $actsHome[$k]->max = $v[1];
  $actsHome[$k]->unicity = $v[2];
}

foreach ($arAway as $k => $v) {
  $actsAway[$k]->num = $k;
  $actsAway[$k]->name = $v[0];
  $actsAway[$k]->max = $v[1];
  $actsAway[$k]->unicity = $v[2];
}


// $ar=array(6,63,63,495,162,164,217,223,224,283,296,315); //D3
// $team=100 //D3
// $type="type=11 or type=12 or type=4"; //D3
// $group="Suivi D3";

$ar = array(209, 2, 11, 200, 164, 296); //P1
$team = 252; //P1
$type = "type=0 or type=2 or type=4 or type=3"; //P1
$group = "Suivi P1";


$inv->max = 2;
foreach ($ar as $v)
  $inv->nums[$v]->pres = true;

$inv = serialize($inv);
$actsHome = serialize($actsHome);
$actsAway = serialize($actsAway);

// Home-------------------------------------------
$r = jmysql_query("select date, away from calendar where team=$team and ($type) and home like '%" . KEY_CLUB . "%' and date>='" . $now->format('Y-m-d') . "' order by date");
while ($tup = jmysql_fetch_row($r)) {
  $dt = new DateTime($tup[0]);
  jmysql_query("insert into activities set owner=217, title='$tup[1]', date='$tup[0]', status=0,activities='$actsHome',invited='$inv', groupNm='$group'", 1);
}

//Away---------------------------------------------
$r = jmysql_query("select date, home from calendar where team=$team and ($type) and away like '%" . KEY_CLUB . "%' and date>='" . $now->format('Y-m-d') . "' order by date");
while ($tup = jmysql_fetch_row($r)) {
  $dt = new DateTime($tup[0]);
  jmysql_query("insert into activities set owner=217, title='$tup[1]', date='$tup[0]', status=0,activities='$actsAway',invited='$inv', groupNm='$group'", 1);
}
  
  
 