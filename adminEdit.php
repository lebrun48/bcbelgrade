<?php

include "admin.php";
include "common/tableTools.php";
//dump($request);
//rights
//0x01 = Modify category for members
//0x04 = View All attendances
//0x08 = View cotisations
//0x10 = Manifestations
//0x20 = Biller result
//0x40 = Report charting
//0x80 = Export member allowed.
selectAction();
if ($action == "edit") {
  include "head.php";
  $col = array("num"        => array(0, "Nom", "width:200", null, "concat(firstName, ' ', name)"),
      "rights"     => array(5, "Droits"),
      "events"     => array(100, "Manifestations"),
      "categories" => array(80, "Catégories"),
      "attributes" => array(200, "Attributs autorisés"),
      "visible"    => array(200, "Attributs cachés"),
      "conditions" => array(200, "Conditions"),
      "dftWhere"   => array(200, "Limitations"),
      "sort"       => array(10, "Tri par"),
      "login"      => array(20, "Utilisateur"),
      "pwd"        => array(20, "Mot de passe")
  );

  include "common/tableRowEdit.php";

  class AdminEdit extends TableRowEdit
  {

    function applyValue(&$key, &$format, &$row)
    {
      switch ($key) {
        case "num":
          echo '<select name="_val_num">';
          $res = jmysql_query("select num,concat(name,' ',firstName) from members where type<>0 and category<>0 and category<=".CAT_PLAYER." order by name");
          while ($r = jmysql_fetch_row($res))
            echo "<option value=$r[0]" . ($r[0] == $row[$key] ? " selected=selected" : '') . ">$r[1]</option>";
          echo '</select>' . nl;
          break;

        default:
          TableRowEdit::ApplyValue($key, $format, $row);
      }
    }

  }

  $obj = new AdminEdit();
  $obj->title = "Edition d'adminstrateur";
  $obj->colDef = &$col;
  $obj->editRow();
  exit();
}
include "common/tableEdit.php";

$col = array("name"       => array(1, "Nom", "width:200", null, "concat(name, ' ', firstName)"),
    "rights"     => array(0, "Droits", "width:50", "text-align:right"),
    "events"     => array(0, "Manifestations", "width:50"),
    "categories" => array(0, "Catégories", "max-width:100"),
    "attributes" => array(0, "Attributs autorisés", "max-width:200", "max-width:200;overflow-x:hidden"),
    "visible"    => array(0, "Attributs cachés", "max-width:200", "max-width:200;overflow-x:hidden"),
    "conditions" => array(0, "Conditions", "max-width:70", "max-width:70;overflow-x:hidden"),
    "dftWhere"   => array(0, "Limitation", "max-width:70", "max-width:70;overflow-x:hidden"),
    "sort"       => array(0, "Tri par", "width:50"),
);

class Admin extends TableEdit
{

//---------------------------------------------------------------------------------------------------
  public function buildKey(&$row)
  {
    return "admin where num=" . $row["num"];
  }

//---------------------------------------------------------------------------------------------------
  public function applyDisplay($key, $ft, $row, $idxLine)
  {
    if ($key == "rights")
      $dispVal = decbin($row[$key]);
    return TableEdit::applyDisplay($key, $ft, $row, $idxLine, $dispVal);
  }

}

include "head.php";
//table itself
echo '<title>admin';
echo '</title></head><body>';

$obj = new Admin;
$obj->colDef = &$col;
$obj->addCol = "admin.num";
$obj->tableName = "admin,members";
$obj->tableId = "members";
$obj->defaultWhere = "members.num=admin.num and";
$obj->bHeader = true;
$obj->build();
?>
