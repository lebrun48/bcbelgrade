<?php
include "common/common.php";
include "head.php";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//FR">  
<head>            
  <meta content="text/html; charset=UTF-8" http-equiv="content-type">
  <link rel="shortcut icon" href="<?php echo LOGO_LITTLE_CLUB ?>" type="image/x-icon" />
</head><title>Equipes</title>
<?php
echo '<title>Equipe</title></head>' . nl;

if (!($team = $_GET["team"])) {
  $r = getExistingCategories("category&" . CAT_PLAYER . " and type<>0 and", false, false, true);
  echo "</h2><p>Choisir l'équipe</p>";
  echo "<select onchange=\"location.assign('team.php?team='+(this.options[this.selectedIndex].value))\">";
  echo "<option></option>";
  foreach ($r as $k => $v)
    echo "<option value=$k>$v</option>";
  exit();
}

echo "<body onload=window.print()>";
$tup = getCategory($team);
$name = $tup["name"];
$c = unserialize($tup["teamSite"]);
$set = ($team & 0xFFFF) % 10;
$c = $c[$set];
echo "<h2><img src=" . LOGO_LITTLE_CLUB . " style=padding-right:20px;vertical-align:middle>Composition équipe '$name'</h2>";
echo "<table border=1px style='margin-left:auto;margin-right:auto;border:0px;font-family:Comic Sans MS;width:19cm'>";
$t = explode(" ", $c->div);
if (sizeof($t) > 2)
  foreach ($t as $v)
    $div .= strlen($v) > 4 ? substr($v, 0, 3) . '. ' : "$v ";
else
  $div = $c->div;
echo "<tr><td colspan=4>" . NAME_CLUB . " <b>" . ($set ? chr(ord('A') + $set - 1) : '') . "</b></td><td>MAT " . MATRICULE_CLUB . "</td></tr>" . nl;
echo "<tr><th colspan=5 style=border:0px>Série: $div</th></tr>" . nl;
echo "<tr><td colspan=5 style=border:0px>&nbsp;</td></tr>";
echo "<tr><th style=border:0px;width:50px>Photo<th>Lic</th><th>Nom</th><th>Prénom</th><th>N°</th></tr>" . nl;
$r = jmysql_query("select num,date_format(birth,'%y') as year, name, firstName, numEquip as numEquip1, numEquip2, numEquip3, category as category1, category2, category3 from members where type<>0 and (category=$team or category2=$team or category3=$team)");
while ($row = jmysql_fetch_assoc($r)) {
  for ($i = 1; $i <= 3; $i++)
    if ($row["category$i"] == $team) {
      if ($row["numEquip"] = $row["numEquip$i"])
        $bExist = true;
      break;
    }
  $sort[] = $row;
}
usort($sort, "cmp");
$i = 4;
foreach ($sort as $v) {
  while ($bExist && ($i < $v["numEquip"] || !$v["numEquip"] && $i <= 17)) {
    echo "<tr><td style=border:0px></td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>$i</td></tr>" . nl;
    $i++;
  }
  echo "<tr><td style=border:0px><img src=/members/pictures/N_" . $v["num"] . ".jpg width=30 height=39></td><td>" . $v["year"] . "</td><td>" . $v["name"] . "</td><td>" . $v["firstName"] . "</td><td>" . $v["numEquip"] . "&nbsp;</td></tr>" . nl;
  if ($v["numEquip"])
    $i = $v["numEquip"] + 1;
}

echo "<tr><td style=border:0px></td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>" . nl;
echo "<tr><td colspan=5 style=;border:0px>&nbsp;</td></tr>";
$t = CAT_COACH | ($team & 0xffff);
$coach = jmysql_fetch_row(jmysql_query("select concat(members.name,' ',members.firstName),licence,num from members where category=$t or category2=$t or category3=$t"));
echo "<tr><td style=border:0px><img src=/members/pictures/N_" . $coach[2] . ".jpg width=30 height=39></td><td colspan=4 style=font-size:16;font-weight:bold>Coach:  $coach[0] $coach[1]</td></tr>" . nl;
$t = CAT_ASSIST | ($team & 0xffff);
if ($coach = jmysql_fetch_row(jmysql_query("select concat(members.name,' ',members.firstName),licence,num from members where category=$t or category2=$t or category3=$t")))
  echo "<tr><td style=border:0px><img src=/members/pictures/N_" . $coach[2] . ".jpg width=30 height=39></td><td colspan=4 style=font-size:16;font-weight:bold>Assistant:  $coach[0] $coach[1]</td></tr>" . nl;

function cmp(&$a, &$b)
{
  if (!$a["numEquip"]) {
    if (!$b["numEquip"])
      return strcmp($a["name"], $b["name"]);
    return 1;
  }
  if (!$b["numEquip"])
    return -1;
  return $a["numEquip"] - $b["numEquip"];
}
