<?php

include "admin.php";
include "common/tableTools.php";
//$seeRequest=1;

if ($action == "edit") {
  include "head.php";
  include "common/tableRowEdit.php";
  $col = array("num"       => array(0, "Numéro"),
      "descr"     => array(100, "Description"),
      "single"    => array(5, "Unique", INTEGER_ONLY),
      "first"     => array(5, "Premier", INTEGER_ONLY),
      "second"    => array(5, "Second", INTEGER_ONLY),
      "third"     => array(5, "Troisième", INTEGER_ONLY),
      "noCaution" => array(0, "Pas de caution"),
  );

  class PaieCode extends TableRowEdit
  {

    function applyValue(&$att, &$format, &$row)
    {
      switch ($att) {
        case "num":
          echo "<span style=color:blue;font-weight:bold>$row[$att]</span>";
          return;

        case "noCaution":
          echo "<input type=checkbox name=_val_$att value=1" . ($row[$att] ? " checked>" : '>');
          return;

        default:
          TableRowEdit::applyValue($att, $format, $row);
          return;
      }
    }

  }

  $obj = new PaieCode();
  $obj->title = "Edition de code paie";
  $obj->colDef = &$col;
  $obj->editRow();
  exit();
}
SelectAction();

//-----------------------------------------------------------------------------------------------------------
$col = array("num"       => array(0, "Numéro", '', "text-align:right"),
    "descr"     => array(0, "Description"),
    "single"    => array(0, "Unique", '', "text-align:right"),
    "first"     => array(0, "Premier", '', "text-align:right"),
    "second"    => array(0, "Second", '', "text-align:right"),
    "third"     => array(0, "Troisième", '', "text-align:right"),
    "noCaution" => array(0, "Pas de caution", '', "text-align:center"),
);

include "common/tableEdit.php";
include "head.php";
//table itself
echo '<title>com';
echo '</title></head><body>';

class PaieCode extends TableEdit
{

//--------------------------------------------------------------------------------------------------
  public function buildKey(&$row)
  {
    return "code_paie where num=" . $row["num"];
  }

//--------------------------------------------------------------------------------------------------
  public function applyDisplay(&$key, &$val, &$row, $idxLine)
  {
    switch ($key) {
      case "single": case "first": case "second": case "third":
        return TableEdit::applyDisplay($key, $val, $row, $idxLine, number_format($row[$key], 2, ',', '.') . "€");

      case "noCaution":
        if ($row[$key])
          $dispVal = "<b>X</B>";
        break;
    }
    return TableEdit::applyDisplay($key, $val, $row, $idxLine, $dispVal);
  }

}

$obj = new PaieCode;
$obj->colDef = &$col;
$obj->tableId = "Code paie";
$obj->tableName = "code_paie";
$obj->bHeader = true;
$obj->bInsertNew = true;

$obj->build();
?>
