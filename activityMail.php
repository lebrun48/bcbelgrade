<?php
include "common/common.php";

//------------------------------------------------------------------------------------------------------
function checkRights()
{
  
}

$genMail = "out/activityMail";

//------------------------------------------------------------------------------------------------------
//Only to build mail in ajax requests
function specific($key, $row)
{
  global $now, $request, $gConfig, $genMail;
  switch ($key) {
    case "activityNb": return $request["_fwd_activity"];
    case "from":
      return "no_reply@newbcbelgrade.be";

    case "outFile": return "§Open:${genMail}§";
  }
  return null;
}

//------------------------------------------------------------------------------------------------------
$requestedParams = array("activity" => $request["_fwd_activity"]);
$req = "select name,firstName,emailp as email,id from members where num";
//Ajax request
switch ($action = $request["action"]) {
  case "getMail":
    //get single mail name
    $r = jmysql_query("select concat(name,' ',firstName),emailp as email from members where num=" . $request["num"]);
    //Get single mail data
    $query = "$req=" . $request["num"];
    break;

  case "sendMail":
    $query = "$req in (" . $request["nums"] . ")";
    $to = "§name§ §firstName§ <§email§>";
    //$bTest = true;
    //$bSendResult=true;
    $bResult = true;
    break;

  case "viewMail":
    checkRights();
    break;
}

//------------------------------------------------------------------------------------------------------
//other Ajax request (do not change)
if ($action)
  include "common/buildMailAjax.php";

$rights = $_SESSION["user"]["rights"];
//Mailing variables
?>
<script>

  function getVariables()
  {
    return [
      ['Lien activité', '<a target=_blank href=<?php echo URL_CLUB ?>/members/activityOp.php?id=§id§&activity=§activityNb§>lien validation</a>'],
    ];
  }
</script>
<?php
//------------------------------------------------------------------------------------------------------
//main
//$from must contain the from
$tup = jmysql_fetch_row(jmysql_query("select invited from activities where ri=" . $request["_fwd_activity"]));
$from = "no_reply@newbcbelgrade.be";

//$nums must contain the list destinations ref
$inv = unserialize($tup[0]);
foreach ($inv->nums as $k => $v)
  $nums .= ",$k";
$nums = jmysql_result(jmysql_query("select group_concat(num) from members where num in (" . substr($nums, 1) . ") and emailp is not null"), 0);
//$res must contain the request to all dest name(s)
$res = jmysql_query("select concat(name,' ',firstName) from members where num in ($nums)");

//------------------------------------------------------------------------------------------------------
//always at the end  (do not change)
include "common/buildMail.php";
