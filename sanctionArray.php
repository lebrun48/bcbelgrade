<?php

$sanctionArray = array(1 =>
    array("(-50%) ", "</div>Déclaration pouvant nuire à limage ou au bon renom du NEW BC Belgrade, en général, du staff technique, médical ainsi quaux dirigeants et sponsors en particulier.</li>"),
    array("(-50%) ", "</div>Participation aux soupers du club.</li>"),
    array("(-5%) ", "</div>Présence au <u>minimum 1h00</u> avant le début de la rencontre.</li>"),
    array("(-5%) ", "</div>Pour le briefing davant match, les joueurs doivent être prêts (soins, tape, lacets noués, toilette, kiné..). Prendre ses dispositions.</li>"),
    array("(-5%) ", "</div>Obligation est faite tant en match quà léchauffement, de porter léquipement complet et exclusif du club.</li>"),
    array("(-5%) ", "</div>Tous les joueurs regagnent directement le vestiaire à la mi-temps ainsi quà la fin du match. Ils y attendront les éventuelles remarques du Coach avant denlever leur équipement et de prendre leur douche.</li>"),
    array("(-50%) ", "</div>Tous les joueurs repasseront à la cafétéria du club après les <u>matches à domicile</u> et sy attarderont au moins une demi-heure avec les supporters.</li>"),
    array("(-15%) ", "</div>Aucun conflit ne peut opposer des joueurs.</li>"),
    array("(-15%) ", "</div>Aucun manque de respect ne sera toléré.</li>"),
    array("(-15%) ", "</div>Aucun signe de mécontentement ne pourra être perçu lors dun remplacement.</li>"),
    array("(-15%) ", "</div>Obligation de participer au cri de léquipe.</li>"),
    array("(-20%) ", "</div>Interdiction de quitter le banc avant la fin de la rencontre.</li>"),
    array("(-10%) ", "</div>Toute faute technique dun joueur présent sur le banc ou sur le terrain sera sanctionnée sauf décidée ou justifiée par le Coach fera exception à cette amende</li>"),
    array("(-100%) ", "</div>Un joueur blessé doit être présent à tous les matches et entraînements <u>(Sauf dérogation accordée par le Coach)</u>.</li>"),
    array("(-100%) ", "</div>Toute absence au match <u>même justifiée</u>  sauf cas de force majeure à apprécier par le comité et le coach -sera sanctionnée.</li>"),
    array("(-200%) ", "</div>Une absence au match injustifiée ou inacceptée sera sanctionnée</li>"),
    array("(-5%) ", "</div>Présence en tenue sur le terrain <u>5 minutes avant le début de la séance</u>.</li>"),
    array("(-5%) ", "</div>La politesse élémentaire prévoit en cas de retard, prévisible ou non, dinformer le Coach sur son GSM   0495/799.820 ; tout retard non accepté par le coach entraînera une amende.</li>"),
    array("(-100%) ", "</div>Toute absence entraînera une amende.</li>"),
    array("(-5%) ", "</div>Manque de participation à lentraînement.</li>"),
    array("(-1%) ", "</div>Les GSM sont éteints sauf dérogation du coach.</li>"),
    array("(-100%) ", "</div>Un joueur exclu ou quittant un entraînement, sera sanctionné dune absence.</li>")
);
?>