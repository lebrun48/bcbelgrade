<?php

include "common/Engine.php";
include "cotiConf.php";

//$seeRequest=1;

class DetailPay extends Engine
{

//--------------------------------------------------------------------------------
  function buildLines()
  {
    global $yearSeason;
    $this->lines = null;
    $i = 1;
//Caution
//bit 0x01 Pay�e
//bit �x02 Rembours�e
//bit 0x04 Pay�e sur versement
//bit 0x08 
//bit 0x10 Pas de caution
//bit 0x20 Caution obligatoire
//status
//0=>"Rien pay�" 
//1=>"Pay� 1er" 
//2=>"Pay� 2i�me"
//3=>"Pay� 3i�me"
//4=>"Pay� totalit�"
//5=>"Pay� partie"
//9=>"Factur�"
//10=>"Nouveau"
//11=>"En attente"
//0x10=>"Only single payment;
//0x20=>"Recommand�"
//0x40=>"Count Status";
//0x80=>"Keep single price";
//descr payIn
//0=>"R�duction"
//1=>"Suppl�ment"
//2=>"Ch�ques"
//3=>"Versement"
//4=>"Transfert"
//5=>"Cotisation"
//6=>"Remboursement");

    if (!($code = $this->row["code_cat"])) {
      $cat = getCategory($this->row["category"]);
      $code = $cat["code"];
    }
    if (!$r = jmysql_query("select descr,single, first, second, third,noCaution from code_paie where num=$code"))
      echo "Code paie invalide pour " . $this->row["name"] . " " . $this->row["firstName"] . NL;
    else {
      $this->pay = jmysql_fetch_row($r);
      $this->pay[0] = utf8_decode($this->pay[0]);
    }
    //Caution
    if (!$this->pay[5] && !($this->row["caution"] & 0x10) && !$this->row["previous"] || ($this->row["caution"] & 0x20)) {
      $this->lines[$i]["single"] = CAUTION_VALUE;
      if ($this->row["status"] != 4)
        $this->lines[$i]["first"] = CAUTION_VALUE;
      $this->lines[$i++]["descr"] = "Caution" . ($this->row["next"] ? " famille" : "") . " pour �quipement";
      if ($this->row["caution"] & 0x01) {
        $this->lines[$i]["single"] = -CAUTION_PAID;
        if ($this->row["status"] != 4)
          $this->lines[$i]["first"] = -CAUTION_PAID;
        $this->lines[$i++]["descr"] = "En ordre de caution";
      }
    }

    //Bon activit�s extra-sportives
    if ($yearSeason >= 2016 && !$this->row["previous"]) {
      $this->pay[1] += VOUCHER;
      $this->pay[2] += VOUCHER;
    }
    //Cotisation
    $this->lines[$i]["descr"] = "Cotisation " . $this->pay[0];
    global $payDate, $now;
    if (($bAll = !$this->bReduc && $this->row["status"] != 4) && $now > $payDate[0])
      $this->lines[$i]["single"] = $this->pay[2] + $this->pay[3] + $this->pay[4];
    else
      $this->lines[$i]["single"] = $this->pay[1];
    if ($bAll) {
      $this->lines[$i]["first"] = $this->pay[2];
      $this->lines[$i]["second"] = $this->pay[3];
      $this->lines[$i]["third"] = $this->pay[4];
    }
    $i++;

    //R�duction famille
    if ($this->row["previous"]) {
      //dump($this->n);
      $this->lines[$i]["descr"] = "R�duction " . ($this->n + 1) . "<sup>i�me</sup> inscrit.";
      if ($bAll && $now > $payDate[0])
        $this->lines[$i]["single"] = -($this->pay[2] + $this->pay[3] + $this->pay[4]) * 0.25 * min($this->n, 2);
      else
        $this->lines[$i]["single"] = -$this->pay[1] * 0.25 * min($this->n, 2);
      if ($this->row["status"] != 4) {
        $this->lines[$i]["first"] = -$this->pay[2] * 0.25 * min($this->n, 2);
        $this->lines[$i]["second"] = -$this->pay[3] * 0.25 * min($this->n, 2);
        $this->lines[$i]["third"] = -$this->pay[4] * 0.25 * min($this->n, 2);
      }
      $i++;
    }

    //Entr�es dans pay_in � rajouter � cotisation
    $first = jmysql_result(jmysql_query("select sum(amount) from pay_in where num=" . $this->row["num"] . " and amount>0 and descr<>1"), 0);
    foreach ($this->lines as $v) {
      $first += $v["first"];
      $second += $v["second"];
    }
    if ($this->bCountStatus)
      switch ($this->row["status"]) {
        case 2:
          $t = 0;
          foreach ($this->lines as $v)
            $t += $v["second"];
          $this->lines[$i]["second"] = -$t;
          $this->lines[$i]["single"] -= $t;
          $second -= $t;
        case 1:
          $t = 0;
          foreach ($this->lines as $v)
            $t += $v["first"];
          $this->lines[$i]["first"] = -$t;
          $this->lines[$i]["descr"] = "Versement(s) effectu�(s)";
          $this->lines[$i]["single"] -= $t;
          $first -= $t;
          if ($this->lines[$i]["single"])
            $i++;
          else
            unset($this->lines[$i]);
          break;
      }
    if ($r = jmysql_query("select amount,descr,detail from pay_in where num=" . $this->row["num"] . " and descr<>3 and descr<>1 order by ri asc"))
      $this->distributePayment($r, $i, $first, $second);

    //suppl�ment � r�partir sur 3
    if ($r = jmysql_query("select amount,detail from pay_in where num=" . $this->row["num"] . " and descr=1 order by ri asc"))
      while ($tup = jmysql_fetch_row($r)) {
        $this->lines[$i]["descr"] = utf8_decode($tup[1]);
        $this->lines[$i]["first"] = $tup[0] / 3;
        $this->lines[$i]["second"] = $tup[0] / 3;
        $this->lines[$i]["third"] = $tup[0] / 3;
        $this->lines[$i++]["single"] = $tup[0];
        $single += $tup[0];
      }

    if ($this->bRecommended && $this->row["status"] != 3 && $this->row["status"] != 4) {
      $this->lines[$i]["descr"] = "Frais adminstratifs de rappel";
      $this->lines[$i]["single"] = 10;
      $this->lines[$i++]["third"] = 10;
      $single += 10;
    }

    //Versements dans pay_in
    $r = jmysql_query("select amount,descr,detail from pay_in where num=" . $this->row["num"] . " and descr=3 order by ri asc");
    if (jmysql_num_rows($r)) {
      $this->distributePayment($r, $i, $first, $second);
      if ($this->row["status"] == 3) {
        $this->lines[$i]["descr"] = "Versement(s) effectu�(s)";
        foreach ($this->lines as $v)
          $single += $v["single"];
        $this->lines[$i]["single"] = -$single;
        $this->lines[$i]["first"] = -$first;
        $this->lines[$i]["second"] = -$second;
        $this->lines[$i]["third"] = -$first + $single + $second;
        if ($this->lines[$i]["single"])
          $i++;
        else
          unset($this->lines[$i]);
      }
    } else
    //Status
      switch ($this->row["status"]) {
        case 3:
          foreach ($this->lines as $v)
            $t += $v["third"];
          $this->lines[$i]["third"] = -$t;
          $this->lines[$i]["single"] -= $t;
        case 2:
          $t = 0;
          foreach ($this->lines as $v)
            $t += $v["second"];
          $this->lines[$i]["second"] = -$t;
          $this->lines[$i]["single"] -= $t;
        case 1:
          $t = 0;
          foreach ($this->lines as $v)
            $t += $v["first"];
          $this->lines[$i]["first"] = -$t;
          $this->lines[$i]["descr"] = "Versement(s) effectu�(s)";
          $this->lines[$i]["single"] -= $t;
          if ($this->lines[$i]["single"])
            $i++;
          else
            unset($this->lines[$i]);
          break;

        case 4:
          foreach ($this->lines as $v)
            $t += $v["single"];
          $this->lines[$i]["single"] = -$t;
          $this->lines[$i]["descr"] = "Versement effectu�";
          $i++;
          break;
      }
  }

  //------------------------------------------------------------------------------------
  function distributePayment(&$r, &$i, &$first, &$second)
  {
    static $descr = array("R�duction", "Suppl�ment", "Ch�ques", "Versement effectu�", "Transfert", "Cotisation");
    while ($row = jmysql_fetch_row($r)) {
      //echo NL."first=$first, second=$second, amount=$row[0]";
      $this->lines[$i]["single"] = $row[0];
      if (!$row[2])
        $this->lines[$i]["descr"] = $descr[$row[1]];
      else
        $this->lines[$i]["descr"] = $row[2];
      if ($this->row["status"] != 4) {
        if ($row[0] > 0) {
          $this->lines[$i++]["first"] = $row[0];
          continue;
        }
        if ($row[1] == 0) {
          //reduction a r�partir
          $red = $row[0] / 3;
          $this->lines[$i]["first"] = $red;
          $this->lines[$i]["second"] = $red;
          $this->lines[$i]["third"] = $red;
        } else {
          //single reduction
          if (-$row[0] > $first) {
            //report on second 
            $this->lines[$i]["first"] = -$first;
            if (-($row[0] += $first) > $second) {
              $this->lines[$i]["second"] = -$second;
              //report on third
              $this->lines[$i]["third"] = $row[0] + $second;
              $second = 0;
            } else {
              $this->lines[$i]["second"] = $row[0];
              $second += $row[0];
            }
            $first = 0;
          } else {
            $this->lines[$i]["first"] = $row[0];
            $first += $row[0];
          }
        }
      }
      $i++;
    }
  }

//--------------------------------------------------------------------------------------
  function __construct()
  {
    global $payDate, $now;

    $this->period = 3;
    foreach ($payDate as $i => $v)
      if ($now < $v) {
        $this->period = $i;
        break;
      }
    switch ($this->period) {
      case 1:
      case 2:
        $t = mktime(12, 0, 0, $payDate[$this->period]->format('n'), $payDate[$this->period]->format('j'), $payDate[$this->period]->format('Y'));
        $this->reminder = ($t - mktime()) < 15 * 24 * 3600;
        break;

      case 3:
        if ($now > $payDate[3])
          $this->veryLast = true;
        break;
    }
  }

  //------------------------------------------------------------------------------------
  public function getSQL($n)
  {
    //dump($n);
    global $seeRequest;
    //$seeRequest=1;
    $this->row = is_array($n) ? $n : jmysql_fetch_assoc(jmysql_query(
                            "select id,category, members.num, name, firstName, sex, address, cp,town,previous, status,caution,code_cat,firstLine,verser,type from members,cotisations where members.num=$n and cotisations.num=members.num"));
    $this->row["catName"] = utf8_decode(getCategory($this->row["category"], true));
    $this->row["name"] = utf8_decode($this->row["name"]);
    $this->row["firstName"] = utf8_decode($this->row["firstName"]);
    if ($t = $this->row["firstLine"])
      $this->row["firstLine"] = utf8_decode($t);
    if ($t = $this->row["verser"])
      $this->row["verser"] = utf8_decode($t);
    if ($t = $this->row["address"])
      $this->row["address"] = utf8_decode($t);
    $r = jmysql_query("select cotisations.num from cotisations,members where previous=" . $this->row["num"] . " and members.num=cotisations.num and members.type <>0");
    if ($r && jmysql_num_rows($r) == 1)
      $this->row["next"] = jmysql_result($r, 0);
    if (!$this->n) {
      $this->mainName = $this->row["name"] . ' ' . $this->row["firstName"];
      $this->id = $this->row["id"];
      $this->mainCatName = $this->row["catName"];
      $r = jmysql_query("select concat(firstName,' ',name) from members where address='" . jmysql_escape_string($this->row["address"]) . "' and num in (select num from coach)");
      $this->coach = jmysql_num_rows($r) != 0 ? jmysql_result($r, 0) : null;
    }
    $this->paidPlayer = !$this->coach && jmysql_result(jmysql_query("select count(distinct num) from pay_out where num=" . $this->row["num"]), 0);
    $this->bReduc = (bool) ($this->row["status"] & 0x80);
    $this->bCountStatus = (bool) ($this->row["status"] & 0x40);
    $this->bRecommended = (bool) ($this->row["status"] & 0x20);
    $this->bThreeTimes = !(bool) ($this->row["status"] & 0x10);
    $this->row["status"] &= 0x0F;
    $this->lastCat = intval(($this->row["category"] & 0xFFFF) / 10);
    $this->bAlready[$this->row["num"]] = true;
    //dump($this);
  }

  //------------------------------------------------------------------------------------
  public function bTest($key)
  {
    switch ($key) {
      case "family":
        $this->tot = null;
        if ($n = $this->row["next"]) {
          $this->n++;
          $this->getSQL($n);
          return true;
        }
        $this->n = 0;
        return false;

      case "lineTab":
        if (!$this->nLine)
          $this->buildLines();
        $this->nLine++;
        if ($this->nLine <= count($this->lines))
          return true;
        $this->nLine = 0;
        return false;

      case "isFamily":
        return $this->bFamily = $this->row["previous"] || $this->row["next"];

      case "first":
        if ($b = !$this->row["previous"]) {
          if ($this->printer == 2)
            $this->mainList[] = $this->row["num"];
        }
        return $b;

      case "isPrint":
        return $this->printer;

      case "bThreeTimes":
        return $this->bThreeTimes;

      case "singleFam":
        return $this->getValue($key);
    }

    return Engine::bTest($key);
  }

  //------------------------------------------------------------------------------------
  public function getValue($key)
  {
    switch ($key) {
      case "descr":
        return $this->lines[$this->nLine]["descr"];

      case "singleDesc":
        global $now, $orderMonths;
        return $this->period == 0 ? "Paiement<br>unique 15/08" : 'Solde<br>paiement';

      case "single":
      case "first":
      case "second":
      case "third":
        $t = (float) $this->lines[$this->nLine][$key];
        $this->tot[$key] += $t;
        if ($key != "single" && ($this->bReduc || $this->period == 3 || !$t && $this->row["status"] == 4))
          return "";
        return sprintf("%.02f", $t);

      case "singleTot":
      case "firstTot":
      case "secondTot":
      case "thirdTot":
        $k = substr($key, 0, -3);
        $t = (float) $this->tot[$k];
        if ($key != "singleTot" && ($this->bReduc || $this->period == 3 || !$t && $this->row["status"] == 4))
          return "";
        $this->totFam[$k] += $t;
        return sprintf("%.02f", $t);

      case "singleFam":
      case "firstFam":
      case "secondFam":
      case "thirdFam":
        $k = substr($key, 0, -3);
        if ($key != "singleFam" && ($this->bReduc || $this->period == 3 || !($t = (float) $this->totFam[$k]) && $this->row["status"] == 4))
          return "";
        return sprintf("%.02f", $t);

      default:
        return isset($this->row[$key]) ? $this->row[$key] : $this->$key;
    }
  }

}

//---------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------

class CotiStatus extends DetailPay
{

  function bScreen()
  {
    return true;
  }

//--------------------------------------------------------------------------------------
  public function templateBegin()
  {
    $this->allName = array();
    unset($this->allNum);
    $n = $this->row["next"];
    if ($this->row["status"] == 9 || $this->row["status"] == 10) {
      $this->allName[] = $this->row["firstName"];
      $this->masc = $this->row["sex"] == 'M';
    }
    $status[] = $this->row["status"] & 0x0F;
    $this->allNum .= "," . $this->row["num"];
    while ($n) {
      $this->allNum .= ",$n";
      $r = jmysql_fetch_row(jmysql_query("select members.status&0x0F, members.firstName,members.sex from cotisations, members where cotisations.num=$n and members.num=$n"));
      $r1 = jmysql_query("select num from cotisations where previous=$n");
      $n = $r1 && jmysql_num_rows($r1) == 1 ? jmysql_result($r1, 0) : null;
      if ($r[0] == 10 || $r[0] == 9) {
        $this->allName[] = $r[1];
        if ($r[2] == 'M')
          $this->masc = true;
      }
      $status[] = $r[0];
    }
    $this->status = 100;
    foreach ($status as $v)
      if ($v < 5) {
        if ($v < $this->status)
          $this->status = $v;
      } else if ($v == 11)
        $this->status = 11;
      else if ($this->status != 11)
        $this->status = $v;
  }

//--------------------------------------------------------------------------------------
  public function templateEnd()
  {
    unset($this->totFam);
  }

//--------------------------------------------------------------------------------------
  public function initialize()
  {
    /*
      if (!($id=$_GET["id"]))
      {
      if (!$this->curNum)
      stop(__FILE__, __LINE__, "id and curNum not present", null, true);
      }
      else
      {
      if (!($r=jmysql_query("select num from members where id='$id'")))
      stop(__FILE__, __LINE__, "invalid sql: $id", null, true);
      if (!jmysql_num_rows($r))
      stop(__FILE__, __LINE__, "invalid id: $id", null, true);
      $this->curNum=jmysql_result($r,0);
      } */

    $this->getSQL($this->curNum);
    $prev = $this->row["previous"];
    while ($prev) {
      //echo "prev=$prev".NL;
      if ($t = jmysql_result(jmysql_query("select previous from cotisations where num=$prev"), 0))
        $prev = $t;
      else {
        $this->getSQL($prev);
        return;
      }
    }
  }

  //------------------------------------------------------------------------------------
  public function getEntry()
  {
    if (!$this->bFirst) {
      $this->bFirst = true;
      return true;
    }
    return false;
  }

//------------------------------------------------------------------------------------
  public function bTest($key)
  {
    switch ($key) {
      case "M":
        return $this->masc;

      case "noPay":
        return $this->status == 0;

      case "young":
        return ($this->row["category"] & 0xFFFF) >= 800 && ($this->row["category"] & 0xFFFF) < 1000;

      case "news":
        return count($this->allName) > 1;

      case "period0":
        return $this->period == 0;

      case "retPrest":
        return ($this->coach || $this->paidPlayer) && $this->totFam["single"];

      case "retPrest1":
        return $this->coach || $this->paidPlayer;

      case "coach":
        return $this->coach != null && !$this->paidPlayer;

      case "onload":
        return isset($_GET["onload"]);

      case "veryLast":
        return $this->veryLast;
    }

    return DetailPay::bTest($key);
  }

//--------------------------------------------------------------------------------------
  public function getValue($key)
  {
    global $firstLine, $ruleBaby, $verser, $sig, $payDate;
    static $namePer = array("single", "first", "second", "third");

    switch ($key) {
      case "firstLine":
        if (isset($this->row["firstLine"]))
          return $this->row["firstLine"];
        if (isset($firstLine[$this->row["num"]]))
          return $firstLine[$this->row["num"]];

        if (count($this->allName))
          return $firstLine["new"];

        if ($this->status == 11)
          return $firstLine["wait"];

        if ($this->period == 0 || $this->period == 1 && intval(($this->row["category"] & 0xFFFF) / 10) == 95)
          return $firstLine["first"];

        if ($this->status == 3 || $this->status == 4 || $this->status > $this->period || $this->status == $this->period && !$this->reminder)
          return $firstLine["ok"];

        if ($this->bRecommended)
          return $firstLine["recommended"];
        if ($this->period == 3)
          return $firstLine["last"];
        return $this->reminder && $this->status == $this->period ? $firstLine["limit"] : $firstLine["rappel"];

      case "verser":
        if (isset($this->row["verser"]))
          return $this->row["verser"];
        if ($this->status == 11)
          return $verser["wait"];

        $period = $this->period == 1 && $this->lastCat == 95 ? 2 : $this->period;

        if ($period == 0)
          $t = $this->totFam["first"];
        else if ($period == 3)
          $t = $this->totFam["single"];
        else
          for ($i = 1; $i <= $period; $i++)
            $t += $this->totFam[$namePer[$i]];
        if (($this->bThreeTimes || !$this->totFam["single"]) && (($this->status == 3 || $this->status == 4) && !$this->totFam["single"] || $this->status != 0 && $this->status != 10 && !$t && !$this->reminder))
          return $verser["ok"];

        if ($t == $this->totFam["single"])
          $t = 0;

        switch ($period) {
          case 0:
          case 1:
            $this->row["tranche"] = $t ? sprintf(" ou <b>%.02f �</b> pour paiement de la 1<sup>ere</sup> tranche", $t) : '';
            return $verser["partiel"];

          case 2:
            if ($t == 0 || $this->reminder) {
              $this->row["tranche"] = "";
              return $verser["partiel"];
            }
            if ($t) {
              $this->row["tranche"] = sprintf(" ou <b style=white-space:nowrap>%.02f �</b> pour paiement ", $t);
              $this->row["tranche"] .= $this->totFam["first"] ? "des 2 premi�res tranches" : "de la " . ($this->lastCat == 95 && !$this->bFamily ? "1<sup>er</sup>" : "2<sup>i�me</sup>") . " tranche";
            }
            return $verser["partiel"];
        }
        $this->row["tranche"] = "";
        return $verser["partiel"];

      case "trancheNum":
        if ($this->status + 1 == $this->period)
          if ($this->period == 1)
            return " de la 1<sup>er</sup> tranche";
          else
            return " de la " . $this->period . "<sup>i�me</sup> tranche";
        return "";

      case "trancheNumNT":
        if ($this->period + 1 == 1)
          return " de la 1<sup>er</sup> tranche";
        return " de la " . ($this->period + 1) . "<sup>i�me</sup> tranche";

      case "nextPay":
        if ($this->status == 4 || $this->status == 3)
          return "";
        $t = "Il vous reste <b>" . $this->totFam["single"] . "�</b>";
        if (($s = $this->totFam["second"] + $this->totFam["third"]) && $this->totFam["single"] != $s) {
          $t .= " en paiement unique ou ";
          if ($this->totFam["second"])
            return "$t une seconde tranche de <b>" . $this->totFam["second"] . "�</b> pour le " . $payDate[1]->format("d/m/Y") . '.';
          return "$t une troisi�me tranche de <b>" . $this->totFam["third"] . "�</b> pour le " . $payDate[2]->format("d/m/Y") . '.';
        }
        return "$t � r�gler.";

      case "singleFam":
        return $this->totFam["single"];

      case "allName":
        $l = count($this->allName) - 1;
        for ($i = 0; $i <= $l; $i++) {
          if ($t && $i == $l)
            $t .= " et ";
          else if ($i)
            $t .= ", ";
          $t .= $this->allName[$i];
        }
        return $t;

      case "sig":
        return $sig;

      case "paper":
        return "";

      case "coach":
        return $this->coach;

      case "rule":
        global $rule;
        return $rule;

      case "date":
        global $date, $now, $months;
        return isset($date) ? $date : $now->format("d") . ' ' . utf8_decode($months[$now->format("n")]) . ' ' . $now->format("Y");

      default:
        return DetailPay::getValue($key);
    }
  }

}

?>