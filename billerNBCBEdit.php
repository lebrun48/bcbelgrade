<?php
include "head.php";

$col = array("season"   => array(0, "Saison :"),
    "account"  => array(0, "Compte :"),
    "date"     => array(0, "Date :"),
    "num"      => array(6, "Numéro :"),
    "orig"     => array(500, "Tiers :"),
    "detail"   => array(200, "Detail :"),
    "category" => array(0, "Catégorie :"),
    "amount"   => array(10, "Montant : ", '', "statusChange(0)"),
    "TVA"      => array(0, "TVA : "),
    "type"     => array(0, "Type : "),
);

include "common/billerEdit.php";

if (!$bOfficial)
  $col["hidden"] = array(0, "Comptabilisé ? : ");

class BillerNBCBEdit extends BillerEdit
{

//----------------------------------------------------------------------------------------------
  function applyValue(&$key, &$format, &$row)
  {
    $bSet = isset($row);
    switch ($key) {
      case "detail":
        global $resultCategory;
        if (isset($resultCategory)) {
          if (($team = strpos($row[$key], " \$T:")) !== false)
            $team = substr($row[$key], $team + 4, strpos($row[$key], " \$T:", $team) - $team - 1);
          echo TableRowEdit::applyValue($key, $format, $row);
          echo "<b>Equipe : </b>";
          echo "<select onchange=teamChg(this)><option></option>";
          $teams = getExistingCategories("(category&" . CAT_PLAYER . ") and type<>0 and ", true);
          foreach ($teams as $v)
            echo "<option" . ($team == $v ? " selected=selected" : '') . ">$v</otion>";
          echo "</select>";
          return;
        }
        break;

      case "season":
        global $currentYear;
        echo "<select name=_val_season>";
        if (!$bSet)
          echo "<option selected=selected></option>";
        for ($y = $this->begin - 2000; $y <= $currentYear - 1999; $y++)
          echo "<option value=$y" . ($bSet && $y == $row[$key] ? " selected=selected" : '') . ">" . (2000 + $y) . "-" . (2001 + $y) . "</option>";
        echo "</select>" . nl;
        return;
    }
    BillerEdit::applyValue($key, $format, $row);
  }

}

$obj = new BillerNBCBEdit;
$obj->title = "Edition de facturier";
$obj->colDef = &$col;
$obj->begin = 2010;
$obj->editRow();
?>
<script type="text/javaScript">
  function teamChg(obj)
  {
  dt=document.getElementsByName('_val_detail')[0];
  if (idx=dt.value.indexOf(" $T:"))
  end=dt.value.indexOf(";",idx);

  if (obj.selectedIndex==0 && idx!=-1)
  dt.value=dt.value.substr(0,idx)+dt.value.substr(end+1);
  else if (idx!=-1)
  dt.value=dt.value.substr(0,idx)+' $T:'+obj.value+';'+dt.value.substr(end+1);
  else
  dt.value+=" $T:"+obj.value+';';
  }
</script>    


