<?php

include "coach.php";
//$seeRequest=1;
if (!($team = $request["team"]))
  stop(__FILE__, __LINE__, "Pas d'équipes définie");
$val = $coach->assemble ? strtok($coach->team, '-') : $team;
$ageMin = $yearSeason + 100;
$ageMax = 0;
while ($val) {
  $cat = getCategory($val | CAT_PLAYER);
  $bNoCaution |= jmysql_result(jmysql_query("select noCaution from code_paie where num=" . $cat["code"]), 0);
  if (!$cat)
    stop(__FILE__, __LINE__, "Impossible d'extraire le nom de l'équipe");
  $teamName .= $cat["name"] . " / ";
  $ageMin = min($ageMin, $yearSeason - $cat["ageMin"]);
  if ($t = $cat["ageMax"])
    $ageMax = max($ageMax, $yearSeason - $t);
  else
    $ageMax = 0;
  if ($coach->assemble)
    $val = strtok("-");
  else
    break;
}
$res = jmysql_query("select com from com_presences where date='" . $date . "' and num=" . $team);
if (jmysql_num_rows($res) == 1) {
  $bExist = true;
  $com = jmysql_result($res, 0);
}
if ($right != ROOT) {
  if (strpos($coach->team, '-' . $team . '-') !== FALSE)
    $right = UPDATE;
  else if (strpos($coach->teamView, '-' . $team . '-') !== FALSE)
    $right = DISPLAY;
  else
    stop(__FILE__, __LINE__, "Cette équipe n'est pas autorisée!");
}
$teamName = substr($teamName, 0, -3);
?>