<?php

include "common/common.php";
include "head.php";
?>
<title>Classement          
</title>    
</head>   
<body>
  <?php

//---------------------------------------------------------------------------------------------------------
  function dispEl($item, &$t, $bCont = false, $bRec = false)
  {
    echo "<pre style=text-align:left>";
    static $level = 0;
    $tab = str_repeat(" - ", $level);
    echo "$tab Item($item)->length=$t->length" . NL;
    for ($i = 0; $i < $t->length; $i++) {
      echo $tab . $t->item($i)->nodeName . "[$i]=" . $t->item($i)->nodeValue . NL;
      if ($bRec && $t->item($i)->childNodes->length) {
        $level++;
        dispEl($item, $t->item($i)->childNodes, $bCont, true);
      }
    }

    if ($level) {
      $level--;
      echo "</pre>";
      return;
    }
    if (!$bCont)
      exit();
  }

  if (!($team = $_GET["team"]))
    stop(__FILE__, __LINE__, "L'équipe n'est pas renseignée!!");

  $f = file_get_contents("http://www.cpnamur.be/ranking.php?team=$team");
//dump($f);
  $document = new DOMDocument();
  @$document->loadHTML($f);
  $t = $document->getElementsByTagName("body")->item(0)->childNodes->item(0)->childNodes;
  echo "<div style=text-align:center><h3><img src=" . LOGO_LITTLE_CLUB . " style=vertical-align:middle>" . $t->item(0)->nodeValue . "</h3>";
  echo "<table class=main style='margin-right:auto; margin-left:auto; color:black'><tr><th></th><th>Equipe</th><th>J</th><th>G</th><th>P</th><th>N</th><th>P+</th><th>P-</th><th>(+/-)</th><th>point</th></tr>" . nl;
//dispEl(0,$t);
  $t = $t->item(2)->childNodes;
  for ($i = 1; $i < $t->length; $i++) {
    $t1 = $t->item($i)->childNodes;
    //dispEl($i, $t1,true);
    if (stripos($t1->item(1)->nodeValue, "belgrade") !== false)
      echo "<tr style=font-weight:bold;color:green";
    else
      echo "<tr";
    echo ($bEven = 1 - $bEven) ? " class=parity0>" : '>';
    for ($j = 0; $j < $t1->length; $j++)
      if ($j == 1)
        echo "<td>" . $t1->item($j)->nodeValue . "</td>";
      else
        echo "<td style=text-align:right>" . $t1->item($j)->nodeValue . "</td>";
    echo "</tr>" . nl;
  }
  echo "</table></div>";
  