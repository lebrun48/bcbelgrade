<?php

$prest = jmysql_query("select prest, remp0, remp1, remp2, remp3, remp4, remp5, remp6 from prestations where date='$date' and num=$coach->num");
$bPrest = jmysql_num_rows($prest);
if ($right != ROOT && !$coach->end) {
  //check if entries for the week are complete
  //First presence teams
  $val = strtok($coach->team, '-');
  while ($val) {
    $dt = new DateTime($date);
    $dt = credate($dt, 8);
    //dump($dt->format("Y-m-d"));
    if (!jmysql_result(jmysql_query("SELECT COUNT(*) from com_presences WHERE date='" . $date . "' AND num=$val" . ($now <= $dt ? " and updTime='" . $now->format("Y-m-d") . "'" : '')), 0)) {
      header("Location: presencesClub.php$basePath&team=$val" . ($bList ? "&begin=$begin" : ""));
      exit();
    }
    //Count presences
    $nPres = array(0, 0, 0, 0, 0, 0, 0);
    $res = jmysql_query("select pres from presences where date='$date' and team=$val and num in (select num from members where category=" . ($val | CAT_PLAYER) . " or category2=" . ($val | CAT_PLAYER) . " or category3=" . ($val | CAT_PLAYER) . ")");
    while ($row = jmysql_fetch_assoc($res))
      for ($i = 0; $i < 7; $i++)
        $nPres[$i] |= $row["pres"][$i];
    for ($i = 0; $i < 7; $i++)
      $nTotPrest[$i] += (bool) $nPres[$i];

    if ($coach->assemble)
      break;
    $val = strtok('-');
  }
  //Now check prestations
  if (!$bPrest) {
    $sql = '';
    for ($i = 0; $i < 7; $i++)
      if ($nTotPrest[$i])
        $sql .= $nTotPrest[$i];
      else
        $sql .= ' ';
    if (!jmysql_query("insert into prestations set date='$date', num=$coach->num, prest='$sql'"))
      stop(__FILE__, __LINE__, "Problème d'enregistrement: " . jmysql_error());
    header("Location: prest_Club.php$basePath" . ($bList ? "&begin=$begin" : ""));
    exit();
  } else {
    $day = array("lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi", "dimanche");
    $row = jmysql_fetch_assoc($prest);
    for ($i = 0; $i < 7; $i++)
      if ((int) $row["prest"][$i] != $nTotPrest[$i] && !$row["remp$i"]) {
        include "head.php";
        echo "</head>";
        echo "<body>";
        echo "<p>" . $coach->prenom . ", " . $day[$i] . " tu renseignes '" . (int) $row["prest"][$i] . "' prestation(s) alors ";
        if ($nTotPrest[$i])
          echo "que tu as encodé des présences de joueurs";
        else
          echo "que tu n'as encodé aucune présence de joueurs";
        echo " pour ce jour.<br>";
        echo "Pourrais-tu<br>";
        echo "<ul><li>Soit corriger ton <a href=prest_Club.php$basePath&bp=1" . ($bList ? "&begin=$begin" : "") . ">nombre de prestations</a>";
        echo "<li>Soit mettre <a href=prest_Club.php$basePath&bp=1" . ($bList ? "&begin=$begin" : "") . ">un commentaire</a> dans la colonne <i>\"Raison\"</i>&nbsp;de tes prestations pour $day[$i] (<i>'Remplacement de...'</i> ou <i>'Remplacé par...'</i>ou <i>'Tournoi...'</i> ou ...)";
        echo "<li>Soit corriger tes présences d'équipe:<ul>";
        if ($coach->assemble) {
          $val = $f = strtok($coach->team, '-');
          while ($val) {
            $t .= getCategory($val | CAT_PLAYER, true) . " / ";
            $val = strtok('-');
          }
          echo "<li><a href=presencesClub.php$basePath&team=$f" . ($bList ? "&begin=$begin" : "") . ">" . substr($t, 0, -3) . "</a></li>";
        } else {
          $val = strtok($coach->team, '-');
          while ($val) {
            echo "<li><a href=presencesClub.php$basePath&team=$val" . ($bList ? "&begin=$begin" : "") . ">" . getCategory($val | CAT_PLAYER, true) . "</a></li>";
            $val = strtok('-');
          }
        }
        echo "</ul>";
        exit();
      }
  }
}
?>