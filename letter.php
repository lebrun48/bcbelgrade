<?php

include "common/common.php";
include "templateClub.php";

/* $cont[]=html_entity_decode("<p>Juin 2015, bilan d'une saison.</p>".
  "<p>Bonjour &agrave; tous. Il est de coutume de faire le bilan d'une saison au terme de celle-ci, je ne d&eacute;rogerai donc pas &agrave; la r&egrave;gle.</p>".
  "<p>Tout d'abord, puisque ce courrier concerne les cotisations, quelques chiffres. Nous finissons la saison avec un d&eacute;ficit de 1.129€. Ceci est d&ucirc; principalement &agrave; un achat important de nouveaux &eacute;quipements (pour un total de 14.148€) pour toutes les &eacute;quipes. Ceci a &eacute;t&eacute; en partie compens&eacute; par un excellent travail de la cellule sponsoring, ainsi que des rentr&eacute;es exceptionnelles dues aux play-offs de nos &eacute;quipes P1 hommes et dames.</p>".
  "<p>Vous constaterez que les montants des cotisations restent inchang&eacute;es. C'est probablement la derni&egrave;re fois que nous pouvons le faire. En effet, elles ne couvrent que partiellement les frais de formation et de comp&eacute;tition de nos &eacute;quipes de jeunes. Le compl&eacute;ment n&eacute;cessaire provient principalement des autres organisations du club. Nous devons malheureusement constater que de moins en moins de joueurs et de parents participent aux diff&eacute;rentes activit&eacute;s propos&eacute;es. Les diff&eacute;rents repas, les ventes diverses sont de moins en moins suivies.</p>".
  "<p>Nous esp&eacute;rons donc vous retrouver nombreux aux prochaines activit&eacute;s, de m&ecirc;me qu'aux matchs. Nous voyons peu de jeunes ou de leurs parents &agrave; nos matchs seniors malgr&eacute; la gratuit&eacute; des acc&egrave;s. Pourtant, du baby au joueur de D3 nous portons tous le m&ecirc;me maillot, soyons en fiers, soutenons nos &eacute;quipes&nbsp;!</p>".
  "<p>Ceci nous am&egrave;ne tout naturellement au bilan sportif. Certes on pourrait s'arr&ecirc;ter &agrave; la descente de l'&eacute;quipe D2. Nous ne pouvons pas avoir de regret pour autant. Nous avons donn&eacute; l'occasion au basket namurois de rego&ucirc;ter &agrave; un niveau inaccessible depuis de tr&egrave;s longues ann&eacute;es. Mais cette saison sera malgr&eacute; cela pour le club un grand mill&eacute;sime&nbsp;:</p>".
  "<ul>".
  "	<li>Les hommes gagnent le titre en P1&nbsp;+ finaliste de la coupe de Province</li>".
  "	<li>Les dames ont disput&eacute; la finale des play-offs et remportent la super coupe</li>".
  "	<li>Les P3 remportent le titre invaincus</li>".
  "	<li>Nos U18 r&eacute;gionaux sont arriv&eacute;s en &frac12; finale de la coupe AWBB</li>".
  "	<li>Victoire finale au BIP pour les U10</li>".
  "	<li>Finale pour les U12 au BEN</li>".
  "</ul>".
  "<p>Enfin, notre sport favori a subi cette ann&eacute;e une profonde mutation au niveau de la formation avec la mise en place du 3&amp;3. Bravo &agrave; nos animateurs qui ont permis une mise en place avec succ&egrave;s de cette &eacute;volution.</p>".
  "<p>En conclusion, je voudrais souhaiter &agrave; tous d'excellentes vacances, profitez-en, pr&eacute;parez-vous bien pour se retrouver tous en forme d&eacute;but ao&ucirc;t pour une nouvelle saison. Et pour tous ceux qui le souhaitent, nous serons tr&egrave;s heureux d'accueillir des renforts au sein du comit&eacute;. Nous avons toujours besoin de renforts.</p>".
  "<p>A bient&ocirc;t&nbsp;!</p>"); */

class Letter extends HeaderClub
{

  function Header()
  {
    $this->Template();
    $this->SetAutoPageBreak(true, 20);
    $this->SetMargins(50, 15, 20);
    $this->SetY(15);
  }

}

//$pdf = new Letter();
$pdf = new Letter();
$pdf->AddPage();
/*
  $pdf->SetFont("Arial",'',9);
  $pdf->SetXY(145,8);
  $pdf->WriteHTML("Belgrade, le ".formatDate($now,"d m Y"));
  $pdf->SetY(25);
  $pdf->SetAutoPageBreak(true,20);
  //$pdf->WriteHTML("<p>Madame, Monsieur,</p>");
  foreach($cont as $v)
  $pdf->WriteHTML($v);
  $pdf->SetLeftMargin(140);
  //$pdf->Ln(10);
  $pdf->Write(4,"Le président,\n\nEric Tillieux");
 */
$pdf->Output("Le mot du président.pdf", 'I');

