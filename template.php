<?php

include "connectDB.php";
include "templateClub.php";

class Letter extends HeaderClub
{

  function Header()
  {
    $this->Template();
    $this->SetAutoPageBreak(true, 20);
    $this->SetMargins(50, 15, 20);
    $this->SetY(15);
  }

}

$pdf = new Letter();
$pdf->AddPage();
$pdf->Output("Template " . SHORT_NAME_CLUB . ".pdf", 'I');
