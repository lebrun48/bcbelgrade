<?php

$evName = array(
    1   => "St Nicolas",
    2   => "Lasagnes",
    8   => "Raclette",
    3   => "Repas choucroute",
    4   => "BBQ rentrée",
    6   => "Gaufres",
    9   => "Boulettes",
    10  => "Repas Tex Mex",
    11  => "Repas photo club",
    12  => "Sapins Noël",
    13  => "Stage Chiny",
    7   => "Boutique",
    100 => "Stage août mini",
    101 => "Stage août maxi",
    102 => "Stage Xmas",
    103 => "Stage carnaval",
    104 => "Stage Pâques mini",
    105 => "Stage Pâques maxi"
);

function getEvents()
{
  global $admin, $evName, $basePath1;
  if ($admin["events"]) {
    $rts = "<span style=color:green>Manifestations</span><ul>";
    $t = strtok($admin["events"], ",");
    while ($t) {
      if ($t < 100)
        $rts .= "<li><a target=_blank href=event.php${basePath1}eventNB=$t>$evName[$t]</a></li>";
      else
        $rts .= "<li><a target=_blank href=stages.php${basePath1}stage=" . ($t - 100) . ">$evName[$t]</a></li>";
      $t = strtok(",");
    }
    return $rts . "</ul>";
  }
}
