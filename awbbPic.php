<?php

include "admin.php";
include "head.php";

echo "<title>AWBB pictures</title><body>" . nl;

if (isset($_GET["overwrite"]))
  $bOverwrite = $_GET["overwrite"];
else {
  echo "<input type=button onclick=location.assign('awbbPic.php?id=$id&overwrite=1') value='Overwrite all'>" . NL;
  echo "<input type=button onclick=location.assign('awbbPic.php?id=$id&overwrite=0') value='Only news'" . NL;
  exit();
}
$res = jmysql_query("select num, AWBB_id, name, firstName from members where type<>0");
while ($row = jmysql_fetch_row($res)) {
  if (!file_exists($_SERVER["DOCUMENT_ROOT"] . "/members/pictures/N_$row[0].jpg"))
    continue;
  if (file_exists($_SERVER["DOCUMENT_ROOT"] . "/members/pictures/awbb/$row[1].jpg")) {
    if ($bOverwrite) {
      echo "Overwriting existing file $row[1].jpg ($row[2] $row[3])" . NL;
      copy($_SERVER["DOCUMENT_ROOT"] . "/members/pictures/N_$row[0].jpg", $_SERVER["DOCUMENT_ROOT"] . "/members/pictures/awbb/$row[1].jpg");
    }
  } else {
    echo "Coying new file $row[1].jpg ($row[2] $row[3])" . NL;
    copy($_SERVER["DOCUMENT_ROOT"] . "/members/pictures/N_$row[0].jpg", $_SERVER["DOCUMENT_ROOT"] . "/members/pictures/awbb/$row[1].jpg");
  }
}

echo "Terminated" . NL;

