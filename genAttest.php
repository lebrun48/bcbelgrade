<?php

include "common/Engine.php";

class GenerateAttest extends Engine
{

//-----------------------------------------------------------------------------------------
  public function initialize()
  {
    global $stage;
    $sql = "select ri as rowid,stages.num,days,price+meal as amount,birth,category,name,firstName,address,cp,town as city from stages,members where members.num=stages.num and stage=$stage" . (isset($request["num"]) ? " and ri=" . $_GET["num"] : '') . " order by name,category";
    //echo $sql.NL;
    $this->res = jmysql_query($sql);
    if (!$this->res)
      stop(__FILE__, __LINE__, "Erreur table : " . jmysql_error());
  }

//-----------------------------------------------------------------------------------------
  public function getEntry()
  {
    global $stages, $stage;
    $lim = ($stages[$stage][2]->format("Y") - 12) . $stages[$stage][2]->format("-m-d");
    do {
      if (!($this->row = jmysql_fetch_assoc($this->res)))
        return false;
      $b = new dateTime($this->row["birth"]);
    } while ($lim > $this->row["birth"]);
    for ($i = 0; $i < strlen($this->row["days"]); $i++)
      if ($this->row["days"][$i] != '0' && $this->row["days"][$i] != '4')
        $s++;
    $this->row["days"] = $s;
    return $this->row;
  }

//-----------------------------------------------------------------------------------------
  public function getValue($key)
  {
    switch ($key) {
      case "birth":
        if (empty($this->row["birth"]))
          return "........";
        $dt = new DateTime($this->row["birth"]);
        return $dt->format("d/m/Y");

      case "in":
        global $stages, $stage;
        return $stages[$stage][1]->format("d/m/Y");

      case "out":
        global $stages, $stage;
        return $stages[$stage][2]->format("d/m/Y");

      case "days":
        return $this->row["days"] ? $this->row["days"] : $_GET["j" . $this->row["stageNum"]];

      case "payDay":
        $v = $this->row["amount"] / $this->row["days"];
        return $v > 11.20 ? sprintf("%.02f", $v) : "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp";

      case "catName":
        return ($t = getCategory($this->row["category"], true)) ? $t : 'stagiaire';

      case "treasurer":
        return TREASURER_NAME_CLUB;

      default:
        if (!empty($this->row[$key]))
          return $this->row[$key];
        return "........";
    }
  }

//-----------------------------------------------------------------------------------------
  function terminate()
  {
    //fwrite($this->hOutput, "</body></html>");
  }

}

$gen = new GenerateAttest;
$gen->template = "template/attestations.thtml";
$gen->run();



