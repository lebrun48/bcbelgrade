<?php
include "head.php";
$col = array("team"        => array(0, "Equipe :"),
    "type"        => array(0, "Activité: "),
    "color"       => array(0, "Couleur de fond:"),
    "num"         => array(20, "N° Match: "),
    "date"        => array(0, "Date/heure : "),
    "field"       => array(5, "Terrain : "),
    "home"        => array(50, "Visité : ", null, "document.getElementsByName('_val_clubNb')[0].selectedIndex=0"),
    "away"        => array(50, "Visiteur : "),
    "results"     => array(9, "Score :"),
    "hallAddr"    => array(250, "Adresse rencontre"),
    "comment"     => array(100, "Remarque : "),
    "info"        => array(100, "Info (secrétaire) : "),
    "lastUpdated" => array(0, "Date modification : "),
);
?>
<title>Edition calendrier          
</title>
<script type="text/javascript" src="/tools/jscolor/jscolor.js"></script>
<script>

  function colorToHex(color) {
    if (color.substr(0, 1) === '#') {
      return color;
    }
    var digits = /(.*?)rgb\((\d+), (\d+), (\d+)\)/.exec(color);

    var red = parseInt(digits[2]);
    var green = parseInt(digits[3]);
    var blue = parseInt(digits[4]);

    var rgb = blue | (green << 8) | (red << 16);
    return digits[1] + '#' + rgb.toString(16);
  }

  function setColor(obj)
  {
    t = document.getElementsByName('_val_color')[0].value = colorToHex(obj.style.backgroundColor).substr(1).toUpperCase();
    document.getElementsByName('_val_color')[0].style.backgroundColor = t;
  }
  function activityType(opt)
  {
    if (opt.value ==<?php echo ACTIVITY ?> || opt.value ==<?php echo OTHER ?> || opt.value ==<?php echo STAGE ?>)
    {
      document.getElementsByName("_val_team")[0].selectedIndex = 0;
      document.getElementById("team").style.visibility = "hidden";
      document.getElementsByName("_val_home")[0].value = '';
      document.getElementById("home").style.visibility = "hidden";
      document.getElementsByName("_val_away")[0].value = '';
      document.getElementById("away").style.visibility = "hidden";
      document.getElementsByName("_val_results")[0].value = '';
      document.getElementById("results").style.visibility = "hidden";
    } else
    {
      document.getElementById("team").style.visibility = "visible";
      document.getElementById("home").style.visibility = "visible";
      document.getElementById("away").style.visibility = "visible";
      document.getElementById("results").style.visibility = "visible";
    }
  }
</script>    
</head>   
<body>
  <?php
  include "common/tableRowEdit.php";
  /*
    define ('CHAMPIONSHIP_PROV', 0);
    define ('CHAMPIONSHIP_REG', 1);
    define ('PROV_CUP', 2);
    define ('REG_CUP', 3);
    define ('FRIENDLY', 4);
    define ('TOURNAMENT', 5);
    define ('EXT_TOURNAMENT', 6);
    define ('BIP_BAP_BEN', 8);
    define ('PLAY_OFF', 9);
    define ('STAGE', 7);
    define ('ACTIVITY', 10);
   */
  if ($request["list"]) {
    unset($col["team"]);
    unset($col["type"]);
    unset($col["num"]);
    unset($col["date"]);
    unset($col["field"]);
    unset($col["home"]);
    unset($col["away"]);
    unset($col["results"]);
    unset($col["lastUpdated"]);
  }

  class CalendarEdit extends TableRowEdit
  {

//-----------------------------------------------------------------------------
    function printLine($key, $format, $row)
    {
      $bActivity = $row["type"] == ACTIVITY || $row["type"] == OTHER || $row["type"] == STAGE;
      switch ($key) {
        case "num":
        case "home":
        case "away":
        case "results":
        case "team":
          echo "<tr id=$key " . ($bActivity ? " style=visibility:hidden" : '') . "><th style=padding-left:20px>" . $format[1] . "</th><td>";
          $this->applyValue($key, $format, $row);
          echo "</td></tr>" . nl;
          return;

        default:
          return TableRowEdit::printLine($key, $format, $row);
      }
    }

//-----------------------------------------------------------------------------
    function applyValue(&$key, &$format, &$row)
    {
      switch ($key) {
        case "team":
          $row[$key] |= CAT_PLAYER;
          $row["team1"] |= CAT_PLAYER;
          echo "<select name=_val_team>";
          echo '<option value="0">&nbsp;</option>';
          $t = getExistingCategories("category&" . CAT_PLAYER . " and type<>0 and", false, true, true);
          foreach ($t as $k => $v)
            echo "<option value=" . ($k & 0xFFFF) . ($row[$key] == $k ? " selected=selected>" : '>') . $v . "</option>";
          echo "</select> <i><b> Et si autre équipe de " . CITY_CLUB . " aussi: </b></i>";
          echo "<select name=_val_team1>";
          echo '<option>&nbsp;</option>';
          foreach ($t as $k => $v)
            echo "<option value=" . ($k & 0xFFFF) . ($row["team1"] == $k ? " selected=selected>" : '>') . $v . "</option>";
          echo "</select>";
          return;

        case "type":
          global $activityTypes;
          echo '<select onchange="activityType(this)" name="_val_type">';
          foreach ($activityTypes as $k => $v)
            echo '<option value="' . $k . '" ' . ($row[$key] == $k ? 'selected="selected">' : '>') . $v . "</option>";
          echo "</select>";
          return;

        case "color":
          echo "<i><b>Clique pour mettre une autre couleur de fond (blanc annule la couleur):</b></i> <input class=color" . (isset($row[$key]) ? " value=" . sprintf("#%06X", $row[$key]) : '') . " name=_val_color>";
          $r = jmysql_query("select color from calendar where color is not null group by color");
          if (jmysql_num_rows($r)) {
            echo "<table style=margin:0;padding:0><tr><th style=margin:0;padding:0>Ou choisis une couleur déjà utilisée</th><td style=margin:0;padding:0><table>";
            $i = 0;
            while ($tup = jmysql_fetch_row($r)) {
              if (!($i % 5))
                echo ($i ? "</tr>" : '') . "<tr style=height:20px>";
              echo "<th style=background:#" . sprintf("%06X", $tup[0]) . ";width:20px onclick=setColor(this)></th>";
              $i++;
            }
            echo "</tr></table></td></tr></table>";
          }
          return;

        case "home":
          TableRowEdit::applyValue($key, $format, $row);
          echo "<br><input type=checkbox name=_val_extern" . ($row["updated"] & 0x10 ? " checked=checked" : '') . "><b><i>Match en déplacement.</b></i>";
          return;

        case "lastUpdated":
          echo $row[$key];
          return;

        case "date":
          if ($row[$key]) {
            $dt = new dateTime($row[$key]);
            $y = $dt->format('Y');
            $m = $dt->format('m');
            $d = $dt->format('d');
            $h = $dt->format('H');
            $min = $dt->format('i');
          }

          echo "<input type=hidden name=_val_date><select id=day>";
          for ($i = 1; $i <= 31; $i++)
            echo '<option value="' . sprintf("%02d", $i) . '" ' . ($i == $d ? 'selected="selected"' : '') . '>' . sprintf("%02d", $i) . '</option>';
          echo "</select><select id=month>";
          global $months;
          for ($i = 1; $i <= 12; $i++)
            echo '<option value="' . sprintf("%02d", $i) . '" ' . ($i == $m ? 'selected="selected"' : '') . '>' . $months[$i] . '</option>';
          echo "</select><select id=\"year\">";
          global $yearSeason;
          for ($i = 0; $i < 3; $i++)
            echo '<option value="' . ($yearSeason + $i) . '" ' . ($y == $yearSeason + $i ? 'selected="selected">' : '>') . ($yearSeason + $i) . '</option>';
          echo '</select><b style="color:black"> à ';
          echo "<select id=hour>";
          for ($i = 0; $i <= 23; $i++)
            echo '<option value="' . $i . '" ' . ($i == $h ? 'selected="selected"' : '') . '>' . sprintf("%02d", $i) . '</option>';
          echo '</select> H </b><select id="minute">';
          for ($i = 0; $i < 60; $i += 15)
            echo '<option value="' . $i . '" ' . ($i == $min ? 'selected="selected"' : '') . '>' . sprintf("%02d", $i) . '</option>';
          echo "</select>";
          echo "<br><input name=_val_keep type=checkbox><b><i>Ne pas tenir compte des changements (couleur rouge)</b></i>";
          return;

        case "clubNb":
          //dump($row);
          echo "<select name=_val_clubNb><option></option>";
          $r = jmysql_query("select num, name from clubs order by name");
          while ($tup = jmysql_fetch_row($r))
            echo "<option value=$tup[0]" . ($row[$key] == $tup[0] ? " selected=selected" : '') . ">$tup[1]</option>";
          echo "</select>";
          return;
      }

      TableRowEdit::applyValue($key, $format, $row);
    }

    function applySave()
    {
      if (!$this->bList) {
        ?>
        y=document.getElementById('year');
        m=document.getElementById('month');
        d=document.getElementById('day');
        h=document.getElementById('hour');
        mt=document.getElementById('minute');
        date=y.value+'-'+m.value+'-'+d.value+' '+h.value+':'+mt.value+':00';
        dt=new Date(date);
        now=new Date();
        if (dt<now && !confirm("La date "+dt.getDate()+'/'+(dt.getMonth()+1)+'/'+dt.getFullYear()+' à '+dt.toTimeString().substr(0,8)+" est dans le passé.\nEs-tu sûr de vouloir modifier le calendrier pour une date passée?"))
        return;
        document.getElementsByName('_val_date')[0].value=date;
        t=document.getElementsByName("_val_type")[0].value;
        /*
        if (t<5 && (!document.getElementsByName("_val_team")[0].selectedIndex || !document.getElementsByName("_val_home")[0].value || !document.getElementsByName("_val_away")[0].value))
        {
        alert("L'équipe, le visiteur et le visité doivent être renseigné pour un match!");
        return;
        }
        */
        <?php
      }
    }

  }

  $obj = new CalendarEdit;
  $obj->title = "Edition de calendrier";
  $obj->colDef = &$col;
  if (!$bRoot)
    unset($col["clubNb"]);
  $obj->editRow();


  