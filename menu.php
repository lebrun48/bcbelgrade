<?php

$bSession = true;
include "admin.php";
$rights = $admin["rights"];
if ($bCoach = jmysql_result(jmysql_query("select count(*) from members where num=" . $admin["num"] . " and (category&(" . CAT_COACH . "|" . CAT_ASSIST . ") or category2&(" . CAT_COACH . "|" . CAT_ASSIST . ") or category3&(" . CAT_COACH . "|" . CAT_ASSIST . "))"), 0))
  include "coach.php";
include "head.php";
echo "<title>Menu NBCB</title></head><body>" . nl;
echo "<img src=" . LOGO_LITTLE_CLUB . ">" . nl;
echo "<h1 align=middle>Menu " . (!$bCoach ? "administrateur" : ($rights & 0xFD ? "administrateur et " : '') . "coach $coach->nom $coach->prenom");
echo "<input type=button style=margin-left:30px value=Déconnection onclick=location.replace('login.php?action=remcnx&loginScript=" . basename($_SERVER["SCRIPT_NAME"], ".php") . "')></h1>";
echo "<table class=training style=margin-left:auto;margin-right:auto><tr><th style=min-width:400px>Saison $yearSeason - " . ($yearSeason + 1) . "</th></tr>" . nl;
if ($bCoach) {
  $realCoach = getRealCoach($coach->team);
  if ($realCoach) {
    $bSingle = true;
    foreach ($realCoach as $k => $v)
      if ($v["num"] != $coach->num)
        $bSingle = false;
    if ($bSingle)
      echo "<tr><td class=tabLink><a target=_blank href=listePrest.php$basePath" . ($right == ROOT ? "&user=root" : "") . ">Encodage des présences/prestations</a></td></tr>";
    else {
      echo "<tr><td>Encodage des présences/prestations<ul>" . nl;
      foreach ($realCoach as $k => $v)
        echo "<li><a target=_blank href=listePrest.php?id=" . $v["id"] . ($right == ROOT ? "&user=root" : "") . ">Pour " . $v["firstName"] . ' ' . $v["name"] . "</a>";
      echo "</ul></td></tr>";
    }
  }
}
echo "<tr><td class=tabLink><a target=_blank href=membersClub.php$basePath>Coordonnées des membres</a></td></tr>" . nl;
echo "<tr><td class=tabLink><a target=_blank href=calendar.php>Calendrier</a></td></tr>" . nl;
echo "<tr><td class=tabLink><a target=_blank href=training.php$basePath>Horaire entrainements</a></td></tr>" . nl;
if (jmysql_result(jmysql_query("select count(*) from pay_out where num=" . $admin["num"]), 0))
  echo "<tr><td class=tabLink><a target=_blank href=payOutSumary.php$basePath>Résumé de tes paiements et prestations</a></td></tr>";
if ($bCoach) {
  echo "<tr><td>Résumé des présences<ul>";
  $val = strtok($coach->team, '-');
  while ($val) {
    $cat = getCategory($val | CAT_PLAYER, true);
    if (!$cat)
      stop(__FILE__, __LINE__, "Impossible d'extraire le nom de l'équipe");
    $teamName .= "$cat / ";
    if (!$coach->assemble)
      echo "<li><a href=sumaryPres.php$basePath&team=$val>$cat</a></li>";
    $val = strtok("-");
  }
  if ($coach->assemble) {
    $teamName = substr($teamName, 0, -3);
    echo "<li><a href=sumaryPres.php$basePath&team=" . strtok($coach->team, "-") . ">$teamName</a></li>";
  }
  $teamListV = explode("-", $coach->teamView);
  foreach ($teamListV as $val) {
    if (!empty($val)) {
      echo "<li><a href=sumaryPres.php$basePath&team=$val>";
      echo getCategory($val | CAT_PLAYER, true);
      echo "</a></li>";
    }
  }
  echo "</ul></td></tr>";
  echo "<tr><td>Imprimer la composition d'équipe<ul>";
  $val = strtok($coach->team, '-');
  while ($val) {
    echo "<li><a target=_blank href=team.php?team=" . ($val | CAT_PLAYER) . ">";
    echo getCategory($val | CAT_PLAYER, true);
    echo "</a></li>";
    $val = strtok("-");
  }
  echo "</ul></td></tr>";
}
//0x0001 = Modify category for members
//0x0004 = View All attendances
//0x0008 = View cotisations
//0x0010 = Manifestations
//0x0020 = Biller result
//0x0040 = Report charting
//0x0080 = Export member allowed.
//0x0100 = Sponsors
if ($rights & 0x04)
  echo "<tr><td class=tabLink><a target=_blank href=sumaryAllAttendances.php$basePath>Résumé des présences toutes équipes</a></td></tr>" . nl;
if ($rights & 0x08)
  echo "<tr><td class=tabLink><a target=_blank href=cotisations.php$basePath>Etat des cotisations</a></td></tr>" . nl;
include "menuEvents.php";
if ($t1 = getEvents())
  echo "<tr><td>$t1</td></tr>" . nl;
if ($rights & 0x20) {
  echo "<tr><td class=tabLink><a target=_blank href=payment.php?id=$id>Résumé des paiements</a></td></tr>" . nl;
  echo "<tr><td class=tabLink><a target=_blank href=billerResults.php?tab=billerNBCB&id=$id>Bilan financier</a></td></tr>" . nl;
  echo "<tr><td class=tabLink><a target=_blank href=billerBudget.php?id=$id&tab=billerNBCB&year=" . ($yearSeason - 2000) . "&action=print>Budget saison $yearSeason - " . ($yearSeason + 1) . "</a></td></tr>" . nl;
  if (jmysql_result(jmysql_query("select count(*) from billerNBCB_budget where season=" . ($yearSeason - 1999)), 0))
    echo "<tr><td class=tabLink><a target=_blank href=billerBudget.php?id=$id&tab=billerNBCB&year=" . ($yearSeason - 1999) . "&action=print>Budget saison " . ($yearSeason + 1) . " - " . ($yearSeason + 2) . "</a></td></tr>" . nl;
}

if (!$bCoach || ($rights & 0xFD))
  echo "<tr><td class=tabLink><a target=_blank href=template.php>Modèle lettre " . SHORT_NAME_CLUB . "</a></td>" . nl;
if ($rights & 0x100)
  echo "<tr><td class=tabLink><a target=_blank href=bill.php$basePath>Factures sponsors</a></td></tr>" . nl;
echo "</table>";
?>