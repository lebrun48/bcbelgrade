<?php
include "billerNBCBConf.php";
include "common/tableTools.php";
//dump($request);
switch ($request["action"]) {
  case "setNbr":
    //dump($request);
    jmysql_query("update billerNBCB set num=concat('B',num) where ri in (" . $request["list"] . ")", 1);
    break;

  case "buvette":
    include "head.php";
    echo "<h3>Donnée buvette</h3><form autocomplete=off method=post action=billerNBCB.php${basePath1}action=save_buvette>";
    echo "<table style=color:black>";
    //Date
    echo "<tr><td style=font-weight:bold;border:0>Date</td><td style=border:0><input name=date type=date></td></tr>";
    echo "<tr><td style=font-weight:bold;border:0>Detail</td><td style=border:0><input name=detail type=text></td></tr>";
    echo "<tr><td style=font-weight:bold;border:0>Recette brut</td><td style=border:0><input size=3 name=income type=text><b>&nbsp;&nbsp;Officiel: <input type=text size=2 value=100 name=percentage>%</b></td></tr>";
    echo "<tr><td style=font-weight:bold;border:0>Frais arbitrage</td><td style=border:0><input size=3 name=referee type=text></td></tr>";
    echo "<tr><td style=font-weight:bold;border:0>Boissons arbitres</td><td style=border:0><input size=3 name=drink type=text></td></tr>";
    echo "<tr><td style=font-weight:bold;border:0>Restauration</td><td style=border:0><input size=3 name=resto type=text></td></tr>";
    echo "<tr><td style=font-weight:bold;border:0>\"Verre\" club</td><td style=border:0><input size=3 name=club type=text></td></tr>";
    for ($i = 1; $i < 4; $i++)
      echo "<tr><td style=font-weight:bold;border:0>Autre : <input type=text name=other$i></td><td style=border:0><input size=3 name=other_am$i type=text><b>&nbsp;&nbsp;Avec N° propre </b><input type=checkbox name=num$i></td></tr>";
    echo "</table><br>";
    echo "<input type=submit value=Enregistrer>&nbsp;&nbsp;";
    echo "<input type=button value=Annuler onclick='window.close()'></form>";
    exit;

  case "save_buvette":
    $sum = $_POST["income"];
    $num = jmysql_result(jmysql_query("select num1 from billerNBCB_num"), 0);
    $sql = "insert into billerNBCB set season=" . ($yearSeason - 2000) . ", date='" . $_POST["date"] . "'" . (($t = $_POST["detail"]) ? ",detail='" . addslashes($t) . "'" : '');
    if ($sum) {
      $num += $now->format("y") * 1000;
      jmysql_query("$sql, num='B$num', orig='Gestion', amount=-" . round($sum * 0.17, 2) . ", account=50,category=1284");
      $out = ($sum * 0.17);
    }
    if ($am = $_POST["referee"]) {
      jmysql_query("$sql, num='B$num', orig='Arbitres', amount=-$am, account=10,category=1536,status=2");
      $out += $am;
    }
    if ($am = $_POST["drink"]) {
      jmysql_query("$sql, num='B$num', orig='Boisson arbitres', amount=-$am, account=10,category=1536,status=2");
      $out += $am;
    }
    if ($am = $_POST["resto"]) {
      jmysql_query("$sql, num='B$num', orig='Restauration', amount=-$am, account=10,category=3336,status=0");
      $out += $am;
    }
    if ($am = $_POST["club"]) {
      jmysql_query("$sql, num='B$num', orig='Verre club', amount=-$am, account=10,category=1285,status=0");
      $out += $am;
    }
    $n = $num;
    for ($i = 1; $i < 4; $i++)
      if ($am = $_POST["other_am$i"]) {
        jmysql_query("$sql, num=" . ($_POST["num$i"] ? ++$n : "'B$num'") . ", orig='" . addslashes($_POST["other$i"]) . "', amount=-$am, account=10,category=1285,status=0");
        $out += $am;
      }
    jmysql_query("$sql,num='B$num', orig='Reçu renseigné', amount=" . ($out - $sum) . ", account=50,hidden=1,category=1281");
    jmysql_query("$sql,num='B$num', orig='Frais', amount=$out, account=50,category=1281");
    if ($sum) {
      $s = round($sum * $_POST["percentage"] / 100, 2);
      jmysql_query("$sql,num='B$num', orig='Total vente', amount=$sum, account=50,category=1281,hidden=1");
      jmysql_query("$sql,num='B$num', orig='Vente boissons', amount=" . round($s * 0.85, 2) . ",TVA=" . round($s * 0.85 * 0.21 / 1.21, 2) . ", TVAType=3, account=10,hidden=1,category=1281,status=103");
      jmysql_query("$sql,num='B$num', orig='Vente nourriture', amount=" . round($s * 0.15, 2) . ",TVA=" . round($s * 0.15 * 0.12 / 1.12, 2) . ", TVAType=2, account=10,hidden=1,category=1281,status=103");
    }
    jmysql_query("update billerNBCB_num set num1=" . (($n + 1) % 1000));
    echo "<h2>Numéro document: B$num</h2>";
    exit;
}

//dump($request);
include "common/billerAction.php";
include "common/biller.php";

$col = array("account"  => array(-1, "Compte", "width:80px"),
    "ri"       => array(1, "RI"),
    "date"     => array(30, "Date", "width:40px", "[Date]white-space:nowrap"),
    "num"      => array(3, "N°", '', 'text-align:center'),
    "orig"     => array(50, "Description", "width:500px"),
    "category" => array(-1, "Catégorie", "width:50px", "white-space:nowrap;overflow:clip"),
    "status"   => array(-1, "Bilan", "min-width:100px", "max-width:100px;white-space:nowrap;overflow-x:hidden"),
    "TVAType"  => array(-1, "Type TVA", "min-width:50px", "max-width:100px;white-space:nowrap;overflow-x:hidden"),
    "amount"   => array(10, "Montant", "width:90px", "text-align:right"),
    "base"     => array(0, "Base TVA", "width:50px", "text-align:right", "TVA"),
    "TVA"      => array(0, "TVA", "width:50px", "text-align:right")
);

$obj = new Biller;
$obj->addCol = "detail,ri,season,hidden,status,TVA_status,TVAType,accountNb";
$obj->tableId = "members";
$obj->colDef = &$col;
$obj->tableName = TABLE_NAME;
$obj->bHeader = true;
//$obj->bPrintNumLine=true;
$obj->selKey = "ri";
$obj->bExportAvailable = true;
$obj->exportFileName = "FacturierNBCAB.xls";
$obj->exportButtonEnd = true;

if ($bOfficial)
  $obj->defaultWhere = "account<50 and ";

if (!isset($request["year"]))
  $obj->season = $currentYear - 2000;
else if ($request["year"] != "all") {
  $obj->season = $request["year"];
  $currentYear = $obj->season + 2000;
}
if ($bRoot) {
  echo "<input title='Année civile complète' type=checkbox " . ($request["civil"] == "true" ? "checked=checked " : '') . "onChange=actionClick('filter','civil',this.checked);>";
  echo "<select onChange=actionClick('filter','year',this.options[this.selectedIndex].value);>";
  echo "<option value=all" . (!$obj->season ? " selected=selected" : '') . ">&lt;Toutes&gt;</option>";
  for ($y = 10; $y <= $yearSeason - 1999; $y++)
    echo "<option value=$y" . ($y == $obj->season ? " selected=selected" : '') . ">" . ($y + 2000) . "-" . ($y + 2001) . "</option>";
  echo "</select>" . nl;
  if ($admin["num"] != 507)
    echo "<input title='Officiel' type=checkbox " . ($bOfficial ? "checked=checked " : '') . "onChange=actionClick('filter','official',this.checked);>";
  createButton(-1, "TVA", "billerTVA.php${basePath1}tab=" . TABLE_NAME . "\" target=\"_blank");
  if ($admin["num"] != 507)
    createAction(-1, "Buvette", "buvette()");
  //createAction(-1, "setNb", "setNb()");
}
if (!$obj->order) {
  $obj->order = "date";
  $obj->dir = "desc";
}
$obj->build();
?>
<script type="text/javaScript">
  function setNb()
  {
  list=''
  $("[name^='_select_']:checked").each(function() {
  list+=','+this.value.substr(0,this.value.indexOf('$'));
  })
  actionClick("setNbr","list",list.substr(1))
  }

  function buvette()
  {
  window.open('billerNBCB.php'+document.getElementById('params').value+'&action=buvette','_blank','scrollbars=no,left=400,width=800,height=400,top=200,titlebar=no,menubar=no,status=no,location=no');
  }
</script>

