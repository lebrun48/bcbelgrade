<?php
include "admin.php";
include "common/tableTools.php";
//dump($request);
//$seeRequest=1;
define(PREPAID, 10);
//-------------------------------------------------------------------------------------------------
//Action
$filesInc = array(1  => "Nicolas",
    2  => "Lasagnes",
    3  => "Choucroute",
    4  => "BBQ",
    5  => "Gibier",
    6  => "Gaufres",
    7  => "Shop",
    8  => "Raclette",
    9  => "Boulettes",
    10 => "TexMex",
    11 => "PhotoClub",
    12 => "Sapin",
    13 => "Chiny",
);

include "event" . $filesInc[$request["eventNB"]] . ".php";
for ($i = substr($yearSeason, 2); $i; $i--) {
  if (isset(${"selTitle_$i"})) {
    $selTitle = ${"selTitle_$i"};
    $selAmount = ${"selAmount_$i"};
    $maxSel = ${"maxSel_$i"};
    if (isset(${"comment_$i"}))
      $comment = ${"comment_$i"};
    break;
  }
}

//-----------------------------------------------------------------------------------------------------------------------
function getPrepaid()
{
  if (EVENT_NUM == 0 || EVENT_NUM == 11)
    return;
  global $request, $selAmount, $action, $seeRequest;
  $prev = 0;
  switch ($action) {
    case "delete":
      $toPay = 0;
      $t = jmysql_fetch_row(jmysql_query("select num,prepaid from " . $request["key"]));
      if (!$t[0])
        return;
      $prev = $t[1];
      $num = $t[0];
      break;

    case "save":
      //dump($request);
      //$seeRequest=1;
      $prev = jmysql_result(jmysql_query("select prepaid from " . $request["key"]), 0);
    case "insert":
      if (!($num = $request["_val_num"]))
        return;
      $toPay = 0;
      foreach ($request as $k => $v)
        if (substr($k, 0, 8) == "_val_sel")
          $toPay += $request[$k] * $selAmount[substr($k, -1)];
      $toPay += $request["_val_reduction"];
      break;

    default:
      return;
  }

  $t = jmysql_result(jmysql_query("select count(*) from cotisations where num=$num"), 0);
  if (!$t && !$num = jmysql_result(jmysql_query("select num from members where concat(address,cp)=(select concat(address,cp) from members where num=$num) and type<>0 and (category&0xFF0000=" . CAT_PLAYER . " or category2&0xFF0000=" . CAT_PLAYER . " or category3&0xFF0000=" . CAT_PLAYER . ") limit 1"), 0))
    return;
  $r = jmysql_query("select num from cotisations where previous=$num");
  while (jmysql_num_rows($r)) {
    $num = jmysql_result($r, 0);
    unset($t);
    $r = jmysql_query("select num from cotisations where previous=$num");
  }
  $t = jmysql_fetch_row(jmysql_query("select prepaid,previous,status from cotisations join members on members.num=cotisations.num where cotisations.num=$num"));
  $bStatus = ($t[2] & 0xF) >= 1 && ($t[2] & 0xF) < 9;
  $cur = $t[0];
  $l = "$num";
  while ($t[1]) {
    $num = $t[1];
    $l .= ",$num";
    $t = jmysql_fetch_row(jmysql_query("select prepaid,previous,status from cotisations join members on members.num=cotisations.num where cotisations.num=$num"));
    $bStatus |= $t[2] >= 1 && $t[2] < 9;
    if ($t[0])
      $cur = $t[0];
  }
  //echo "bStatus=$bStatus, prev=$prev, topay=$toPay, cur=$cur".NL;
  $cur += $prev;
  if ($cur < PREPAID && $bStatus) {
    $t = min(PREPAID - $cur, $toPay);
    $request["_val_prepaid"] = -$t;
    jmysql_query("update cotisations set prepaid=" . ($cur + $t) . " where num in ($l)");
  } else
    $request["_val_prepaid"] = 0;
}

//-----------------------------------------------------------------------------------------------------------------------

$maxSelNb = sizeof($selAmount);
if ($admin["num"] == 6 || $admin["num"] == 201 || $admin["num"] == 63)
  $bRoot = true;
//dump($request);
$bPh = true;
getPrepaid();
switch ($action) {
  case "mails":
    include "head.php";
    $r = jmysql_query("select emailF,emailM,emailP,concat(members.name,' ',firstName) from events,members where " . $request["wh"]);
    while ($tup = jmysql_fetch_row($r)) {
      if ($tup[0])
        echo $tup[0] . NL;
      if ($tup[1])
        echo $tup[1] . NL;
      if ($tup[2])
        echo $tup[2] . NL;
      if (!$tup[0] && !$tup[1] && !$tup[2])
        $noList .= $tup[3] . NL;
    }
    if ($noList)
      echo "<br><b>Sans mail</b>" . NL . $noList;
    return;

  case "allMails":
    $res = jmysql_query("select distinct emailF,name,firstName,members.num,cotisations.num,previous from members left join cotisations on cotisations.num=members.num "
            . "where type and previous is null and emailF is not null and category<=" . CAT_PLAYER . " and members.num not in (select num from events where event=" . EVENT_NUM . ")");
    if (jmysql_num_rows($res))
      while ($row = jmysql_fetch_row($res)) {
        $list[$row[0]] = true;
        if (substr($row[0], 0, 1) != '-')
          echo $row[0] . "<br>";
        else
          echo substr($row[0], 1) . "<br>";
      }

    $res = jmysql_query("select distinct emailM from members left join cotisations on cotisations.num=members.num where type and previous is null and emailM is not null and category<=".CAT_PLAYER." and members.num not in (select num from events where event=" . EVENT_NUM . ")");
    if (jmysql_num_rows($res))
      while ($row = jmysql_fetch_row($res)) {
        if (!$list[$row[0]]) {
          $list[$row[0]] = true;
          if (substr($row[0], 0, 1) != '-')
            echo $row[0] . "<br>";
          else
            echo substr($row[0], 1) . "<br>";
        }
      }

    $res = jmysql_query("select distinct emailP from members left join cotisations on cotisations.num=members.num where type and previous is null and emailP is not null and category<=".CAT_PLAYER." and members.num not in (select num from events where event=" . EVENT_NUM . ")");
    if (jmysql_num_rows($res))
      while ($row = jmysql_fetch_row($res)) {
        if (!$list[$row[0]]) {
          $list[$row[0]] = true;
          if (substr($row[0], 0, 1) != '-')
            echo $row[0] . "<br>";
          else
            echo substr($row[0], 1) . "<br>";
        }
      }

    echo "</body></html>";
    exit();

  case "viewall":
    unset($request["view"]);
    break;

  case "insert":
    $request["key"] = "events where";
    unset($request["name"]);
    $request["_val_event"] = EVENT_NUM;
    selectAction();
    break;

  case "present":
    jmysql_query("update events set present=1-present where ri=" . $request["key"]);
    $tup = jmysql_fetch_assoc(jmysql_query("select * from events where ri=" . $request["key"]));
    $s = $tup["account"] + $tup["cash"] - $tup["reduction"] - $tup["prepaid"];
    foreach ($selAmount as $i => $v)
      $s -= $tup["sel$i"] * $v;
    if (round($s, 2) < 0) {
      $request["key"] = "events where events.ri=" . $request["key"];
      $bEd = true;
    }
    break;

  case "edit":
    if ($request["name"] && !is_numeric($request["name"]) &&
            jmysql_result(jmysql_query("select count(*) from events where event=" . EVENT_NUM . " and name='" . $request["name"] . "'"), 0)) {
      include_once "tools.php";
      alert("'" . $request["name"] . "' est déjà enregistré!");
    } else
      $bEd = true;
    break;

  case "account":
  case "cash":
    $tup = jmysql_fetch_assoc(jmysql_query("select * from events where ri=" . $request["key"]));
    foreach ($selAmount as $i => $v)
      $s += $tup["sel$i"] * $v;
    $s -= $tup["account"] + $tup["cash"] - $tup["reduction"];
    if ($action == "cash")
      $s += $tup["prepaid"];
    jmysql_query("update events set $action=$s+ifnull($action,0) where ri=" . $request["key"]);
    break;

  case "sumary":
    include "head.php";
    echo "<title>" . EVENT . " résumé</title></head><body>" . nl;
    echo "<h1 style=text-align:center>Résumé " . EVENT . "</h1>";
    echo "<table style=border:0;width:100%><tr style=border:0><td style=width:50%;border:0;vertical-align:top>";
    echo "<h2>Résumé par équipe</h2>";
    echo "<table class=main>";
    for ($i = 1; $i <= $maxSelNb; $i++)
      $t .= "sum(sel$i),";
    $r = jmysql_query("SELECT members.category,$t count(*) FROM members,events where event=" . EVENT_NUM . " and members.num=events.num group by members.category");
    while ($tup = jmysql_fetch_row($r)) {
      //dump($tup);
      $s = 0;
      foreach ($selAmount as $k => $v) {
        $s += $tup[$k] * $v;
        $t = "sel$k";
        $cat[$tup[0]]->$t = $tup[$k];
      }
      $tab[$tup[0]] = $s;
      $cat[$tup[0]]->count = $tup[$k + 1];
    }
    $cat[-1]->name = "&lt;Autres&gt; (Externes, comité,...)";
    $cat[262144]->name = "&lt;Externes&gt";
    foreach ($tab as $k => $v) {
      if ($k & CAT_PLAYER)
        continue;
      $idx = ($k & CAT_COACH) || ($k & CAT_ASSIST) ? $k & 0xFFFF | CAT_PLAYER : -1;
      $tab[$idx] += $tab[$k];
      $cat[$idx]->count += $cat[$k]->count;
      foreach ($selAmount as $k1 => $v1) {
        $t = "sel$k1";
        $cat[$idx]->$t += $cat[$k]->$t;
      }
      unset($tab[$k]);
    }
    $t = getExistingCategories("category&" . CAT_PLAYER . " and type<>0 and", false);
    arsort($tab);
    foreach ($t as $k => $v) {
      $cat[$k]->name = $v;
      if (!isset($tab[$k]))
        $tab[$k] = 0;
    }
    //dump($tab);
    echo "<tr><th>Equipe</th>";
    foreach ($selAmount as $k => $v)
      echo "<th>$selTitle[$k]</th>";
    echo "<th>Total</th><th>Nb<br>cmd</th></tr>" . nl;
    $par = 0;
    foreach ($tab as $k => $v) {
      echo "<tr class=parity$par><td>" . $cat[$k]->name . "</td>";
      $par = 1 - $par;
      $s = 0;
      foreach ($selAmount as $k1 => $v1) {
        $t = "sel$k1";
        echo "<td style=text-align:right>" . $cat[$k]->$t . "</td>";
        if ($v1)
          $s += $cat[$k]->$t;
        $sum[$k1] += $cat[$k]->$t;
      }
      echo "<td style=text-align:right;color:blue>$s</td><td style=text-align:right;color:brown>" . $cat[$k]->count . "</td></tr>" . nl;
      $sumCount += $cat[$k]->count;
    }
    echo "<tr style='border-top:2 solid black;border-bottom:2 solid black;font-weight:bold'><td style=text-align:center>Total</td>";
    $s = 0;
    foreach ($sum as $k => $v) {
      if ($selAmount[$k])
        $s += $sum[$k];
      echo "<td style=text-align:right>$sum[$k]</td>";
    }
    echo "<td style=text-align:right;color:blue>$s</td><td style=text-align:right;color:brown>$sumCount</td></tr></table>" . nl;
    if ($bScoring) {
      unset($tab);
      unset($cat);
      echo "</td><td style=border:0;vertical-align:top>";
      echo "<h2>Meilleurs 30 premiers vendeurs</h2>";
      $r = jmysql_query("select events.*, members.category,members.name from events,cotisations,members where event=" . EVENT_NUM . " and members.num=events.num");
      while ($tup = jmysql_fetch_assoc($r)) {
        $num = $tup["num"];
        $s = 0;
        foreach ($selAmount as $k => $v) {
          $t = "sel$k";
          $s += $tup[$t] * ($gain[$k] ? $gain[$k] : 1);
          $cat[$num]->$t = $tup[$t];
        }
        $cat[$num]->name = $tup["name"];
        $cat[$num]->catName = getCategory($tup["category"], true);
        if (($r1 = jmysql_query("select previous from cotisations where num=$num")) && jmysql_num_rows($r1))
          $cat[$num]->previous = jmysql_result($r1, 0);
        $tab[$num] = $s;
      }
      $cat[10000] = 0;
      do {
        $bCont = false;
        foreach ($cat as $k => &$v) {
          if (!$v->previous || !$tab[$k])
            continue;
          if (!isset($tab[$v->previous])) {
            $v->previous = 0;
            continue;
          }
          $bCont = true;
          $tab[$v->previous] += $tab[$k];
          $tab[$k] = 0;
          foreach ($selAmount as $k1 => $v1) {
            $t = "sel$k1";
            $cat[$v->previous]->$t += $cat[$k]->$t;
            $cat[$k]->$t = 0;
          }
        }
      } while ($bCont);
      arsort($tab);
      $i = 1;
      echo "<table class=main><tr><th>Famille</th><th>Catégorie</th>";
      foreach ($selAmount as $k => $v)
        echo "<th>$selTitle[$k]</th>";
      echo "<th>Total</th></tr>" . nl;
      foreach ($tab as $k => $v) {
        if (!$v)
          break;
        echo "<tr><td>$i " . $cat[$k]->name . "</td><td>" . $cat[$k]->catName . "</td>";
        $s = 0;
        foreach ($selAmount as $k1 => $v1) {
          $t = "sel$k1";
          echo "<td style=text-align:right>" . $cat[$k]->$t . "</td>";
          $s += $cat[$k]->$t;
        }
        echo "<td style=text-align:right;color:blue>$s</td></tr>" . nl;
        if ($i++ == 30)
          break;
      }
    }

    echo "</td></tr></table>" . nl;
    exit();

  default:
    selectAction();
    break;
}

//-------------------------------------------------------------------------------------------------
//Edition
if ($bEd) {
  include "head.php";
  echo "<title>" . EVENT . "</title></head><body onload=eventLoad()>" . nl;
  global $selAmount;
  $col = array("name"    => array(0, "Nom : "),
      "catName" => array(0, "Catégorie: ")
  );
  for ($i = 1; $i <= $maxSelNb; $i++)
    $col["sel$i"] = array(3, str_replace("<br>", ' ', $selTitle[$i]) . sprintf(" (%0.2f €)", $selAmount[$i]), 1, "eventLoad()");
  $col["comment"] = array(50, "Commentaire: ");

  if ($bRoot) {
    $col["amount"] = array(0, "A payer");
    $col["account"] = array(6, "Compte", 2, "eventLoad()");
    $col["cash"] = array(6, "Caisse", 2, "eventLoad()");
    $col["reduction"] = array(6, "Ajustement", 2, "eventLoad()");
    $col["rest"] = array(0, "Reste");
  }
  include "common/tableRowEdit.php";
  include_once "tools.php";

  class EventEdit extends TableRowEdit
  {

    //------------------------------------------------------
    function getTuple()
    {
      global $request;
      $this->bNew = $this->bClearAll = false;
      if (!$request["key"]) {
        $this->saveClick = "saveNew";
        if (is_numeric($request["name"])) {
          $tup = jmysql_fetch_assoc(jmysql_query("select num,category,concat(name,' ',firstName) as name from members where num=" . $request["name"]));
          $tup["catName"] = getCategory($tup["category"], true);
        } else {
          $tup["name"] = $request["name"];
          $tup["num"] = 0;
        }
        //dump($tup);
        return $tup;
      }
      return TableRowEdit::getTuple();
    }

    //------------------------------------------------------
    function applyValue(&$key, &$format, &$row)
    {
      switch ($key) {
        case "name":
          //dump($row);
          echo "<input type=hidden name=_val_num value=" . $row["num"] . ">";
          if (!$row["ri"])
            echo "<input type=hidden name=_val_name value='" . $row["name"] . "'>";
          echo "<span style=color:blue;font-weight:bold>$row[$key]</span>";
          return;

        case "catName":
          echo "<span style=color:blue;font-weight:bold>" . getCategory(jmysql_result(jmysql_query("select category from members where num=" . $row["num"]), 0), true) . "</span>";
          return;

        case "amount":
          echo "<div style=font-weight:bold id=toPay></div>";
          return;

        case "account":
          TableRowEdit::applyValue($key, $format, $row);
          createAction(-1, "Ok", "document.getElementsByName('_val_account')[0].value=document.getElementById('rest').innerHTML");
          return;

        case "rest":
          echo "<div style=font-weight:bold id=rest></div>";
          return;
      }
      TableRowEdit::applyValue($key, $format, $row);
    }

  }

  $obj = new EventEdit;
  $obj->title = "Inscription " . EVENT;
  $obj->colDef = &$col;
  $obj->editRow();
  ?>
  <script type="text/javaScript">
    function saveNew(p)
    {
    saveClick('insert');
    }

    function eventLoad()
    {
    <?php
    if (!$bRoot) {
      echo "}</script>" . nl;
      exit();
    }
    echo "s=";
    foreach ($selAmount as $i => $v)
      echo "document.getElementsByName('_val_sel$i')[0].value*" . $v . "+";
    echo "0" . nl;
    ?>
    el=document.getElementById('toPay');
    el.style.color=s<0 ? 'red':'blue';
    el.innerHTML=s.toFixed(2);
    el=document.getElementById('rest');
    s-=document.getElementsByName('_val_account')[0].value/1+document.getElementsByName('_val_cash')[0].value/1-document.getElementsByName('_val_reduction')[0].value/1;
    el.style.color=s<0 ? 'red':'blue';
    el.innerHTML=s.toFixed(2);

    }
  </script>
  <?php
  exit();
}

//$seeRequest=0;
//-------------------------------------------------------------------------------------------------
//List
include_once "tools.php";
include "common/tableEdit.php";
include "head.php";
echo "<title>" . EVENT;
echo "</title></head><body onload=loadEvent()>" . nl;
//-------------------------------------------------------------------------------------

$col = array("num"      => array(0, "N°", '', '', "@"),
    "name"     => array(1, "Nom", '', '', "events.name"),
    "category" => array(-1, "Catégorie", null, null, "members.category")
);

foreach ($selAmount as $k => $v) {
  $col["sel$k"] = array(0, $selTitle[$k] . ($v ? sprintf("<span style=white-space:nowrap;font-size:x-small> %0.2f €</span>", $v) : ''), '', 'text-align:right');
  $t .= "+IFNULL(sel$k,0)";
}
$col["price"] = array(0, "Montant", '', 'text-align:right;color:blue', '@');
$col["prepaid"] = array(0, "Bon achat", '', '[money]', null);
$col["reduction"] = array(0, "Ajustement", '', '[money]', null);
$col["globSel"] = array(0, '', '', '', substr($t, 1));
if ($bRoot) {
  $col["present"] = array(0, "Pr.", '', '', '', true);
  $col["account"] = array(0, "Prépaiement", 'min-width:60px', 'text-align:right');
  $col["cash"] = array(0, "A la livraison", 'min-width:60px', 'text-align:right', '');
  $col["rest"] = array(0, "A Payer", 'width:50px;min-width:50px', 'text-align:right;font-weight:bold', '@');
//  $col["received"]=array(0,"Reçu",'','text-align:right;','@',true);
//  $col["return"]=array(0,"A rendre",'','text-align:right;','@',true);
}
$col["comment"] = array(0, "Commentaire", '', '', 'events.comment');

class Event extends TableEdit
{

//---------------------------------------------------------------------------------------------------
  public function build()
  {
    global $maxSel, $bRoot, $request, $action, $s, $selAmount, $maxSelNb;
    if ($action != "export")
      unset($this->colDef["num"]);
    if (!$this->order) {
      $this->order = "ri";
      $this->dir = "desc";
    }
    $bViewAbsent = $request["view"] == "absent";
    $this->bHeader = true;
    if ($bRoot)
      $this->bPrintNumLine = true;
    $this->defaultWhere = "event=" . EVENT_NUM . ($bViewAbsent ? " and present<>1" : '') . " and members.num=events.num and";
    $this->tableName = "events,members";
    $this->addCol = "ri,reduction,events.num,concat(members.name,' ',members.firstName) as mName";
    echo "<h2 style=text-align:center;text-decoration:underline>Inscription " . EVENT . "</h2>";
    if ($maxSel) {
      echo "<table class=main style=table-layout:fixed;border:0px><tr style=border:0px><td style=border:0px;min-width:" . ($bRoot ? "370>" : "200>");
      if ($bRoot)
        echo "<table style=table-layout:fixed;border:0px><tr style='border-bottom:1px solid black;color:black;font-size:small'><td style=border:0px>Menu</td><td style=border:0px>Présent</td><td style=border:0px>Reste</td><td style=border:0px>Commande</td><td style=border:0px>Réserve</td></tr>" . nl;
      else
        echo "<table style=table-layout:fixed;border:0px><tr style='border-bottom:1px solid black;color:black;font-size:small'><td style=border:0px>Menu</td><td style=border:0px>Commande</td></tr>" . nl;
      for ($i = 1; $i <= $maxSelNb; $i++)
        $t .= ",sum(sel$i)";
      $p = jmysql_fetch_row(jmysql_query("select count(*)$t from events where present=1 and event=" . EVENT_NUM));
      $c = jmysql_fetch_row(jmysql_query("select count(*),sum(account),sum(cash),sum(reduction),sum(prepaid)$t from events where event=" . EVENT_NUM));
      foreach ($maxSel as $k => $v) {
        $k1 = $k + 4;
        if (!$p[$k])
          $p[$k] = 0;
        if ($bRoot)
          echo "<tr style=color:blue;font-size:small><td style=border:0px>" . str_replace("<br>", ' ', $this->colDef["sel$k"][1]) . "</td><td style=border:0px;text-align:right><b>$p[$k]</b></td><td style=border:0px;text-align:right><b>" . ($c[$k1] - $p[$k]) . "</b></td><td style=border:0px;text-align:right><b>$c[$k1]</b></td><td style=border:0px;text-align:right" . (($v - $c[$k1]) < 0 ? ";color:red" : '') . "><b>" . ($v ? $v - $c[$k1] : '') . "</b></td></tr>" . nl;
        else
          echo "<tr style=color:blue;font-size:small><td style=border:0px>" . str_replace("<br>", ' ', $this->colDef["sel$k"][1]) . "</td><td style=border:0px;text-align:right><b>$c[$k1]</b></td></tr>" . nl;
        if ($selAmount[$k])
          $sum += $c[$k1];
        $sAmount += $selAmount[$k] * $c[$k1];
      }
      if (!$p[0])
        $p[0] = 0;
      if ($bRoot)
        echo "<tr style=color:darkblue;font-size:small><td style=border:0px>Familles</td><td style=border:0px;text-align:right><b>$p[0]</b></td><td style=border:0px;text-align:right><b>" . ($c[0] - $p[0]) . "</b></td><td style=border:0px;text-align:right><b>$c[0]</b></td></tr>" . nl;
      else
        echo "<tr style=color:darkblue;font-size:small><td style=border:0px>Familles</td><td style=border:0px;text-align:right><b>$c[0]</b></td></tr>" . nl;
      echo "</table>" . nl;
      echo "</td><td style=vertical-align:top;border:0px>";
      if ($bRoot) {
        echo "<table style=table-layout:fixed;border:0px><tr style='border-bottom:1px solid black;color:black;font-size:small'><td style=border:0px>Total commandes</td><td style=border:0px>Caisse</td><td style=border:0px>Compte</td><td style=border:0px>Bon d'achat</td><td style='border:0px;border-left:1px solid black;text-align:center'>Total</td></tr>" . nl;
        echo "<tr style=color:black;font-size:small;font-weight:bold><td style=border:0px;text-align:center>$sum</td><td style=border:0px;text-align:right>" . number_format($c[2], 2, ',', '.') . " €</td><td style=border:0px;text-align:right>" . number_format($c[1], 2, ',', '.') . " €</td><td style=border:0px;text-align:right>" . number_format($c[4], 2, ',', '.') . " €</td><td style='border:0px;border-left:1px solid black;text-align:center'>" . number_format($c[2] + $c[1] - $c[4], 2, ',', '.') . " € / " . number_format($sAmount + $c[3], 2, ',', '.') . " €</td></tr>" . nl;
      } else {
        echo "<table style=table-layout:fixed;border:0px><tr style='border-bottom:1px solid black;color:black;font-size:small'><td style=border:0px>Total commandes</td></tr>" . nl;
        echo "<tr style=color:black;font-size:small;font-weight:bold><td style=border:0px;text-align:center>$sum</td></tr>" . nl;
      }
      echo "</table></td></tr></table>" . NL . nl;
    }

    $r = function_exists("selectMemberList") ? selectMemberList() : jmysql_query("select num, category, concat(members.name,' ',members.firstName) from members"
            . " where name is not null and type<>0 and category<".CAT_BLESSE." and num not in (select num from events where event=" . EVENT_NUM . ") order by name");
    echo "<span style=font-size:14;font-weight:bold>Choisis un nom à insérer: </span><select name=_val_num onchange=insertCmd(this)>";
    echo "<option></option><option style=color:red>&lt;Extérieur Au Club&gt;</option>";
    while ($tup = jmysql_fetch_row($r)) {
      $c = getCategory($tup[1]);
      $c = $c["short"];
      echo "<option value=$tup[0]>$tup[2]. ($c)</option>";
    }
    echo "</select></span>" . nl;
    if ($bRoot) {
      if ($bViewAbsent)
        createAction(-1, "Tous", "actionClick('viewall')");
      else
        createAction(-1, "Absents", "actionClick('filter','view','absent')");
    }
    createButton(-1, "Résumé", "event.php" . buildGetParam($this, "action", "sumary") . "\" target=\"_blank");
    if ($bRoot) {
      createAction(-1, "Liste mails", "openList('mails','" . urlencode($this->buildWhere()) . "')", 120);
      createAction(-1, "Mails autres", "openMail()", 120);
    }
    TableEdit::build();
  }

//---------------------------------------------------------------------------------------------------
  function printNumLine($num, &$row)
  {
    if (!$row["num"])
      return TableEdit::printNumLine($num, $row);
    global $basePath1;
    return "<a style=color:black target=_blank href=membersClub.php${basePath1}action=filter&_sel_num=%3D" . $row["num"] . ">$num</a>";
  }

//---------------------------------------------------------------------------------------------------
  function Terminate()
  {
    if (!$this->bExport)
      return;
    global $selAmount, $maxSelNb;
    //fwrite($this->exportFile,"<tr style=\"border-top:2px solid black;border-bottom:2px solid black\"><th colspan=2 style=text-align:center>Total</th>");
    fwrite($this->exportFile, "<tr><th colspan=2 style=text-align:center>Total</th>");
    for ($i = 1; $i <= $maxSelNb; $i++)
      $t .= ",sum(sel$i)";
    $tup = jmysql_fetch_row(jmysql_query("select sum(account) $t from events where event=" . EVENT_NUM));
    foreach ($selAmount as $k => $v) {
      fwrite($this->exportFile, "<th style=text-align:right>" . $tup[$k] . "</th>");
      $s += $tup[$k] * $v;
    }
    fwrite($this->exportFile, "<th style=text-align:right>" . number_format($tup[0], 2) . " €</th>");
    fwrite($this->exportFile, "<th style=text-align:right>" . number_format($s, 2) . " €</th></tr>");
  }

//---------------------------------------------------------------------------------------------------
  function applyDisplay(&$key, &$val, &$row, $idxLine)
  {
    $dispValue = null;
    if (strlen($key) <= 5 && substr($key, 0, 3) == "sel") {
      global $selAmount, $comment;
      $this->savPrice += $row[$key] * $selAmount[$key[3]];
      $this->$key += $row[$key];
      $t = "tot$key";
      $this->$t += $row[$key];
      if ($this->bExport && $row[$key] && isset($comment))
        $dispValue = $row[$key] . " " . $comment[substr($key, 3)] . ($row[$key] > 1 ? 's' : '');
      return TableEdit::applyDisplay($key, $val, $row, $idxLine, $dispValue);
    }
    switch ($key) {
      case "num":
        $dispValue = $this->numLine;
        break;

      case "name":
        if ($row["num"])
          $dispValue = $row["mName"];
        break;

      case "category":
        $dispValue = getCategory($row["category"], true);
        $this->savPrice = 0;
        break;

      case "price":
        $dispValue = sprintf("%0.2f €", $this->savPrice);
        $this->$key += $this->savPrice;
        $t = "tot$key";
        $this->$t += $this->savPrice;
        break;

      case "account":
      case "cash":
        $this->savRest = round($this->savPrice - $row["account"] - $row["cash"] + $row["reduction"] + $row["prepaid"], 2);
        if ($this->savRest > 0 || $key == "cash" && $this->savRest < 0) {
          if ($this->bExport)
            $this->line[$idxLine] = "<td style=text-align:right>" . ($row[$key] ? number_format($row[$key], 2) . " €</td>" : '');
          $dispValue = "<input type=radio onclick=actionClick('$key','key'," . $row["ri"] . ")>&nbsp;" . number_format($row[$key], 2) . " €";
        } else
          $dispValue = number_format($row[$key], 2) . " €";
        $this->$key += $row[$key];
        $t = "tot$key";
        $this->$t += $row[$key];
        break;

      case "rest":
        $this->$key += $this->savRest;
        $t = "tot$key";
        $this->$t += $this->savRest;
        $dispValue = "<span style=color:" . ($this->savRest < 0 ? "red>" : (round($this->savRest, 2) == 0 ? "green>" : "black>")) . number_format($this->savRest, 2) . " €</span>";
        break;

      case "present":
        $dispValue = "<input type=checkbox " . ($row[$key] ? "checked=checked " : '') . "onclick=actionClick('present','key'," . $row["ri"] . ")>";
        break;

      case "received":
        $dispValue = "<input type=text maxlength=6 size=7 onkeyup=computeReturn(this.value-$this->savRest," . $row["ri"] . ")>";
        break;

      case "return":
        $dispValue = "<span style=font-weight:bold;color:indigo id=ret_" . $row["ri"] . "></span>";
        break;

      case "comment":
        if ($row[$key])
          $dispValue = "<span name=blinked>$row[$key]</span>";
        break;
    }
    return TableEdit::applyDisplay($key, $val, $row, $idxLine, $dispValue);
  }

//---------------------------------------------------------------------------------------------------
  function buildHelpArray(&$key, &$where)
  {
    switch ($key) {
      case "name":
        return jmysql_query("select events.ri,events.name from events,members $where order by name");

      case "category":
        $r = jmysql_query("select members.category from members,events $where");
        while ($tup = jmysql_fetch_row($r))
          $tab[$tup[0]] = $tup[0] ? getCategory($tup[0], true) : "&lt;Externe&gt;";
        return $tab;
    }
    return TableEdit::buildHelpArray($key, $where);
  }

//---------------------------------------------------------------------------------------------------
  function buildKey(&$row)
  {
    return "events where ri=" . $row["ri"];
  }

//---------------------------------------------------------------------------------------------------
  function applyFilter(&$key, &$val)
  {
    switch ($key) {
      case "category":
        return $this->buildFilter("members.category", $val);
    }
    return TableEdit::applyFilter($key, $val);
  }

//---------------------------------------------------------------------------------------------------
  function applyOrder()
  {
    if (substr($this->order, 0, 3) == "sel")
      $this->order = "globSel";
  }

}

$obj = new Event;
$obj->bExportAvailable = true;
$obj->addCol = "num";
$obj->exportFileName = str_replace(' ', '_', EVENT . ".xls");
$obj->colDef = &$col;
$obj->tableId = "members";

$obj->build();
?>
<script>

  function openMail()
  {
    loc = "event.php<?php echo $basePath ?>&action=allMails&eventNB=<?php echo $request['eventNB'] ?>";
    if (win && !win.closed)
      win.location.replace(loc);
    else
      win = window.open(loc, "_blank", "scrollbars=yes,left=800,width=300,height=" + screen.availHeight + ",titlebar=no,menubar=no,status=no,location=no");
    win.focus();
    return false;
  }


  var elmts = new Array();
  var bShow = 1;
  function loadEvent()
  {
<?php if ($bMSIE) {
  ?>
      spans = document.getElementsByTagName('span');
      for (i = 0; i < spans.length; i++)
        if (spans[i].name == 'blinked')
          elmts.push(spans[i]);
  <?php
} else
  echo "elmts=document.getElementsByName('blinked');" . nl
  ?>
    //if (elmts)
    //setTimeout("blink()",200+300*bShow);
    genericLoadEvent();
  }

  function blink()
  {
    bShow = 1 - bShow;
    for (i = 0; i < elmts.length; i++)
    {
      if (bShow)
        elmts[i].style.visibility = 'visible';
      else
        elmts[i].style.visibility = 'hidden';
    }
    setTimeout("blink()", 200 + 300 * bShow);
  }

  function insertCmd(obj)
  {
    if (obj.selectedIndex == 0)
      return;
    if (obj.selectedIndex == 1)
    {
      n = prompt("Entre le nom de la personne", '');
      if (!n)
      {
        obj.selectedIndex = 0;
        return;
      }
      actionClick('edit', 'name', n);
    } else
      actionClick('edit', 'name', obj.options[obj.selectedIndex].value);
  }

  function computeReturn(val, ri)
  {
    el = document.getElementById("ret_" + ri);
    if (val < 0)
      el.innerHTML = '';
    else
      el.innerHTML = val.toFixed(2);
  }

  var win = null;
  function openList(a, wh)
  {
    loc = "event.php<?php echo ${basePath1} ?>eventNB=<?php echo $request['eventNB'] ?>&action=" + a + "&wh=" + wh;
    if (win && !win.closed)
      win.location.replace(loc);
    else
      win = window.open(loc, "_blank", "scrollbars=yes,left=800,width=300,height=" + (screen.availHeight - 100) + ",titlebar=no,menubar=no,status=no,location=no");
    win.focus();
    return false;
  }
</script>


