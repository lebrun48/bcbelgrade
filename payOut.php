<?php
include "admin.php";
include "common/tableTools.php";
include "payOutData.php";
//dump($request);
//$seeRequest=true;
if (($action == "save" || $action == "insert") && ($type = intval($request["_val_type"]) & 0x3F) >= PLAYER_DESC) {
  $val = buildObj($type);
  $sum = 0;
  if ($val) {
    $nMonth = 8 + (($request["_val_type"] >> 6) & 0x03);
    $team = ($team = $request["_val_team"]) ? ", team=$team" : '';
    $num = $request["_val_num"];
    jmysql_query("delete from pay_out where num=$num and month=0 and (" . ($type == PLAYER_DESC ? "type&0x3F<=" . PLAYER_MATCH . " or type&0x3F=" . PLAYER_ABSENT : "type&0x3F>=" . MONTHLY . " and type&0x3F<=" . HOME) . ") and team" . (($t = $request["_val_team"]) ? "=$t" : " is null"));
    $tr[20] = PLAYER_PRESENCE;
    $tr[21] = PLAYER_MATCH;
    $tr[22] = PLAYER_BONUS_WIN;
    $tr[23] = PLAYER_BONUS;
    $tr[24] = PLAYER_ABSENT;
    for ($i = 0; $i < 2; $i++)
      foreach ($val as $idx => $v) {
        if (intval(($v->type % 20) / 10) != $i)
          continue;
        if (!$descr)
          $descr = $v->descr ? ", descr='" . addslashes($v->descr) . "'" : null;
        $s = computeCost($val, $idx, $nMonth, $prest, $sum);
        if ($v->type >= 100)
          jmysql_query("delete from pay_out where num=$num and month=0 and type>=" . ACCOUNT . " and type<=" . DISTR_ACCOUNT);
        switch ($v->type) {
          case 3:
            $dep = $v;
          case 5:
            jmysql_query("insert into pay_out set num=$num, month=0, amount=$v->value, type=" . ($v->type == 3 ? MOVE : HOME));
            break;

          case 10:
            jmysql_query("insert into pay_out set num=$num, month=0$team$descr, amount=$v->amount, type=" . (($t = MONTHLY | (($nMonth - 8) << 6)) > 127 ? $t - 256 : $t));
            unset($descr);
            break;

          case 20:
          case 21:
          case 22:
          case 23:
          case 24:
            jmysql_query("insert into pay_out set num=$num, month=0, amount=$v->amount, type=" . $tr[$v->type]);
            break;

          case 33:
            jmysql_query("insert into pay_out set num=$num, month=0, amount=$v->amount, type=" . PLAYER_MONTHLY . $descr);
            unset($descr);
            break;

          case 100:
            jmysql_query("insert into pay_out set num=$num, month=0, type=" . DISTR_ACCOUNT);
            break;

          case 101:
            jmysql_query("insert into pay_out set num=$num, month=0" . ($v->amount ? ", amount=$v->amount" : '') . ", type=" . ACCOUNT);
            break;

          case 102:
            jmysql_query("insert into pay_out set num=$num, month=0, type=" . MONEY);
            break;
        }
      }
  }
  $request["_val_amount"] = $sum;
  $request["_val_descr"] = serialize($val);

  if ($type == COACH_DESC && $prest)
    jmysql_query("insert into pay_out set num=$num, month=0$team$descr, amount=" . ($dep ? $prest - $dep->value * PAY_KM_REAL * 2 : $prest) . ", type=" . PREST);
}
selectAction();

function buildObj($type)
{
  global $request;
  if ($request["_val_descr"])
    return null;
  if (isset($request["delete"]))
    $rem = $request["delete"];
  else if (isset($request["add"])) {
    $type = $request["add"];
    if (isset($request["l"]))
      $add = $request["l"] + 1;
    else if ($type % 20)
      $val[]->type = $type;
    else
      $add = 1000;
  }

  for ($i = 0; isset($request["_val_typeDsc$i"]); $i++) {
    if (isset($rem) && $rem == $i)
      continue;
    if (isset($add) && $add == $i) {
      $val[]->type = $type;
      unset($add);
    }
    $v->type = $request["_val_typeDsc$i"];
    $v->value = $request["_val_nbDsc$i"];
    $v->descr = $request["_val_descrDsc$i"];
    $v->amount = $request["_val_amountDsc$i"];
    $val[] = $v;
    unset($v);
    unset($request["_val_typeDsc$i"]);
    unset($request["_val_nbDsc$i"]);
    unset($request["_val_descrDsc$i"]);
    unset($request["_val_amountDsc$i"]);
  }
  if (isset($add))
    $val[]->type = $type;
  //dump($val, "build obj");
  return $val;
}

if ($action == "edit" || $action == "getDesc") {

//------------------------------------------------------------------------------------------
  function buildDescr($type, $nMonth, $desc)
  {
    global $request;
    if ($type < PLAYER_DESC)
      return "<textarea id=desc name=_val_descr cols=100 rows=3>$desc</textarea>";

    global $prestDsc;
    $line .= "<table id=desc><tr><th>Type</th><th>Nombre</th><th>Montant</th><th>Description</th></tr>";
    $k = $type == COACH_DESC ? 0 : 20;
    for ($i = 0; $i < 2; $i++) {
      if ($desc)
        foreach ($desc as $kp => $val) {
          if ($val->type >= 100 && $i == 0 || $val->type < 100 && intval(($val->type % 20) / 10) != $i)
            continue;
          $line .= "<tr><td><select name=_val_typeDsc$kp onchange=getDesc('')>";
          for ($j = $k; isset($prestDsc[$j]); $j++)
            $line .= "<option value=$j" . ($val->type == $j ? " selected=selected>" : '>') . "$prestDsc[$j]</option>";
          if ($k % 20 >= 10)
            for ($j = 100; isset($prestDsc[$j]); $j++)
              $line .= "<option value=$j" . ($val->type == $j ? " selected=selected>" : '>') . "$prestDsc[$j]</option>";
          $line .= "</select></td><td><input type=text name=_val_nbDsc$kp size=5 maxlength=5 value='$val->value' onchange=getDesc('') style=text-align:right" . ($val->type == 0 || $val->type == 4 || $val->type == 10 || intval($val->type / 10) == 2 || $val->type == 33 || $val->type >= 100 ? ";display:none" : '') . "></td>";
          $line .= "<td><input style=text-align:right size=5 maxlength=5 name=_val_amountDsc$kp";
          switch ($val->type) {
            case 1:
            case 2:
            case 3:
            case 5:
            case 11:
            case 12:
            case 100:
            case 102:
              $line .= " disabled=disabled";
              break;
          }
          $s = computeCost($desc, $kp, $nMonth, $prest, $sum);
          $line .= " onchange=getDesc('') value=" . number_format($s, 2, '.', '') . ">&euro;</td>";
          $line .= "<td><input type=text, size=20 maxlength=30 name=_val_descrDsc$kp value=\"$val->descr\"></td>";
          $line .= "<td style=text-align:center><img onclick=getDesc('&delete=$kp') src=cross_delete.jpg width=25 height=25></td>";
          $line .= "<td style=text-align:center><img onclick=getDesc('&add=$k&l=$kp') src=add.png width=25 height=25></td></tr>";
          $bSet = true;
        }
      if ($i == 0) {
        if ($k < 20)
          $line .= "<tr><td colspan=2 style=text-wight:bold>Total Prestation</td><td style=text-align:right>" . number_format($prest, 2, '.', '') . " &euro;</td>";
        else
          $line .= "<tr><td colspan=2 style=text-wight:bold>Description prestation</td><td></td>";
      } else
        $line .= "<tr><td colspan=2 style=text-wight:bold>Total Saison</td><td style=text-align:right>" . number_format($sum, 2, '.', '') . " &euro;<br>" . round($sum / $nMonth, -1) . " &euro;/mois</td>";
      if (!$bSet)
        $line .= "<td style=text-align:center><img onclick=getDesc('&add=$k') src=add.png width=25 height=25></td></tr>";
      $bSet = false;
      $k += 10;
    }
    return $line . "</table>";
  }

//------------------------------------------------------------------------------------------
  if ($action == "getDesc") {
    header("Content-Type:text/html; charset=UTF-8", true);
    echo buildDescr($request["_val_type"], $request["nMonth"] + 8, buildObj($request["_val_type"]));
    exit();
  }

  include "common/tableRowEdit.php";
  $rowEdit = array("num"    => array(6, "Numéro : "),
      "name"   => array(0, "NOM Prénom : "),
      "month"  => array(0, "Mois facturation : "),
      "type"   => array(0, "Type : "),
      "team"   => array(0, "Equipe : "),
      "nMonth" => array(0, "Nombre de mois : "),
      "amount" => array(7, "Montant : ", 2),
      "descr"  => array(200, "Description : "),
      "detail" => array(20, "Détails : ")
  );

  include "head.php";
  include "tools.php";

  if ($request["list"]) {
    unset($rowEdit["name"]);
    unset($rowEdit["num"]);
  }
  ?>
  <script type="text/javascript">
    function DataPrestations(type, amount)
    {
      this.type = type;
      this.amount = amount;
    }

    function compute(obj, res)
    {
      if (!obj.value.length)
      {
        res.value = '';
        return;
      }
      res.value = eval(obj.value).toFixed(2);
    }

    function getDesc(t)
    {

      $this = $("#editForm");
      $.ajax({
        url: $this.attr('action') + "&action=getDesc" + t, // Le nom du fichier indiqué dans le formulaire
        type: $this.attr('method'), // La méthode indiquée dans le formulaire (get ou post)
        data: $this.serialize(), // Je sérialise les données (j'envoie toutes les valeurs présentes dans le formulaire)
        success: function (html) { // Je récupère la réponse du fichier PHP
          document.getElementById("desc").parentNode.innerHTML = html;
        }
      });


      /*
       var xmlhttp;
       if (window.XMLHttpRequest)  xmlhttp=new XMLHttpRequest();else xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
       xmlhttp.onreadystatechange=function()
       {
       if (xmlhttp.readyState==4 && xmlhttp.status==200)
       document.getElementById("desc").parentNode.innerHTML=xmlhttp.responseText;
       }
       xmlhttp.open("post","payOut.php<?php echo $basePath1 ?>action=getDesc",true);
       xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
       xmlhttp.setRequestHeader("Content-type","text/html; charset=UTF-8");
       l="type="+document.getElementsByName("_val_type")[0].value+"&month="+(document.getElementsByName("nMonth")[2].checked ? '10':'9');
       if (t!="ignore")
       {
       l+=t;
       for (i=0; (o=document.getElementsByName("_val_typeDsc"+i)).length;i++)
       {
       l+="&_val_typeDsc"+i+"="+o[0].value;
       l+="&_val_nbDsc"+i+"="+document.getElementsByName("_val_nbDsc"+i)[0].value;
       l+="&_val_amountDsc"+i+"="+document.getElementsByName("_val_amountDsc"+i)[0].value;
       l+="&_val_descrDsc"+i+"="+escape(document.getElementsByName("_val_descrDsc"+i)[0].value).replace(/\+/g, "%2B");
       //l+="&_val_descrDsc"+i+"="+document.getElementsByName("_val_descrDsc"+i)[0].value;
       alert(l);
       }
       }
       xmlhttp.send(l);
       //document.getElementById("desc").parentNode.innerHTML=xmlhttp.responseText;
       return;
       */
    }

  </script>

  <?php

  class PayOutEd extends TableRowEdit
  {

    function ApplySave()
    {
      echo "var closed=new Array(); closed.length=13;" . nl;
      $r = jmysql_query("select month from pay_out where type=" . ENCLOSE_MONTH);
      while ($row = jmysql_fetch_row($r))
        if ($row[0])
          echo "closed[" . $row[0] . "]=true; ";
      echo nl;
      ?>
      var months=["", "janvier", "février", "mars", "avril", "mai", "juin", "juillet", "août", "septembre", "octobre", "novembre", "décembre"];
      sel=document.getElementsByName('_val_type')[0];
      opt=document.getElementsByName('nMonth');
      m=document.getElementsByName('_val_month')[0];
      m=m.options[m.selectedIndex].value;
      amount=document.getElementsByName('_val_amount')[0].value;
      if (closed[m])
      {
      if (!confirm("Le mois de "+months[m]+" est déjà clôturé! Continuer quand même ?"))
      return;
      }
      if (sel.options[sel.selectedIndex].value==<?php echo PLAYER_SANCTION ?>)
      {
      alert("Le montant doit être négatif pour une sanction");
      return;
      }
      if (m==0 && ((type=sel.options[sel.selectedIndex].value)==<?php echo MONTHLY ?> || type>=<?php echo PLAYER_DESC ?>))
      if (opt[0].checked)
      {
      alert("Nombre de mois impossible pour description ou paiement mensuel");
      return;
      }
      else
      sel.options[sel.selectedIndex].value=sel.options[sel.selectedIndex].value/1+(opt[1].checked ? 64 : -128);

      opt.name=null;
      if (sel.value==<?php echo ENCLOSE_MONTH ?>)
      {
      t=document.getElementsByName("_val_num")[0];
      t.options[t.selectedIndex].value=0;
      }
      <?php
    }

    function ApplyValue(&$key, &$val, &$row)
    {
      switch ($key) {
        case "num":
          echo $row["num"];
          return;

        case "name":
          echo '<select name="_val_num">';
          $res = jmysql_query("select num,name,firstName from members where type<>0 order by name");
          while ($r = jmysql_fetch_row($res))
            echo '<option value="' . $r[0] . '"' . ($r[0] == $row["num"] ? ' selected="selected"' : '') . '>' . $r[1] . " " . $r[2] . '</option>';
          echo '</select>' . nl;
          return;

        case "type":
          echo "<select name=_val_type onchange=getDesc('')>";
          echo '<option></option>';
          global $aType;
          foreach ($aType as $k => $v)
            echo '<option value="' . $k . '"' . (($row[$key] & 0x3F) == $k && !$this->bList ? ' selected="selected"' : '') . '>' . $v . '</option>';
          echo '</select>' . nl;
          return;

        case "team":
          echo "<select name=_val_team>";
          echo '<option></option>';
          $r = jmysql_query("select category from members where category&" . CAT_PLAYER . " and type<>0 and category<>0 group by category");
          while ($t = jmysql_fetch_row($r))
            echo "<option value=" . ($t[0] & 0xFFFF) . (($t[0] & 0xFFFF) == $row[$key] ? " selected=selected>" : '>') . getCategory($t[0], true) . "</option>";
          echo "</select>";
          return;

        case "nMonth":
          $n = 0;
          if ($row["type"] & 0xC0)
            $n = ($row["type"] >> 6) & 0x03;
          echo '<input onchange=getDesc("") type="radio" name="nMonth" value="0"' . ($n == 0 ? ' checked="checked"' : '') . '>Non défini<br>';
          echo '<input onchange=getDesc("") type="radio" name="nMonth" value="1"' . ($n == 1 ? ' checked="checked"' : '') . '>9 mois<br>';
          echo '<input onchange=getDesc("") type="radio" name="nMonth" value="2"' . ($n == 2 ? ' checked="checked"' : '') . '>10 mois<br>';
          return;

        case "month":
          echo '<select name="_val_month">';
          echo '<option></option>';
          global $months;
          foreach ($months as $k => $v)
            echo '<option value="' . $k . '"' . ($row[$key] == $k && !$this->bList ? ' selected="selected"' : '') . '>' . $v . '</option>';
          echo '</select>' . nl;
          return;

        case "amount":
          TableRowEdit::ApplyValue($key, $val, $row);
          echo "&nbsp;&nbsp;";
          echo "<input type=text size=20 maxlength=100 onkeyup=compute(this,document.getElementsByName('_val_amount')[0])>";
          createAction(-1, "Calcul prest.", "computePrest()");
          return;

        case "descr":
          if (($row["type"] & 0x3F) >= PLAYER_DESC) {
            $obj = unserialize($row["descr"]);
          } else
            $obj = $row["descr"];
          echo buildDescr($row["type"] & 0x3F, ($row["type"] & 0xC0) == 0x80 ? 10 : 9, $obj);
          return;
      }
      TableRowEdit::ApplyValue($key, $val, $row);
    }

  }

  echo '<title>pay_out';
  echo '</title></head><body>';
  $ed = new PayOutEd;
  $ed->title = "Donnée de table pay_out";
  $ed->colDef = &$rowEdit;
  $ed->editRow();
  exit();
}

//main table
include "head.php";
echo "<title>pay_out</title>" . nl;
echo "</head><body onload=genericLoadEvent()>";
include "common/tableEdit.php";
//$seeRequest=true;

$col = array("name"   => array(20, "NOM Prénom", "width:150", null, "concat(members.name, ' ', members.firstName)"),
    "month"  => array(5, "mois", "width:70"),
    "type"   => array(-1, "Type", "width:200", null, "pay_out.type"),
    "team"   => array(0, "Equipe", "width:50", 'white-space:nowrap;overflow:hidden'),
    "nMonth" => array(0, "Nb mois", "width:30", null, "@"),
    "amount" => array(6, "Montant", "width:70", "text-align:right"),
    "descr"  => array(10, "Description", "width:500"),
    "detail" => array(10, "Détails", "width:150")
);
$others = "ri";

function validMonth($m)
{
  global $months;
  if ($m != 0 && jmysql_result(jmysql_query("select count(*) from pay_out where type=" . ENCLOSE_MONTH . " and month=$m"), 0)) {
    header("refresh:0; url=" . $__SERVER['REQUEST_URI'] . getParam());
    include "tools.php";
    alert("le mois de $months[$m] est déjà clôturé!");
    return false;
  }
  return true;
}

//table itself
echo '<title>pay_out';
echo '</title></head><body>';

class PayOut extends TableEdit
{

  function buildKey(&$row)
  {
    return "pay_out where ri=" . $row["ri"];
  }

//---------------------------------------------------------------------------------------------------
  function printNumLine($num, &$row)
  {
    
  }

//-----------------------------------------------------------------------------------------------------
  public function applyDisplay(&$key, &$val, &$row, $idxLine)
  {
    $dispVal = null;
    switch ($key) {
      case "name":
        if ($row["num"] > 0)
          $row["name"] = jmysql_result(jmysql_query("select concat(name,' ',firstName) from members where num=" . $row["num"]), 0);
        break;

      case "type":
        global $aType;
        return $this->formatValue(is_null($row[$key]) ? '' : $aType[$row[$key] & 0x3F], $val);

      case "nMonth":
        $n = ($row["type"] >> 6) & 0x03;
        return $this->formatValue($n == 0 ? '' : 8 + $n, $val);

      case "month":
        global $months;
        return $this->formatValue($months[$row[$key]], $val);

      case "team":
        if ($row[$key]) {
          $t = getCategory($row[$key] | CAT_PLAYER);
          return $this->formatValue($t["short"], $val);
        }
        break;

      case "amount":
        if (is_null($row[$key]))
          return $this->formatValue('', $val);
        switch ($row["type"]) {
          case MOVE:
          case HOME:
            return $this->formatValue($row["month"] == 0 ? $row[$key] . " Km" : sprintf("%.02f €", $row[$key]), $val);

          case PREST_FICT:
            return $this->formatValue($row[$key] . " Prest.", $val);

          case PLAYER_SANCTION_PERC_AUTO:
          case PLAYER_SANCTION_PERC:
            return $this->formatValue($row[$key] . " %", $val);

          default:
            return $this->formatValue(sprintf("%.02f €", $row[$key]), $val);
        }

      /* case "descr":
        if ($row["type"]&0x3f>=40)
        {
        global $prestDsc;
        $a=unserialize($row[$key]);
        foreach($a as $k=>$v)
        {
        echo $prestDsc[$v->type]." : ";
        switch($v->type)
        {
        case 0:
        echo number_format($v->amount,2,',','')."€";
        case 1: */
    }
    return TableEdit::applyDisplay($key, $val, $row, $idxLine, $dispVal);
  }

//-----------------------------------------------------------------------------------------------------
  public function applyFilter(&$key, &$val)
  {
    switch ($key) {
      case "month":
        global $months;
        $s = "month in (";
        foreach ($months as $k => $v)
          if (stripos($v, $val) !== false) {
            $s .= $sep . $k;
            $sep = ", ";
          }
        return $sep ? $s . ')' : $s . "0)";

      case "type":
        $t = "pay_out.$key&0x3F";
        return TableEdit::applyFilter($t, $val);

      default:
        return TableEdit::applyFilter($key, $val);
    }
  }

//-----------------------------------------------------------------------------------------------------
  public function applyOrder()
  {
    if ($this->order == "type")
      $this->order = "pay_out.type&0x3F";
  }

//-----------------------------------------------------------------------------------------------------
  public function removeClick(&$row)
  {
    return "deleteLine(" . $row["month"] . ", '" . $this->buildKey($row) . "')";
  }

//-----------------------------------------------------------------------------------------------------
  function buildHelpArray(&$key, &$where)
  {
    switch ($key) {
      case "type":
        global $aType;
        return $aType;
    }
    return TableEdit::buildHelpList($key);
  }

}

$payOut = new PayOut;
$payOut->colDef = &$col;
$payOut->tableId = "PayOut";
$payOut->visible = &$visible;
$payOut->tableName = "pay_out,members";
$payOut->tableId = "members";
$payOut->defaultWhere = "members.num=pay_out.num and";
$payOut->addCol = &$others;
$payOut->bHeader = true;
$payOut->selKey = "ri";
$payOut->build();
echo "</body></html>";
?>
<script type="text/javaScript">

  function deleteLine(m, par)
  {
  var closed=new Array(); closed.length=13;
  <?php
  $r = jmysql_query("select month from pay_out where type=15");
  while ($row = jmysql_fetch_row($r))
    echo "closed[" . $row[0] . "]=true; ";
  echo nl;
  ?>
  var months=["", "janvier", "février", "mars", "avril", "mai", "juin", "juillet", "août", "septembre", "octobre", "novembre", "décembre"];
  if (closed[m])
  {
  alert("Le mois de "+months[m]+" est déjà clôturé");
  return;
  }
  deleteOpen(par);
  return;
  }


</script>

