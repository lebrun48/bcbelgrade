<?php

include "common/common.php";
include "DetailPay.php";
include "cotiLines.php";
include "templateClub.php";

//$seeRequest=1;
//------------------------------------------------------------------------------------------------------------------------
class CotiPDF extends CotiStatus
{

//--------------------------------------------------------------------------------
  function buildLetter()
  {
    $this->pdf->SetTitle("Cotisation");
    $this->pdf->SetSubject("Cotisation");
    $this->pdf->SetFont("Arial", '', 10);
    $this->pdf->SetXY(145, 8);
    $this->pdf->WriteHTML($this->getLine(CITY_CLUB . ", le �date�"));
    $this->pdf->SetMargins(50, 0, 20);
    $this->pdf->SetY(20);
    $this->pdf->WriteHTML("<p>Madame, Monsieur,</p>");
    $this->pdf->WriteHTML($this->getLine("�firstLine�"));
    $this->pdf->SetFont("Times", '', 10);
    $this->pdf->WriteHTML($this->getLine("Ci-dessous, votre d�compte pour la saison �Year�-�Year+1�"));
    $this->buildTable();
    $this->pdf->SetFont("Arial", '', 10);
    $this->pdf->WriteHTML($this->getLine("�verser�"));
    $this->pdf->WriteHTML($this->getLine("<p>Si vous constatez une erreur ou pour tout renseignement compl�mentaire, n'h�sitez pas � prendre contact avec le tr�sorier au N�<b>0474/72.61.24</b> ou en envoyant un email � <a href='mailto:" . TREASURER_MAIL_CLUB . "'>" . TREASURER_MAIL_CLUB . "</a></p>"));
    $this->pdf->SetLeftMargin(140);
    $this->pdf->Write(4, "Bien � vous.");
    $this->pdf->Ln(10);
    $this->pdf->Write(4, "Pour le comit�, le tr�sorier,\nC�dric Juste");
    global $rule;
    $this->pdf->SetMargins(20, 20);
    $this->pdf->AddPage();
    $this->pdf->SetFont("Times", '', 12);
    $this->pdf->WriteHTML($rule);
    $this->pdf->SetMargins(110, 235, 0);
    $this->pdf->SetY(235);
    $this->pdf->SetFontSize(12);

    $this->pdf->WriteHTML($this->getLine("�Test:isFamily�Famille�Tend�<BR>�mainName�<BR>"));
    $this->pdf->SetFontSize(10);
    $this->pdf->WriteHTML($this->getLine("�mainCatName�<br>"));
    $this->pdf->SetFontSize(12);
    if ($this->row["address"]) {
      $this->pdf->WriteHTML($this->getLine("�address�<br>"));
      $this->pdf->WriteHTML($this->getLine("�cp�   �town�"));
    }
    if ($this->mc) {
      //medicinal certificate
      global $yearSeason;
      $this->pdf->AddPage("L");
      $r = jmysql_query("select concat(firstName, ' ',name) as name,sex,category,emailF,emailM,emailP,phF,phM,phP from members where num in (" . substr($this->allNum, 1) . ")");
      for ($i = 0; $t = jmysql_fetch_assoc($r); $i++) {
        if ($i && !($i % 2)) {
          //$this->pdf->SetLineWidth(0.);
          $this->pdf->Line(5, 102, 290, 102);
          $this->pdf->Line(150, 7, 150, 102);
          $this->pdf->AddPage("L");
          $this->pdf->AddPage("L");
        }
        $x = 5 + ($i % 2) * 145;
        $this->pdf->Image("cm$yearSeason.jpg", $x, 7, 140);
        $this->pdf->SetMargins($x, 108);
        $this->pdf->SetXY($x, 105);
        $this->pdf->SetFont("Arial", '', 10);
        $this->pdf->SetTextColor(0);
        if (!$i) {
          $this->pdf->SetMargins($x, 108);
          $this->pdf->x += 3;
          $this->pdf->WriteHTML("Afin d'�tre assur� de vous atteindre dans nos communications, pourriez-vous confirmer ou modifier les renseignements ci-dessous?");
          $this->pdf->Ln(3);
          $this->pdf->SetFontSize(8);
          $this->pdf->x += 7;
          $this->pdf->y += 1;
          $this->pdf->SetFontSize(7);
          $this->pdf->WriteHTML("Merci de remettre ce document compl�t� avec le certificat m�dical au secr�taire Francis Davin ou � votre entraineur.<br>");
        } else
          $this->pdf->Ln(7.5);
        $this->pdf->SetMargins($x + 1, 108, 150 - $x);
        $this->pdf->SetTextColor(0x19, 0x4F, 0x89);
        $this->pdf->SetLeftMargin($this->pdf->lMargin + 2);
        $this->pdf->SetFontSize(14);
        $this->pdf->WriteHTML("<i>" . utf8_decode($t["name"]) . "</i></b><br>");
        $y = $this->pdf->y;
        if (($c = $t["category"] & 0xFFFF) >= 1000 || $c <= 750) {
          $this->pdf->SetFontSize(12);
          $this->pdf->x += 5;
          //$this->pdf->WriteHTML("<b><u>".($t["sexe"]=='F' ? "Elle":"Lui")."-m�me.</u></b><br>");
          $this->printInfo($t, "EMail :", "emailP");
          $this->printInfo($t, "GSM :", "phP");
          $this->pdf->Rect($this->pdf->lMargin - 1, $y, 140, $this->pdf->y - $y);
          $y = $this->pdf->y += 1;
        }
        if ($c > 1200 || $c < 950 && $c > 350) {
          $this->pdf->SetFontSize(12);
          $this->pdf->x += 5;
          $this->pdf->WriteHTML("<b><u>Parent 1</u></b><br>");
          $this->printInfo($t, "EMail :", "emailF");
          $this->printInfo($t, "GSM :", "phF");
          $this->pdf->Rect($this->pdf->lMargin - 1, $y, 140, $this->pdf->y - $y);
          $y = $this->pdf->y += 1;
          $this->pdf->SetFontSize(12);
          $this->pdf->x += 5;
          $this->pdf->WriteHTML("<b><u>Parent 2</u></b><br>");
          $this->printInfo($t, "EMail :", "emailM");
          $this->printInfo($t, "GSM :", "phM");
          $this->pdf->Rect($this->pdf->lMargin - 1, $y, 140, $this->pdf->y - $y);
          $y = $this->pdf->y += 1;
        }
      }
      $this->pdf->SetTextColor(0);
      //$this->pdf->SetLineWidth(0.);
      $this->pdf->Line(5, 102, 290, 102);
      $this->pdf->Line(147, 7, 147, 102);
      $this->pdf->AddPage("L");
    }
  }

  function printInfo($tup, $com, $att)
  {
    $this->pdf->SetFontSize(9);
    $this->pdf->Ln(1);
    $this->pdf->WriteHTML($com);
    if (!$tup[$att]) {
      $this->pdf->x = $this->pdf->lMargin + 10;
      $this->pdf->SetFontSize(8);
      $this->pdf->x += 2;
      $this->pdf->y += 1;
      $this->pdf->WriteHTML(str_repeat('.', 158));
    } else {
      $this->pdf->SetTextColor(0);
      $this->pdf->SetFont("Courier", '', 9);
      $this->pdf->x = $this->pdf->lMargin + 12;
      $this->pdf->WriteHTML($tup[$att]);
      $this->pdf->SetDrawColor(0x19, 0x4F, 0x89);
      $this->pdf->Rect($this->pdf->lMargin + 12, $this->pdf->y, 90, 4);
      $this->pdf->Image('rect.jpg', $this->pdf->x = $this->pdf->lMargin + 105, $this->pdf->y - 0.5, 4);
      $this->pdf->x += 4;
      $this->pdf->SetFont("Arial", '', 9);
      $this->pdf->SetTextColor(0x19, 0x4F, 0x89);
      $this->pdf->WriteHTML("Ok");
      $this->pdf->Image('rect.jpg', $this->pdf->x + 5, $this->pdf->y - 0.5, 4);
      $this->pdf->x += 9;
      $this->pdf->WriteHTML("Supprimer<br>");
      $this->pdf->Ln(1.7);
      $this->pdf->x += 12;
      $this->pdf->WriteHTML("Nouveau :");
      $this->pdf->x = $this->pdf->lMargin + 20;
      $this->pdf->SetFontSize(8);
      $this->pdf->x += 7;
      $this->pdf->y += 1;
      $this->pdf->WriteHTML(str_repeat('.', 140));
    }
    $this->pdf->SetFont("Arial", '', 9);
    $this->pdf->Ln();
    $this->pdf->SetDrawColor(0);
  }

//--------------------------------------------------------------------------------
  function buildTable()
  {
    $height = 4.2;
    $this->pdf->SetLineWidth(0.1);
    $this->pdf->Ln($height);
    $this->pdf->SetFont('', 'B');
    $this->pdf->Cell(80, $height * 2, "DESCRIPTION", "R", 0, 'C');
    $x = $this->pdf->GetX() + 23;
    $y = $this->pdf->GetY();
    $t = $this->getLine("�singleDesc�");
    //dump($t);
    $t = str_ireplace("<br>", "\n", $t);
    $this->pdf->MultiCell(23, $height, $t, "R", 'C');
    if ($this->bTest("bThreeTimes")) {
      $this->pdf->SetXY($x, $y);
      $this->pdf->Cell(45, $height, "Paiement en tranches", '', 2, 'C');
      $this->pdf->Cell(15, $height, "15/08", "", 0, 'C');
      $this->pdf->Cell(15, $height, "15/11", "", 0, 'C');
      $this->pdf->Cell(15, $height, "15/01", "", 1, 'C');
    }
    $this->pdf->SetFont('', '');
    do {
      $x = $this->pdf->Getx();
      $y = $this->pdf->GetY();
      $this->pdf->Line($x, $y, $x + ($this->bTest("bThreeTimes") ? 148 : 103), $y);
      $this->pdf->Ln(1);
      $this->pdf->SetFillColor(0xE0);
      $this->pdf->Cell($this->bTest("bThreeTimes") ? 148 : 103, 5, '', '', 0, '', true);
      $this->pdf->Ln(-1.5);
      $this->pdf->CellHTML(148, 7, $this->getLine("       <i><b>Nom: </b>�name� �firstName� (�catName�)</i>"));
      $this->pdf->Ln($height + 2);
      while ($this->bTest("lineTab")) {
        $this->pdf->CellHTML(80, $height, $this->getLine("�descr�"), "R", 0, 'L');
        $this->pdf->Cell(23, $height, $this->getLine("�single�"), "R", 0, 'R');
        if ($this->bTest("bThreeTimes")) {
          $this->pdf->Cell(15, $height, $this->getLine("�first�"), '', 0, 'R');
          $this->pdf->Cell(15, $height, $this->getLine("�second�"), '', 0, 'R');
          $this->pdf->Cell(15, $height, $this->getLine("�third�"), '', 0, 'R');
        }
        $this->pdf->Ln($height);
      }

      $border = $this->bTest("isFamily") ? '' : 'T';
      $this->pdf->SetFont('', 'B');
      $this->pdf->Cell(80, $height + 2, $this->getLine("A Payer pour �firstName�"), $border . 'R', 0, 'L');
      $this->pdf->Cell(23, $height + 2, $this->getLine("�singleTot�"), $border . 'R', 0, 'R');
      if ($this->bTest("bThreeTimes")) {
        $this->pdf->Cell(15, $height + 2, $this->getLine("�firstTot�"), $border, 0, 'R');
        $this->pdf->Cell(15, $height + 2, $this->getLine("�secondTot�"), $border, 0, 'R');
        $this->pdf->Cell(15, $height + 2, $this->getLine("�thirdTot�"), $border, 0, 'R');
      }
      $this->pdf->Ln($height + 2);
      $this->pdf->SetFont('', '');
    } while ($this->bTest("family"));

    if ($this->bTest("isFamily")) {
      $this->pdf->SetFont('', 'B');
      $this->pdf->Cell(80, $height, $this->getLine("Total pour famille"), 'TR', 0, 'L');
      $this->pdf->Cell(23, $height, $this->getLine("�Format:%.02f;singleFam�"), 'TR', 0, 'R');
      if ($this->bTest("bThreeTimes")) {
        $this->pdf->Cell(15, $height, $this->getLine("�firstFam�"), 'T', 0, 'R');
        $this->pdf->Cell(15, $height, $this->getLine("�secondFam�"), 'T', 0, 'R');
        $this->pdf->Cell(15, $height, $this->getLine("�thirdFam�"), 'T', 0, 'R');
      }
      $this->pdf->Ln($height);
    }
  }

//--------------------------------------------------------------------------------
  function __construct(&$pdf)
  {
    $this->pdf = $pdf;
    parent::__construct();
  }

//--------------------------------------------------------------------------------
  function run()
  {
    $this->bAnsi = true;
    $this->date = time();
    $this->initialize();
    $this->nLevel = -1;
    while ($this->getEntry()) {
      $this->templateBegin();
      $this->buildLetter();
      $this->templateEnd();
    }
    $this->terminate();
  }

  function bScreen()
  {
    return false;
  }

//--------------------------------------------------------------------------------
  function initialize()
  {
    global $request, $bMail;
    if (!$request["nums"]) {
      if (!($this->res = jmysql_query("select num from members where id='" . $request["id"] . "'")) || !jmysql_num_rows($this->res))
        stop(__FILE__, __LINE__, "invalid sql: $id", null, true);
    } else if ($nums = $request["nums"]) {
      $this->printer = true;
      if (!($this->res = jmysql_query("select num from members where num in ($nums) " . (!$bMail ? "" : "and ifnull(emailF,emailP) is null") . " order by category,name")))
        stop(__FILE__, __LINE__, "invalid sql: $id", null, true);
    }
  }

//--------------------------------------------------------------------------------
  function getEntry()
  {
    do {
      if (!($r = jmysql_fetch_row($this->res)))
        return false;
    }
    while ($this->bAlready[$r[0]]);
    $this->curNum = $r[0];
    CotiStatus::initialize();
    return true;
  }

//--------------------------------------------------------------------------------
  function templateEnd()
  {
    parent::templateEnd();
    if ($this->action == 2) {
      $this->pdf->Output(BASE_PATH . "/members/cotisations/last/" . $this->id . ".pdf", 'F');
      unset($this->pdf);
      $this->pdf = new HeaderClub();
    }
  }

//--------------------------------------------------------------------------------
  function templateBegin()
  {
    $this->pdf->AddPage();
    $this->pdf->Template();
    CotiStatus::templateBegin();
  }

//--------------------------------------------------------------------------------
  function bTest($key)
  {
    switch ($key) {
      case "isPrint":
        return $this->printer;
      case "printer2":
        return $this->printer == 2;
      default:
        return CotiStatus::bTest($key);
    }
  }

}

//$seeRequest=true;
$nums = $request["nums"];
if ($request["action"] == 2) {
  if (!isset($request["del"])) {
    ?>
    <script>
      res = confirm("Sauvegarder les fichiers existants?");
      document.location.replace(document.location.href + '&del=' + res);
    </script>
    <?php

    exit();
  }
  if ($request["del"] == "true") {
    echo "<h3>Renaming</h3>";
    if (file_exists($f = BASE_PATH . "/members/cotisations/last")) {
      $n = date('Y-m-d', filectime($f));
      rename($f, BASE_PATH . "/members/cotisations/$n");
    }
    mkdir(BASE_PATH . "/members/cotisations/last");
  }
} else if ($request["action"] == 1 && !isset($request["mc"])) {
  ?>
  <script>
    res = confirm("Ajouter les certificats m�dicaux ?");
    document.location.replace(document.location.href + '&mc=' + res);
  </script>
  <?php

  exit();
}

if ($request["action"] != 3) {
  //dump($_GET);

  $pdf = new HeaderClub();
  $coti = new CotiPDF($pdf);
  $coti->action = $request["action"];
  $coti->mc = $request["mc"] == 'true';
  $coti->run(true);
  if ($coti->action == 2)
    echo "<h2>Cotisations g�n�r�es</h2>";
  else
    $pdf->Output("Cotisation.pdf", 'I');
} else {
  $to = "Famille �name� �email�";
  $subject = "Cotisation " . NAME_CLUB . " famille �name�";
  $headerLine = "Madame, Monsieur";
  $bodyLine = array(
      "<p>Nous vous invitons � r�gler le montant de la cotisation suivant le document en <a href=" . URL_CLUB . "/members/cotisations/�id�.pdf>cliquant ici</a>.</p>" .
      "<p>Une copie papier de ce document sera �galement distribu�e par l'entraineur.</p>" .
      "</p>Si votre versement a crois� ce courrier vous pouvez bien s�r ignorer ce mail.</p>"
  );

  $lastLine = "<p>Merci et bonne journ�e.</p>";

  $query = "select name,id,firstName,emailF,emailM,emailP,address,cp,town from members,cotisations where members.num in ($nums) and (emailF is not null or emailM is not null or emailP is not null) and cotisations.previous is null and cotisations.num=members.num group by emailF";
  include "genMailCommon.php";
}

