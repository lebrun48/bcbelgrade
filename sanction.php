<?php include "head.php" ?>
<title>Sanctions joueurs          
</title>
<style type="text/css">
  .sanction
  {
    color:red;
    text-decoration:underline;
    cursor:pointer;
  } 
</style>

</head>     
<body><h3>Sanctions joueurs</h3>
  <p>Ce cadre sert à appliquer une sanction à un joueur. Le joueur peut recevoir plusieurs sanctions de type différent par jour. Pour appliquer une sanction à un joueur, il suffit de cliquer sur le N° du réglement non respecté (en rouge) qui se met en 'inverse'. Pour supprimer la faute, recliquer sur le N°. Une fois la(les) faute(s) sélectionnée(s), cliquer sur <b>"Confirmer"</b> ou <b>"Annuler"</b> pour terminer.</p>
  <ul>
    <?php
    include "sanctionArray.php";
    echo "<div style='border:1 solid black;height:70%;overflow-y:auto'>";
    foreach ($sanctionArray as $i => $val)
      echo '<li><div onclick="bSelect[' . $i . ']=!bSelect[' . $i . '];setSelect()" id="sanction' . $i . '" class="sanction">' . "$i.$val[0]$val[1]\n";
    echo "</ul></div>";
    include "tools.php";
    createAction(50, "Confirmer", "save()");
    createAction(170, "Annuler", "window.close()");
    ?>

    <script type="text/javascript">

      var n =<?php echo count($sanctionArray) ?> + 1;
      var bSelect = [false];
      bSelect.length = n;
      var inp = opener.document.getElementById("sanction<?php echo $_GET["n"] ?>");
      var div = opener.document.getElementById("div<?php echo $_GET["n"] ?>");
      var str = inp.value;
      var idx = 0;
      var token = str.indexOf(",", idx);
      while (token > 0)
      {
        bSelect[str.substring(idx, token)] = true;
        idx = token + 1;
        token = str.indexOf(",", idx);
      }
      setSelect();

      function setSelect()
      {
        for (i = 1; i < n; i++)
        {
          if (bSelect[i])
          {
            document.getElementById("sanction" + i).style.backgroundColor = 'red';
            document.getElementById("sanction" + i).style.color = 'white';
          } else
          {
            document.getElementById("sanction" + i).style.backgroundColor = 'white';
            document.getElementById("sanction" + i).style.color = 'red';
          }
        }
      }

      function save()
      {
        var val = '';
        for (i = 1; i < n; i++)
          if (bSelect[i])
            val = val + i + ',';
        inp.value = val;
        div.innerHTML = val + '&nbsp;';
        window.close();
      }
    </script>