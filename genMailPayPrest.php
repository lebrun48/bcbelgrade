<?php

include "head.php";
echo "<title>GenPrest</title>\n";
echo "</head><body>\n";
include "common/common.php";
include "payOutData.php";

$r = jmysql_query("SELECT count(date),date from prestations where status=" . $now->format('n') . " group by date");
while ($tup = jmysql_fetch_row($r)) {
  $dateEnd = new DateTime($tup[1]);
  if ($tup[0] < 10 || isset($dateBeg))
    continue;
  $dateBeg = new DateTime($tup[1]);
}
$dateEnd = creDate($dateEnd, 6);
$dateEnd = $dateEnd->format("d/m/Y");
$dateBeg = $dateBeg->format("d/m/Y");
dump($dateBeg, 'begin');
dump($dateEnd, 'end');

$subject = "Défraiement §type§";
$headerLine = "Bonjour §firstName§";

$bodyLine = array(
    0 =>
    /* "<p>Ton attente est terminée. Voici <a href=http://www.newbcbelgrade.be/members/payOutPayment.php?id=§id§&month=§month§>le décompte</a> du dernier paiement pour les semaines du §dateBeg§ au §dateEnd§. Le virement est effectué aujourd'hui.</p>".
      "<p>Désolé pour ce retard mais il fallait s'assurer de la rentrée des équipements.</p>" */

    "<p>Le paiement des semaines du §dateBeg§ au §dateEnd§ sera effectu&eacute; dans le courant de la semaine prochaine.</p>" .
    "<p>Tu peux voir le <a href=http://www.newbcbelgrade.be/members/payOutPayment.php?id=§id§&month=§month§>d&eacute;compte de ton paiement</a> en cliquant <a href=http://www.newbcbelgrade.be/members/payOutPayment.php?id=§id§&month=§month§>ici</a>"
);

$lastLine = "<p>Bonne journée.</p>";
//$lastLine="<p>Merci pour ta patience et bonnes vacances bien méritées.</p>";

$query = "select category,num,name,firstName,id,emailP,emailF from members where num in (select distinct num from pay_out where month=" . $now->format('n') . " and (type>=" . ACCOUNT . " or type<=" . DISTR_ACCOUNT . ") and amount<>0) order by name";
$dt = new DateTime();
$month = $dt->format("n");

include "genMailCommon.php";

function specific(&$key)
{
  global $gen;
  switch ($key) {
    case "emailF":
      $m = isset($gen->row["emailP"]) ? $gen->row["emailP"] : $gen->row["emailF"];
      if ($m[0] == '-')
        $m = substr($m, 1);
      return $m;


    case "dateBeg":
    case "dateEnd":
    case "month":
      global $dateBeg, $dateEnd, $month;
      return $$key;

    case "type":
      if (jmysql_result(jmysql_query("select count(*) from coach where num=" . $gen->row["num"]), 0))
        return "prestations";
      if ($gen->row["category"] & CAT_PLAYER)
        return "joueur";
      return jmysql_result(jmysql_query("select descr from pay_out where num=" . $gen->row["num"] . " and month=0 and type&0x3F<>" . COACH_DESC . " and descr is not null"), 0);
  }

  return null;
}
