<?php

$bOnlyRoot = true;
$bSession = true;
include "admin.php";
if (strlen($t = $request["setDB"])) {
  $_SESSION["prodDB"] = $t == "real";
  exit();
}
include "head.php";
?>
<script>
  function setDBToUse(o)
  {
    $.get("menuRoot.php", {setDB: o.checked ? 'real' : 'test'}, function ()
    {
      location.reload()
    })
  }
</script>
<?php

echo "<title>Menu Root $yearSeason - " . ($yearSeason + 1) . "</title></head>";
echo "<h3 align=middle>Menu root saison $yearSeason - " . ($yearSeason + 1);
echo "<input type=button style=margin-left:30px value=Déconnection onclick=location.replace('login.php?action=remcnx&loginScript=" . basename($_SERVER["SCRIPT_NAME"], ".php") . "')>";
echo "</h3>";
if ($request["ask"]) {
  echo "<select onchange=location.replace('menuRoot.php$basePath'+this.value)><option value=''></option>";
  for ($y = $yearSeason; $y > $yearSeason - 5; $y--)
    echo "<option value='&season=" . ($y - 2000) . "'>$y-" . ($y + 1) . "</option>";
  echo "</select></body>";
  exit();
}
//----------------------------------------------------------------------------------------------------------------------------
echo "<input type=checkbox" . ($_SESSION["prodDB"] ? ' checked' : '') . " onclick=setDBToUse(this) style=margin-right:5px>Utiliser DB Réelle<br>";
echo "<table><tr><td style=vertical-align:top>";
echo "<table class=training style=margin:auto><tr><th style=min-width:200>Financier</th></tr>" . nl;

if (!$request["season"])
  echo "<tr><td class=tabLink><a target=_blank href=" . BILLER_FILE_CLUB . "$basePath>Compta</a></td></tr>";
if (!$bOfficial)
  echo "<tr><td class=tabLink><a target=_blank href=cash.php$basePath>Caisses</a></td></tr>";
echo "<tr><td class=tabLink><a target=_blank href=cotisations.php$basePath>Cotisations</a></td></tr>";
echo "<tr><td class=tabLink><a target=_blank href=validPrest.php$basePath>Valider les prestations</a></td></tr>";
echo "<tr><td class=tabLink><a target=_blank href=client.php$basePath>Sponsors</a></td></tr>";
echo "<tr><td class=tabLink><a target=_blank href=bill.php$basePath>Factures</a></td></tr>";
echo "<tr><td class=tabLink><a target=_blank href=payment.php$basePath>Paiements</a></td></tr>";
echo "<tr><td class=tabLink><a target=_blank href=payOutBuild.php$basePath>Créer les paiements</a></td></tr>";
echo "<tr><td class=tabLink><a target=_blank href=checkCaution.php$basePath>Vérifier les cautions.</a></td></tr>";
if (!$bOfficial)
  echo "<tr><td class=tabLink><a target=_blank href=payOut.php$basePath>pay_out</a></td></tr>";
echo "<tr><td class=tabLink><a target=_blank href=teamCost.php$basePath>Coût par équipe</a></td></tr>";
echo "<tr><td class=tabLink><a target=_blank href=code_paie.php$basePath>Code paie</a></td></tr>";
echo "<tr><td class=tabLink><a target=_blank href=payIn.php$basePath>pay_in</a></td></tr>";
echo "<tr><td class=tabLink><a target=_blank href=cashRegister.php>Caisse enregistreuse</a></td></tr>";
echo "<tr><td class=tabLink><a target=_blank href=products.php$basePath>Produits buvette</a></td></tr>";

//----------------------------------------------------------------------------------------------------------------------------
echo "</table></td><td style=vertical-align:top>";
echo "<table class=training style=margin:auto><tr><th style=min-width:200>Gestion membres</th></tr>" . nl;
echo "<tr><td class=tabLink><a target=_blank href=membersClub.php$basePath>Membres</a></td></tr>";
echo "<tr><td class=tabLink><a target=_blank href=memberHistory.php$basePath>Historique Membres</a></td></tr>";
echo "<tr><td class=tabLink><a target=_blank href=coachEdit.php$basePath>Coaches</a></td></tr>";
echo "<tr><td class=tabLink><a target=_blank href=category.php$basePath>Catégories</a></td></tr>";
echo "<tr><td class=tabLink><a target=_blank href=adminEdit.php$basePath>Admin</a></td></tr>";

//----------------------------------------------------------------------------------------------------------------------------
echo "</table></td><td style=vertical-align:top>";
echo "<table class=training style=margin:auto><tr><th style=min-width:200>Sportif</th></tr>" . nl;
echo "<tr><td class=tabLink><a target=_blank href=calendar.php$basePath>Calendrier.</a></td></tr>";
echo "<tr><td class=tabLink><a target=_blank href=sumaryPrest.php$basePath>Résumé des prestations</a></td></tr>";
echo "<tr><td class=tabLink><a target=_blank href=sumaryAllAttendances.php$basePath>Résumé des présences</a></td></tr>";
echo "<tr><td class=tabLink><a target=_blank href=training.php$basePath>Entrainements</a></td></tr>";
echo "<tr><td class=tabLink><a target=_blank href=comment.php$basePath>Commentaires présences</a></td></tr>";
echo "<tr><td class=tabLink><a target=_blank href=memberTeam.php$basePath>Liste d'équipe</a></td></tr>";
echo "<tr><td class=tabLink><a target=_blank href=team.php>Générer les fiches d'équipes.</a></td></tr>";

//----------------------------------------------------------------------------------------------------------------------------
echo "</table></td><td style=vertical-align:top>";
echo "<table class=training style=margin:auto><tr><th style=min-width:200>Activités</th></tr>" . nl;
include "menuEvents.php";
echo "<tr><td>" . getEvents() . "</td>";
echo "<tr><td class=tabLink><a target=_blank href=involve.php$basePath>Participation aux activités</a></td></tr>";
echo "<tr><td class=tabLink><a target=_blank href=activities.php$basePath>Activites partagées</a></td></tr>";

//----------------------------------------------------------------------------------------------------------------------------
echo "</table></td><td style=vertical-align:top>";
echo "<table class=training style=margin:auto><tr><th style=min-width:200>Divers</th></tr>" . nl;
echo "<tr><td class=tabLink><a target=_blank href=littleTools.php$basePath>Petits outils</a></td></tr>";
echo "<tr><td class=tabLink><a target=_blank href=sponsors.html>Sponsors défilement</a></td></tr>";
echo "<tr><td class=tabLink><a target=_blank href=letter.php$basePath>Ecrire lettre</a></td></tr>";
echo "<tr><td class=tabLink><a target=_blank href=sendmails.php$basePath>Envoi des mails</a></td></tr>";
echo "<tr><td class=tabLink><a target=_blank href=genMailEnc.php$basePath>Mail présences/prestations</a></td></tr>";
echo "<tr><td class=tabLink><a target=_blank href=genMailPayPrest.php$basePath>Mail paiement prestations</a></td></tr>";
echo "<tr><td class=tabLink><a target=_blank href=label.php$basePath>Générer les étiquettes.</a></td></tr>";
echo "<tr><td class=tabLink><a target=_blank href=getCalendar.php$basePath>Mise à jour calendrier.</a></td></tr>";
echo "<tr><td class=tabLink><a target=_blank href=clubs.php$basePath>Clubs.</a></td></tr>";
echo "<tr><td class=tabLink><a target=_blank href=menuSecretary.php?logAs=DAV_fra.sw8ZW5PD>Menu secrétaire</a></td></tr>";
echo "<tr><td class=tabLink><a target=_blank href=menuTreasurer.php?logAs=JUS_ced.wu1PPqK6>Menu trésaurier</a></td></tr>";
echo "<tr><td>Voir le menu des administrateurs</td></tr><tr><td><ul>";
$res = jmysql_query("select id, name, firstName from members where num in (select num from admin) order by name,firstName");
while ($row = jmysql_fetch_assoc($res)) {
  echo "<li><a target=_blank href=menu.php?logAs=" . $row["id"] . (($t = $request["season"]) ? "&season=$t" : '') . '>' . $row["name"] . ' ' . $row["firstName"] . '</a></li>';
}
echo "</ul></tr>";
//echo "<tr><td class=tabLink><a target=_blank href=importCalCP.php$basePath>Mise à jour Calendrier avec CP.</a></td></tr>";
echo "<tr><td class=tabLink><a target=_blank href=getClubs.php$basePath>Extraire les clubs.</a></td></tr>";
//echo "<tr><td class=tabLink><a target=_blank href=clubNamur.php?season=11>Mise à jour club avec 'CP Namur'.</a></td></tr>";
//echo "<tr><td class=tabLink><a target=_blank href=pic.php$basePath>Voir la liste des photos manquantes</a></td></tr>";
echo "<tr><td class=tabLink><a target=_blank href=calendarBuild.php$basePath>Construire calendrier avec xls</a></td></tr>";
echo "<tr><td class=tabLink><a target=_blank href=genConvention.php>Génération des conventions</a></td></tr>";
echo "<tr><td class=tabLink><a target=_blank href=nextYear.php$basePath>Mise à jour saison</a></td></tr>";

