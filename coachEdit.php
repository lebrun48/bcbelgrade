<?php
include "admin.php";
include "common/tableTools.php";
//dump($request,"request");
//echo "action=$action".NL;
//$seeRequest=1;

if ($action == "save") {
  if (!$request["_val_END"])
    $request["_val_END"] = '=null';
  if (!$request["_val_ASSEMBLE"])
    $request["_val_ASSEMBLE"] = '=null';
  if (!$request["_val_TRI"])
    $request["_val_TRI"] = '=null';
}
selectAction();

if ($action == "edit") {
  include "common/tableRowEdit.php";
  include "head.php";

  $col = array("NUM"       => array(0, "Nom Prénom"),
      "TEAM"      => array(0, "Equipe"),
      "TEAM_VIEW" => array(0, "Liste des équipes"),
      "ASSEMBLE"  => array(0, "Assembler ses équipes"),
      "END"       => array(0, "Terminé"),
      "TRI"       => array(0, "Ordonné par N°"),
  );

  class CoachEdit extends TableRowEdit
  {

    function applyValue(&$key, &$format, &$row)
    {
      switch ($key) {
        case "NUM":
          $res = jmysql_query("select num,concat(name,' ',firstName) from members"
                  . " where type<>0 and (category&" . (CAT_COACH | CAT_ASSIST) . " or category2 &" . (CAT_COACH | CAT_ASSIST) . " or category3&" . (CAT_COACH | CAT_ASSIST)
                  . " or category=" . CAT_REMP_COACH . " or category2=" . CAT_REMP_COACH . " or category3=" . CAT_REMP_COACH . " or category4=" . CAT_REMP_COACH . " or category5=" . CAT_REMP_COACH . " or category6=" . CAT_REMP_COACH . ")"
                  . " order by name");
          echo '<select name="_val_num">';
          while ($r = jmysql_fetch_row($res))
            echo "<option value=$r[0]" . ($r[0] == $row[$key] ? " selected=selected" : '') . ">$r[1]</option>";
          echo '</select>' . nl;
          break;

        case "TEAM":
        case "TEAM_VIEW":
          $t = array_filter(explode('-', $row[$key]));
          echo "<select name=_val_$key multiple=multiple><option value=''" . ($row[$key] ? '' : " selected=selected") . "></option>";
          $cat = getExistingCategories("category&" . CAT_PLAYER . " and type<>0 and", false);
          foreach ($cat as $k => $v) {
            $k &= 0xFFFF;
            echo "<option value=-$k" . (array_search($k, $t) !== false ? " selected=selected" : '') . ">$v</option>";
          }
          echo "</select>";
          break;


        case "END":
        case "TRI":
        case "ASSEMBLE":
          echo "<input name=_val_$key value=1 type=checkbox" . ($row[$key] ? " checked=checked>" : ">");
          break;

        default:
          return TableRowEdit::applyValue($key, $format, $row);
      }
    }

    
    function applySave()
    {
      ?>
      el=document.getElementsByName("_val_TEAM")[0];
      idx=-1;
      for (i=el.length-1; i>=0; i--)
      if (el.options[i].selected)
      {
      if (idx==-1)
      idx=i;
      else
      el.options[idx].value+='-'+el.options[i].value;
      }
      if (idx>0)
      el.options[idx].value+='-';                                    
      el=document.getElementsByName("_val_TEAM_VIEW")[0];
      idx=-1;
      for (i=el.length-1; i>=0; i--)
      if (el.options[i].selected)
      {
      if (idx==-1)
      idx=i;
      else
      el.options[idx].value+='-'+el.options[i].value;
      }
      if (idx>0)
      el.options[idx].value+='-';
      el=document.getElementsByName('_val_TRI')[0];
      if (el.checked)
      el.value='1';                                    
      <?php
    }

  }

  $obj = new CoachEdit;
  $obj->title = "Edition de coach";
  $obj->colDef = &$col;
  $obj->editRow();
  exit();
}

include "common/tableEdit.php";

$col = array("name"      => array(20, "Nom Prénom", "width:150", NULL, "concat(members.name, ' ', members.firstname)"),
    "team"      => array(0, "Equipe", "width:70"),
    "team_view" => array(0, "Liste des équipes", "width:300"),
    "assemble"  => array(0, "Assembler", "width:10", "text-align:center"),
    "end"       => array(0, "Terminé", "width:10", "text-align:center"),
    "tri"       => array(0, "Tri par N°", "width:40", "text-align:center"),
);

include "head.php";
//table itself
echo '<title>Coaches';
echo '</title></head><body onload=genericLoadEvent()>';

class Coaches extends TableEdit
{

//---------------------------------------------------------------------------------------------------
  function buildKey($row)
  {
    return "coach where num=" . $row["num"];
  }

//---------------------------------------------------------------------------------------------------
  public function applyDisplay($key, $val, $row, $idxLine)
  {
    switch ($key) {
      case "team":
      case "team_view":
        $t = strtok($row[$key], "-");
        while ($t) {
          $v .= ($v ? "<br>" : "") . getCategory($t | CAT_PLAYER, true);
          $t = strtok("-");
        }
        return TableEdit::applyDisplay($key, $val, $row, $idxLine, $v);

      case "end":
      case "tri":
      case "assemble":
        return TableEdit::applyDisplay($key, $val, $row, $idxLine, $row[$key] ? "X" : "");

      default:
        return TableEdit::applyDisplay($key, $val, $row, $idxLine);
    }
  }

  public function applyFilter($key, &$val)
  {
    switch ($key) {
      default:
        return TableEdit::applyFilter($key, $val);
    }
  }

}

$coaches = new Coaches;
$coaches->colDef = &$col;
$coaches->addCol = "coach.num";
$coaches->visible = &$visible;
$coaches->tableName = "coach,members";
$coaches->tableId = "members";
$coaches->defaultWhere = "members.num=coach.num and";
$coaches->bHeader = true;
$coaches->bInsertNew = true;
$coaches->build();
?>
