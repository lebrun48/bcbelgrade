<?php

include "admin.php";
include "common/tableTools.php";
include "common/tableEdit.php";

$col = array("num"  => array(20, "Team", "width:150"),
    "date" => array(0, "Date", "width:70"),
    "com"  => array(0, "Commentaire", "width:300"),
);
$visible = array("num", "date", "com");

include "head.php";
//table itself
echo '<title>com';
echo '</title></head><body>';

class Comment extends TableEdit
{

  public function applyDisplay(&$key, &$val, &$row, $idxLine)
  {
    switch ($key) {
      case "num":
        $r = getRealCoach($row["num"]);
        $id = $r[0]["id"];
        $beg = new DateTime($row["date"]);
        $beg = mktime(12, 0, 0, $beg->format("n"), $beg->format("j"), $beg->format("Y"));
        return $this->formatValue("<a style=\"color:black\" href=\"presencesClub.php?id=$id&team=" . $row["num"] . "&begin=$beg\">" . getCategory($row["num"] | CAT_PLAYER, true) . "</a>", $val);

      default:
        return TableEdit::applyDisplay($key, $val, $row, $idxLine);
    }
  }

  public function applyFilter(&$key, &$val)
  {
    switch ($key) {
      case "num":
        return "num in (select num&0xFFFF from category where name like '%$val%')";

      case "com":
        return "com is not null";

      default:
        TableEdit::applyFilter($key, $val);
    }
  }

}

$comment = new Comment;
$comment->colDef = &$col;
$comment->tableId = "Commentaires présences";
$comment->visible = &$visible;
$comment->tableName = "com_presences";
$comment->tableId = "members";
$comment->filter["com"] = "null";

$comment->build();
?>
