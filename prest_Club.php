<?php
include "coach.php";
if (!$_GET["bp"] && !$request["skip"])
  include "check.php";
include "head.php"
?>
<title>Encodage prestations NBCB          
</title>     
</head>     
<body>          <h1>Prestations du lundi               
    <?php
    echo prDate(0) . ' au dimanche ' . prDate(6) . '</h1>';
    if (empty($_GET["begin"]))
      echo "<p>Bonjour ";
    echo $coach->prenom . ",</p>";
    $d = array("Lundi ", "Mardi ", "Mercredi ", "Jeudi ", "Vendredi ", "Samedi ", "Dimanche ");
//echo "SELECT * FROM prestations WHERE num=".$coach->num." AND DATE='".$date."'".NL;
    $res = jmysql_query("SELECT * FROM prestations WHERE num=" . $coach->num . " AND DATE='" . $date . "'");
    if (!$res)
      stop('Invalid query: ' . jmysql_error());
    $bExist = jmysql_num_rows($res);
    if (!$bExist) {
      ?>                 
      <p>Pour remplir ce formulaire indique simplement le nombre de prestations <u>effectu&eacute;es</u> en face du jour correspondant.         
      </p>               
      <p>Si&nbsp;tu as &eacute;t&eacute; remplac&eacute; pour un entra&icirc;nement ou un match mets "A" (Absent) ou "0"&nbsp; dans le la colonne <b>"Nombre Prestations"</b> et indique le nom de ton rempla&ccedil;ant dans la colonne <b>"Raison"</b>, de m&ecirc;me si tu as remplac&eacute; un entraineur indique &eacute;galement son nom dans la m&ecirc;me derni&egrave;re colonne.         
      </p>               
      <p>Une fois rempli clique sur le bouton <b>"Enregistrer"</b>.         
      </p>
      <?php
    } else {
      $prest = jmysql_result($res, 0, "PREST");
      ?>        
      <p>Le formulaire est d&eacute;j&agrave; rempli pour la semaine indiqu&eacute;e ci-dessus. Le tableau contient les valeurs entr&eacute;es &agrave; la date du                 
        <?php
        echo jmysql_result($res, 0, "UPD_TIME");
        echo "</p>";
        if ($status = jmysql_result($res, 0, "STATUS") >= 1)
          echo "<p>Le paiement a déjà été effectué pour cette semaine, tu ne peux plus modifier les données</p>";
        else {
          ?>               
        <p>Cependant si tu dois faire une correction, tu as la possiblilit&eacute; de modifier ces valeurs. Elles seront prises en compte dans ton prochain paiement.         
        </p>             
        <p><b>Attention: </b>Si tu ajoutes des prestations supplémentaires à tes encodages de présences d'équipes, tu dois donner une raison dans la dernière colonne.         
        </p>
        <?php
      }
    }
    ?>             
    <form autocomplete=off method="post" action="savePrest.php<?php echo $basePath . ($bList ? "&begin=$begin" : "") . ($right == ROOT ? "&user=root" : "") ?>">                      
      <table class=main>                            
        <tr>                                    
          <th>Jour               
          </th>                                    
          <th>Nombre                                
            <br>&nbsp;Prestations               
          </th>                                    
          <th>Raison               
          </th>                              
        </tr>        
        <?php
        $par = 0;
        for ($i = 0; $i < 7; $i++) {
          echo "<tr class=\"parity$par\"><td>";
          $par = 1 - $par;
          echo $d[$i] . prDate($i) . "</td>";
          echo "<td style=\"text-align: center;\">";
          echo '<input name="prestation' . $i . '"';
          if ($bExist && $prest[$i] != ' ') {
            echo 'value="' . $prest[$i] . '"';
            if ((int) $prest[$i]) {
              $nPrest += (int) $prest[$i];
              $nDep += 2;
            }
          }
          echo 'maxlength="1" size="1" type="text"></td><td>';
          echo '<input name="remplacement' . $i . '"';
          if ($bExist) {
            $val = jmysql_result($res, 0, "REMP" . $i);
            if (!empty($val))
              echo 'value="' . $val . '"';
          }
          echo 'maxlength="50" size="60" type="text"></td></tr>';
        }
        echo "</table>";
        if ($right == ROOT) {
          echo '<p>Entre le nombre de prestations : <input name="nPrest" maxlength="4" size="5" type="text" ' . ($bExist ? 'value="' . jmysql_result($res, 0, "NPREST") . '"' : $nPrest) . '></p>';
          echo '<p>Entre le nombre de déplacements: <input name="nDep" maxlength="4" size="5" type="text"></p>';
        }
        ?>             
        <p>Indique un &eacute;ventuel commentaire ci-dessous</p>
        <textarea wrap="soft" cols="60" rows="3" name="commentaire"><?php if ($bExist) echo jmysql_result($res, 0, "COM"); ?></textarea>             
        <input id="dummySave" type="submit" style="visibility:hidden">
        <p>
          <?php
          if (!$request["skip"]) {
            include "tools.php";
            createAction(200, "Enregister", "save()");
            if ($bList)
              createButton(-1, "Retour à la liste", "listePrest.php$basePath" . ($right == ROOT ? "&user=root" : ""));
            if ($bRoot = $request["user"] == "root") {
              echo "<input name=status id=status type=hidden>";
              createAction(-1, "Sauver ET garder status", "keepStatus()");
            }
          }
          ?>
        </p><br><br>
        </form>     
        </body>
        <script>
          function save()
          {
<?php
if ($bRoot && $status > 0)
  echo "if (!confirm('Attention: cette semaine est déjà clôturé. Es-tu sûr de la réouvrir?')) return;"
  ?>
            document.getElementById('dummySave').click();
          }

          function keepStatus()
          {
            $('#status').val('x');
            document.getElementById('dummySave').click();
          }
        </script>
        </html>
