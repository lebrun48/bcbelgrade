<?php
checkRights();
unset($request[$request["delAttach"]]);
include "head.php";
//dump($request);
?>
<!-- Place inside the <head> of your HTML -->
<script src="/tools/ckeditor/ckeditor.js"></script>

<!-- Place this in the body of the page content -->
<style>
  .selection {width:300px;height:300px;border:1px solid black;font-size:small;font-family:calibri;overflow-y:auto;cursor:pointer}
  .icones {margin-top:4px;margin-left:5px;height:18px;margin-right:5px}
  .select_mod {width:100%;padding-left:10px}
  .select_mod:hover {color:white;background:darkblue;cursor:pointer}
  .list{font-size:x-small;background:lightgray;padding:2px;margin:2px}
  .delAttach{margin-right:10px;cursor:pointer}
  .box{
    -moz-border-radius: 10px;
    -webkitborder-radius: 10px;
    /*border-radius: 10px;*/
    border: 1px solid rgb(182,182,182);
    padding:10px;
    background: #ffffff;
    background: -moz-linear-gradient(top,  #ffffff 0%, #cfd1cf 100%);
    background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#ffffff), color-stop(100%,#cfd1cf));
    background: -webkit-linear-gradient(top,  #ffffff 0%,#cfd1cf 100%);
    background: -o-linear-gradient(top,  #ffffff 0%,#cfd1cf 100%);
    background: -ms-linear-gradient(top,  #ffffff 0%,#cfd1cf 100%);
    background: linear-gradient(to bottom,  #ffffff 0%,#cfd1cf 100%);
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#cfd1cf',GradientType=0 );
  }

</style>

<?php
//dump($request);
echo "<title>Mailing</title></head><body>";
echo "<h2 style=width:100%;text-align:center>Mailing</h2>";
echo "<form id=form action=" . $_SERVER["SCRIPT_NAME"] . " method=post enctype='multipart/form-data'><input id=submit type=submit style=display:none><input id=action type=hidden name=action><input type=hidden name=id value=$id><input type=hidden name=list value=" . $request["list"] . "><input type=hidden name=name value='" . htmlspecialchars($request["name"], ENT_QUOTES) . "'>";
echo "<input type=file  onchange=getFile() name=upload id=upload style=position:absolute;top:-20000px;/>";
echo "<input name=nums type=hidden value='$nums'>";

echo "<div class=box><table><tr><td><b>De : </b></td><td>$from</td></tr>";

echo "<tr><td><b>Pour : </b></td><td>";
$i = 0;
while ($tup = jmysql_fetch_row($res)) {
  echo "<span class=list>$tup[0]</span>,";
  if (++$i % 10 == 0)
    echo "<br>";
}
echo "</td></tr> ";
echo "<tr><td><b>Sujet : </b></td><td><input id=subject size=50 type=text name=subject value='" . htmlspecialchars($request["subject"], ENT_QUOTES) . "'>$otherFunction</td></tr>";
foreach ($request as $k => $v)
  if (substr($k, 0, 6) == "attach") {
    if (!$bFirst) {
      echo "<tr><td><input type=hidden name=delAttach><b>Fichiers:</b></td><td>";
      $bFirst = true;
    }
    echo "<input type=text name=$k value='" . ($t = htmlspecialchars($v)) . "'><img class=delAttach onclick=remAttach('$k') src=common/b_drop.png>";
  }
getFile();
if ($bFirst)
  echo "</td></tr>";

echo "</table></div><textarea class=ckeditor id=template_content name=_val_body>" . $request["_val_body"] . "</textarea></form>";

//------------------------------------------------------------------------------------------------------------------
function getFile()
{
  global $bCheck, $bFirst;
  if (!$_FILES["upload"] || !($n = $_FILES["upload"]["tmp_name"]))
    return;
  //dump($_FILES["upload"]);
  if ($t = $_FILES["upload"]["error"]) {
    include "tools.php";
    alert("Une erreur s'est produite lors du téléchargement ($t)");
    return;
  }
  $n1 = "attach" . substr($n, strrpos($n, '/') + 4);
  move_uploaded_file($n, "mailAttach/$n1");
  if (!$bFirst) {
    echo "<tr><td><input type=hidden name=delAttach><b>Fichiers:</b></td><td>";
    $bFirst = true;
  }
  echo "<input type=text name=$n1 value='" . htmlspecialchars($_FILES["upload"]["name"], ENT_QUOTES) . "'><img class=delAttach onclick=remAttach('$n1') src=common/b_drop.png>";
  $bFirst = true;
}
?>

<script>
  var openName = '<?php echo addslashes($request["name"]) ?>';
  function getFile() {
    $(document).css("cursor", "wait");
    $('#action').val('');
    document.getElementsByName("name")[0].value = openName;
    document.getElementById('submit').click();
  }
  function remAttach(n) {
    $('#action').val('');
    document.getElementsByName('delAttach')[0].value = n;
    document.getElementById('submit').click();
  }

  $(function () {

    CKEDITOR.replace('template_content',
            {
              dialog_noConfirmCancel: true,
              height: '25em',
              extraPlugins: 'mailing',
              toolbar:
                      [
                        {name: 'document', items : [<?php $bRoot ? "'Source'," : '' ?> 'open', 'saveAs', 'NewPage', 'DocProps', 'view', 'attachFile', 'sendMail' ]},
                        {name: 'clipboard', items: ['insertField', 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo']},
                        {name: 'editing', items: ['Find', 'Replace', '-', 'SelectAll', '-', 'SpellChecker', 'Scayt']},
                        '/',
                        {name: 'basicstyles', items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat']},
                        {name: 'paragraph', items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock']},
                        {name: 'links', items: ['Link', 'Unlink']},
                        {name: 'insert', items: ['Table', 'HorizontalRule', 'Smiley', 'SpecialChar']},
                        '/',
                        {name: 'styles', items: ['Styles', 'Format', 'Font', 'FontSize']},
                        {name: 'colors', items: ['TextColor', 'BGColor']},
                        {name: 'tools', items: ['Maximize']}

                        /*
                         { name: 'document', groups: [ 'mode', 'document', 'doctools' ], items: [ 'Source', '-', 'Save', 'NewPage', 'Preview', 'Print', '-', 'Templates' ] },
                         { name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },
                         { name: 'editing', groups: [ 'find', 'selection', 'spellchecker' ], items: [ 'Find', 'Replace', '-', 'SelectAll', '-', 'Scayt' ] },
                         { name: 'forms', items: [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ] },
                         '/',
                         { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ] },
                         { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language' ] },
                         { name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] },
                         { name: 'insert', items: [ 'Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe' ] },
                         '/',
                         { name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
                         { name: 'colors', items: [ 'TextColor', 'BGColor' ] },
                         { name: 'tools', items: [ 'Maximize', 'ShowBlocks' ] },
                         { name: 'others', items: [ '-' ] },
                         { name: 'about', items: [ 'About' ] }
                         */

                      ],
            });


  });
</script>
