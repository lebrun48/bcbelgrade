<?php

//$seeRequest=1;
function buildSumaryPres($bUpd = false, $bViewStop = false, $bAdm = false)
{
  global $team, $last, $id, $yearSeason, $fin, $player, $bRoot, $coach, $basePath;
  if (!isset($fin))
    $fin = mktime(0, 0, 0, 7, 20, $yearSeason);
  $saison = 0;
  $wk = 0;
  if ($coach->assemble) {
    $teamFull = '(';
    $t = strtok($coach->team, '-');
    while ($t) {
      $teamFull .= ($t | CAT_PLAYER) . ",";
      $t = strtok('-');
    }
    $teamFull = substr($teamFull, 0, -1) . ')';
  } else
    $teamFull = '(' . ($team | CAT_PLAYER) . ')';
  $fin = new DateTime("@$fin");
  $fin = mktime(0, 0, 0, $fin->format("n"), $fin->format("j"), $fin->format('Y'));
  for ($d = $last; $d >= $fin; $d -= 7 * 24 * 3600) {
    $dt = new DateTime('@' . $d);
    $date = $dt->format('Y-m-d');
    $res = jmysql_query("select num, pres, coti_state, reason from presences where date='$date' and team=$team and num in (select num from members where " . ($bViewStop ? '' : "type<>0 and ") . "(category in $teamFull or category2 in $teamFull or category3 in $teamFull))");
    $nPres = array(0, 0, 0, 0, 0, 0, 0);
    while ($row = jmysql_fetch_assoc($res)) {
      $pres = $row["pres"];
      $num = $row["num"];
      $all[$num][$wk][0] = $pres;
      $all[$num][$wk][1] = $row["reason"];
      $all[$num][$wk][2] = $row["coti_state"];
      $bPres = false;
      for ($j = 6; $j >= 0; $j--) {
        $dt = new DateTime('@' . ($d + $j * 24 * 3600));
        $m = (int) $dt->format('m');
        $nPres[$j] |= $pres[$j];
        if ($pres[$j]) {
          $bPres = true;
          $allSaison[$num][0] = $d + $j * 24 * 3600;
        }
        $totPlayerMonth[$num][$m] += $pres[$j];
        if (!$max)
          $max = $m;
        $min = $m;
      }
      if ($bPres) {
        $allSaison[$num][1] = $saison;
        for ($j = 6; $j >= 0; $j--)
          $allSaison[$num][1] += $nPres[$j];
      }
    }

    $tmp = "       ";
    for ($j = 6; $j >= 0; $j--) {
      $tmp[$j] = $nPres[$j];
      $dt = new DateTime('@' . ($d + $j * 24 * 3600));
      $m = $dt->format('n');
      $nTotPrest[$m] += $nPres[$j];
      $saison += $nPres[$j];
    }
    $all[0][$wk][0] = $tmp;
    $wk++;
  }

  echo "<table class=main id=members><tr><th style=width:220;min-width:220>NOM Prénom</th><th style=width:40;min-width:40>N°</th><th>" . ($player ? "Période" : "Saison") . "</th>";
  $bCont = true;
  $wk = 0;
  $days = array(1 => "Lu", "Ma", "Me", "Je", "Ve", "Sa", "Di");
  for ($d = $last; $d >= $fin; $d -= 7 * 24 * 3600) {
    for ($j = 6; $j >= 0; $j--) {
      if ($all[0][$wk][0][$j]) {
        $dt = new DateTime("@" . ($d + $j * 24 * 3600));
        echo "<th class=days>" . $days[$dt->format("N")] . "<br>" . ($bUpd ? "<a href=presencesClub.php?id=$id&team=$team&begin=$d" . ($bRoot ? "&user=root" : '') . ">" . $dt->format("d/m") . "</a>" : $dt->format("d/m")) . "</th>";
      }
    }
    $wk++;
  }
  /* for ($i=$max; $i && $bCont; $i--)
    {
    if ($i==0)
    $i=12;
    if ($i==$min)
    $bCont=false;
    echo "<th style=\"min-width:110; width:110\">$months[$i]</th>";
    } */

  echo "</tr>" . nl;
  $res = jmysql_query("select num, name, status, firstName, dateIn, numEquip as numEquip1, numEquip2, numEquip3,category as category1,category2,category3,type from members where $player " . ($bViewStop ? '' : "type<>0 and ") . "(category in $teamFull or category2 in $teamFull or category3 in $teamFull) order by name");
  $par = 0;
  while ($row = jmysql_fetch_assoc($res)) {
    $num = $row["num"];
    $tot = 0;
    $in = 0;
    if (!empty($totPlayerMonth[$num]))
      foreach ($totPlayerMonth[$num] as $m => $val)
        $tot += $val;
    $date = $row["dateIn"];
    if (!$date)
      $date = "1990-01-01";
    if ($bNew = ($date > "$yearSeason-08-01"))
      $t = $allSaison[$num][1];
    else
      $t = $saison;
    if (!$row["type"] && !$tot)
      continue;
    echo "<tr class=parity$par" . (!$row["type"] ? " style=text-decoration:line-through>" : '>');
    $par = 1 - $par;

    echo "<td>" . getImgCotiState($row) . $row["name"] . ' ' . $row["firstName"] . "</td>";
    $tm = strtok($teamFull, '(,)');
    unset($n);
    while ($tm) {
      for ($i = 1; $i < 4; $i++)
        if ($row["category$i"] == $tm) {
          $n .= $row["numEquip$i"] . "-";
          break;
        }
      $tm = strtok(",)");
    }
    $n = substr($n, 0, -1);
    echo "<td style=text-align:center>$n</td>";
    if ($t)
      echo "<td style=white-space:nowrap>&nbsp;$tot/$t (" . (int) ($tot * 100 / $t) . "%)&nbsp;</td>";
    else
      echo "<td>&nbsp;</td>";
    $bCont = true;
    $wk = 0;
    $cotiColor = array("green", "orange", "red");
    for ($d = $last; $d >= $fin; $d -= 7 * 24 * 3600) {
      for ($j = 6; $j >= 0; $j--) {
        if ($all[0][$wk][0][$j]) {
          if ($bNew && ($d + $j * 24 * 3600) < $allSaison[$num][0])
            echo "<td>&nbsp;</td>";
          else {
            echo "<td style=text-align:center";
            if (!$all[$num][$wk][0][$j]) {
              if ($all[$num][$wk][1]) {
                echo ";font-size:small title='" . htmlspecialchars($all[$num][$wk][1], ENT_QUOTES) . "'>... ";
              } else
                echo ">";
            } else {
              echo ($bAdm ? ";color:" . $cotiColor[$all[$num][$wk][2][$j]] : '') . ">P";
            }
            echo "</td>";
          }
        }
      }
      $wk++;
    }
    /* for ($i=$max; $i && $bCont; $i--)
      {
      if ($i==0)
      $i=12;
      if ($i==$min)
      $bCont=false;
      if (!$totPlayerMonth[$num][$i])
      echo "<td>&nbsp;0/".$nTotPrest[$i]." (0%)</td>";
      else
      echo "<td>&nbsp;".$totPlayerMonth[$num][$i]."/".$nTotPrest[$i]." (".(int)($totPlayerMonth[$num][$i]*100/$nTotPrest[$i])." %)&nbsp;</td>";
      if ($i==$in)
      $in=0;
      } */
    echo "</tr>" . nl;
  }
  echo "</table>";
}

?>