<?php

include_once "common/common.php";
define("DISPLAY", 0);
define("UPDATE", 1);
define("ROOT", 2);
if ($_GET["user"] == "root")
  $right = ROOT;
else
  $right = UPDATE;
$begin = $_GET["begin"];
$dt = $_GET["date"];
if (!empty($dt)) {
  $y = strtok($dt, "/");
  $m = strtok("/");
  $d = strtok("/");
  $begin = mktime(12, 0, 0, $m, $d, $y);
}
$last = time() - ($now->format('N') - 1) * 24 * 3600;
$bList = 0;
if (empty($begin))
  $begin = $last - ($now->format('N') == 7 ? 0 : 7 * 24 * 3600) + (23 - $now->format('G')) * 3600;
else if ($begin > time())
  stop(__FILE__, __LINE__);
else {
  if ($begin < mktime(0, 0, 0, 5, 15, $yearSeason))
    stop(__FILE__, __LINE__, null, null, true);
  $bList = 1;
  if ($_GET["cal"])
    $bList = 2;
}
$dt = new DateTime('@' . $begin);
if ($dt->format('N') != 1)
  stop(__FILE__, __LINE__);
$date = $dt->format('Y-m-d');

class Coach
{

  public $nom, $prenom, $num, $team, $teamView;

  public function __construct($res)
  {
    if (!$res) {
      $this->prenom = "Root";
      $this->num = 0;
    } else {
      $this->prenom = jmysql_result($res, 0, "firstName");
      $this->nom = jmysql_result($res, 0, "name");
      $this->num = jmysql_result($res, 0, "num");
      $res = jmysql_query("SELECT TEAM, TEAM_VIEW, END, TRI,ASSEMBLE FROM coach WHERE num=" . $this->num);
      if (!$res || jmysql_num_rows($res) != 1)
        stop(__FILE__, __LINE__, "Impossible to extract coach name", null, true);
      $this->team = jmysql_result($res, 0, "TEAM");
      $this->teamView = jmysql_result($res, 0, "TEAM_VIEW");
      $this->end = jmysql_result($res, 0, "END");
      $this->order = jmysql_result($res, 0, "TRI");
      $this->assemble = jmysql_result($res, 0, "ASSEMBLE");
    }
  }

}

if (empty($id)) {
  if ($right == ROOT)
    $coach = new Coach($res);
  else
    stop(__FILE__, __LINE__, "Invalid URL!!");
} else {
  $res = jmysql_query("SELECT num, name, firstName FROM members WHERE id='$id'");
  if (!$res || jmysql_num_rows($res) != 1)
    stop(__FILE__, __LINE__, "Impossible to extract coach name", "Désolé, vous n'avez pas accès à cette page.");
  $coach = new Coach($res);
  $r = jmysql_query("select events,rights from admin where num=$coach->num");
  if ($r && jmysql_num_rows($r)) {
    $admin["events"] = jmysql_result($r, 0);
    $admin["rights"] = jmysql_result($r, 0, 1);
  }
}

function prDate($offset)
{
  global $begin;
  $dt = new DateTime('@' . ($begin + $offset * 24 * 3600));
  return $dt->format('d/m');
}

?>