<?php

define('PLAYER_MONTHLY', 0);
define('PLAYER_BONUS', 1);
define('PLAYER_BONUS_WIN', 2);
define('PLAYER_PRESENCE', 3);
define('PLAYER_MATCH', 4);
define('PLAYER_SANCTION_PERC', 5);
define('PLAYER_SANCTION', 6);
define('PLAYER_SANCTION_PAID', 7);
define('PLAYER_SANCTION_PERC_AUTO', 8);
define('PLAYER_ABSENT', 9);
define('MONTHLY', 10);
define('PREST', 11);
define('MOVE', 12);
define('HOME', 13);
define('ONE_SHOT', 15);
define('PREST_FICT', 16);
define('YEAR_ACCOUNT', 19);
define('ACCOUNT', 20);
define('MONEY', 21);
define('DISTR_ACCOUNT', 22);
define('NO_MAIL', 25);
define('ENCLOSE_MONTH', 30);
define('PLAYER_DESC', 40);
define('COACH_DESC', 41);

$aType = array(PLAYER_MONTHLY            => "Indemnité joueur mensuel",
    PLAYER_BONUS              => "Prime par point",
    PLAYER_BONUS_WIN          => "Prime par match gagné",
    PLAYER_PRESENCE           => "Indemnité présence",
    PLAYER_MATCH              => "Indemnité par match",
    PLAYER_SANCTION_PERC      => "Application réglement",
    PLAYER_SANCTION           => "Réduction joueur",
    PLAYER_SANCTION_PAID      => "Sanction calculée",
    PLAYER_SANCTION_PERC_AUTO => "Absence auto",
    PLAYER_ABSENT             => "Déduction par absence",
    MONTHLY                   => "Mensuel",
    PREST                     => "Prestation coach",
    MOVE                      => "Remboursement déplacement",
    HOME                      => "Distance domicile",
    ONE_SHOT                  => "One shot",
    PREST_FICT                => "Prestations équivalente",
    YEAR_ACCOUNT              => "Saison passée",
    ACCOUNT                   => "Sur compte",
    MONEY                     => "Liquide",
    DISTR_ACCOUNT             => "Mix",
    ENCLOSE_MONTH             => "Clôturer mois",
    PLAYER_DESC               => "Description joueur",
    COACH_DESC                => "Description entraineur",
    NO_MAIL                   => "Pas de mail"
);

define('PAY_KM_PLAYER', 0.3542);
define('PAY_KM_COACH', 0.542);
define('MAX_YEAR_KM', 2000);
define('PAY_YEAR_PLAYER', 1388.40);
define('PAY_YEAR_COACH', 2549.90);
define('PAY_MONTH', 500);
define('MV_WEEK', 30);

define('BASE', 11.75);
define('Ancienneté', 0.25);
define('NIVEAU', 0.40);
define('PAY_KM_REAL', 0.10);

define('WIN_MATCH', 10);

$prestDsc = array(
    0   => "Base prestation",
    1   => "Niveau formation",
    2   => "Ancienneté",
    3   => "Frais de déplacement",
    4   => "Bonus",
    5   => "Distance domicile",
    10  => "Mensuel",
    11  => "Nb entrainement semaine",
    12  => "Nb matches saison",
    20  => "Indemnité par entrainement",
    21  => "Indemnité par match",
    22  => "Prime match gagné (si aligné)",
    23  => "Prime par point",
    24  => "Déduction absence",
    30  => "Nb entrainement semaine",
    34  => "Nb entrainement saison",
    31  => "Nb matches saison",
    32  => "Nb matches gagné",
    33  => "Indemnité fixe par mois",
    100 => "Paiement mix",
    101 => "Paiement compte",
    102 => "Paiement liquide",
);

function computeCost($obj, $k, $nMonth, &$prest, &$sum)
{
  $val = $obj[$k];
  switch ($val->type) {
    case 0: $s = $val->amount ? $val->amount : BASE;
      break;
    case 1: $s = ($val->value + 1) * NIVEAU;
      break;
    case 2: $s = $val->value * Ancienneté;
      break;
    case 3: $s = $val->value * PAY_KM_REAL * 2;
      break;
    case 4: $s = $val->amount;
      break;
    case 101: return $val->amount;
      break;
    case 10: case 33: $s = $val->amount;
      $sum += $s * $nMonth;
      return $s;
    case 11: $s = $val->value * (round($nMonth * 30 / 7, 0) - 2) * $prest;
      break;
    case 12: $s = $val->value * $prest;
      break;
    case 20: case 21: case 22: case 24: $s = $val->amount;
      break;
    case 30: case 31: case 32: case 34:
      $i = $val->type == 34 ? 20 : $val->type - 10;
      if ($obj)
        foreach ($obj as $v)
          if ($v->type == $i) {
            $s = $val->value * $v->amount;
            if ($val->type == 30)
              $s *= (round($nMonth * 30 / 7, 0) - 2);
            $sum += $s;
            return $s;
          }
    default:
      return 0;
  }
  if ($val->type % 20 < 10)
    $prest += $s;
  else
    $sum += $s;
  return $s;
}

?>
