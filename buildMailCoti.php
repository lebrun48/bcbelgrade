<?php

//------------------------------------------------------------------------------------------------------
function checkRights()
{
  global $bRoot;
  if (!$bRoot)
    stop(__FILE__, __LINE__, "invalid id", true);
}

//------------------------------------------------------------------------------------------------------
function specific($key, $row)
{
  switch ($key) {
    case "email":
      $m = "<";
      if ($t = $row["emailF"]) {
        if ($t[0] == '-')
          $t = substr($t, 1);
        $m .= "$t>";
      }
      if ($t = $row["emailM"]) {
        if ($t[0] == '-')
          $t = substr($t, 1);
        $m .= substr($m, -1) == '>' ? ",$t" : "$t>";
      }
      if ($t = $row["emailP"]) {
        if ($t[0] == '-')
          $t = substr($t, 1);
        $m .= substr($m, -1) == '>' || strlen($m) > 1 ? ",$t" : "$t>";
      }
      return $m;

    case "signing":
      return "<pre style=color:gray;><i>" . htmlspecialchars(TREASURER_NAME_CLUB) . "<br>" .
              "Tr&eacute;sorier " . NAME_CLUB . NL .
              "GSM:" . TREASURER_PHONE_CLUB . "</i></pre>";

    case "from":
      return mb_encode_mimeheader(TREASURER_NAME_CLUB) . " <" . TREASURER_MAIL_CLUB . ">";
  }
}

include "common/common.php";
//------------------------------------------------------------------------------------------------------
//Ajax request
switch ($action = $request["action"]) {
  case "getMail":
    //get single mail name
    $r = jmysql_query("select concat(name,' ',firstName),concat(emailF,',',emailM,',',emailP) from members where num=" . $request["num"]);
    //Get single mail data
    $query = "select id,name,firstName from members where num=" . $request["num"];
    break;

  case "sendMail":
    //dump($request);
    $query = "select id,name,firstName,emailF,emailM,emailP from members,cotisations where members.num in (" . $request["nums"] . ") and (emailF is not null or emailM is not null or emailP is not null) and cotisations.num=members.num and cotisations.previous is null";
    $to = "§name§ §firstName§ §email§";
    //$bTest=true;
    //$bSendResult=true;
    //$bDumpMail=true;
    $bResult = true;
    break;

  case "viewMail":
    checkRights();
    break;
}
//------------------------------------------------------------------------------------------------------
//other Ajax request (do not change)
if ($action)
  include "common/buildMailAjax.php";

//mailing variables must be here
?>
<script>

  function getVariables()
  {
    return [];
  }
</script>
<?php

//------------------------------------------------------------------------------------------------------
//main
//$from must contain the from 
//dump($request);
$from = TREASURER_NAME_CLUB . " &lt;" . TREASURER_MAIL_CLUB . "&gt;";
//$nums must contain the list
//$res must contain the dest name(s)
$res = jmysql_query("select concat(name,' ',firstName),members.num from members,cotisations where members.num in (" . $request["list"] . ") and cotisations.num=members.num and cotisations.previous is null");
while ($t = jmysql_fetch_row($res))
  $nums .= ",$t[1]";
$nums = substr($nums, 1);
jmysql_data_seek($res, 0);
//*otherFunction is added after mail
$otherFunction = "<input type=checkbox name=noSend value=true checked>Tester (pas d'envois)";

//------------------------------------------------------------------------------------------------------
//always at the end  (do not change)
include "common/buildMail.php";






