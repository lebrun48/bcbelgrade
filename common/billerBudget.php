<?php
if (!$_GET["action"] == "print")
  $bOnlyRoot = true;
include "admin.php";
include $request["tab"] . "Conf.php";
//dump($request);
//$seeRequest=1;
$sub = $request["subbudget"];
if (!$season = $request["year"])
  $season = $currentYear - 2000;
$bSubBudget = jmysql_num_rows(jmysql_query("show columns from " . TABLE_NAME . "_budget like 'subBudget'"));

switch ($request["action"]) {
//**********************************************************************************************************
//**********************************************************************************************************
  case "edit":
    include "tableTools.php";
    //dump($request);

    $col = array("subBudget" => array(0, "Sous budget",),
        "season"    => array(0, "Année"),
        "category"  => array(0, "Catégorie"),
        "amount"    => array(10, "Montant"),
        "updAmount" => array(10, "Mis à jour"),
        "official"  => array(10, "Officiel"),
        "ended"     => array(0, "Terminé")
    );
    if (!$bSubBudget)
      unset($col["subBudget"]);

    if (!isset($resultCategory)) {
      unset($col["official"]);
      unset($col["updAmount"]);
    } else if ($bOfficial) {
      unset($col["amount"]);
      unset($col["updAmount"]);
      $col["official"][1] = "Montant";
    }
    include "head.php";
    include "tableRowEdit.php";

    class BudgetEdit extends TableRowEdit
    {

      //-----------------------------------------------------------------------------------------------
      function getTuple()
      {
        global $sub;
        $row = parent::getTuple();
        $row["subBudget"] = $sub;
        return $row;
      }

      //----------------------------------------------------------------------------------------------
      function applyValue(&$key, &$format, &$row)
      {
        switch ($key) {
          case "subBudget":
            echo "<input type=hidden name=_val_subBudget value='$row[$key]'><span style=color:gray>" . $row["subBudget"] . "</span>";
            return;

          case "season":
            global $currentYear, $season;
            if (!$row[$key])
              $row[$key] = $season;
            echo "<input type=hidden name=_val_season value=$row[$key]><span style=color:blue>" . ($row[$key] + 2000);
            return;

          case "category":
            global $catSerial;
            echo "<select name=_val_category>";
            $ar = ($t = $row["subBudget"]) ? $catSerial[$t] : $catSerial;
            foreach ($ar as $k => $v)
              echo "<option value=$k" . ($row[$key] == $k ? " selected=selected" : '') . ">" . (!$v ? "&lt;aucune&gt;" : $v) . "</option>";
            echo "</select>";
            return;

          case "official":
          case "amount":
          case "updAmount":
            global $bOfficial;
            if (!isset($row["official"]) && $bOfficial)
              $row["official"] = $row["amount"];
            TableRowEdit::applyValue($key, $format, $row);
            echo " Calcul: <input type=text size=15 maxlength=50 onkeyup=compute(this,'$key')>";
            include_once "tools.php";
            createAction(-1, "Restore", "document.getElementsByName('_val_$key')[0].value=save");
            return;

          case "ended":
            echo "<input type=hidden name=_val_ended" . ($row[$key] ? " value=1" : '') . '>';
            echo "<input type=checkbox" . ($row[$key] ? " checked=checked" : '') . " onchange=changed(this)>";
            return;
        }
        TableRowEdit::applyValue($key, $format, $row);
      }

    }

    $obj = new BudgetEdit;
    $obj->title = "Edition de budget";
    $obj->colDef = &$col;
    $obj->editRow();
    ?>
    <script type="text/javaScript">
      function changed(obj)
      {
      document.getElementsByName('_val_ended')[0].value=obj.checked ? 1:'';
      }

      var save;
      function compute(obj, k)
      {
      el=document.getElementsByName("_val_"+k)[0];
      if (save==undefined)
      save=el.value;
      if (!obj.value.length)
      {
      el.value='';
      return;
      } 
      el.value=eval(obj.value);
      //unicode=e.keyCode? e.keyCode : e.charCode;
      //k=String.fromCharCode(unicode);
      }
    </script>
    <?php
    exit();

//**********************************************************************************************************
//**********************************************************************************************************
  case "print":
    //dump($request);
    $season = $request["year"] ? $request["year"] : $currentYear - 2000;
    ?>
    <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//FR">  
    <head>            
      <meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">            
      <link rel="shortcut icon" href="<?php echo LOGO_LITTLE_CLUB ?>" type="image/x-icon" />
    </head>
    <body>
      <?php
      $r = jmysql_query("select category," . ($bOfficial ? "ifnull(official,amount)" : "ifnull(updAmount,amount)") . " from " . $request["tab"] . "_budget where season=$season" . ($bSubBudget ? " and subBudget" . ($sub ? "='$sub'" : ' is null') : '') . " order by category");
      $n = -1;
      $ar = $sub ? $subCategory[$sub] : $category;
      while ($tup = jmysql_fetch_row($r)) {
        if ($tup[0] / 256 != $n)
          $n = $tup[0] / 256;
        $n1 = $tup[0] % 256;
        if (!isset($ar[$n][$n1]))
          continue;

        $sumCat[$n] += $tup[1];
        if ($tup[1])
          $cat[$n][$tup[0] % 256] = $tup[1];
      }
      arsort($sumCat);
      foreach ($cat as $k => &$v)
        arsort($v);
      ?>
      <style type="text/css">
        table {border-collapse:collapse;margin-left:auto;margin-right:auto;}
        tr {border:0px}
        td,th {padding-left:0.2cm;padding-right:0.2cm;border:0px;}
        a {color:black;}
      </style>

      <h3 style=margin-top:0px;width:100%;text-align:center;><img  width=50px style=vertical-align:middle;margin:15px src=<?php echo "$enterprise->logo>Budget" . ($sub ? " '$sub'" : '') . " $enterprise->name " . (isset($resultCategory) ? "saison " . ($season + 2000) . " - " . ($season + 2001) : "année " . ($season + 2000)) ?></h3>
      <table style=width:100%>
        <tr> 
          <td style=vertical-align:top;width:50%>
            <h3 style=width:100%;text-align:center>Recettes</h3>  
            <table border=2px style=width:90%;table-layout:fixed;white-space:nowrap>
              <tr style='border:2px solid black'>
                <th colspan=2 style=font-size:large>Catégorie</th>
                <th style='border-left:1px solid black;font-size:large'>Montant</th>
              </tr>
              <?php
              foreach ($sumCat as $k => $v) {
                if ($v < 0)
                  break;
                $income += $v;
                echo "<tr style='border-top:1px solid black'><td colspan=2 style=font-weight:bold>" . $ar[$k][0][0] . "</td><td style='border-left:1px solid black;text-align:right;color:darkgreen;font-weight:bold'>" . number_format($v, 0, '.', '.') . "€</td></tr>";
                if (sizeof($cat[$k]) > 1)
                  foreach ($cat[$k] as $kk => $vv)
                    echo "<tr><td style=padding-left:0.5cm;font-size:small>" . $ar[$k][$kk][0] . "</td><td style=text-align:right;font-size:small>" . number_format($vv, 0, '.', '.') . "€</td><td style='border-left:1px solid black'></td></tr>";
              }
              asort($sumCat);
              foreach ($cat as $k => &$v)
                if (is_array($v))
                  asort($v);
              ?>
            </table>
          </td>
          <td style=vertical-align:top>
            <h3 style=width:100%;text-align:center>Dépenses</h3>  
            <table border=2px style=width:95%;table-layout:fixed;white-space:nowrap>
              <tr style='border-bottom:2px solid black'>
                <th colspan=2 style=font-size:large>Catégorie</th>
                <th style='border-left:1px solid black;font-size:large'>Montant</th>
              </tr>
              <?php
              $ar = $sub ? $subCategory[$sub] : $category;
              foreach ($sumCat as $k => $v) {
                if ($v >= 0)
                  break;
                $outcome += $v;
                echo "<tr style='border-top:1px solid black'><td colspan=2 style=font-weight:bold>" . $ar[$k][0][0] . "</td><td style='border-left:1px solid black;text-align:right;color:darkred;font-weight:bold'>" . number_format(-$v, 0, '.', '.') . "€</td></tr>";
                if (sizeof($cat[$k]) > 1)
                  foreach ($cat[$k] as $kk => $vv)
                    echo "<tr><td style=padding-left:0.5cm;font-size:small>" . $ar[$k][$kk][0] . "</td><td style=text-align:right;font-size:small>" . number_format($vv, 0, '.', '.') . "€</td><td style='border-left:1px solid black'></td></tr>";
              }
              ?>
            </table>
          </td>
        </tr>
        <tr>
          <th style=font-size:large;height:1cm>Total Recettes : <span style=color:darkgreen><?php echo number_format($income, 0, '.', '.') ?>€</span></th> 
          <th style=font-size:large;height:1cm>Total Dépenses : <span style=color:darkred><?php echo number_format(-$outcome, 0, '.', '.') ?>€</span></th>
        <tr>
          <th colspan=2 style=font-size:large;height:1cm>Solde : <span style=color:dark<?php echo ($income + $outcome < 0 ? "red" : "green") . ">" . number_format($income + $outcome, 0, '.', '.') ?>€</span></th> 
      </table>
      <?php
      exit();

//**********************************************************************************************************
//**********************************************************************************************************
    case "nextSeason":
      $season++;
      dump($season);
      if ($season <= $currentYear - 1999 && jmysql_result(jmysql_query("select count(*) from " . TABLE_NAME . "_budget where season=$season"), 0) == 0) {
        $r = jmysql_query("select * from " . TABLE_NAME . "_budget where season=" . ($season - 1));
        while ($tup = jmysql_fetch_assoc($r))
          jmysql_query("insert into " . TABLE_NAME . "_budget (category,amount,official,season" . ($bSubBudget ? ",subBudget" : '') . ") values (" . $tup["category"] . ',' . ($tup["updAmount"] ? $tup["updAmount"] : $tup["amount"]) . ',' . ($tup["official"] ? $tup["official"] : "null") . ",$season" . ($bSubBudget ? "," . (($t = $tup["subBudget"]) ? "'$t'" : "null") : '') . ")", 1);
      }
      break;

//**********************************************************************************************************
//**********************************************************************************************************
    case "subBudget":
      include "tableTools.php";
      getRequest($obj);
      unset($obj->paramArray['subbudget']);
      echo "Entre le nom du sous budget: <input id=inp type=text><input type=button value=Ok onclick=location.assign('billerBudget.php" . buildAllParam($obj) . "&subbudget='+document.getElementById('inp').value)>";
      exit();

    case "delete":
      $request["_val_subBudget"] = $sub;
      break;
  }

//**********************************************************************************************************
//**********************************************************************************************************
  $bOnlyRoot = true;
  include "tableTools.php";
  SelectAction();

  include "tableEdit.php";
  include "head.php";
  include "tools.php";
  echo "<title>" . substr(TABLE_NAME, 6) . " budget</title></head><body onload=genericLoadEvent()>" . nl;
//-------------------------------------------------------------------------------------

  $col = array("category"  => array(-1, "Catégorie", "width:50"),
      "amount"    => array(10, "Montant", "width:50", "text-align:right"),
      "updAmount" => array(10, "Mis à jour", "width:50", "text-align:right"),
      "ended"     => array(10, "Terminé", "width:20", "text-align:center"),
      "official"  => array(10, "Officiel", "width:50", "text-align:right")
  );
  if ($bOfficial || !isset($resultCategory)) {
    unset($col["official"]);
    unset($col["updAmount"]);
  }

  class Budget extends TableEdit
  {

//----------------------------------------------------------------------------------------------
    function build()
    {
      global $currentYear, $sub, $season, $bSubBudget, $bOfficial, $basePath;
      if (!$this->order)
        $this->order = "category";
      $currentYear -= 2000;
      echo "<select name=year onchange=actionClick('filter','year',this.options[this.selectedIndex].value)><option value=$currentYear" . ($season == $currentYear ? " selected=selected" : '') . ">" . ($currentYear + 2000) . "</option>";
      $r = jmysql_query("select distinct season from " . TABLE_NAME . "_budget order by season desc");
      while ($tup = jmysql_fetch_row($r))
        if ($tup[0] != $currentYear)
          echo "<option value=$tup[0]" . ($tup[0] == $season ? " selected=selected>" : '>') . (2000 + $tup[0]) . "</option>";
      echo "</select>" . nl;
      if ($bSubBudget) {
        echo "<select name=subbudget onchange=actionClick('filter','subbudget',this.options[this.selectedIndex].value)>";
        $r = jmysql_query("select subBudget from " . TABLE_NAME . "_budget group by subBudget");
        while ($tup = jmysql_fetch_row($r))
          echo "<option value='$tup[0]'" . ($tup[0] == $sub ? " selected=selected>" : '>') . "$tup[0]</option>";
        echo "</select>" . nl;
      }


      $currentYear = $season + 2000;
      if ((!$bOfficial || isset($_GET["official"])))
        echo "<input type=checkbox " . ($bOfficial ? "checked=checked" : '') . " title='Billan officiel' onchange=location.replace('billerBudget.php${basePath}&tab=" . $_GET["tab"] . "&season=$season'+(this.checked?'&official=true':'&official=false'))>Officiel";
      createAction(-1, "Imprimer", "window.open('billerBudget.php" . buildAllParam($this, "action", "print") . "')");
      createAction(-1, "Prochaine saison", "window.open('billerBudget.php" . buildAllParam($this, "action", "nextSeason") . "')");
      if ($bSubBudget)
        createAction(-1, "Sous Budget", "window.open('billerBudget.php" . buildAllParam($this, "action", "subBudget") . "')");
      TableEdit::build();
    }

    //---------------------------------------------------------------------------------------------------
    function printLine(&$row)
    {
      global $catSerial, $bOfficial;
      if ($bOfficial && !isset($catSerial[$row["category"]]))
        return;
      parent::printLine($row);
    }

    //---------------------------------------------------------------------------------------------------
    public function applyDisplay(&$key, &$val, &$row, $idxLine)
    {
      $dispValue = null;
      switch ($key) {
        case "category":
          global $category, $subCategory;
          $ar = ($t = $row["subBudget"]) ? $subCategory[$t] : $category;
          $dispValue = $ar[$row[$key] / 256][0][0];
          if ($row[$key] % 256)
            $dispValue .= " > " . $ar[$row[$key] / 256][$row[$key] % 256][0];
          break;

        case "amount":
          global $bOfficial;
          $t = $bOfficial && ($t = $row["official"]) ? $t : $row[$key];
          if ($t > 0)
            $this->totalRec += $t;
          else
            $this->totalDep += $t;
          $dispValue = sprintf("%0.2f €", $t);
          if ($t < 0)
            $dispValue = "<span style=color:red>$dispValue</span>";
          break;

        case "updAmount":
          $t = $row[$key] ? $row[$key] : $row["amount"];
          if ($t > 0)
            $this->totalRecUpd += $t;
          else
            $this->totalDepUpd += $t;
          if ($row[$key]) {
            $dispValue = sprintf("%0.2f €", $t);
            if ($t < 0)
              $dispValue = "<span style=color:red>$dispValue</span>";
          }
          break;

        case "official":
          $t = $row[$key];
          $dispValue = isset($t) ? sprintf("%0.2f €", $t) : '';
          break;

        case "ended":
          $dispValue = $row[$key] ? 'X' : '';
          break;
      }
      return TableEdit::applyDisplay($key, $val, $row, $idxLine, $dispValue);
    }

//---------------------------------------------------------------------------------------------------
    public function applyFilter(&$key, &$val)
    {
      switch ($key) {
        case "category":
          if ($val[0] == '=' && ($cat = substr($val, 1)) % 256 == 0)
            return "$key&0xff00=" . ($cat / 256) * 256;
          break;
      }
      return TableEdit::applyFilter($key, $val);
    }

//---------------------------------------------------------------------------------------------------
    function buildKey(&$row)
    {
      return TABLE_NAME . "_budget where " . $this->defaultWhere . "category=" . $row["category"];
    }

//---------------------------------------------------------------------------------------------------
    function buildHelpArray(&$key, &$where)
    {
      switch ($key) {
        case "category":
          global $catSerial, $sub;
          return $sub ? $catSerial[$sub] : $catSerial;
      }
    }

//-----------------------------------------------------------------------------------------
    public function terminate()
    {
      echo "<tr><th colspan=2 style=font-weight:bold;text-align:center>Total Recettes</th><th style=color:black;font-weight:bold;text-align:right>" . sprintf("%0.2f €", $this->totalRec) . "</th>";
      if ($this->totalRecUpd)
        echo "<th style=color:black;font-weight:bold;text-align:right>" . sprintf("%0.2f €", $this->totalRecUpd) . "</th>";
      echo "</tr>" . nl;
      echo "<tr><th colspan=2 style=font-weight:bold;text-align:center>Total Dépenses</th><th style=color:red;font-weight:bold;text-align:right>" . sprintf("%0.2f €", $this->totalDep) . "</th>";
      if ($this->totalDepUpd)
        echo "<th style=color:black;font-weight:bold;text-align:right>" . sprintf("%0.2f €", $this->totalDepUpd) . "</th>";
      echo "</tr>" . nl;
      echo "<tr><th colspan=2 style=font-weight:bold;text-align:center>Total</th><th style=" . (($t = $this->totalRec + $this->totalDep) < 0 ? "color:red" : "color:black") . ";font-weight:bold;text-align:right>" . sprintf("%0.2f €", $t) . "</th>";
      if ($this->totalDepUpd)
        echo "<th style=" . (($t = $this->totalRecUpd + $this->totalDepUpd) < 0 ? "color:red" : "color:black") . ";font-weight:bold;text-align:right>" . sprintf("%0.2f €", $t) . "</th>";
      echo "</tr>" . nl;
    }

  }

  $obj = new Budget;
  $obj->tableId = "members";
  $obj->colDef = &$col;
  $obj->tableName = TABLE_NAME . "_budget";
  $obj->bHeader = true;
  $obj->bInsertNew = true;
  $obj->defaultWhere = "season=$season and " . ($bSubBudget ? "subBudget" . ($sub ? "='$sub'" : " is null") . " and " : '');
  if (isset($resultCategory))
    $obj->addCol = "official,subBudget";
  $obj->paramArray["year"] = $season;
  $obj->build();
  