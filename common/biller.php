<?php
include "tableEdit.php";
include "head.php";
//$seeRequest=1;
//dump($request);
echo "<title>" . TITLE_BILLER_CLUB . " facturier";
echo "</title></head><body onload=genericLoadEvent()>" . nl;
include "tools.php";

//-------------------------------------------------------------------------------------

class Biller extends TableEdit
{

//---------------------------------------------------------------------------------------------------
  public function build()
  {
    $this->bInsertNew = true;
    $this->bNoLimitButton = true;
    global $TVA, $request, $bOfficial, $basePath, $action, $resultCategory, $resultCategory, $bRoot;
    if (!($t = $request["limit"]) || $t == 'n')
      $this->bLimit = 200;
    if (!($bNum = jmysql_num_rows(jmysql_query("show columns from " . TABLE_NAME . " like 'num'"))))
      unset($this->colDef["num"]);
    if (!isset($resultCategory))
      unset($this->colDef["status"]);
    echo "<input type=hidden id=params style=display:none value='" . buildAllParam($this) . "'>";
    $basePath .= $basePath ? '&' : '?';
    if ($bRoot) {
      createAction(-1, "Bilan", "window.open('billerResults.php${basePath}tab=" . TABLE_NAME . "')");
      createAction(-1, "Budget", "window.open('billerBudget.php${basePath}tab=" . TABLE_NAME . "')");
      createButton(-1, "Extraits", TABLE_NAME . 'Read.php' . $basePath . '" target="_blank');
      if ($bNum)
        createAction(-1, "Numéro", "window.open('" . TABLE_NAME . ".php${basePath}action=updnum&tab=" . TABLE_NAME . "','_blank','scrollbars=yes,left=400,top=700,width=450,height=200,titlebar=no,menubar=no,status=no,location=no')");
      global $bOfficial;
      if (!$bOfficial) {
        createAction(-1, "Classer", "window.open('" . TABLE_NAME . ".php${basePath}action=classify&tab=" . TABLE_NAME . "')");
        createAction(-1, "Classeur", "window.open('billerClassify.php${basePath}tab=" . TABLE_NAME . "')");
      }
    }
    $this->sumKey = "amount";
    //$this->bExportAvailable=true;
    //$this->exportFileName=TABLE_NAME.".xls";      
    $this->bComputeAll = $bOfficial;
    $bReport = true;
    if ($this->filter)
      foreach ($this->filter as $k => $v)
        if ($k != "account") {
          $bReport = false;
          break;
        }
    //compute max date and min date
    $this->bHidden = jmysql_num_rows(jmysql_query("show columns from " . TABLE_NAME . " like 'hidden'"));
    $sql = " and account<>20";
    if (!$this->bComputeAll && $this->bHidden)
      $sql .= " and hidden=0";
    $sql = "select sum(amount) from " . $this->tableName . " where " . $this->buildWhere() . $sql;
    if ($this->season) {
      if ($request["civil"] == "true") {
        $df = "date>='" . ($this->season + 2000) . "-01-01' and date <='" . ($this->season + 2000) . "-12-31' and ";
        $sql .= " and date <='" . ($this->season + 2000) . "-12-31'" . (!$bReport && $request["win"][0] != '0' ? " and date>='" . ($this->season + 2000) . "-01-01'" : '');
      } else {
        $tup = jmysql_fetch_row(jmysql_query("select min(date), max(date) from " . $this->tableName . " where season=$this->season"));
        $df = "date>='$tup[0]' and date <='$tup[1]' and " . ($this->filter["category"] || strstr($this->order, "category") ? "season=$this->season and " : '');
        $sql .= " and date <='$tup[1]'" . (!$bReport ? " and date>='$tup[0]'" : '') . ($this->filter["category"] ? " and season=$this->season" : '');
      }
    }
    //echo NL.$sql.NL;
    $this->total = jmysql_result(jmysql_query($sql), 0);
    $this->defaultWhere .= $df;
    //dump($this->filter);
    //echo "df=$this->defaultWhere".NL;
    if (isset($TVA)) {
      if ($this->bTVA = $request["TVA"] == 'y') {
        $this->colDef["orig"][2] = "width:350px;max-width:350px";
        $w = " where " . $this->buildWhere() . " and TVA is not null";
        $this->totTVA += jmysql_result(jmysql_query("select sum(TVA) from " . TABLE_NAME . $w), 0);
        $this->colDef["base"][1] .= "</a><br><span style=color:darkblue>" . sprintf("%0.2f€", $this->total - $this->totTVA) . "</span>";
        $this->colDef["TVA"][1] .= "</a><br><span style=color:darkblue>" . sprintf("%0.2f€", $this->totTVA) . "</span>";
      } else {
        unset($this->colDef["base"]);
        unset($this->colDef["TVA"]);
        unset($this->colDef["TVAType"]);
        $this->addCol .= ",TVA";
      }
      echo "<input onclick=actionClick('filter','TVA'," . ($this->bTVA ? "'n'" : "'y'") . ") type=checkbox" . ($this->bTVA ? " checked=checked title='Sans TVA'" : " title='Avec TVA'") . ">Avec TVA";
    } else {
      unset($this->colDef["base"]);
      unset($this->colDef["TVA"]);
    }
    $this->colDef["amount"][1] .= "</a><br><span style=color:blue;font-weight:bold>" . sprintf("%0.2f€", $this->total) . "</span>";
    if (!$bRoot) {
      $this->bHeader = false;
      $this->bLimit = false;
    } else
      echo "<input onclick=actionClick('filter','limit'," . ($this->bLimit ? "'y'" : "'n'") . ") type=checkbox" . ($this->bLimit ? " title='Tous les tuples'>" : " checked=checked title='Limité à 200'>");
    echo " Calculatrice: <input type=text size=20 maxlength=100 onkeyup=compute(this,document.getElementById('result'))> = <span style=color:black;font-weight:bold id=result></span>";
    TableEdit::build();
  }

//--------------------------------------------------------------------------------------------
  public function editImg(&$row)
  {
    return TableEdit::editImg($row);
  }

//--------------------------------------------------------------------------------------------
  public function removeImg(&$row)
  {
    global $disableDate;
    if ($row["date"] >= $disableDate && !$row["TVA_status"])
      return TableEdit::removeImg($row) . "<img width=20 height=20 src=common/split.png title=Diviser style=border-style:none;cursor:pointer onclick=split(" . $row["ri"] . ")>";
  }

//--------------------------------------------------------------------------------------------
  function colTitle($key, &$val)
  {
    if ($this->bExport)
      switch ($key) {
        case "amount":
          fwrite($this->exportFile, "<th>TVA</th>");
          break;
        case "category":
          fwrite($this->exportFile, "<th>Détail</th>");
          break;
      }
    parent::colTitle($key, $val);
  }

//--------------------------------------------------------------------------------------------
  public function printLine(&$row)
  {
    global $bRoot;
    if (($c = $this->order) == "category" || $c == "status") {

      if (!isset($this->curCat))
        $this->curCat = $row[$c];

      $cat = $row[$c];
      $colspan = 4 + (int) !$bRoot + isset($this->colDef["TVAType"]) + isset($this->colDef["num"]) + isset($this->colDef["status"]) + ($this->bHeader) * 2;
      if ($this->curCat != $cat) {
        $bRevert = $c == "status" && $this->curCat < 100;
        global $category, $resultCategory;
        $t = $c == "status" ? $resultCategory[$this->curCat] : $category[$this->curCat / 256][0][0];
        $this->write("<tr'><td colspan=$colspan style='border-top:3px solid black;border-bottom:3px solid black;font-weight:bold;text-align:center'>$t");
        if ($c == "category" && $this->curCat % 256)
          $this->write(" &gt; " . $category[$this->curCat / 256][$this->curCat % 256][0]);
        $this->write("</td><td style='border-top:3px solid black;border-bottom:3px solid black;text-align:right;font-weight:bold'>" . sprintf("%0.2f €", $bRevert ? -$this->sumAmount : $this->sumAmount) . "</td>");
        if ($this->bTVA) {
          $this->write("<td style='border-top:3px solid black;border-bottom:3px solid black;white-space:nowrap;text-align:right;font-weight:bold'>" . sprintf("%0.2f €", $bRevert ? $this->sumTVA - $this->sumAmount : $this->sumAmount - $this->sumTVA) . "</td>");
          $this->write("<td style='border-top:3px solid black;border-bottom:3px solid black;white-space:nowrap;text-align:right;font-weight:bold'>" . sprintf("%0.2f €", $bRevert ? -$this->sumTVA : $this->sumTVA) . "</td>");
        }
        $this->write("</tr>");
        $this->sumAmount = 0;
        $this->sumTVA = 0;
        $this->curCat = $cat;
      }
    }
    if ($row)
      TableEdit::printLine($row);
  }

//--------------------------------------------------------------------------------------------
  public function terminate()
  {
    $t = null;
    $this->printLine($t);
  }

//--------------------------------------------------------------------------------------------
  public function applyStyle(&$row)
  {
    if ($this->bComputeAll)
      $row["hidden"] = 0;
    return 'color:' . ($row["hidden"] || $row["account"] == 20 ? 'gray' : 'black');
  }

//---------------------------------------------------------------------------------------------------
  public function applyDisplay($key, $val, $row, &$idxLine)
  {
    $dispValue = null;
    switch ($key) {
      case "account":
        global $account;
        $dispValue = ($t = $row["accountNb"]) ? "<span title='$t'>" . $account[$row[$key]] . "</span>" : $account[$row[$key]];
        break;

      case "date":
        global $sWeekDays;
        $dispValue = new DateTime($row[$key]);
        $dispValue = $dispValue->format("d/m/Y") . ' ' . $sWeekDays[$dispValue->format("w")];
        if ($this->season && $row["season"] != $this->season)
          $dispValue = "<span style=color:green>$dispValue</span>";
        break;

      case "orig":
        /* if (!$row["category"])
          $val[3]="tablink onclick=classify(".$row["ri"].")"; */
        $dispValue = $row[$key];
        $c = array('[' => ']', '{' => '}');
        foreach ($c as $k => $v) {
          $j = 0;
          if (($i = strpos($dispValue, $k, $j)) !== false) {
            if (($j = strpos($dispValue, $v, $i)) === false)
              continue;
            $dispValue = substr($dispValue, 0, $i) . "<span style=color:blue>" . substr($dispValue, $i, $j - $i + 1) . "</span>" . substr($dispValue, $j + 1);
            $j += 30;
          }
        }
        if ($row["detail"])
          $dispValue = "<span title='" . htmlspecialchars($row["detail"], ENT_QUOTES) . "'>" . ($dispValue ? $dispValue : str_repeat("&nbsp;", 10)) . "</span>";
        break;

      case "category":
        global $category;
        $dispValue = $category[$row[$key] / 256][0][0];
        if ($row[$key] % 256)
          $dispValue .= " > " . $category[$row[$key] / 256][$row[$key] % 256][0];
        if ($this->bExport)
          $this->line[$idxLine++] = "<td>" . $row["detail"] . "</td>";
        break;

      case "amount":
        $am = $row[$key];
        $bRevert = $this->order == "status" && $row["status"] < 100;
        if (!$row["hidden"] && $row["account"] <> 20) {
          $dispValue = sprintf("<span" . ($am < 0 ? " style=color:red" : '') . " title='%0.2f €'>%0.2f €</span>", $this->total, $bRevert ? -$am : $am);
          $this->total -= $row[$key];
          $this->sumAmount += $row[$key];
        } else
          $dispValue = sprintf("%0.2f", $am) . " €";
        if ($t = $row["TVA"]) {
          $t = round(100 * $t / ($row["amount"] - $t), 0);
          if ($t == 6 || $t == 21 || $t == 12 && $row["TVA"] > 0)
            $t .= '%';
          else
            $t = "Mix";
        }
        if ($this->bExport) {
          $this->line[$idxLine] = "<td style='color:blue;text-align:right'>$t</td>";
          $this->line[++$idxLine] = "<td style='$val[3]'>$dispValue</td>";
        }
        if ($t)
          $dispValue = "<div clear:both><span style=color:blue;float:left>$t</span><span style=float:right>$dispValue</span></div>";
        break;

      case "status":
        global $resultCategory;
        $dispValue = isset($row["status"]) ? $resultCategory[$row["status"]] : '';
        break;

      case "base":
        if ($row["TVA"]) {
          $bRevert = $this->order == "status" && $row["status"] < 100;
          $this->base = $row["amount"] - $row["TVA"];
          $dispValue = "<span" . ($row["amount"] < 0 && !$row["hidden"] ? " style=color:red>" : '>') . sprintf("%0.2f", $bRevert ? -$this->base : $this->base) . " €</span>";
        } else
          $dispValue = '';
        break;

      case "TVAType":
        global $TVAType;
        if (isset($row["TVA"]))
          $dispValue = $TVAType[$row[$key]];
        else
          $dispValue = '';
        break;

      case "TVA":
        if ($t = $row[$key]) {
          $bRevert = $this->order == "status" && $row["status"] < 100;
          $this->sumTVA += $t;
          $dispValue = "<span" . ($row["amount"] < 0 ? " style=color:red>" : '>') . sprintf("%0.2f", $bRevert ? -$t : $t) . " €</span>";
        } else
          $dispValue = '';
        break;
    }
    return TableEdit::applyDisplay($key, $val, $row, $idxLine, $dispValue);
  }

//---------------------------------------------------------------------------------------------------
  public function applyFilter(&$key, &$val)
  {
    switch ($key) {
      case "category":
        if ($val[0] == '=' && ($cat = substr($val, 1)) % 256 == 0)
          return "$key&0xff00=" . ($cat / 256) * 256;
        break;

      case "orig":
        if (strchr($val, '!') === false)
          return "(" . $this->buildFilter("orig", $val) . " or " . $this->buildFilter("detail", $val) . ')';
        break;

      case "amount":
        if (is_numeric($val)) {
          $t = "Total=%$val";
          return "(" . $this->buildFilter("amount", $val) . " or " . $this->buildFilter("detail", $t) . ')';
        }
        break;

      case "status":
        global $resultCategory;
        foreach ($resultCategory as $k => $v)
          if (stripos($v, $val) !== false)
            $t .= "$k,";
        if ($t) {
          $t = "_c=" . substr($t, 0, -1);
          return $this->buildFilter("status", $t);
        }
        break;
    }
    return TableEdit::applyFilter($key, $val);
  }

//---------------------------------------------------------------------------------------------------
  function buildKey(&$row)
  {
    return $this->tableName . " where ri=" . $row["ri"];
  }

//---------------------------------------------------------------------------------------------------
  function applyOrder()
  {
    $num = jmysql_num_rows(jmysql_query("show columns from $this->tableName like 'num'")) ? "num," : '';
    return "order by $this->order $this->dir, " . ($this->order != "date" ? "date desc, " : '') . "$num subpart asc, ri desc";
  }

//---------------------------------------------------------------------------------------------------
  function buildHelpArray(&$key, $where)
  {
    switch ($key) {
      case "account":
        global $account;
        return $account;

      case "category":
        global $category, $yearSeason;
        $s[0] = '';
        $where = substr($where, 0, strpos("group", $where));
        if (!$where)
          $where = "where 1";
        $r = jmysql_query("select category from " . TABLE_NAME . " $where" . ($this->season ? " and season=$this->season" : "") . " group by category");
        while ($tup = jmysql_fetch_row($r)) {
          if ($cur != ($tup[0] >> 8)) {
            $cur = $tup[0] >> 8;
            if ($tup[0] & 0xFF && isset($category[$cur][0]))
              $s[$cur << 8] = $category[$cur][0][0];
          }
          $t = $tup[0] & 0xFF;
          if (isset($category[$cur][$t]))
            $s[$tup[0]] = ($t = $tup[0] & 0xFF) ? "--- " . $category[$cur][$t][0] : $category[$cur][0][0];
        }
        return $s;

      case "TVAType":
        global $TVAType;
        return $TVAType;

      case "status":
        global $resultCategory;
        foreach ($resultCategory as $k => $v) {
          if ($k >= 200)
            break;
          $s[$k] = "<b>" . ($k >= 100 ? "Recette -- " : "Dépense -- ") . "</b>$v";
        }
        return $s;
    }
  }

}
?>
<script type="text/javaScript">
  function split(ri)
  {
  if (pars=document.getElementById('params').value)
  pars+='&';
  else
  pars='?';
  window.open('<?php echo TABLE_NAME . ".php" ?>'+pars+'action=split&ri='+ri,'_blank','scrollbars=no,left=400,width=800,height=400,top=200,titlebar=no,menubar=no,status=no,location=no');
  }

  bAll=true;
  function select()
  {
  els=document.getElementsByName("select");
  for (i in els)
  els[i].checked=bAll;
  bAll=!bAll;
  document.getElementById("selAll").innerHTML=bAll ? 'A':'N';
  }

  function compute(obj, res)
  {
  if (!obj.value.length)
  {
  res.innerHTML='';
  return;
  } 
  res.innerHTML=eval(obj.value).toFixed(2);
  //unicode=e.keyCode? e.keyCode : e.charCode;
  //k=String.fromCharCode(unicode);
  }

  function dispSumChecked(sum)
  {
  document.getElementById('result').innerHTML=sum.toFixed(2);
  }

  /*
  function classify(ri) {
  $.post("common/billerClassify.php",{ri:ri,cust:'<?echo TABLE_NAME?>'},function(data,status) {
  if (data.
  */     

</script>



