<?php
include "head.php";
include "tools.php";
echo "<title>Biller edit</title></head><body onload=billerLoad()>" . nl;
//dump($request);

if (!($bNum = jmysql_num_rows(jmysql_query("show columns from " . TABLE_NAME . " like 'num'")))) {
  unset($col["num"]);
  $lastNumber = 0;
} else
  $lastNumber = jmysql_result(jmysql_query("select substr(max(num),1,2) from " . TABLE_NAME . " where num not like 'B%'"), 0) . sprintf("%03d", jmysql_result(jmysql_query("select num from " . TABLE_NAME . "_num"), 0));


if (!($bSet = !isset($request["list"]))) {
  unset($col["date"]);
  unset($col["amount"]);
  $am = jmysql_result(jmysql_query("select sum(amount) from " . $request["list"]), 0);
}
include "tableRowEdit.php";
if (!isset($TVA))
  unset($col["TVA"]);

class BillerEdit extends TableRowEdit
{

//----------------------------------------------------------------------------------------------
  function getTuple()
  {
    global $disableDate, $col;
    $row = TableRowEdit::getTuple();
    if ($row["date"] && $row["date"] < $disableDate) {
      $this->disabled = " disabled=disabled";
      $this->TVAdisabled = " disabled=disabled";
      //unset($col["season"]);
    } else if ($row["TVA_status"])
      $this->TVAdisabled = " disabled=disabled";
    return $row;
  }

//----------------------------------------------------------------------------------------------
  function applyValue(&$key, &$format, &$row)
  {
    global $bSet, $request;
    switch ($key) {
      case "account":
        global $account;
        echo "<select$this->disabled name=_val_account onchange=statusChange(1)>";
        if (!$bSet)
          echo "<option selected=selected></option>";
        foreach ($account as $k => $v)
          echo "<option value=$k" . ($bSet && $row[$key] == $k ? " selected=selected" : '') . ">$v</option>";
        echo "</select>";
        if ($row["accountNb"])
          echo "<span style=margin-left:20px;color:gray>(" . $row["accountNb"] . ")</span>";
        return;

      case "date":
        if ($bSet) {
          $dt = new dateTime($row[$key]);
          $y = $dt->format('Y');
          $m = $dt->format('m');
          $d = $dt->format('d');
        }
        global $sWeekDays;
        echo "<input type=hidden name=_val_date><select$this->TVAdisabled id=day onchange=checkDay()>";
        if (!$bSet)
          echo "<option selected=selected></option>";
        for ($i = 1; $i <= 31; $i++)
          echo "<option value=" . sprintf("%02d", $i) . ($i == $d ? " selected=selected" : '') . '>' . sprintf("%02d", $i) . "</option>";
        echo "</select><select$this->TVAdisabled id=month onchange=checkDay()>";
        global $months;
        if (!$bSet)
          echo "<option selected=selected</option>";
        for ($i = 1; $i <= 12; $i++)
          echo "<option value=" . sprintf("%02d", $i) . ($i == $m ? " selected=selected" : '') . ">$months[$i]</option>";
        echo "</select><select$this->TVAdisabled id=year>";
        global $currentYear;
        if (!$bSet)
          echo "<option selected=selected</option>";
        for ($year = $this->begin; $year <= $currentYear + 1; $year++)
          echo "<option value=$year" . ($y == $year ? " selected=selected" : '') . ">$year</option>";
        echo "</select>" . nl;
        if (!$this->TVAdisabled)
          createAction(-1, "Aujourd'hui", "today()");
        return;

      case "category":
        global $catSerial, $resultCategory, $resultCatName;
        echo "<select name=_val_category onchange=statusChange(-1)>";
        if (!$bSet)
          echo "<option selected=selected></option>";
        foreach ($catSerial as $k => $v)
          echo "<option value=$k" . ($bSet && $row[$key] == $k ? " selected=selected" : '') . ">" . (!$v ? "&lt;Aucune&gt;" : $v) . "</option>";
        echo "</select>";
        if (isset($resultCategory)) {
          echo "<b>  Catégorie Bilan : </b><select$this->disabled name=_val_status>";
          if ($this->bList)
            echo "<option></option>";
          echo "<option value='=null'>&lt;Aucune&gt;</option>";
          $step = -1;
          foreach ($resultCategory as $k => $v) {
            if ((int) ($k / 100) != $step) {
              if ($step != -1)
                echo "</optgroup>";
              $step = (int) ($k / 100);
              echo "<optgroup label='" . $resultCatName[$step] . "'>";
            }
            echo "<option value=$k" . (strlen($row["status"]) && $row["status"] == $k ? " selected=selected" : '') . ">$v</option>";
          }
          echo "</select>";
        }
        return;

      case "TVA":
        global $TVAType, $TVA;
        echo "<input name=_val_TVA style=margin-right:15px size=10 maxlength=10 onchange=statusChange(1) type=text onkeypress='return keyPressed(event,2)'$this->TVAdisabled value='" . $row["TVA"] . "'>";
        echo "<select$this->TVAdisabled id=TVA_val onchange=statusChange(2)>";
        echo "<option></option>";
        $bFixed = !isset($row["TVA"]);
        if ($this->bList)
          echo "<option value='=null'>Pas de TVA</option>";
        $perc = round(100 * $row["TVA"] / ($row["amount"] - $row["TVA"]), 0);
        foreach ($TVA as $v) {
          $bFixed |= !$this->blist && $perc == $v;
          echo "<option value=$v" . (!$this->blist && $perc == $v ? " selected=selected" : '') . ">$v%</option>";
        }
        echo "<option" . (!$this->blist && !$bFixed ? " selected=selected" : '') . ">Autre</option>";
        echo "</select>";
        echo "<span id=TVA_disp" . (!$this->bList && !isset($row["TVA"]) ? " style=visibility:hidden>" : '>');
        echo "<b>&nbsp;&nbsp;Type TVA: </b><select name=_val_TVAType$this->TVAdisabled><option></option><optgroup label=Sortie>";
        foreach ($TVAType as $k => $v) {
          if ($k == 81)
            echo "</optgroup><optgroup label=Entrée>";
          echo "<option value=$k" . ($row["TVAType"] == $k ? " selected=selected" : '') . ">$v</option>";
        }
        echo "</optgroup></select>";

        if ($quarter)
          echo "&nbsp;Clôturé dans le $quarter<sup>" . ($quarter == 1 ? "er" : "ième") . "</sup> trimestre ";
        $b = $row["amount"] * 100 / (100 + $row["TVA"]);
        echo "<span id=TVAPrint style=font-weight:bold;color:Magenta;font-style:italic>&nbsp;  Base=" . sprintf("%0.2f€. TVA " . $row["TVA"] . "%%=%0.2f€", $b, $row["amount"] - $b) . "</span></span>";
        break;

      case "type":
        global $bNum;
        echo "<input type=checkbox name=sel_type value=0 onclick=typeClick(this)>&lt;aucune&gt;" . NL;
        if ($bNum)
          echo "<input type=hidden name=_val_increment value=1><input type=checkbox name=sel_type value=1 onclick=typeClick(this)>Numéroté" . NL;
        echo "<input type=checkbox name=sel_type value=2 onclick=typeClick(this)>Divisé" . NL;
        echo "<input type=checkbox name=sel_type value=3 onclick=typeClick(this)>Manuel" . NL;
        return;


      case "amount":
        echo "<input name=_val_amount size=10 type=text value='" . $row["amount"] . "'$this->TVAdisabled onchange=statusChange(0)>";
        if (!$this->TVAdisabled) {
          echo " Calcul: <input type=text size=15 maxlength=50 onkeyup=compute(this)>";
          include_once "tools.php";
          createAction(-1, "Restore", "document.getElementsByName('_val_amount')[0].value=save");
        }
        return;

      case "hidden":
        echo "<input$this->disabled type=radio name=sel_hidden " . ($bSet && $row["hidden"] ? " checked=checked" : '') . ">  Non comptabilisé" . NL;
        echo "<input$this->disabled type=radio name=sel_hidden " . ($bSet && !$row["hidden"] ? " checked=checked" : '') . ">  Comptabilisé" . NL;
        echo "<input name=_val_hidden type=hidden>";
        return;
    }
    TableRowEdit::applyValue($key, $format, $row);
  }

//-------------------------------------------------------------------------------------------
  function applySave()
  {
    global $disableDate;
    ?>
    y=document.getElementById('year');
    if (y)
    if (!y.value)
    document.getElementsByName('_val_date')[0].value='';
    else
    {
    m=document.getElementById('month');
    d=document.getElementById('day');
    date=y.value+'-'+m.value+'-'+d.value;
    document.getElementsByName('_val_date')[0].value=date;
    }
    el=document.getElementsByName('sel_hidden');
    if (el[0])
    {
    document.getElementsByName('_val_hidden')[0].value=el[0].checked ? 1 : (el[1].checked ? 0 : '');
    el[0].name=el[1].name='';
    }
    el=document.getElementsByName('_val_increment')[0];
    if (!bNewNb && el)
    {
    el.name='';
    el.value='';
    }
    var status=document.getElementsByName("_val_status")[0];
    if (status)
    status.value=status.options[status.selectedIndex].value;
    tva=document.getElementById("TVA_val");
    if (tva && tva.selectedIndex==tva.options.length-1)
    {
    t=document.getElementsByName("_val_TVA")[0].value;
    if (!t.length)
    {
    alert("la valeur de la TVA doit être indiquée pour 'Autre'");
    return;
    }
    }

    <?php
  }

}
?>
<script type="text/javaScript">
  var save;
  var bNewNb=false;
  function compute(obj)
  {
  el=document.getElementsByName("_val_amount");
  if (!el)
  return;
  el=el[0];
  if (save==undefined)
  save=el.value;
  if (!obj.value.length)
  {
  el.value='';
  return;
  } 
  el.value=eval(obj.value);
  //unicode=e.keyCode? e.keyCode : e.charCode;
  //k=String.fromCharCode(unicode);
  }

  function today()
  {
  d=new Date();
  el=document.getElementById("day");
  el.options[d.getDate()<?php echo $bSet ? '-1' : '' ?>].selected=true;
  el=document.getElementById("month");
  el.options[d.getMonth()<?php echo $bSet ? '' : '+1' ?>].selected=true;
  el=document.getElementById("year");
  for (i=0; i<el.options.length; i++)
  if (el.options[i].value==d.getFullYear())
  {
  el.options[i].selected=true;
  break;
  }
  }


  function billerLoad()
  {
  <?php
  if ($request["key"]) {
    ?>
    v=document.getElementsByName('_val_orig')[0].value;
    chk=document.getElementsByName('sel_type');
    chk[0].checked=true;
    if ((tok=v.indexOf('['))>=0)
    {
    chk[0].checked=false;
    c=v.charAt(tok+1);
    if (c=='M' || c=='T' || c=='C')
    chk[3].checked=true;
    else  
    chk[2].checked=true;
    }
    if ((v=document.getElementsByName("_val_num")) && v[0].value>0)
    {
    chk[0].checked=false;
    chk[1].checked=true;
    }
  <?php }
  ?>
  }

  function remove(t)
  {
  if (t=='{')
  document.getElementsByName('_val_num')[0].value='';
  else
  {
  el=document.getElementsByName('_val_orig')[0];
  v=el.value;
  b=v.indexOf(t);
  if (b>=0)
  {
  e=v.indexOf(t.charAt(0)=='[' ? ']':'}',b+1);
  el.value=v.substr(0,b)+v.substr(e+2);
  }
  }
  }   

  function typeClick(obj)
  {
  chk=document.getElementsByName('sel_type');
  el=document.getElementsByName('_val_orig')[0];
  v=el.value;
  switch(obj.value/1)
  {
  case 0:
  if (obj.checked)
  {
  remove('[');
  remove('[');
  remove('{');
  for (i=1; i<4; i++)
  chk[i].checked=false;      
  }
  else
  {
  for (i=1; i<4; i++)
  if (chk[i].checked)
  {
  chk[0].checked=true;
  break;
  }
  }      
  break;

  case 1:
  bNewNb=false;
  if (obj.checked)
  {
  bNewNb=true;
  document.getElementsByName('_val_num')[0].value=<?php echo $lastNumber ?>;
  }
  else
  remove('{');
  break;

  case 2:
  if (obj.checked)
  {
  alert("Impossible de remettre la division, il faut la refaire complètement.");
  obj.checked=false;
  return;
  }
  b=v.indexOf('[');
  c=v.charAt(b+1);
  if (c=='M'||c=='T'||c=='C')
  b=v.indexOf('[',b+1);
  if (b!=-1)
  el.value=v.substr(0,b)+v.substr(v.indexOf(']',b+1)+2);
  break;

  case 3:
  if (obj.checked)
  el.value="[M<?php echo $now->format("ymd") ?>] "+v;
  else
  {
  remove("[M");
  remove("[T");
  remove("[C");
  }
  break;
  }
  chk[0].checked=true;      
  for (i=1; i<4; i++)
  if (chk[i].checked)
  {
  chk[0].checked=false;
  break;
  }
  }

  <?php
  if (isset($resultCategory)) {
    echo "allCat=[";
    foreach ($category as $k => $v)
      foreach ($v as $kk => $vv) {
        echo $sep . ($k * 256 + $kk);
        $sep = ',';
      }
    echo "];\nconvPos=[";
    unset($sep);
    foreach ($category as $k => $v)
      foreach ($v as $kk => $vv) {
        echo $sep . $vv[1];
        $sep = ',';
      }
    echo "];\nconvNeg=[";
    unset($sep);
    foreach ($category as $k => $v)
      foreach ($v as $kk => $vv) {
        echo $sep . $vv[2];
        $sep = ',';
      }
    echo "];" . nl;
  }
  ?>

  function statusChange(oper)
  {
  if (oper>=0 && (obj=document.getElementById("TVA_val")))
  {
  tva=document.getElementsByName("_val_TVA")[0];
  am=document.getElementsByName('_val_amount')[0].value;
  if (oper==2) {
  if (obj.selectedIndex==0)
  tva.value='';
  if (obj.selectedIndex>0 && obj.selectedIndex<obj.options.length-1)
  tva.value=(am*obj.value/(100+obj.value/1)).toFixed(2);
  }
  else if (tva.value)
  obj.selectedIndex=obj.options.length-1;
  if (obj.selectedIndex<?php echo!$bSet ? ">1" : '' ?>)
  {
  <?php if ($bSet) { ?>
    b=am-tva.value;
    document.getElementById("TVAPrint").innerHTML='&nbsp;  Base='+b.toFixed(2)+'€. TVA ='+(tva.value/1).toFixed(2)+'€';
    document.getElementById("TVA_disp").style.visibility='visible';
    v=document.getElementsByName('_val_TVAType')[0];
    if (!v.selectedIndex||v.selectedIndex<=3)
    v.selectedIndex=am>0 ? obj.selectedIndex : 10;
  <?php } else { ?>
    document.getElementById("TVAPrint").innerHTML='';
    document.getElementById("TVA_disp").style.visibility='visible';
  <?php } ?>
  }
  else
  document.getElementById("TVA_disp").style.visibility='hidden';
  if (tva.value.length && document.getElementsByName("_val_account")[0].value>=50)
  alert("Attention: TVA sur ce compte ne peut pas exister")
  }

  <?php
  if (isset($resultCategory)) {
    ?>
    if (oper<=0)
    {
    obj=document.getElementsByName('_val_category')[0];
    cat=obj.options[obj.selectedIndex].value;
    obj=document.getElementsByName('_val_status')[0];
    am=document.getElementsByName('_val_amount');
    if (!am || obj.disabled)
    return;
    am=<?php echo $bSet ? "am[0].value;\n" : "$am;\n" ?>
    for (i in allCat)
    if (allCat[i]==cat)
    {
    v=am<0 ? convNeg[i]:convPos[i];
    if (v==-1)
    obj.selectedIndex=0;  
    for (j=0; j<obj.length; j++)
    if (obj.options[j].value.length && obj.options[j].value==v)
    { 
    obj.selectedIndex=j;
    break;
    }
    }
    }

    <?php
  }
  ?>
  }  

</script>
