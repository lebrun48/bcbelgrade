<?php
include $_GET["tab"] . "Conf.php";
include "head.php";
echo "<title>" . substr($_GET["tab"], 6) . " TVA</title></head>" . nl;
include "tools.php";

//$seeRequest=1;
if (!($currentYear = $_GET["year"]) || !($quarter = $_GET["quarter"])) {
  ?>
  <script type="text/javaScript">
    function go()
    {
    year=document.getElementById('year');
    year=year.options[year.selectedIndex].value;
    quarter=document.getElementById('quarter');
    quarter=quarter.options[quarter.selectedIndex].value;
    if (!year || !quarter)
    return;
    location.assign('billerTVA.php<?php echo "${basePath1}tab=" . $_GET["tab"] . "&quarter=" ?>'+quarter+'&year='+year);
    }
  </script>
  <?php
  echo "<h2>Selection du trimestre</h2>";
  $e = jmysql_result(jmysql_query("select substr(min(date),1,4) from " . TABLE_NAME), 0);
  echo "Année <select id=year onchange=go()>" . nl;
  echo "<option></option>";
  for ($i = $now->format("Y"); $i >= $e; $i--)
    echo "<option>$i</option>";
  echo "</select>";

  echo "&nbsp;  Trimestre <select id=quarter onchange=go()>" . nl;
  echo "<option></option>";
  for ($i = 1; $i <= 4; $i++)
    echo "<option value=$i>$i" . ($i == 1 ? "er" : "ième") . " trimestre</option>";
  echo "</select>" . nl;
  exit();
}

$TVASt = ($currentYear - 2000) * 100 + $quarter;
$dt = sprintf("'%d-%02d-01'", $currentYear + ($quarter == 4 ? 1 : 0), ($quarter == 4 ? 1 : ($quarter + 1) * 3 - 2));
$bRecorded = jmysql_result(jmysql_query("select count(*) from " . TABLE_NAME . " where TVA_status=$TVASt"), 0) != 0;
$date = $bRecorded ? "TVA_status=$TVASt" : "TVA_status is null and date<$dt";
$filter = "official=true&civil=true&year=all&TVA=y" . ($bRecorded ? "&_sel_TVA_status=$TVASt" : "&_sel_TVA_status=$&_sel_date=" . urlencode(str_replace("'", '', '<' . $dt)));
$sql = "select count(*) from " . TABLE_NAME . " where TVA is not null and $date";
$url = TABLE_NAME . ".php$basePath&$filter&_sel_tva=!$";

foreach ($category as $k => $v)
  foreach ($v as $k1 => $v1) {
    if (jmysql_result(jmysql_query("$sql and category=" . ($k * 256 + $k1) . " and amount>0 and (status" . ($v1[1] == -1 ? " is not null)" : "<>$v1[1] or status is null)")), 0)) {
      echo "<a target=_blank href=" . htmlspecialchars("$url&_sel_category=" . ($k * 256 + $k1) . "&_sel_amount=>0&_sel_status=" . ($v1[1] == -1 ? "!$" : "!=$v1[1]%2B$")) . ">Montant positif ne correspond pas à la config</a>" . NL;
      break;
    }
    if (jmysql_result(jmysql_query("$sql and category=" . ($k * 256 + $k1) . " and amount<0 and (status" . ($v1[2] == -1 ? " is not null)" : "<>$v1[2] or status is null)")), 0)) {
      echo "<a target=_blank href=" . htmlspecialchars("$url&_sel_category=" . ($k * 256 + $k1) . "&_sel_amount=<0&_sel_status=" . ($v1[2] == -1 ? "!$" : "!=$v1[2]%2B$")) . ">Montant négatif ne correspond pas à la config</a>" . NL;
      break;
    }
  }
//check validity
//TVAType not filled
if (jmysql_result(jmysql_query("$sql and TVA is not null and TVAType is null"), 0)) {
  echo "<a target=_blank id=shower href=" . htmlspecialchars("$url&_sel_TVA=!$&_sel_TVAType=$") . ">Type TVA non renseigné</a>" . NL;
}
//wrong TVAtype
if (jmysql_result(jmysql_query("$sql and (TVAType<48 or TVAType=84 or TVAType=85) and amount<0"), 0)) {
  echo "<a target=_blank id=shower href=" . htmlspecialchars("$url&_sel_TVAType=" . urlencode("<48+=84+=85") . "&_sel_amount=<0") . ">Montant négatif pour TVA positive</a>" . NL;
}
if (jmysql_result(jmysql_query("$sql and (TVAType=48 or TVAType=49 or TVAType>80 and TVAType<>84 and TVAType<>85) and amount>0"), 0)) {
  echo "<a target=_blank id=shower href=" . htmlspecialchars("$url&_sel_TVAType=" . urlencode("=48+=49+>80&!=84&!=85") . "&_sel_amount=>0") . ">Montant positif pour TVA négative</a>" . NL;
}
if (jmysql_result(jmysql_query("$sql and status=0 and TVAType<>81 and TVAType<>85"), 0)) {
  echo "<a target=_blank id=shower href=" . htmlspecialchars("$url&_sel_status=" . urlencode("_c=a:1:{i:0;s:1:\"0\";}") . "&_sel_TVAType=" . urlencode("!=81&!=85")) . ">Status bilan != Type de tva</a>" . NL;
}
if (jmysql_result(jmysql_query("$sql and status=2 and TVAType<>82 and TVAType<>86"), 0)) {
  echo "<a target=_blank id=shower href=" . htmlspecialchars("$url&_sel_status=" . urlencode("_c=a:1:{i:0;s:1:\"2\";}") . "&_sel_TVAType=" . urlencode("!=82&!=86")) . ">Status bilan != Type de tva</a>" . NL;
}

$TVASum = array(0  => 54,
    1  => 54,
    2  => 54,
    3  => 54,
    81 => 59,
    82 => 59,
    83 => 59,
    85 => 63,
);
//recettes
foreach ($TVAType as $i => $v) {
  $t = jmysql_fetch_row(jmysql_query("select sum(amount),sum(amount-tva) from " . TABLE_NAME . " where $date and TVA is not null and TVAType=$i"));
  $amount[$i] = abs(round($t[1], 2));
  if ($TVASum[$i])
    $amount[$TVASum[$i]] += abs(round($t[0] - $t[1], 2));
  if ($i == 86 || $i == 46) {
    $t = round(abs($t[0] * .21), 2);
    $amount[59] += $t;
    $amount[55] += $t;
  }
}
$amount[81] -= round(jmysql_result(jmysql_query("select sum(amount-tva) from " . TABLE_NAME . " where $date and TVA is not null and TVAType=85 and status=0"), 0), 2);
$amount[82] -= round(jmysql_result(jmysql_query("select sum(amount-tva) from " . TABLE_NAME . " where $date and TVA is not null and TVAType=85 and status=2"), 0), 2);
$amount[91] = -round(jmysql_result(jmysql_query("select sum(amount) from " . TABLE_NAME . " where category=0x1301 and $date"), 0), 2);
$res = $amount[54] + $amount[55] + $amount[56] + $amount[57] + $amount[61] + $amount[63] - $amount[59] - $amount[62] - $amount[64];
$amount[$res < 0 ? 72 : 71] = abs($res);
$amount[-1] = jmysql_result(jmysql_query("select sum(amount) from " . TABLE_NAME . " where category=0x1300 and TVA_status=" . ($quarter == 1 ? ($currentYear - 2001) * 100 + 4 : ($currentYear - 2000) * 100 + $quarter - 1), 0), 0);
$sTVA = $res - $amount[91] + ($amount[-1] < 0 ? $amount[-1] : 0);
$t = $amount[71] > 312.5 ? round($amount[71] / 3, 2) : 0;
if (($amount["A1"] = $t + ($sTVA < 0 ? $sTVA : 0)) < 0) {
  if (($amount["A2"] = $t + $amount["A1"]) < 0)
    $amount["A2"] = 0;
  $amount["A1"] = 0;
} else
  $amount["A2"] = $t;

//dump($sTVA);
//dump($amount);

if (!$bRecorded && $_GET["action"] == "save") {
  jmysql_query("insert into " . TABLE_NAME . " set season=" . ($yearSeason - 2000) . ", date='" . $now->format('Y-m-d') . "', account=21, orig='TVA trimestre N° $quarter', amount=$res, TVA_status=$TVASt,category=0x1300,hidden=0", 1);
  jmysql_query("update " . TABLE_NAME . " set TVA_status=$TVASt where TVA_status is null and category=0x1301", 1);
  jmysql_query("update " . TABLE_NAME . " set TVA_status=$TVASt where $date and TVA is not null", 1);
  $bRecorded = true;
}
?>
<style type="text/css">
  .line {width:100%;border:0px;font-family:Arial;color:black}
  .cols {table-layout:fixed;border:0px;font-family:Arial;color:black;margin:auto;width:95%}
  .col {width:30%;white-space:nowrap;border:0px;font-size:80%}
  .val {padding-right:0.5cm;text-align:right;border:0px;font-weight:bold;font-size:80%}
  a {color:black;}
</style>

<table class=line>
  <tr border=1px style=text-align:center;font-size:25px;font-weight:bold;padding-bottom:20px>
    <td>DECLARATION TVA<?php
      if (!$bRecorded)
        echo "&nbsp;&nbsp;<input type=button value='Enregistrer le trimestre' onclick=save()>";
      else
        echo "&nbsp;&nbsp;<span style=color:blue;font-weight:normal>&lt;Trimestre enregistré&gt;</span>";
      ?>    
    </td>
  </tr>
</table>

<table class=line style=margin-top:0.5cm>
  <tr border=1px style=height:2cm>
    <td><table class=cols>
        <tr style=width:50%;height:50%;font-size:20px;font-weight:bold>
          <td style=border:0px><?php echo $enterprise->name ?></td>
          <td style=border:0px><?php echo $enterprise->num ?></td>
        </tr>
        <tr>
          <td style=border:0px>Période <?php echo "$quarter - $currentYear" ?></td>
        </tr>
      </table>
    </td>
  </tr>
</table>

<table class=line style=margin-top:0.5cm>
  <tr border=1px style=height:1.2cm>
    <td><table class=cols>
        <tr style=width:50%;height:100%;font-weight:bold>
          <td style=border:0px>Opérations à la sortie</td>
          <td style=border:0px>Opérations à l'entrée</td>
        </tr>
      </table>
    </td>
  </tr>
  <tr border=1px style=height:9cm>
    <td><table class=cols style=height:8cm>
        <tr style=width:100%;height:10%>
          <td class=val></td>
          <td class=col>(00) 0%</td>
          <td class=val><?php displayAmount(81, "&_sel_TVA=!$&_sel_TVAType=" . urlencode("=81+=85")) ?></td>
          <td class=col>(81) Marchandises</td>
        </tr>
        <tr style=width:100%;height:10%>
          <td class=val><?php displayAmount(1, "&_sel_TVA=!$&_sel_TVAType==1") ?></td>
          <td class=col>(01) 6%</td>
          <td class=val><?php displayAmount(82, "&_sel_TVA=!$&_sel_TVAType=82") ?></td>
          <td class=col>(82) Biens et services</td>
        </tr>
        <tr style=width:100%;height:10%>
          <td class=val><?php displayAmount(2, "&_sel_TVA=!$&_sel_TVAType==2") ?></td>
          <td class=col>(02) 12%</td>
          <td class=val><?php displayAmount(83, "&_sel_TVA=!$&_sel_TVAType=83") ?></td>
          <td class=col>(83) Investissements</td>
        </tr>
        <tr style=width:100%;height:10%>
          <td class=val><?php displayAmount(3, "&_sel_TVA=!$&_sel_TVAType==3") ?></td>
          <td class=col>(03) 21%</td>
          <td class=val></td>
          <td class=col></td>
        </tr>
        <tr style=width:100%;height:10%>
          <td class=val></td>
          <td class=col>(44) Prestations intracomm.</td>
          <td class=val></td>
          <td class=col></td>
        </tr>
        <tr style=width:100%;height:10%>
          <td class=val></td>
          <td class=col>(45) Co-contractant</td>
          <td class=val></td>
          <td class=col>(84) Crédits s/acq. intracomm.</td>
        </tr>
        <tr style=width:100%;height:10%>
          <td class=val></td>
          <td class=col>(46) Marchandises intracomm.</td>
          <td class=val><?php displayAmount(85, "&_sel_TVA=!$&_sel_TVAType=85") ?></td>
          <td class=col>(85) Autres Crédits sur achats</td>
        </tr>
        <tr style=width:100%;height:10%>
          <td class=val><?php displayAmount(47, "&_sel_TVA=!$&_sel_TVAType=47") ?></td>
          <td class=col>(47) Exportations hors CEE</td>
          <td class=val><?php displayAmount(86, "&_sel_TVA=!$&_sel_TVAType=86") ?></td>
          <td class=col>(86) Marchandises intracomm.</td>
        </tr>
        <tr style=width:100%;height:10%>
          <td class=val></td>
          <td class=col>(48) Crédits intracomm.</td>
          <td class=val></td>
          <td class=col>(87) Hors CEE / Co-contractant</td>
        </tr>
        <tr style=width:100%;height:10%>
          <td class=val><?php displayAmount(49, "&_sel_TVA=!$&_sel_TVAType=49") ?></td>
          <td class=col>(49) Autres Crédits sur ventes</td>
          <td class=val></td>
          <td class=col>(88) Prestations intracomm.</td>
        </tr>
      </table>
    </td>
  </tr>
  <tr border=1px style=height:1.2cm>
    <td><table class=cols>
        <tr style=width:50%;height:100%;font-weight:bold>
          <td style=border:0px>TVA à payer</td>
          <td style=border:0px>TVA à récupérer</td>
        </tr>
      </table>
    </td>
  </tr>
  <tr border=1px style=height:6cm>
    <td><table class=cols style=height:6cm>
        <tr style=width:100%;height:14%>
          <td class=val><?php displayAmount(54, "&_sel_TVA=!$&_sel_TVAType=" . urlencode("=1+=2+=3")) ?></td>
          <td class=col>(54) TVA due sur ventes</td>
          <td class=val><?php displayAmount(59, "&_sel_TVA=!$&_sel_TVAType=" . urlencode("=81+=82+=83+=86")) ?></td>
          <td class=col>(59) TVA à récupérer sur achats</td>
        </tr>
        <tr style=width:100%;height:14%>
          <td class=val><?php displayAmount(55, "&_sel_TVA=!$&_sel_amount=" . urlencode("<0") . "&_sel_TVAType=86") ?></td>
          <td class=col>(55) TVA s/ach. intracomm.</td>
          <td class=val></td>
          <td class=col>(62) TVA déd. s/régularisations</td>
        </tr>
        <tr style=width:100%;height:14%>
          <td class=val></td>
          <td class=col>(56) TVA s/ach. co-contractants</td>
          <td class=val></td>
          <td class=col>(64) TVA s/n. de crédits émises</td>
        </tr>
        <tr style=width:100%;height:14%>
          <td class=val></td>
          <td class=col>(57) TVA s/ach hors CEE</td>
          <td class=val></td>
          <td class=col></td>
        </tr>
        <tr style=width:100%;height:14%>
          <td class=val></td>
          <td class=col>(61) TVA due s/régularisations</td>
          <td class=val></td>
          <td class=col></td>
        </tr>
        <tr style=width:100%;height:14%>
          <td class=val><?php displayAmount(63, "&_sel_TVA=!$&_sel_amount=" . urlencode(">0") . "&_sel_TVAType=85") ?></td>
          <td class=col>(63) TVA s/n. de crédits reçues</td>
          <td class=val></td>
          <td class=col></td>
        </tr>
        <tr style=width:100%;height:14%>
          <td class=val></td>
          <td class=col></td>
          <td class=val></td>
          <td class=col></td>
        </tr>
      </table>
    </td>
</table>
<table class=line style=margin-top:0.2cm>
  <tr border=1px style=height:3cm>
    <td><table class=cols style=height:3cm>
        <tr style=width:100%;height:50%>
          <td class=val><?php if ($amount[71]) echo number_format($amount[71], 2, ',', '.') ?></td>
          <td class=col>(71) Solde TVA dû</td>
          <td class=val><?php if ($amount[72]) echo number_format($amount[72], 2, ',', '.') ?></td>
          <td class=col>(72) Solde TVA à récupérer</td>
        </tr>
        <tr style=width:100%;height:50%>
          <td class=val><?php echo number_format(abs($sTVA), 2, ',', '.') ?></td>
          <td class=col>TVA à <?php echo ($sTVA < 0 ? "recevoir" : "payer") . ". Solde précédant: " . number_format($amount[-1], 2, ',', '.') . "€" ?></td>
          <td class=val><?php displayAmount(91, "&_sel_category=4865"); ?></td>
          <td class=col>(91) Acompte<?php echo ($amount['A1'] || $amount['A2'] ? ". <span style=font-weight:bold;color:red> 1er acompte: " . number_format($amount['A1'], 2, ',', '.') . "€." . ($amount['A2'] ? ", 2ième acompte: " . number_format($amount['A2'], 2, ',', '.') . "€." : '') . "</span>" : '') ?></td>
        </tr>
      </table>
    </td>
</table>

<?php

function displayAmount($idx, $tva)
{
  global $amount, $filter, $basePath1;
  if ($amount[$idx])
    echo "<a target=_blank href=" . TABLE_NAME . ".php$basePath1$filter&_sel_base=$tva>" . number_format($amount[$idx], 2, ',', '.') . "</a>";
}
?>
<script type="text/javaScript">
  function save()
  {
  if (!confirm("Cette operation est irréversible. Es-tu sûr?"))
  return;
  location.assign('billerTVA.php?<?php echo "id=$id&tab=" . $_GET["tab"] . "&action=save&year=$currentYear&quarter=$quarter" ?>');
  }
</script>
