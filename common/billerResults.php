<?php
include $_GET["tab"] . "Conf.php";
include "head.php";
include "tools.php";

//-----------------------------------------------------------------------------------------------------
function getSum($sql, $bTVA = true)
{
  global $TVA;
  $t = jmysql_result(jmysql_query("select sum(amount) from " . TABLE_NAME . " where $sql"), 0);
  if (isset($TVA) && $bTVA) {
    $sql = str_replace(" and hidden=0", '', $sql);
    $t -= jmysql_result(jmysql_query("select sum(tva) from " . TABLE_NAME . " where $sql and TVA is not null"), 0);
  }
  return $t;
}

$basePath .= $basePath ? '&' : '?';
$url = "billerResults.php${basePath}tab=" . $_GET["tab"];
$bPrint = $_GET["print"] == 'y' || !$bRoot;
echo "<title>" . substr(TABLE_NAME, 6) . " bilan</title><body" . ($bPrint && $bRoot ? " onload=window.print()>" : '>') . nl;
if ($m = $_GET["month"])
  $pMonth = "&month=$m";
$currentYear -= 2000;
$subBud = jmysql_num_rows(jmysql_query("show columns from " . TABLE_NAME . "_budget like 'subBudget'")) ? "where subBudget is null and 1" : "where 1";

if ($_GET["year"]) {
  $season = $_GET["year"];
  $pSeason = "&year=$season";
} else
  $season = $currentYear;
if ($_GET["civil"] == 'true') {
  $pCivil = "&civil=true";
  $period = "date like '" . ($season + 2000) . "-%'";
} else
  $period = "season=$season";
$bCivil = $pCivil || !isset($resultCategory);
$bPreviousYear = $season != $currentYear || $pCivil && $season < ($now->format('Y') - 2000);

if (!$bOfficial || $_GET["official"])
  $pOfficial = "&official=" . ($bOfficial ? "true" : "false");
if (!$bOfficial && jmysql_num_rows(jmysql_query("show columns from " . TABLE_NAME . " like 'hidden'")) != 0)
  $hidden = " and hidden=0";

if (!$bPrint && $bDetail = $_GET["detail"] == 'y')
  $pDetail = "&detail=y";
$month = $now->format('n');
?>
<script type="text/javaScript">
  var win=null;
  function biller(c)
  {
  loc='<?php echo TABLE_NAME . ".php${basePath}&action=filter&TVA=y$pSeason$pCivil$pOfficial&order=category" . ($m ? "&_sel_date=" . sprintf("%%2d%02d%%2d", $m) : '') . "&_sel_category=%3d" ?>'+c;
  if (win && !win.closed)
  win.location.replace(loc);
  else
  win=window.open(loc, "_blank");
  win.focus();
  return false;
  }
</script>
<?php
if (!$bPrint) {
  //Civil year
  echo "<input type=checkbox " . ($pCivil ? "checked=checked" : '') . " title='Année civile' onchange=location.replace('$url$pDetail$pMonth$pSeason$pOfficial'+(this.checked?'&civil=true':''))>";
  //Month 
  echo "<select onchange=location.replace('$url$pDetail$pSeason$pCivil$pOfficial&month='+this.options[this.selectedIndex].value)><option value=0" . ($m == 0 ? " selected=selected" : '') . ">&lt;Annuel&gt;</option><option value=$month" . ($m == $month ? " selected=selected" : '') . ">$months[$month]</option>";
  foreach ($months as $k => $v)
    if ($k && $k != $month)
      echo "<option value=$k" . ($m == $k ? " selected=selected" : '') . ">$v</option>";
  echo "</select>" . nl;
  //Season
  echo "<select name=year onchange=location.replace('$url$pDetail$pMonth$pCivil$pOfficial&year='+this.options[this.selectedIndex].value)><option" . ($season == $currentYear ? " selected=selected" : '') . " value=$currentYear>" . (2000 + $currentYear) . "</option>";
  $r = jmysql_query("select distinct season from " . TABLE_NAME . " order by season desc");
  while ($tup = jmysql_fetch_row($r))
    if ($tup[0] != $currentYear)
      echo "<option" . ($season == $tup[0] ? " selected=selected" : '') . " value=$tup[0]>" . (2000 + $tup[0]) . "</option>";
  echo "</select>" . nl;
  //official
  if (isset($resultCategory) && (!$bOfficial || isset($_GET["official"])))
    echo "<input type=checkbox " . ($bOfficial ? "checked=checked" : '') . " title='Billan officiel' onchange=location.replace('$url$pDetail$pMonth$pSeason'+(this.checked?'&official=true':'&official=false'))>";
} else if (!$bRoot) {
  echo "<span style=font-size:large;color:RoyalBlue;margin-right:10px>Choisis " . ($bCivil ? "l'année" : "la saison") . " à afficher</span>";
  echo "<select name=year onchange=location.replace('$url$pDetail$pMonth$pCivil$pOfficial&year='+this.options[this.selectedIndex].value)>";
  $r = jmysql_query("select distinct season from " . TABLE_NAME . "_budget $subBud order by season desc");
  while ($tup = jmysql_fetch_row($r))
    echo "<option" . ($season == $tup[0] ? " selected=selected" : '') . " value=$tup[0]>" . (2000 + $tup[0]) . ($bCivil ? '' : '-' . (2001 + $tup[0])) . "</option>";
  echo "</select>" . nl;
}

if ($m)
  $month = sprintf(" and date like '%%-%02d-%%'", $m);
else
  unset($month);
$bEnded = !$month && !$bOfficial && (jmysql_num_rows(jmysql_query("show columns from " . TABLE_NAME . "_budget like 'ended'")) && jmysql_result(jmysql_query("select count(*) from " . TABLE_NAME . "_budget $subBud and season=$season and ended=1"), 0) || jmysql_result(jmysql_query("select count(*) from " . TABLE_NAME . "_budget $subBud and season=$season and updAmount"), 0));
$amount = $bOfficial ? "ifnull(official,amount)" : "amount";


$currentYear = $season + 2000;
if (!$bPrint) {
  createButton(-1, ($bDetail ? "Cacher" : "Voir") . " détails", "$url$pMonth$pSeason$pCivil$pOfficial" . (!$bDetail ? "&detail=y" : ''));
  createButton(-1, "Imprimer", "$url$pMonth$pSeason$pCivil$pSeason$pDetail$pOfficial&print=y\" target=_blank");
  if (isset($resultCategory))
    createButton(-1, "Officiel", "billerBilan.php${basePath}tab=" . $_GET["tab"] . $pSeason . '" target=_blank');
}

//Compute Sum budget and updated budget ONLY for main categories
if (!jmysql_result(jmysql_query("select count(*) from " . TABLE_NAME . "_budget where season=$season"), 0)) {
  include "common/billerResultNoBudget.php";
  exit();
}
foreach ($category as $k => $v) {
  foreach ($v as $k1 => $v1) {
    $r = jmysql_query("select category,$amount" . ($bEnded ? ",updAmount,ended" : '') . " from " . TABLE_NAME . "_budget $subBud and season=$season and category=" . ($k * 256 + $k1));
    if (!jmysql_num_rows($r))
      continue;
    $tup = jmysql_fetch_row($r);
    $sum[$k] += $tup[1];
    if ($bEnded) {
      $aEnded[$tup[0]] = true;
      $sumUp[$k] += $tup[3] ? getSum("$period$hidden$month$resultCond and category=$tup[0]") : ($tup[2] ? $tup[2] : $tup[1]);
    }
  }
  if ($m) {
    $sum[$k] /= 12;
    if ($bEnded)
      $sumUp[$k] /= 12;
  }
}

for ($loop = ($bPrint ? 0 : 1); $loop < 2; $loop++) {
  if ($bPrint)
    echo "<h1 style='" . ($loop ? ($bRoot ? "page-break-before:always;" : "padding-top:60px;border-top:1px solid black;") : '') . "color:RoyalBlue;text-align:center'>" . ($bDetail && $bPrint ? "Recettes " : "Bilan ") . substr(TABLE_NAME, 6) . (!$bCivil ? " saison $currentYear - " . ($currentYear + 1) : " année $currentYear") . "</h1>";
  echo "<table class=main style=margin:auto><tr><th style=width:200px>Poste</th><th style=width:100px>Budget<br>" . ($m ? "mensuel" : $currentYear . (!$bCivil ? "-" . ($currentYear + 1) : '')) . "</th>" . ($bEnded ? "<th style=width:100px>Budget<br>ajusté</th>" : '') . "<th style=width:100px>Etat " . ($m ? "de<br>$months[$m]" : ($bPreviousYear ? ($bCivil ? "année" : "saison") . "<br>" . ($season + 2000) : "au<BR>" . $now->format("d/m/Y"))) . "</th><th style=width:100px>Déjà<br>réalisé</th>" . ($month ? "<th>Réalisé<br>sur annnée</th>" : '') . "</tr>" . nl;
  //recettes
  arsort($sum);
  display(true);

  //Dépenses
  if ($bPrint && $bDetail) {
    echo "</table class=main border=0px><h1 style='" . ($bRoot ? "page-break-before:always;" : "padding-top:60px;border-top:1px solid black;") . "color:RoyalBlue;text-align:center'>Dépenses " . substr(TABLE_NAME, 6) . (!$bCivil ? " saison $currentYear - " . ($currentYear + 1) : " année $currentYear") . "</h1>";
    echo "<table class=main style=margin:auto><tr><th style=width:200px>Poste</th><th style=width:100px>Budget<br>" . ($m ? "mensuel" : $currentYear . (!$bCivil ? "-" . ($currentYear + 1) : '')) . "</th>" . ($bEnded ? "<th style=width:100px>Budget<br>ajusté</th>" : '') . "<th style=width:100px>Etat " . ($m ? "de<br>$months[$m]" : ($bPreviousYear ? "saison<br>" . ($season + 2000) : "au<BR>" . $now->format("d/m/Y"))) . "</th><th style=width:100px>Déjà<br>réalisé</th>" . ($month ? "<th>Réalisé<br>sur annnée</th>" : '') . "</tr>" . nl;
  }
  asort($sum);
  display(false);

  //Bilan
  if (!$month && (!$bPrint || !$bDetail)) {
    echo "</table><table class=main id=edit style=border:0;margin-top:1cm;width:300px;margin-left:auto;margin-right:auto>";
    echo "<tr><td>Bilan " . ($bPreviousYear ? $season + 2000 : "actuel") . "</td><td style=color:" . ($result->t2 < 0 ? "red" : "darkgreen") . ";text-align:right>" . number_format($result->t2, 0, ',', '.') . " €</td></tr>";
    echo "<tr><td>Bilan prévisonnel</td><td style=color:" . ($result->t1 < 0 ? "red" : "darkgreen") . ";text-align:right>" . number_format($result->t1, 0, ',', '.') . " €</td></tr>";
    if ($bEnded && !$bPreviousYear)
      echo "<tr><td>Bilan prévisonnel ajusté</td><td style=color:" . ($result->t3 < 0 ? "red" : "darkgreen") . ";text-align:right>" . number_format($result->t3, 0, ',', '.') . " €</td></tr>";

    //Avoirs
    echo "</table><table class=main id=edit style=page-break-before:always;border:0;margin-top:1cm;width:300px;margin-left:auto;margin-right:auto>";
    echo "<tr style='border-bottom:1 solid #5d73a3'><td colspan=2 style=text-align:center;font-size:20px;padding-top:1cm;color:RoyalBlue>Capital</td>";
    $sum1 = 0;
    $caisse = 0;
    foreach ($account as $k => $v) {
      if ($k >= 20 && $k < 30)
        continue;
      if (($bCash = substr($v, 0, 6) == "Caisse") && $v[strlen($v) - 1] != '+')
        if (abs($t = getSum(" account=$k and " . ($pCivil ? "date<='" . ($season + 2000) . "-12-31'" : "season<=$season"), false)) > 0.1)
          $ofCash += $t;
      if (abs($t = getSum(" account=$k$hidden and " . ($pCivil ? "date<='" . ($season + 2000) . "-12-31'" : "season<=$season"), false)) < 0.1)
        continue;
      if ($bCash) {
        $caisse += $t;
        continue;
      }
      $sum1 += $t;
      echo "<tr><td>$v</td><td style=color:" . ($t < 0 ? "red" : "darkgreen") . ";text-align:right>" . number_format($t, 0, ',', '.') . " €</td></tr>";
    }
    $sum1 += $caisse;
    if ($caisse) {
      echo "<tr><td>Caisse</td><td style=color:" . ($caisse < 0 ? "red" : "darkgreen") . ";text-align:right>" . number_format($caisse, 0, ',', '.') . " €</td></tr>";
      if ($bRoot && !$bOfficial) {
        echo "<tr style=text-align:right;font-size:small;font-style:italic><td style='padding:0 0 0 0'>Officielle</td><td style='padding:0 70 0 0;color:" . ($ofCash < 0 ? "red" : "darkgreen") . "'>" . number_format($ofCash, 0, ',', '.') . " €</td></tr>";
        echo "<tr style=text-align:right;font-size:small;font-style:italic><td style='padding:0 0 0 0'>Black</td><td style='padding:0 70 0 0;color:" . (($t = $caisse - $ofCash) < 0 ? "red" : "darkgreen") . "'>" . number_format($t, 0, ',', '.') . " €</td></tr>";
      }
    }
    //Virements internes
    $t = -getSum("season<=$season$hidden$resultCond and category&0xFF00=" . TRANSACTION_CATEGORY);
    if (abs($t) >= 0.01)
      echo "<tr><td>Virements internes</td><td style=color:" . ($t < 0 ? "red" : "darkgreen") . ";text-align:right>" . number_format($t, 0, ',', '.') . " €</td></tr>";
    $sum1 += $t;

    echo "<tr style='border-top:2px solid #5d73a3;font-size:large'><td style=color:RoyalBlue>Total avoirs</td><td style=color:" . ($sum1 < 0 ? "red" : "darkgreen") . ";text-align:right>" . number_format($sum1, 0, ',', '.') . " €</td></tr>";

    //TVA
    $t = 0;
    $dt = ($pCivil ? "date<='" . ($season + 2000) . "-12-31'" : "season<=$season") . ($bOfficial ? " and account<50" : '');
    if (isset($TVA)) {
      $t -= jmysql_result(jmysql_query("select sum(tva) from " . TABLE_NAME . " where TVA_status is null"), 0);
      if ($t)
        echo "<tr><td>TVA</td><td style=color:" . ($t < 0 ? "red" : "darkgreen") . ";text-align:right>" . number_format($t, 0, ',', '.') . " €</td></tr>";
      $sum1 += $t;
    }

    //Dettes/Créances
    $t = $t1 = 0;
    foreach ($category as $k => $v) {
      if ($v[0][0] == "Dettes")
        $t -= getSum("$dt and category&0xFF00=" . ($k * 256));
      else if ($v[0][0] == "Réserve")
        $t1 -= getSum("$dt and category&0xFF00=" . ($k * 256));
    }
    $sum1 += $t + $t1;
    if ($t)
      echo "<tr><td>" . ($t < 0 ? "Dettes" : "Créances") . "</td><td style=color:" . ($t < 0 ? "red" : "darkgreen") . ";text-align:right>" . number_format($t, 0, ',', '.') . " €</td></tr>";
    if ($t1)
      echo "<tr><td>Réserve</td><td style=color:" . ($t1 < 0 ? "red" : "darkgreen") . ";text-align:right>" . number_format($t1, 0, ',', '.') . " €</td></tr>";

    $t = 0;
    if (!$bPreviousYear) {
      $t = ($bEnded ? $result->t3 : $result->t1) - $result->t2;
      echo "<tr><td>Solde bilan</td><td style=color:" . ($t < 0 ? "red" : "darkgreen") . ";text-align:right>" . number_format($t, 0, ',', '.') . " €</td></tr>";
    }
    echo "<tr style='border-top:2px solid #5d73a3;font-size:large'><td style=color:RoyalBlue>Disponible</td><td style=color:" . (($t1 = $sum1 + ($t < 0 ? $t : 0)) < 0 ? "red;font-weight:bold" : "darkgreen") . ";text-align:right>" . number_format($t1, 0, ',', '.') . " €</td></tr></table>";
    //if ($t1<-300)
    //echo "<h2 style=width:100%;text-align:center;color:red><img width=100px src=attention.png style=vertical-align:middle>Rupture de paiement possible!";
  }
  $bDetail = true;
}

//-----------------------------------------------------------------------------------------------------
function display($bIncome)
{
  $par = 0;
  if ($bIncome)
    $type = "Recettes";
  else
    $type = "Dépenses";
  echo "<tr><td colspan=5 style='border-left:0px;border-right:0;border-bottom:2px solid #5d73a3;font-weight:bold;font-size:large;color:RoyalBlue" . (!$bIncome ? ";padding-top:10px" : '') . "'>$type</td></tr>";
  global $bRoot, $period, $m, $category, $aEnded, $amount, $catSerial, $bEnded, $sum, $sumUp, $season, $bDetail, $hidden, $month, $resultCond, $TVA, $bPrint, $subBud, $seeRequest;
  foreach ($sum as $k => $v) {
    $s = getSum("$period$hidden$month$resultCond and category&0xFF00=" . ($k * 256));
    if (!$s && !$v || $bIncome && ($s < 0 || $s == 0 && $v < 0) || !$bIncome && ($s > 0 || $s == 0 && $v > 0))
      continue;
    echo "<tr class=parity$par><td class=tabLink><a onclick=biller(" . ($k * 256) . ")>" . $category[$k][0][0] . "</a><td style=text-align:right;color:" . ($v > 0 ? 'black' : 'darkred') . ">" . number_format($v, 0, ',', '.') . " €</td>";
    $par = 1 - $par;
    if ($bEnded)
      echo "<td style=text-align:right;color:" . ($sumUp[$k] > 0 ? 'black' : 'darkred') . ($sumUp[$k] != $v ? ";text-decoration:underline" : '') . ">" . number_format($sumUp[$k], 0, ',', '.') . " €</td>";
    echo "<td style=text-align:right;color:" . ($s >= 0 ? 'black' : 'darkred') . ">" . number_format($s, 0, ',', '.') . " €</td>";
    if ($bIncome)
      $col = ($t = $v ? $s * 100 / $v : 0) >= 100 ? "RoyalBlue" : "black";
    else
      $col = ($t = $v ? $s * 100 / $v : 0) >= 100 ? "red" : "black";
    echo "<td style=text-align:right;color:$col>" . ($t ? number_format($t) . " %" : '') . "</td>";
    if ($m) {
      $y = getSum("$period$hidden$resultCond and substr(date,6,2)<=$m and category&0xFF00=" . ($k * 256));
      if ($bIncome)
        $col = ($t = $y * 100 / $v / $m) >= 100 ? "RoyalBlue" : "black";
      else
        $col = ($t = $y * 100 / $v / $m) >= 100 ? "red" : "black";
      echo "<td style=text-align:right;color:$col>" . number_format($t) . " %</td>";
    }
    echo "</tr>";
    if ($bDetail && sizeof($category[$k]) > 1) {
      foreach ($category[$k] as $kk => $vv) {
        $dt = getSum("$period$hidden$month$resultCond and category=" . ($k * 256 + $kk));
        $bdt = jmysql_fetch_row(jmysql_query("select $amount" . ($bEnded ? ",updAmount,ended" : '') . " from " . TABLE_NAME . "_budget $subBud and season=$season and category=" . ($k * 256 + $kk)));
        //dump($bdt, $category[$k][$kk][0]); 
        if (!$dt && (!$kk || !$bdt[0] && !$bdt[1]))
          continue;
        if ($month) {
          $bdt[0] /= 12;
          $bdt[1] /= 12;
        }
        $ct = $catSerial[$k * 256 + $kk];
        echo "<tr class=parity$par style=font-size:small;font-style:italic;color:DarkSlateGray><td>$ct</td>";
        $par = 1 - $par;
        echo "<td style=padding-right:30px;color:" . ($bdt[0] < 0 ? 'darkred' : 'black') . ";text-align:right>" . ($bdt[0] ? number_format($bdt[0], 2, ',', '.') . " €" : '') . "</td>";
        if ($bEnded) {
          $t = $bdt[2] ? $dt : ($bdt[1] ? $bdt[1] : $bdt[0]);
          echo "<td style=padding-right:30px;color:" . ($t < 0 ? 'darkred' : 'black') . ($t != $bdt[0] ? ";text-decoration:underline" : '') . ";text-align:right>" . ($t ? number_format($t, 2, ',', '.') . " €" : '') . "</td>";
        }
        echo "<td style=padding-right:30px;color:" . ($dt < 0 ? 'darkred' : 'black') . ";text-align:right>" . number_format($dt, 2, ',', '.') . " €</td>";
        $bdt = $bdt[1] ? $bdt[1] : $bdt[0];
        if ($bdt)
          if ($bIncome)
            $col = ($t = $dt * 100 / $bdt) >= 100 ? "RoyalBlue" : "black";
          else
            $col = ($t = $dt * 100 / $bdt) >= 100 ? "red" : "black";
        echo "<td style=padding-right:30px;color:$col;text-align:right>" . ($bdt ? number_format($t, 0, ',', '.') . " %" : '') . "</td>";
        if ($m) {
          $ydt = getSum("$period$hidden$resultCond and substr(date,6,2)<=$m and category=" . ($k * 256 + $kk));
          if ($bdt)
            if ($bIncome)
              $col = ($t = $ydt * 100 / $bdt / $m) >= 100 ? "RoyalBlue" : "black";
            else
              $col = ($t = $ydt * 100 / $bdt / $m) >= 100 ? "red" : "black";
          echo "<td style=color:$col;text-align:right>" . ($bdt ? number_format($t, 0, ',', '.') . " %" : '') . "</td>";
        }
        echo "</tr>";
      }
    }
    //$v=main category budget, $s=main category situation, $y=main category for the month 
    $t1 += $v;
    $t2 += $s;
    if ($m)
      $t4 += $y;
    if ($bEnded)
      $t3 += $sumUp[$k];
    //echo "v=$v, sumup=$sumUp[$k]".NL;
  }
  $col = $t1 && ($t = $t2 * 100 / $t1) >= 100 ? ($bIncome ? "RoyalBlue" : "red") : "black";
  echo "<tr style='border-top:2px solid black;border-bottom:2px solid black;font-weight:bold'><td>Total $type</td><td style=text-align:right;color:black>" . number_format($t1, 0, ',', '.') . " €</td>";

  global $result;
  if ($bEnded) {
    echo "<td style=text-align:right;color:black>" . number_format($t3, 0, ',', '.') . " €</td>";
    $result->t3 += $t3;
  }
  echo "<td style=text-align:right;color:black>" . number_format($t2, 0, ',', '.') . " €</td><td style=text-align:right;color:$col>" . number_format($t) . " %</td>";
  if ($m) {
    if ($bIncome)
      $col = ($t = $t4 * 100 / $t1 / $m) >= 100 ? "RoyalBlue" : "black";
    else
      $col = ($t = $t4 * 100 / $t1 / $m) >= 100 ? "red" : "black";
    echo "<td style=text-align:right;color:$col>" . number_format($t) . " %</td>";
  }
  echo "</tr>" . nl;

  $result->t1 += $t1;
  $result->t2 += $t2;
}
