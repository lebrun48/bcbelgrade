<?php

//--------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------
//dump($request);
//$seeRequest=true;
function countDimension($a, $c = 0)
{
  if (is_array($a))
    return countDimension(current($a), ++$c);
  return $c;
}

class TableEdit
{

  public $bHideCol; //true if colums can be hidden
  public $editURL; //URL on click Edit
  public $actionURL; //URL on actions other than edit
  public $tableId; //Table ID for CSS
  public $tableName; //MySql table name
  public $colDef; // Array containing colonne definition
  public $visible; //Contains MySql column to show (between commas)
  public $addCol;  // additional MySql column       
  public $filter;  //array with the filter col name as key and the filter value as value
  public $bExport; // if true an export file will be created
  public $exportFile; //file object for export
  public $exportFileName; //file name for export
  public $exportAvailable; //if false no export button
  public $order;      // order by
  public $line;       // used for export
  public $multiTemplate = 1;
  public $bLimit;        //limit the number of tuple to 100;
  public $primKey;       //Primary key to edit or remove tuple
  public $tableClass = "main";

//--------------------------------------------------------------------------------------------
  function __construct()
  {
    getRequest($this);
  }

//--------------------------------------------------------------------------------------------
  public function printHeaderLine($row)
  {
    echo "<tr class=\"parity$this->par\" style=\"" . $this->applyStyle($row) . '">';
    $this->par = 1 - $this->par;
    $this->numLine++;
    if ($this->bHeader) {
      echo '<td style=white-space:nowrap>';
      if ($this->bPrintNumLine)
        echo $this->printNumLine($this->numLine, $row);
      echo $this->editImg($row);
      echo "&nbsp;" . $this->removeImg($row);
      echo "</td>";
    }
    if ($this->selKey)
      echo "<td>" . ($this->bSelBox($row) ? "<input type=checkbox  onclick=sumChecked(this,event," . ($this->numCB["_select_"] ++) . ",'_select_') name=_select_ value='" . $row[$this->selKey] . ($this->sumKey ? '$' . $row[$this->sumKey] : '') . "'" : '') . "</td>";
  }

//--------------------------------------------------------------------------------------------
  public function printLine($row)
  {
    $this->printHeaderLine($row);
    if ($this->bExport) {
      $this->numCol = 0;
      $this->line = array();
    }
    foreach ($this->colDef as $key => $val) {
      if (!$val[1] || $this->curVisible && !in_array($key, $this->curVisible) || !$this->bViewAll && $this->hideCol && in_array($key, $this->hideCol))
        continue;

      echo $this->applyDisplay($key, $val, $row, $this->numCol, null);
      if (!$val["noExport"] && $val[3] != "[cb]")
        $this->numCol++;
      else
        unset($this->line[$this->numCol]);
    }
    echo "</tr>\n";
    if ($this->bExport) {
      fwrite($this->exportFile, "<tr style=\"" . $this->applyStyle($row) . '">');
      foreach ($this->line as $val)
        fwrite($this->exportFile, $val);
      fwrite($this->exportFile, "</tr>\n");
      $this->applyExport();
    }
  }

//--------------------------------------------------------------------------------------------
  function write($val, $bStore = false)
  {
    if ($this->bExport)
      if ($bStore)
        $this->line[$this->numCol] .= $val;
      else
        fwrite($this->exportFile, $val);
    echo $val;
  }

  //Format value in table cell
  public function formatValue($val, $elmDef, $dispValue = null)
  {
    if ($this->bExport && (!$val["noExport"]) && !isset($this->line[$this->numCol]))
      $this->line[$this->numCol] = "<td style=\"$elmDef[3]\">$dispValue</td>";
    return "<td style=\"$elmDef[3]\">$val</td>";
  }

  // callback function called to display one col
  public function storeDisplay($key, $val, $row, $idxLine, $dispValue)
  {
    if ($this->bExport && !$val["noExport"])
      $this->line[$idxLine] .= "<td style='$val[3]'>" . ($dispValue ? $dispValue : $row[$key]) . "</td>";
  }

//--------------------------------------------------------------------------------------------
  public function applyDisplay($key, $val, $row, $idxLine, $dispValue = null)
  {
    if ($style = $val[3]) {
      $b = 0;
      while ($style[$b] == '[') {
        $e = strpos($style, ']', $b);
        $ft = substr($style, $b + 1, $e - $b - 1);
        if (($p = strpos($ft, ':')) !== false) {
          $par = substr($ft, $p + 1);
          $ft = substr($ft, 0, $p);
        }
        switch ($ft) {
          case "date":
            $st = " style=text-align:center";
            if ($dispValue === null) {
              $dispValue = '';
              if ($t = $row[$key]) {
                $dt = new DateTime($t);
                $dispValue = $dt->format("d/m/Y");
              }
            }
            break;

          case "hour":
            $st = " style=text-align:right";
            if ($dispValue === null) {
              $dispValue = '';
              if ($t = $row[$key]) {
                $dt = new DateTime($t);
                $dispValue = $dt->format("H:i");
              }
            }
            break;

          case "dt":
          case "dts":
            $st = " style=text-align:right";
            if ($dispValue === null) {
              $dispValue = '';
              if ($t = $row[$key]) {
                $dt = new DateTime($t);
                $dispValue = formatDate($dt, $par ? $par : "D/n/Y H:i" . ($ft == "dts" ? ":s" : ''));
              }
            }
            break;

          case "mail":
            if ($dispValue === null)
              $dispValue = "<a href=mailto:$row[$key]>$row[$key]</a>";
            break;

          case "int":
            $st = " style=text-align:right";
            break;

          case "money":
            $st = " style=text-align:right";
            if (!isset($dispValue))
              $dispValue = strlen($row[$key]) ? "<span" . (($t = $row[$key]) < 0 ? " style=color:red>" : '>') . number_format($t, 2, ',', '.') . " €</span>" : '';
            break;

          case "click":
            $st = " onclick=${key}Click(this) $st";
            break;

          case "cb":
            $cb = $this->setCBVal($row, $val);
            $dispValue = $cb ? "<input type=checkbox onclick=sumChecked(this,event," . ($this->numCB[$key] ++) . ",'$key') name=$key value='$cb'>" : '';
            break;

          default:
            if (substr($style, $b + 1, 4) == 'hide') {
              $v = substr($style, $b + 5, $e - $b - 5);
              $st = " class=hide title=\"" . htmlspecialchars(strip_tags($dispValue === null ? $row[$key] : $dispValue)) . "\"" . ($st ? $st : " style=") . "max-width:${v}px;width:${v}px;";
            } else
              unset($st);
        }
        $b = $e + 1;
      }
      $t = substr($style, $b);
      if ($t)
        $st = strpos($t, ':') ? ($st ? $st . $t : " style='$t'") : " class='$t'";
    }
    if (!isset($dispValue))
      $dispValue = $row[$key];
    if (!$st)
      $st = ' ' . $this->formatTD($key, $row);
    if ($this->bExport && !$val[5] && !isset($this->line[$idxLine]))
      $this->line[$idxLine] = "<td$st>$dispValue</td>";
    return "<td$st>$dispValue</td>";
  }

//--------------------------------------------------------------------------------------------
  public function buildFilter($col, $val)
  {
    if (substr($val, 0, 3) == "_c=") {
      //combine
      $ar = unserialize(substr($val, 3));
      foreach ($ar as $v)
        $t .= isset($v) ? "'$v'," : "null,";
      return "$col IN (" . substr($t, 0, -1) . ")";
    }
    $t = strtok($val, "&+");
    $tst = '(';
    while (strlen($t)) {
      if ($t[0] == '(') {
        $tst .= '(';
        $t = substr($t, 1);
      }
      if ($t[strlen($t) - 1] == ')') {
        $t = substr($t, 0, -1);
        $bPar = true;
      } else
        $bPar = false;
      switch ($t[0]) {
        case '$':
          $tst .= $col . " is NULL";
          break;

        case '<':
        case '>':
        case '=':
          $i = $t[1] == '=' ? 2 : 1;
          $tst .= $t[$i] != "'" && is_numeric(substr($t, $i)) ? $col . $t : $col . substr($t, 0, $i) . "'" . addslashes(substr($t, $i)) . "'";
          break;

        case '!':
          switch ($t[1]) {
            case '=':
              $tst .= "$col<>" . ltrim(substr($t, 2));
              break;

            case '$':
              $tst .= "$col is NOT NULL";
              break;

            default:
              $tst .= "$col NOT LIKE '%" . addslashes(ltrim(substr($t, 1))) . "%'";
              break;
          }
          break;

        default:
          $tst .= "$col LIKE '%" . addslashes($t) . "%'";
          break;
      }
      if ($bPar)
        $tst .= ')';
      $and = strpos($val, "&", $beg);
      if (($or = strpos($val, "+", $beg)) === false) {
        if ($and === false)
          return $tst . ')';

        $tst .= " AND ";
        $beg = $and + 1;
      } else if ($and === false || $or < $and) {
        $tst .= " OR ";
        $beg = $or + 1;
      } else {
        $tst .= " AND ";
        $beg = $and + 1;
      }
      $t = strtok("&+");
    }
  }

//--------------------------------------------------------------------------------------------
  //Call back function to build condition
  public function applyFilter($key, $val)
  {
    $c = $this->colDef[$key][4] ? $this->colDef[$key][4] : $key;
    if ($c[0] == '@')
      return;
    if ($this->colDef[$key][3] == "[date]" && substr($val, 0, 3) != "_c=") {
      if (!is_numeric($p = $val[0]))
        $val = substr($val, 1);
      else
        unset($p);
      $t = strtok($val, "/");
      while ($t) {
        $v = sprintf("%02d", $t) . ($v ? "-$v" : '');
        $t = strtok("/");
      }
      $v = $p . $v;
    } else
      $v = $val;
    return $this->buildFilter($c, $v);
  }

//--------------------------------------------------------------------------------------------
  //Events   
  public function editClick($row)
  {
    return "editClick(" . (!empty($row) ? "'" . str_replace("'", "$", $this->buildKey($row)) . "'" : "''") . ")";
  }

  public function deleteClick($row)
  {
    return "deleteClick('" . str_replace("'", "$", $this->buildKey($row)) . "', true)";
  }

  public function hideColClick($key)
  {
    return "hideColClick('$key')";
  }

  public function viewAllClick()
  {
    return "viewAllClick()";
  }

  public function orderClick($key)
  {
    return "orderClick('$key')";
  }

  public function bSelBox($row)
  {
    return true;
  }

  public function formatTD($key, $row)
  {
    
  }

  public function setCBVal($row, $val)
  {
    return $row[$val[1]];
  }

  public function insertClick()
  {
    return $this->editClick('');
  }

  //Terminate table
  public function terminate()
  {
    
  }

  function printNumLine($num, $row)
  {
    return $num;
  }

  //Call back to apply color to a row
  public function applyStyle($row)
  {
    
  }

  //Call back to apply order (analyse $this->ioredr value to change it eventually)
  public function applyOrder()
  {
    
  }

  //Call back to insert edit image
  public function editImg($row)
  {
    return '<img title="Editer" src="common/b_edit.png" style="border-style:none; cursor:pointer" onclick="' . $this->editClick($row) . '">';
  }

  //Build the colum title with chekbox for slection
  public function selectHD($name)
  {
    if (isset($this->numCB[$name]) && !$this->nElements[$name])
      $this->nElements[$name] = $this->numCB[$name];
    $this->numCB[$name] = 0;
    echo "<th style=width:20px><span id=$name" . ($this->tabNb ? $this->tabNb : '') . " onclick=selectAll('$name') title='Sélectionner Tous' class=tabLink><input type=checkbox></span></th>";
  }

  //Call back to insert remove image
  public function removeImg($row)
  {
    return '<img src="common/b_drop.png" title="Supprimer" style="border-style:none; cursor:pointer" onclick="' . $this->deleteClick($row) . '">';
  }

//--------------------------------------------------------------------------------------------
  public function buildKeyList()
  {
    return strtok($this->tableName, ", ") . " where " . $this->selKey;
  }

//--------------------------------------------------------------------------------------------
  public function buildKey($row)
  {
    $tab = strtok($this->tableName, ", ");
    if ($this->primKey)
      return "$tab where $tab.$this->primKey=" . $row[$this->primKey];

    //Main table is the first one
    $len = strlen($tab);
    $req = $tab . " where ";
    foreach ($this->colDef as $k => $v)
      if (!$v[4] || $v[4][0] != '@' && substr($v[4], 0, $len) == $tab && ($att = substr($v[4], $len + 1))) {
        if (!$att)
          $att = $k;
        $v = $row[$k];
        if (is_null($v))
          $req .= "$sep$att is null";
        else {
          if (is_numeric($v))
            if (strpos($v, '.') !== false)
              $req .= "${sep}concat($att)=$v";
            else
              $req .= "$sep$att=$v";
          else
            $req .= "$sep$att='" . addslashes($v) . "'";
        }
        $sep = " and ";
        $att = null;
      }
    return $req;
  }

  //Call back to apply export
  public function applyExport()
  {
    
  }

  public function exportClick()
  {
    
  }

  public function buildFrom()
  {
    return $this->tableName;
  }

  //Call back to add some special function in first col
  public function addHeaderFunction()
  {
    
  }

  //--------------------------------------------------------------------------------------------
  // build sql
  function buildSql($where)
  {
    if ($save = $this->order) {
      if (!($order = $this->applyOrder()))
        $order = "order by $this->order " . $this->dir;
      $this->order = $save;
    }
    foreach ($this->colDef as $k => $v) {
      $t = $v[4];
      if ($t != '@')
        $sql .= ',' . ($t ? $t . " AS $k" : $k);
    }
    if (!$this->addCol)
      $sql[0] = ' ';
    $sql = "select $this->addCol $sql from " . $this->buildFrom() . " where $where $order";
    $res = jmysql_query($sql);
    if (!$res)
      stop(__FILE__, __LINE__, jmysql_error() . ": sql=$sql");
    return $res;
  }

//--------------------------------------------------------------------------------------------
  // build help List
  function buildHelpList($key)
  {
    $where = " WHERE ";
    if ($this->filter) {
      foreach ($this->filter as $k => $v)
        if ($k != $key)
          $where .= $this->applyFilter($k, $v) . " AND ";
    }
    $where .= $this->defaultWhere . " 1 group by " . $key;
    return $this->buildHelpArray($key, $where);
  }

//--------------------------------------------------------------------------------------------
  function isFirstInstance($key)
  {
    if (!$this->bFirstInstance[$key])
      return $this->bFirstInstance[$key] = true;
    else
      return false;
  }

//--------------------------------------------------------------------------------------------
  function buildHelpArray($key, $where)
  {
    $c = $this->colDef[$key][4] ? $this->colDef[$key][4] : $key;
    return jmysql_query("select $c as $key from " . $this->buildFrom() . $where);
  }

//--------------------------------------------------------------------------------------------
  function buildWhere()
  {
    $where = $this->defaultWhere . ' 1';
    if ($this->filter) {
      foreach ($this->filter as $k => $v)
        $where .= " AND " . $this->applyFilter($k, $v);
    }
    return $where;
  }

//--------------------------------------------------------------------------------------------
  function build()
  {
    global $request, $action, $bRefreshExecuted;

    switch ($action) {
      case "_sys_limit":
        $this->suspendLimit = true;
        break;

      case "viewAll":
        $this->bViewAll = !($this->bViewAll);
        break;

      case "export":
        $this->bExport = true;
        break;
    }
    if ($this->bExport) {
      $this->exportFile = fopen($this->exportFileName, 'wt');
      fwrite($this->exportFile, "<table cellspacing=\"0\" rules=\"all\" border=\"1\" style=\"border-collapse:collapse;\"><tr>");
      $this->bLimit = 0;
    }
    if (!$this->exportButtonEnd && $this->bExportAvailable) {
      include_once "tools.php";
      $this->exportExcel();
    }
    $this->createLimitButton();

    echo "<form $this->formPar id=mainForm action=$this->actionURL" . buildGetParam($this) . " method=post>" . nl;
    echo "<input type=submit style=display:none id=dummySubmit><input name=order id=order type=hidden value=$this->order><input name=dir id=dir type=hidden value=$this->dir><input id=scr name=scr type=hidden><input id=operation type=hidden><input name=action id=action value=filter type=hidden>" . nl;
    if (isset($this->visible[0]->title)) {
      includeTabs($this->multiTemplate);
      $this->nTemplates = sizeof($this->visible);
      foreach ($this->visible as $k => $v)
        echo "      <li><a href=#view$k>" . $v->title . "</a></li>" . nl;
      echo "  </ul>";
      echo "<div class=tabcontents>";
      $this->bFirstTab = true;
      foreach ($this->visible as $k => $v) {
        $this->tabNb = $k;
        $this->curVisible = $this->visible[$k]->visible;
        echo "<div id=view$k>";
        $this->bExport = isset($this->exportTab) && $this->exportTab == $k;
        $this->design();
        $this->bFirstTab = false;
        echo "</div>";
      }
      echo "</div>";
    } else {
      if (isset($this->visible[0]->title)) {
        $this->tabNb = $request["tabNbr"];
        $this->curVisible = $this->visible[$this->tabNb]->visible;
      } else
        $this->curVisible = $this->visible;
      $this->bFirstTab = true;
      $this->design();
    }
    unset($this->suspendLimit);
    $this->bExport = $this->bExport || isset($this->exportTab);
    include "common/tableToolsjs.php";
  }

//--------------------------------------------------------------------------------------------
  function createLimitButton()
  {
    if ($this->bLimit && !$this->bNoLimitButton)
      createAction(5, "Afficher tout (<span id=textAllTups></span> lignes)", "getAllTups()\" id=limitButton style=\"display:none;margin-left:20px");
  }

//--------------------------------------------------------------------------------------------
  // build table
  function colTitle($key, $val)
  {
    if ($this->bExport && !$val["noExport"] && $val[3] != "[cb]")
      fwrite($this->exportFile, "<th>$val[1]</th>");
    if ($val[3] == "[cb]") {
      $this->selectHD($key);
      echo "</th>";
      return;
    }
    if ($st = $val[2])
      $st = strpos($st, ':') ? " style='$st'" : " class='$st'";
    echo "<th$st>";

    if ($val[4] != '@')
      echo "<a href=# " . (($b = $this->order == $key) ? "style=text-decoration:underline " : "") . "onclick=" . $this->orderClick($key) . " title='Trier par " . $val[1] . "'>" . $val[1] . ($b ? "<img style=margin-left:2px;border:0px src=common/" . ($this->dir == "asc" ? "up.png" : "down.png") . ">" : '') . "</a>";
    else
      echo $val[1];
    if ($this->bHideCol)
      echo "&nbsp;<a href=# onclick=" . $this->hideColClick($key) . "><img style=width:12px src=common/" . ($this->hideCol && in_array($key, $this->hideCol) ? "eye_plus.png title='Montrer la colonne'" : "eye_minus.png title='Cacher la colonne'") . "></a>" . nl;
    if ($val[0]) {
      echo "<br><img src=common/filter.png class=combo" . (isset($this->filter[$key]) ? " style=background-color:Wheat" : '') . " id=combo" . $this->tabNb . "_$key>" . nl;
      switch ($val[0]) {
        case -1:
          $this->buildHelpCombo($key, $val);
          break;

        case -2:
          if ($b = $this->isFirstInstance($key)) {
            $r = $this->buildHelpList($key);
            echo "<datalist id=list" . $this->tabNb . "_$key>";
            if (is_array($r))
              foreach ($r as $k => $v)
                echo "<option value=\"$v\">";
            else if ($r)
              while ($tup = jmysql_fetch_row($r))
                if ($tup[0])
                  echo "<option value=\"$tup[0]\">";
            echo "</datalist>";
          }
          echo "<span class=combo id=span_combo" . $this->tabNb . "_$key>";
          echo "<input onfocus=this.select() id=input" . $this->tabNb . "_$key class=sel_text list=list_$key value=\"" . $this->filter[$key] . "\">";
          if ($b)
            echo "<input type=hidden name=_sel_$key value=\"" . $this->filter[$key] . "\">";
          break;


        default:
          echo "<span class=combo id=span_combo" . $this->tabNb . "_$key>Filtrer sur une partie de texte:" . NL;
          echo "<input onfocus=this.select() class=sel_text id=input" . $this->tabNb . "_$key type=text value=\"" . $this->filter[$key] . "\" size=20><br>";
          if ($this->isFirstInstance($key))
            echo "<input type=hidden name=_sel_$key value=\"" . $this->filter[$key] . "\">";
          break;
      }
      echo "<input type=button class=combo_ok value=Ok accesskey=return><input type=button class=combo_close value=Fermer></span>";
    }
    echo "</th>";
  }

//--------------------------------------------------------------------------------------------
  // build table
  function buildHelpCombo($key, $val)
  {
    $r = $this->buildHelpList($key);
    if (isset($this->filter[$key])) {
      $t = unserialize(substr($this->filter[$key], 3));
      foreach ($t as $k => $v)
        $t[$k] = $v;
    }
    echo "<span class=combo id=span_combo" . $this->tabNb . "_$key>Filtre:<div style='border:1px solid lightgray;height:200px;overflow:auto'>";
    echo "<input type=checkbox class=combo_check id=combo_check" . $this->tabNb . "_$key><span>(Sélectionner tout)</span>" . NL;
    if (is_array($r))
      foreach ($r as $k => $v)
        echo "<input value='_sel_${key}§$k' type=checkbox class='combo_check" . $this->tabNb . "_$key combo_check'" . ($t && in_array($k, $t) ? " checked=checked" : '') . ">$v" . NL;
    else if ($r) {
      $bDt = $this->colDef[$key][3] == "[date]";
      while ($tup = jmysql_fetch_row($r))
        if (!$bDt || $tup[0])
          echo "<input value='_sel_${key}§$tup[0]' type=checkbox class='combo_check  combo_check" . $this->tabNb . "_$key'" . ($t && in_array($tup[0], $t) ? " checked=checked>" : '>') . (isset($tup[1]) ? $tup[1] : ($bDt ? substr($tup[0], -2) . '/' . substr($tup[0], 5, 2) . '/' . substr($tup[0], 0, 4) : $tup[0])) . NL;
    }
    echo "</div>";
    if ($this->isFirstInstance($key))
      echo "<input type=hidden name=_sel_$key value=\"" . htmlspecialchars($this->filter[$key]) . "\">";
  }

//--------------------------------------------------------------------------------------------
  function exportExcel()
  {
    createAction(-1, "Export 'Excel'", "exportExcel()", 120);
  }

//--------------------------------------------------------------------------------------------
  // build table
  function design()
  {
    echo "<table class=$this->tableClass" . ($this->tableId ? " id='$this->tableId'" : '') . '>' . nl;
    echo "<tr>\n";
    $this->par = 0;
    unset($this->line);
    if ($this->bHeader) {
      echo '<th style=width:37px;text-align:left>';
      if ($this->bHideCol)
        echo "<img onclick=" . $this->viewAllClick() . " class=imgButton src=common/b_props.png title='Voir " . ($this->bViewAll ? "les colonnes sélectionnées" : "toutes les colonnes") . "'>";
      if ($this->bInsertNew)
        echo "<img onclick=\"" . $this->insertClick() . "\" class=imgButton src=common/new.png title='Insérer un nouvel élément'>";
      echo $this->addHeaderFunction() . "</th>";
    }
    if ($this->selKey)
      $this->selectHD("_select_");

    foreach ($this->colDef as $key => $val) {
      if (!$val[1]) {
        echo "<input name=_sel_$key value='" . $this->filter[$key] . "' type=hidden>";
        continue;
      } else if ($this->curVisible && !in_array($key, $this->curVisible) || !$this->bViewAll && $this->hideCol && in_array($key, $this->hideCol))
        continue;
      $this->colTitle($key, $val);
    }


    echo "</tr>\n";
    if ($this->bExport)
      fwrite($this->exportFile, "</tr>\n");
    //liste members
    if ($this->bFirstTab) {
      $where = $this->buildWhere();
      //select rows
      $this->gRes = $this->buildSql($where);

      if (!$this->suspendLimit && $this->bLimit && $this->gRes && ($t = jmysql_num_rows($this->gRes)) > $this->bLimit) {
        $this->nbrTuples = $t;
        ?>
        <script>
          $("#textAllTups").html("<?php echo $this->nbrTuples ?>");
          $("#limitButton").css("display", "inline");
          function getAllTups() {
            actionClick("_sys_limit");
          }
        </script>
        <?php
      }
    } else if (jmysql_num_rows($this->gRes))
      jmysql_data_seek($this->gRes, 0);

    //print rows
    $this->numLine = 0;
    while ($row = jmysql_fetch_assoc($this->gRes)) {
      if ($this->bLimit && $this->numLine == $this->bLimit && !$this->suspendLimit)
        break;
      $this->printLine($row);
    }

    $this->terminate();
    echo "</table>\n";
    echo "</form>\n";
    if ($this->bExport) {
      fclose($this->exportFile);
      file_put_contents($this->exportFileName, utf8_decode(file_get_contents($this->exportFileName)));
    }
    if ($this->exportButtonEnd && $this->bExportAvailable) {
      include_once "tools.php";
      $this->exportExcel();
      //createAction(-1, "Imprimer", "generatePDF()",120);
    }
  }

}
?>