<?php

abstract class Engine
{

  public function initialize()
  {
    
  }

  public function getEntry()
  {
    return false;
  }

  abstract public function getValue($key);

//-----------------------------------------------------------------------------------
  public function templateBegin()
  {
    
  }

//-----------------------------------------------------------------------------------
  public function templateEnd()
  {
    
  }

//-----------------------------------------------------------------------------------
  public function terminate()
  {
    
  }

//-----------------------------------------------------------------------------------
  public function fwrite(&$handle, $s)
  {
    if (empty($s))
      return;
    //echo "out=$s".nl;
    if (!$handle)
      echo $s;
    else
      fwrite($handle, $s);
  }

//-----------------------------------------------------------------------------------
  public function run()
  {
    $this->date = time();
    $this->initialize();
    $this->bInit = true;
    $this->nLevel = -1;
    while ($this->getEntry()) {
      if ($this->hTemplate)
        fclose($this->hTemplate);
      $this->hFiles[$this->nFile] = fopen($this->template, "r");
      $this->hTemplate = &$this->hFiles[$this->nFile++];
      if (!$this->hTemplate) {
        stop(__FILE__, __LINE__, "Erreur ouverture " . $this->template . " : $php_errormsg");
      }
      $this->templateBegin();
      //Search for $
      while (true) {
        while (!feof($this->hTemplate)) {
          $this->bInside = false;
          $this->ftell = ftell($this->hTemplate);
          $line = fgets($this->hTemplate);
          //echo "in=$line".nl;
          $this->extendLine($line);
        }
        if (--$this->nFile == 0)
          break;
        fclose($this->hTemplate);
        //pop file
        $this->hTemplate = &$this->hFiles[$this->nFile];
      }
      $this->templateEnd();
      $this->bInit = false;
      if (!$this->bContOutFile && $this->hOutput) {
        fclose($this->hOutput);
        $this->hOutput = null;
      }
      $this->bContOutFile = false;
    }
    $this->terminate();
  }

//-----------------------------------------------------------------------------------
  public function getLine($line)
  {
    ob_start();
    $this->extendLine($line);
    $l = ob_get_contents();
    ob_end_clean();
    return $l;
  }

//-----------------------------------------------------------------------------------
  public function extendLine($line)
  {
//    dump($line, "before");
    $sep = "§";
    if ($this->bAnsi)
      $line = str_replace(utf8_decode('§'), $sep, $line);
    $line = str_replace("&sect;", $sep, $line);
    $tok = "https://eur01.safelinks.protection.outlook.com/?url=";
    if (($pos = strpos($line, $tok)) !== false) {
      $end = strpos($line, '"', $pos);
      if ($end) {
        $href = substr($line, $pos + strlen($tok), $end);
        $amp = strpos($href, "&amp;data=");
        if ($amp)
          $href = substr($href, 0, $amp);
        $line = substr($line, 0, $pos) . urldecode($href) . substr($line, $end);
      }
    }
    while (($this->varpos = strpos($line, $sep)) !== false) {
      if ($this->bWrite)
        $this->fwrite($this->hOutput, substr($line, 0, $this->varpos));
      $varend = strpos($line, $sep, $this->varpos + 2);
      if ($varend === false || $varend - $this->varpos > 50) {
        if ($this->bWrite)
          $this->fwrite($this->hOutput, '§');
        $line = substr($line, $this->varpos + 2);
        continue;
      }
      $var = substr($line, $this->varpos + 2, $varend - $this->varpos - 2);
      $line = substr($line, $varend + 2);
      if ($this->bWrite) {
        if (($varContent = $this->globalVar($var)) === null && ($varContent = $this->getValue($var)) === null) {
          $this->fwrite($this->hOutput, "§${var}§");
          continue;
        } else if ($varContent == "@@@")
          return;
        $this->bInside = true;
        $this->extendLine($varContent);
      } else
        $this->globalVar($var);
    }
    if ($this->bWrite)
      $this->fwrite($this->hOutput, $line);
  }

//-----------------------------------------------------------------------------------
  public function bTest($key)
  {
    return $key == "Init" ? $this->bInit : false;
  }

//-----------------------------------------------------------------------------------
  public function getTest(&$t)
  {
    if ($not = $t[0] == '!')
      $t = substr($t, 1);
    return ($not && !$this->bTest($t) || !$not && $this->bTest($t));
  }

//-----------------------------------------------------------------------------------
  public function globalVar($var)
  {
    global $yearSeason;
    switch (strtok($var, ":")) {
      case "Test":
        $t = strtok(":");
        if ($bNot = $t[0] == '!')
          $t = substr($t, 1);
        $this->pushBWrite($this->bWrite && ($bNot && !$this->getTest($t) || !$bNot && $this->getTest($t)));
        return "";

      case "Tend":
        $this->popBWrite();
        return "";

      case "Else":
        $this->revertBWrite();
        return "";

      case "While":
        $t = strtok(":");
        if ($this->bWrite && $this->getTest($t))
          $this->newTest();
        else
          $this->pushBWrite(false);
        return "";

      case "Wend":
        if ($this->bWrite) {
          fseek($this->hTemplate, $this->pos[$this->nLevel]);
          $this->popBWrite();
          return "@@@";
        }
        $this->popBWrite();
        return "";

      case "Loop":
        if ($this->bWrite)
          $this->newTest();
        return "";

      case "Lend":
        $t = strtok(":");
        if ($this->bWrite && $this->getTest($t)) {
          fseek($this->hTemplate, $this->pos[$this->nLevel]);
          $this->popBWrite();
          return "@@@";
        }
        $this->popBWrite();
        return "";

      case "Format":
        $t = strtok(";");
        $t1 = strtok(":");
        return sprintf($t, $this->getValue($t1));

      case "Cont":
        $this->bContOutFile = true;
        return "";

      case "Open":
        if ($this->bContOutFile && $this->hOutput)
          return "";

        $t = strtok(":");
        if (!$this->bContOutFile && $this->hOutput)
          fclose($hOutput);
        if ($t[0] == '@') {
          $t = substr($t, 1);
          $t = $this->getValue($t);
        }
        if (!($this->hOutput = fopen($t, "wt"))) {
          alert("Erreur ouvrant fichier de sortie : " . $t);
          exit();
        }
        return "";

      case "File":
        $h = fopen($t = strtok(";"), "r");
        if (!$h) {
          alert("Erreur ouvrant fichier template : $t");
          exit();
        }
        $this->hFiles[$this->nFile++] = $this->hTemplate;
        $this->hTemplate = $h;
        return "";

      case "Date":
        $dt = new DateTime(strtok(";"));
        $this->date = mktime(12, 0, 0, $dt->format("n"), $dt->format("j"), $dt->format("Y"));
        return "";

      case "d":
        $dt = new DateTime('@' . $this->date);
        return $dt->format('j');

      case "dd":
        $dt = new DateTime('@' . $this->date);
        return $dt->format('d');

      case "m":
        $dt = new DateTime('@' . $this->date);
        return $dt->format('n');

      case "mm":
        $dt = new DateTime('@' . $this->date);
        global $months;
        return $months[$dt->format('n')];

      case "y":
        $dt = new DateTime('@' . $this->date);
        return $dt->format('y');


      case "yy":
        $dt = new DateTime('@' . $this->date);
        return $dt->format('Y');

      case "+":
        $this->date += strtok(";") * 24 * 3600;
        return "";

      case "-":
        $this->date -= strtok(";") * 24 * 3600;
        return "";

      case "Year":
        return $yearSeason;

      case "Year+1":
        return $yearSeason + 1;

      default:
        return null;
    }
  }

//-----------------------------------------------------------------------------------
  public function newTest()
  {
    if ($this->bInside) {
      echo "!! A loop can't be inserted as a result of a variable !!" . NL;
      return;
    }
    $this->bWrite = $this->aWrite[++$this->nLevel] = true;
    $this->pos[$this->nLevel] = $this->ftell + $this->varpos;
    //echo "new:bWrite=".$this->bWrite.", nLevel=".$this->nLevel.nl; 
  }

//-----------------------------------------------------------------------------------
  public function pushBWrite($val)
  {
    $this->bWrite = $this->aWrite[++$this->nLevel] = $val;
    //echo "push:bWrite=".$this->bWrite.", nLevel=".$this->nLevel.nl; 
  }

//-----------------------------------------------------------------------------------
  public function revertBWrite()
  {
    if ($this->nLevel < 0 || $this->nLevel > 0 && !$this->aWrite[$this->nLevel - 1])
      return;
    $this->bWrite = $this->aWrite[$this->nLevel] = !$this->aWrite[$this->nLevel];
    //echo "push:bWrite=".$this->bWrite.", nLevel=".$this->nLevel.nl; 
  }

//-----------------------------------------------------------------------------------
  public function popBWrite()
  {
    if (--$this->nLevel == -1)
      $this->bWrite = true;
    else
      $this->bWrite = $this->aWrite[$this->nLevel];
    //echo "pop:bWrite=".$this->bWrite.", nLevel=".$this->nLevel.nl; 
  }

//-----------------------------------------------------------------------------------
  public $template;
  public $hTemplate;
  public $hOutput;
  private $bWrite = true;

}

?>