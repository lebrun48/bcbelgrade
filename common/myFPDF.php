<?php

include BASE_PATH . "/tools/fpdf/fpdf.php";
define('FPDF_FONTPATH', BASE_PATH . '/tools/fpdf/font');

//------------------------------------------------------------------------------------------------------------------------
class MyFPDF extends FPDF
{

  //------------------------------------------------------------------------------------------------------------------------
  function __construct()
  {
    // Initialisation
    $this->B = 0;
    $this->I = 0;
    $this->U = 0;
    $this->HREF = '';
    parent::__construct();
  }

  //------------------------------------------------------------------------------------------------------------------------
  function CellHTML($w, $h = 0, $txt = '', $border = 0, $ln = 0, $align = '', $fill = false, $link = '')
  {
    $x = $this->GetX();
    $y = $this->GetY();
    $this->SetY($y + ($h - $this->FontSize) / 2);
    $this->WriteHTML($txt);
    $this->SetXY($x, $y);
    $this->Cell($w, $h, '', $border, $ln);
  }

  //------------------------------------------------------------------------------------------------------------------------
  function WriteHTML($html)
  {
    // Parseur HTML
    //dump($html);
    $html = str_replace("\n", ' ', $html);
    $a = preg_split('/<(.*)>/U', $html, -1, PREG_SPLIT_DELIM_CAPTURE);
    foreach ($a as $i => $e) {
      if ($i % 2 == 0) {
        // Texte
        if ($this->HREF)
          $this->PutLink($this->HREF, $e);
        else
          $this->Write($this->FontSize + 1, $e);
      } else {
        // Balise
        if ($e[0] == '/')
          $this->CloseTag(strtoupper(substr($e, 1)));
        else {
          // Extraction des attributs
          $a2 = explode(' ', $e);
          $tag = strtoupper(array_shift($a2));
          $attr = array();
          foreach ($a2 as $v) {
            if (preg_match('/([^=]*)=["\']?([^"\']*)/', $v, $a3))
              $attr[strtoupper($a3[1])] = $a3[2];
          }
          $this->OpenTag($tag, $attr);
        }
      }
    }
  }

  //------------------------------------------------------------------------------------------------------------------------
  function OpenTag($tag, $attr)
  {
    // Balise ouvrante
    switch ($tag) {
      case 'B': case 'I': case 'U':
        $this->SetStyle($tag, true);
        break;
      case 'A':
        $this->HREF = $attr['HREF'];
        break;
      case 'BR':
        $this->Ln($this->FontSize + 1);
        break;
      case 'P':
        $this->Ln($this->FontSize + 1);
        $this->cell(10);
        break;
      case 'SMALL': case "SUP":
        $this->SetFontSize($this->FontSizePt - 2);
        break;
      case 'H1':case 'H2':case 'H3':case 'H4':
        $this->Ln($this->FontSize + 1);
        $this->SetFontSize($this->FontSizePt + 6 - $tag[1]);
        $this->SetStyle('B', true);
        break;
      case "OL":
        $this->enum[] = 1;
        $this->SetLeftMargin($this->lMargin + 10);
        break;
      case "LI":
        $i = sizeof($this->enum) - 1;
        $this->WriteHTML("<b>" . ($this->enum[$i] ++) . ". </b>");
        $this->SetLeftMargin($this->lMargin + 5);
        break;
    }
  }

  //------------------------------------------------------------------------------------------------------------------------
  function CloseTag($tag)
  {
    // Balise fermante
    switch ($tag) {
      case 'B': case 'I': case 'U':
        $this->SetStyle($tag, false);
        break;
      case 'A':
        $this->HREF = '';
        break;
      case 'P':
        $this->Ln($this->FontSize + 3);
        break;
      case 'SMALL': case "SUP":
        $this->SetFontSize($this->FontSizePt + 2);
        break;
      case 'H1':case 'H2':case 'H3':case 'H4':
        $this->SetFontSize($this->FontSizePt - 6 + $tag[1]);
        $this->Ln(7 + $tag[1]);
        $this->SetStyle('B', false);
        break;
      case "OL":
        array_pop($this->enum);
        $this->SetLeftMargin($this->lMargin - 10);
        break;
      case "LI":
        $i = sizeof($this->enum);
        $this->SetLeftMargin($this->lMargin - 5);
        $this->Ln($this->FontSize + 3);
    }
  }

  //------------------------------------------------------------------------------------------------------------------------
  function SetStyle($tag, $enable)
  {
    // Modifie le style et sélectionne la police correspondante
    $this->$tag += ($enable ? 1 : -1);
    $style = '';
    foreach (array('B', 'I', 'U') as $s) {
      if ($this->$s > 0)
        $style .= $s;
    }
    $this->SetFont('', $style);
  }

  //------------------------------------------------------------------------------------------------------------------------
  function PutLink($URL, $txt)
  {
    // Place un hyperlien
    $this->SetTextColor(0, 0, 255);
    $this->SetStyle('U', true);
    $this->Write($this->FontSize + 1, $txt, $URL);
    $this->SetStyle('U', false);
    $this->SetTextColor(0);
  }

}
