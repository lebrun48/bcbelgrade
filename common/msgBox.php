<?php
/* * *******************************************************************************************************************************************
 *   Open a popup with title, content, buttons
 *   msgBox(content <string: html content>, 
 *          { title:<title (optional)>,
 *            width:<width px or % (optional)>,
 *            bModal: <if true modal popup (optional)>,
 *            button: [<button 0 label>, ...],  (optional)
 *            buttonWidth: (optional)
 *            setFocus: (optional true|false default:true),
 *            click: function(id,buttonIdx) | [function(id,buttonIdx), ...],  (optional)
 *            dragDrop: (optional true|false default:true)
 *          }, (optional)
 *          id <string: unique id for this popup> (optional)
 *         ); 
 *            
 *   return msgBox object
 *   msgBox.close() : close popup
 *   
 *   global functions:
 *   function msgBoxClick(id <string: unique popup id>, 
 *                        label <string: label button to activate click>) 
 *   
 *   function msgBoxClose(id <string: unique popup id>)
 *   
 * ******************************************************************************************************************************************** */
?>
<style>
  div.msgbox {box-shadow:10px 10px 5px #888888;border-radius:5px;background:#ffffff;color:black;border:1px solid black;position:fixed;z-index:1001}/*top:50%;left:50%; */
  .msgBoxTitle_cl {width:100%;text-align:center;background:lightgray}
  .background{display:none;background:black;opacity:0.2;position:absolute;left:0px;top:0px;filter : alpha(opacity=10);z-index:1000}
</style>

<script>
  function activateDrag(id) {
    $("#msgBox" + id).draggable();
  }

  msgBoxConfig = {};
  function msgBox(content, c, id)
  {
    function getVal(s) {
      if (typeof s == 'string') {
        v = s.match(/([\d]*) ?(.*)?/);
        return v;
      }
      var v = [];
      v[1] = s;
      return v;
    }

    if (!id)
      id = "Default";
    var config = {title: 'Message', width: 400, bModal: true, dragDrop: true, button: ['Ok'], click1: [msgBoxClose], buttonWidth: 60, setFocus: true};
    if (c !== undefined)
      for (var a in c) {
        config[a] = c[a];
      }

    for (i in config.button) {
      if (typeof config.click == "function")
        config.click1[i] = config.click;
      else if (!config.click || !config.click[i])
        config.click1[i] = msgBoxClose;
      else
        config.click1[i] = config.click[i];
      if (i == 0 && !config.button[i])
        config.button[i] = "Ok";
    }
    msgBoxConfig[id] = {button: config.button, click: config.click1, close: function () {
        msgBoxClose(id);
      }};

    if (!(bPres = $("#msgBox" + id).length))
      document.getElementById("msgBoxStore").innerHTML += "<div id=msgBox" + id + " class='msgbox ui-widget-content' style=display:none><h3 id=msgBoxTitle" + id + " class=msgBoxTitle_cl" + (config.dragDrop ? " style=cursor:move" : '') + "></h3><div id=msgBoxContent" + id + " style=margin:10px></div><div id=msgBoxButton" + id + " style=text-align:right;width:100%;margin-right:10px;margin:10px;margin-left:0px></div></div>";

    $('#msgBoxTitle' + id).html(config.title);
    b = $("#msgBox" + id);
    b.width(config.width);
    $("#msgBoxContent" + id).css("overflow-y", '').height('');
    b.height('');
    if (config.height !== undefined)
      b.height(config.height);
    $('#msgBoxContent' + id).html(content);
    var t = '';
    for (i in config.button)
      t += "<input style=margin-right:20px;width:" + config.buttonWidth + "px type=button value='" + config.button[i] + "' onclick=msgBoxConfig." + id + ".click[" + i + "]('" + id + "'," + i + ")>";
    $('#msgBoxButton' + id).html(t);
    if (!bPres) {
      v = getVal(config.width);
      b.css("left", window.pageXOffset / 1 + (window.innerWidth - (v[2] == '%' ? v[1] * window.innerWidth / 100 : v[1])) / 4);
      if ((t = (window.innerHeight - getVal(b.css("height"))[1]) / 3) < 0)
        t = 10;
      b.css("top", t);
    }
    if (b.height() > window.innerHeight - 300) {
      b.height(window.innerHeight - 300);
      $("#msgBoxContent" + id).css("overflow-y", "auto").height(b.height() - 100);
    }
    if (config.dragDrop)
      activateDrag(id);
    b.show();
    if (config.setFocus) {
      f = b.find("input[type=text],input[type=password],input[type=radio],input[type=checkbox],textarea,select").filter(":visible:first");
      if (f)
        f.focus();
    }

    if (config.bModal)
    {
    p = $("#prevent");
            p.css("width", screen.availWidth < (t = $("html").width()) ? t:screen.availWidth);
            p.css("height", screen.availHeight < (t = $("html").height()) ? t:screen.availHeight);
    p.show();
    }
    return msgBoxConfig[id];
  }

  function msgBoxClick(id, label) {
    for (i in msgBoxConfig[id].button)
      if (msgBoxConfig[id].button[i] == label)
        msgBoxConfig[id].click[i](i, id);
  }

  function msgBoxClose(id) {
    if (!id)
      id = "Default";
    $("#msgBox" + id).remove();
    $("#prevent").hide();
  }
</script>
<?php
echo "<div id=prevent class=background></div>";
echo "<span id=msgBoxStore></span>";

function msgBox($cont, $config = null, $id = null)
{
  ?>
  <script>
    msgBox('<?php echo addslashes($cont) . "'" . ($config ? ",$config" : '') ?>);
  </script>
  <?php
}
