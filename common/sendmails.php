<?php

include_once "common/common.php";
checkRights();

if (!($fh = fopen($genMail, 'r'))) {
  mail("jacques.meirlaen@skynet.be", "sendmails.php", "Error openning file '$genMail'\n");
  echo "<h2>Erreur ouverture fichier '$genMail'</h2>";
  exit();
}
$bHd = true;
unset($res);

while (!feof($fh)) {
  $l = fgets($fh);
  if (substr($l, 0, 7) == "From - ") {
    if ($hd) {
      if (!$bTest && strpos($to, "!!") === false)
        $res = mail($to, mb_encode_mimeheader($subj), $mail, $hd);
      $sumTo .= ($res ? " OK" : " NOK") . nl;
      if ($bDumpMail) {
        dump($subj, "subject");
        dump($to, "to");
        dump($mail, "mail");
        dump($hd, "hd");
      }
      //mail("newbc.belgrade@skynet.be",$subj,$mail,$hd);
    }
    unset($hd);
    unset($mail);
    unset($subj);
    unset($to);
    $bHd = true;
    continue;
  }
  if ($bHd) {
    if ($l == "\x0A")
      $bHd = false;
    else if (substr($l, 0, 9) == "Subject: ")
      $subj = utf8_decode(substr($l, 9, -1));
    else if (substr($l, 0, 4) == "To: ") {
      $to = substr($l, 4, -1);
      $sumTo .= $to;
      $n++;
    } else if (substr($l, 0, 2) != "X-" && substr($l, 0, 6) != "Date: " && substr($l, 0, 5) != "FCC: " && substr($l, 0, 12) != "Message-ID: ")
    //else if (substr($l,0,2)!="X-" && substr($l,0,6)!="Date: " && substr($l,0,5)!="FCC: " && substr($l,0,12)!="Message-ID: " && substr($l,0,4)!="To: ")
      $hd .= $l;
    continue;
  }
  $l1 = '';
  while (strlen($l) > 75) {
    if (!($i = strpos($l, ' ', 74)))
      $i = strlen($l);
    $l1 .= substr($l, 0, $i) . "\r\n";
    $l = substr($l, $i + 1);
  }
  $l1 .= $l;
  $mail .= $l1;
}
if ($hd) {
  if (!$bTest && strpos($to, "!!") === false)
    $res = mail($to, mb_encode_mimeheader($subj), $mail, $hd);
  $sumTo .= ($res ? " OK" : " NOK") . nl;
  if ($bDumpMail) {
    dump($subj, "subject");
    dump($to, "to");
    dump($mail, "mail");
    dump($hd, "hd");
  }
  //mail("newbc.belgrade@skynet.be",$subj,$mail,$hd);
}

if ($bSendResult && !$bTest)
  mail("jacques.meirlaen@skynet.be", "sendmails.php", "$n mails envoyés: \n$sumTo");
if ($bTest || $bResult) {
  ?>
  <style>
    .box{
      background: #e7f0f5;
      -moz-border-radius: 10px;
      -webkitborder-radius: 10px;
      border-radius: 10px;
      border: 3px double #000000;
      padding:10px;
      box-shadow: 1px 1px 12px #000000;
      width:40em;
      margin:auto;
    }
    .ok {color:green;font-weight:bold}
    .nok {color:red;font-weight:bold}
  </style>
  <?php

  echo "<div class=box><h2 style=width:100%;text-align:center>Résultat envoi des mails</h2>";
  echo str_replace("\n", "<br>", str_replace(" OK", "<span class=ok>: OK</span>", str_replace(" NOK", "<span class=nok>: NOK</span>", str_replace('<', "&lt;", str_replace('>', "&gt;", $sumTo)))));
  echo "</div>";
}
?>


