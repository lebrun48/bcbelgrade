<?php
header("Content-Type:text/html; charset=UTF-8", true);
$tab = $_GET["tab"];
include "${tab}Conf.php";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//FR">
<head>            
  <meta content="text/html; charset=UTF-8" http-equiv="content-type">
  <link rel="shortcut icon" href="<?php echo LOGO_LITTLE_CLUB ?>" type="image/x-icon" />
</head>
<body style="font-family:Comic Sans MS;font-size:x-large">
  <?php
  echo "<title>" . substr($_GET["tab"], 6) . " Bilan</title></head>" . nl;
  include "tools.php";
//$seeRequest=1;

  if (!($currentYear = $request["year"])) {
    echo "<h2>Selection de l'année</h2>";
    $e = jmysql_result(jmysql_query("select substr(min(date),1,4) from " . TABLE_NAME), 0);
    echo "<input type=checkbox" . (!isset($request["check"]) || $request["check"] == "true" ? " checked" : '') . " id=check> A vérifier. Année <select id=year onchange=location.assign('billerBilan.php$basePath1" . "tab=$tab&year='+this.options[this.selectedIndex].value+'&check='+document.getElementById('check').checked)><option></option>" . nl;
    for ($i = $now->format("Y"); $i >= $e; $i--)
      echo "<option value=" . ($i - 2000) . ">$i</option>";
    echo "</select>";
    exit();
  } else
    $currentYear += 2000;
  $dt = "from $tab where date like '$currentYear-%' and account<50";

//check validity
  if ($request["check"] == "true") {
    //$seeRequest=1;
    $base = "$tab.php$basePath&official=true&civil=true&_sel_date=$currentYear&year=all";
    foreach ($category as $k => $v)
      foreach ($v as $k1 => $v1) {
        //dump($v1);
        if (jmysql_result(jmysql_query("select count(*) $dt and category=" . ($k * 256 + $k1) . " and amount>0 and (status" . ($v1[1] == -1 ? " is not null)" : "<>$v1[1] or status is null)")), 0)) {
          echo "<a target=_blank href=" . htmlspecialchars("$base&_sel_category==" . ($k * 256 + $k1) . "&_sel_amount=>0&_sel_status=" . ($v1[1] == -1 ? "!$" : "!=$v1[1]%2B$")) . ">Montant&gt;0 ne correspond pas à la config</a>" . NL;
          break;
        }
        if (jmysql_result(jmysql_query("select count(*) $dt and category=" . ($k * 256 + $k1) . " and amount<0 and (status" . ($v1[2] == -1 ? " is not null)" : "<>$v1[2] or status is null)")), 0)) {
          echo "<a target=_blank href=" . htmlspecialchars("$base&_sel_category==" . ($k * 256 + $k1) . "&_sel_amount=<0&_sel_status=" . ($v1[2] == -1 ? "!$" : "!=$v1[2]%2B$")) . ">Montant&lt;0 ne correspond pas à la config</a>" . NL;
          break;
        }
      }
  }

//--------------------------------------------------------------------------------------------------------------
  function getAmount($n)
  {
    global $currentYear, $cat, $dt, $income, $outcome, $account, $sTVA;
    if ($n == 509) {
      $am = jmysql_result(jmysql_query("select sum(amount) $dt and account<19"), 0);
      echo number_format(abs($am), 0, ',', '.');
      $r = jmysql_query("select account,sum(amount) $dt and account<19 group by account");
      $refCaisse = -1;
      while ($tup = jmysql_fetch_row($r))
        if (substr($account[$tup[0]], 0, 6) == "Caisse") {
          if ($refCaisse == -1)
            $refCaisse = $tup[0];
          $caisse += $tup[1];
        } else
          $cat[$n][$tup[0]] = number_format($tup[1], 0, ',', '.');
      if ($refCaisse != -1)
        $cat[$n][$refCaisse] = number_format($caisse, 0, ',', '.');
      return;
    }

    $am = jmysql_result(jmysql_query("select sum(amount) $dt and status=$n"), 0);
    //echo "status=$n, am=$am".NL;
    $am -= jmysql_result(jmysql_query("select sum(TVA) $dt and status=$n and TVA is not null"), 0);
    $r = jmysql_query("select category,sum(amount) $dt and status=$n group by category");
    while ($tup = jmysql_fetch_row($r)) {
      $t = $tup[1];
      $t -= jmysql_result(jmysql_query("select sum(TVA) $dt and status=$n and category=$tup[0] and TVA is not null"), 0);
      $cat[$n][$tup[0]] = number_format(abs($t), 0, ',', '.');
    }
    if ($n == 507 && $sTVA > 0 || $n == 604 && $sTVA < 0) {
      $cat[$n][0x1300] = number_format(abs($sTVA), 0, ',', '.');
      $am += $sTVA;
    }
    echo number_format(abs($am), 0, ',', '.');
    if ($am < 0)
      $outcome += abs($am);
    else
      $income += abs($am);
  }

//--------------------------------------------------------------------------------------------------------------
  function head()
  {
    global $enterprise;
    ?>
    <table style=width:100% border=1px>
      <tr>
        <td style=height:.7cm>asbl: <b><?php echo $enterprise->name ?></b>
        </td>
        <td align=right>N° entreprise: <b><?php echo $enterprise->num ?></b>
        </td>
      </tr>
      <tr>
        <td colspan=2 style=padding-bottom:5px;height:.7cm>Siège social: <b><?php echo $enterprise->address ?></b>
        </td>
      </tr>
    </table>
    <?php
  }

//--------------------------------------------------------------------------------------------------------------
  function foot($bBreak = true)
  {
    global $enterprise, $now, $months;
    echo "<div style=width:100%;font-weight:normal;font-family:arial;margin-left:60%;margin-top:1cm>" . CITY_CLUB . ", le " . formatDate($now, "d M Y") . "</div>";
    echo "<div style=width:100%;font-weight:normal;font-family:arial;" . ($bBreak ? "page-break-after:always;" : '') . "padding-left:60%;padding-top:1.2cm>" . $enterprise->responsible . "</div>";
  }

//--------------------------------------------------------------------------------------------------------------
  ?>
  <style type="text/css">
    table {border-collapse:collapse;table-layout:fixed;width:100%}
    tr {border:0px}
    td,th {padding-left:0.2cm;padding-right:0.2cm;border:0px;;color:black}
  </style>

  <?php
  head();
  define('MAX_LINES', 57);
  ?>
  <h2 style='width:100%;text-align:center'>Comptes simplifiés année <?php echo $currentYear ?></h2>  
  <table border=2px>
    <tr>
      <td colspan=2 align=center style='padding-bottom:5px;font-weight:bold;border:2px solid black'>Dépenses
      </td>
      <td colspan=2 align=center style='padding-bottom:5px;font-weight:bold;border:2px solid black'>Recettes
      </td>
    </tr>
    <?php
    for ($i = 0; $i <= 3; $i++) {
      ?>
      <tr <?php echo $i == 3 ? "style='border-bottom:1px solid black'" : '' ?>>
        <td align=center style='width:40%;height:1cm;border-right:1px solid black'><?php echo $resultCategory[$i] ?>
        </td>
        <td align=right style='width:10%;font-weight:bold;border-right:2px solid black'><?php getAmount($i) ?>
        </td>
        <td align=center  style='width:40%;height:1cm;border-right:1px solid black'><?php echo $resultCategory[$i + 100] ?>
        </td>
        <td align=right style=width:10%;font-weight:bold><?php getAmount($i + 100) ?>
        </td>
      </tr>
    <?php }
    ?>
    <tr>
      <td align=center style='font-weight:bold;width:40%;height:1cm;border-right:1px solid black'>Total Dépenses
      </td>
      <td align=right style='width:10%;font-weight:bold;border-right:2px solid black'><?php echo number_format($outcome, 0, ',', '.') ?>
      </td>
      <td align=center style='font-weight:bold;width:40%;height:1cm;border-right:1px solid black'>Total Recettes
      </td>
      <td align=right style='width:10%;font-weight:bold;border-right:2px solid black'><?php echo number_format($income, 0, ',', '.') ?>
      </td>
    </tr>
  </table>
  <br><div style='font-size:xx-large'>Solde: <b><?php echo number_format($income - $outcome, 0, ',', '.') ?></div> 
  <?php foot() ?>


  <?php
  head();
  $dt = "from $tab where date<='$currentYear-12-31' and account<50";
  $sTVA = -jmysql_result(jmysql_query("select sum(TVA) $dt and TVA is not null and TVA_Status is null"), 0);
  ?>
  <h2 style='width:100%;text-align:center'>Etat du patrimoine année <?php echo $currentYear ?></h2>  
  <table border=2px>
    <tr>
      <td align=center style='padding-bottom:5px;font-weight:bold;width:50%;border:2px solid black'>Avoirs
      </td>
      <td align=center style='padding-bottom:5px;font-weight:bold;width:50%;border:2px solid black'>Dettes
      </td>
    </tr>
    <tr>
      <td style='vertical-align:top;padding:10px;margin:0px;border-right:2px solid black'>
        <?php
        foreach ($resultCategory as $i => $v) {
          if ($i < 500)
            continue;
          else if ($i >= 600)
            break;
          $bNoVal = $v[strlen($v) - 1] == ' ';
          ?>
          <div style=clear:both>
            <span style='float:left;white-space:nowrap;font-weight:bold;'><?php echo $v ?>
            </span>
            <span style=float:right;font-weight:bold;><?php if (!$bNoVal) getAmount($i) ?>
            </span>
          </div>
          <?php
          if ($cat[$i] && sizeof($cat[$i]) > 1)
            foreach ($cat[$i] as $k => $v) {
              if (abs($v) < 0.1)
                continue;
              echo "<div style=clear:both;margin-left:10%;width:50%><span style=float:left;font-family:arial;font-size:small>" . ($i == 509 ? $account[$k] : $category[$k / 256][0][0] . ($k % 256 ? $catSerial[$k] : '')) . "</span>";
              echo "<span style='float:right;font-family:arial;font-size:small;font-weight:bold;text-align:right;'>$v</span></div>";
            }
        }
        ?>
      </td> 
      <td style='vertical-align:top;padding:10px;margin:0px;border-right:2px solid black'>
        <?php
        for ($i = 600; isset($resultCategory[$i]); $i++) {
          $v = $resultCategory[$i];
          $bNoVal = $v[strlen($v) - 1] == ' ';
          ?>
          <div style=clear:both>
            <span style='float:left;white-space:nowrap;font-weight:bold;'><?php echo $v ?>
            </span>
            <span style=float:right;font-weight:bold;><?php if (!$bNoVal) getAmount($i) ?>
            </span>
          </div>
          <?php
          if ($cat[$i] && sizeof($cat[$i]) > 1)
            foreach ($cat[$i] as $k => $v) {
              echo "<div style=clear:both;margin-left:10%;width:50%><span style=float:left;font-family:arial;font-size:small>" . ($i == 509 ? $account[$k] : $category[$k / 256][0][0] . ($k % 256 ? $catSerial[$k] : '')) . "</span>";
              echo "<span style='float:right;font-family:arial;font-size:small;font-weight:bold;text-align:right;'>$v</span></div>";
            }
        }
        ?>
      </td> 
    </tr>
  </table>


  <?php
  $dt = "from $tab where date like '$currentYear-%' and account<50";
  echo NL . NL;
  ?>
  <h2 style='width:100%;text-align:center'>Droits et engagements importants année <?php echo $currentYear ?></h2>  
  <table border=2px>
    <tr>
      <td align=center style='padding-bottom:5px;font-weight:bold;width:50%;border:2px solid black'>Droits
      </td>
      <td align=center style='padding-bottom:5px;font-weight:bold;width:50%;border:2px solid black'>Engagements
      </td>
    </tr>
    <tr>
      <td style='vertical-align:text-top;padding:0px;margin:0px;border-right:2px solid black'>
        <table border=0px style=width:100%;margin:0px;padding:0px>
          <?php
          for ($i = 700; isset($resultCategory[$i]); $i++) {
            ?>
            <tr>
              <td colspan=2 style='white-space:nowrap;width:80%;font-weight:bold;height:.7cm;'><?php echo $resultCategory[$i] ?>
              </td>
              <td align=right style=vertical-align:bottom;font-weight:bold;><?php getAmount($i) ?>
              </td>
            </tr>
            <?php
            if ($cat[$i] && sizeof($cat[$i]) > 1)
              foreach ($cat[$i] as $k => $v) {
                echo "<tr><td style=font-family:arial;font-size:small>" . $category[$k / 256][0][0] . ($k % 256 ? $catSerial[$k] : '') . "</td>";
                echo "<td style='font-family:arial;font-size:small;font-weight:bold;text-align:right;'>$v</td>";
                echo "<td></td></tr>";
              }
            echo "<tr style='height:0px;border-bottom:1px solid black'><td colspan=3></td></tr>";
          }
          ?>
        </table>
      </td> 
      <td style='vertical-align:text-top;padding:0px;margin:0px;border-right:2px solid black'>
        <table border=0px style=margin:0px;padding:0px;width:100%>
          <?php
          for ($i = 800; isset($resultCategory[$i]); $i++) {
            ?>
            <tr>
              <td colspan=2 style='white-space:nowrap;width:80%;font-weight:bold;height:.7cm;'><?php echo $resultCategory[$i] ?>
              </td>
              <td align=right style=vertical-align:bottom;font-weight:bold;><?php getAmount($i) ?>
              </td>
            </tr>
            <?php
            if ($cat[$i] && sizeof($cat[$i]) > 1)
              foreach ($cat[$i] as $k => $v) {
                echo "<tr><td style=font-family:arial;font-size:small>" . $category[$k / 256][0][0] . ($k % 256 ? $catSerial[$k] : '') . "</td>";
                echo "<td style='font-family:arial;font-size:small;font-weight:bold;text-align:right;'>$v</td>";
                echo "<td></td>";
              }
            echo "<tr style='height:0px;border-bottom:1px solid black'><td colspan=3></td></tr>";
          }
          ?>
        </table>
      </td> 
    </tr>
  </table>
  <?php foot() ?>




  <?php head() ?>
  <h2 style='width:100%;text-align:center'>Comptes détaillés année <?php echo $currentYear ?></h2>  
  <table border=2px>
    <tr>
      <td align=center style='padding-bottom:5px;font-weight:bold;width:50%;border:2px solid black'>Dépenses
      </td>
      <td align=center style='padding-bottom:5px;font-weight:bold;width:50%;border:2px solid black'>Recettes
      </td>
    </tr>
    <tr>
      <td style='vertical-align:top;margin:15px;border-right:2px solid black'>
        <?php
        for ($i = 0; $i <= 3; $i++) {
          ?>
          <div style=clear:both>
            <span style='margin-top:.7cm;float:left;font-weight:bold;font-size:large'><?php echo $resultCategory[$i] ?>
            </span>
            <span style=margin-top:.7cm;float:right;font-size:large;font-weight:bold;><?php getAmount($i) ?>
            </span>
          </div>
          <?php
          if ($cat[$i] && sizeof($cat[$i]) > 1)
            foreach ($cat[$i] as $k => $v) {
              echo "<div style=clear:both;width:70%;margin-left:10%><span style=white-space:nowrap;float:left;font-size:small>" . $category[$k / 256][0][0] . ($k % 256 ? $catSerial[$k] : '') . "</span>";
              echo "<span style='font-size:small;font-weight:bold;float:right;'>$v</span></div>";
              if (++$numLine == MAX_LINES) {
                $lastIn = $k;
                $lastIIn = $i;
                break;
              }
            }
          if ($lastIn)
            break;
        }
        ?>
      </td> 
      <td style='vertical-align:top;margin:15px;border-right:2px solid black'>
        <?php
        $numLine = 0;
        for ($i = 100; $i <= 103; $i++) {
          ?>
          <div style=clear:both>
            <span style='margin-top:.7cm;float:left;font-weight:bold;font-size:large'><?php echo $resultCategory[$i] ?>
            </span>
            <span style=margin-top:.7cm;float:right;font-size:large;font-weight:bold;><?php getAmount($i) ?>
            </span>
          </div>
          <?php
          if ($cat[$i] && sizeof($cat[$i]) > 1)
            foreach ($cat[$i] as $k => $v) {
              echo "<div style=clear:both;width:70%;margin-left:10%><span style=white-space:nowrap;float:left;font-size:small>" . $category[$k / 256][0][0] . ($k % 256 ? $catSerial[$k] : '') . "</span>";
              echo "<span style='font-size:small;font-weight:bold;float:right;'>$v</span></div>";
              if (++$numLine == MAX_LINES) {
                $lastOut = $k;
                $lastIOut = $i;
                break;
              }
            }
          if ($lastOut)
            break;
        }
        ?>
      </td>
    </tr> 
  </table>
  <?php
//echo "numLine=$numLine, lastIn=$lastIn, lastOut=$lastOut<br>";
  if ($lastIn || $lastOut) {
    echo "<div style=page-break-before:always></div>";
    head()
    ?>
    <h2 style='width:100%;text-align:center'>Comptes détaillés année <?php echo $currentYear ?> (suite)</h2>  
    <table border=2px>
      <tr>
        <td align=center style='padding-bottom:5px;font-weight:bold;width:50%;border:2px solid black'>Dépenses (suite)
        </td>
        <td align=center style='padding-bottom:5px;font-weight:bold;width:50%;border:2px solid black'>Recettes (suite)
        </td>
      </tr>
      <tr>
        <td style='vertical-align:top;margin:15px;border-right:2px solid black'>
          <?php
          if ($lastIIn)
            for ($i = $lastIIn; $i <= 3; $i++) {
              if ($i != $lastIIn) {
                ?>
                <div style=clear:both>
                  <span style='margin-top:.7cm;float:left;font-weight:bold;font-size:large'><?php echo $resultCategory[$i] ?>
                  </span>
                  <span style=margin-top:.7cm;float:right;font-size:large;font-weight:bold;><?php getAmount($i) ?>
                  </span>
                </div>
                <?php
                $bCont = false;
                $lastIn = -1;
              } else
                $bCont = true;
              if ($cat[$i] && sizeof($cat[$i]) > 1)
                foreach ($cat[$i] as $k => $v) {
                  if ($k == $lastIn) {
                    $bCont = false;
                    continue;
                  } else if ($bCont)
                    continue;
                  echo "<div style=clear:both;width:70%;margin-left:10%><span style=white-space:nowrap;float:left;font-size:small>" . $category[$k / 256][0][0] . ($k % 256 ? $catSerial[$k] : '') . "</span>";
                  echo "<span style='font-size:small;font-weight:bold;float:right;'>$v</span></div>";
                }
            }
          ?>
        </td> 
        <td style='vertical-align:text-top;padding-left:0px;padding-right:0px;border-right:2px solid black'>
          <?php
          $numLine = 0;
          if ($lastIOut)
            for ($i = $lastIOut; $i <= 103; $i++) {
              if ($i != $lastIOut) {
                ?>
                <div style=clear:both>
                  <span style='margin-top:.7cm;float:left;font-weight:bold;font-size:large'><?php echo $resultCategory[$i] ?>
                  </span>
                  <span style=margin-top:.7cm;float:right;font-size:large;font-weight:bold;><?php getAmount($i) ?>
                  </span>
                </div>
                <?php
                $bCont = false;
                $lastOut = -1;
              } else
                $bCont = true;
              if ($cat[$i] && sizeof($cat[$i]) > 1)
                foreach ($cat[$i] as $k => $v) {
                  if ($k == $lastOut) {
                    $bCont = false;
                    continue;
                  } else if ($bCont)
                    continue;
                  echo "<div style=clear:both;width:70%;margin-left:10%><span style=white-space:nowrap;float:left;font-size:small>" . $category[$k / 256][0][0] . ($k % 256 ? $catSerial[$k] : '') . "</span>";
                  echo "<span style='font-size:small;font-weight:bold;float:right;'>$v</span></div>";
                }
            }
          ?>
        </td>
      </tr> 
    </table>
    <?php
  }
  foot(false)
  ?>
</body>