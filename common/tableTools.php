<?php

$color = array("gray", "black", "gray", "blue", "red", "green", "orange", "magenta");
$action = $request["action"];

//dump($request, "request");
//dump($_GET, "GET");
//dump($_POST, "POST");
//--------------------------------------------------------------------------------------------
function getRequest(&$obj)
{
  $obj->bVisible = false;
  global $request;
  foreach ($request as $k => $v) {
    if (($t = substr($k, 0, 5)) == "_sel_") {
      if (strlen($v))
        $obj->filter[substr($k, 5)] = $v;
    } else if ($t != "_val_" && substr($k, 0, 9) != "_replace_")
      switch ($k) {
        case "visible":
          $tok = strtok($v, ",");
          while ($tok) {
            $obj->curVisible[] = $tok;
            $tok = strtok(",");
          }
          $obj->bVisible = true;
          break;

        case "order":
          $obj->order = $v;
          break;

        case "dir":
          $obj->dir = $v;
          break;

        case "scr":
          $obj->scroll = $v;
          break;

        case "expTabNbr":
          $obj->exportTab = $v;
          break;

        case "viewAll":
          $obj->bViewAll = true;
          break;

        case "action":
        case "_select_":
        case "key":
        case "list":
        case "tableName":
        case "x":
        case "y":
        case "_sys_limit":
        case "_checkRefresh_":
          break;

        default:
          $obj->paramArray[$k] = $v;
      }
  }
  //echo "param=".$obj->param.NL;
  $obj->editURL = $obj->actionURL = $_SERVER["SCRIPT_NAME"];
}

// build All Param--------------------------------------------------------------------------------------------
function buildAllParam(&$obj, $p = null, $val = null)
{
  $t = buildGetParam($obj, $p, $val);
  if ($obj->bHideCol && $obj->curVisible) {
    foreach ($obj->curVisible as $k => $v) {
      $tt .= $s . $v;
      $s = ",";
    }
    $t .= "&visible=" . urlencode($tt);
  }
  if ($obj->order)
    $t .= "&order=" . urlencode($obj->order) . "&dir=$obj->dir";
  if ($obj->filter)
    foreach ($obj->filter as $k => $v)
      $t .= "&_sel_$k=" . urlencode($v);
  if ($obj->scroll)
    $t .= "&scr=" . $obj->scroll;
  if ($t[0] == '&')
    $t[0] = '?';
  return $t;
}

// build Get Param--------------------------------------------------------------------------------------------
function buildGetParam(&$obj, $p = null, $val = null, $bFilter = true)
{
  if ($obj->paramArray)
    foreach ($obj->paramArray as $k => $v)
      $t .= "&$k=" . urlencode($v);
  if ($p)
    $t .= "&$p=" . urlencode($val);
  if ($bFilter && $obj->filter)
    foreach ($obj->filter as $k => $v)
      if (!($a = $obj->colDef[$k]) || !$a[0])
        $t .= "&_sel_$k=" . urlencode($v);

  if ($obj->bViewAll)
    $t .= "&viewAll";

  if ($t[0] == '&')
    $t[0] = '?';
  return $t;
}

//--------------------------------------------------------------------------------------------
function buildDefaultInputs(&$obj)
{
  global $request;
  echo "<input type=hidden name=" . ($request["key"] ? "key  value=\"" . $request["key"] : "list  value=\"" . $request["list"]) . "\"><input type=hidden name=scr value='" . $request["scr"] . "'><input type=hidden name=order value=" . $request["order"] . "><input type=hidden name=dir value=" . $request["dir"] . ">";
  if ($obj->filter)
    foreach ($obj->filter as $k => $v)
      echo '<input type="hidden" name="_sel_' . $k . '" value="' . htmlspecialchars($v) . '">';
}

//--------------------------------------------------------------------------------------------
function buildSql(&$sql, $bList = false, $attr = null)
{
  global $request;
  if (!$attr)
    $attr = &$request;
  foreach ($attr as $k => $v) {
    if (substr($k, 0, 5) != "_val_")
      continue;
    $k = substr($k, 5);
    if (is_array($v)) {
      $setAttr = ",$k='" . addslashes(serialize($v)) . "'";
      continue;
    }
    if (strlen($v) == 0) {
      if (!$bList || isset($request["_replace_$k"]))
        $setAttr .= ",$k=null";
    } else if ($v[0] == '=') {
      $v = str_replace("\\'", "'", $v);
      $setAttr .= ",$k$v";
    } else {
      $quote = is_numeric($v) ? '' : "'";
      if ($bList && $quote && !isset($request["_replace_$k"]))
        $setAttr .= ",$k=concat('" . addslashes($v) . " ',ifnull($k,''))";
      else
        $setAttr .= ",$k=$quote" . addslashes($v) . $quote;
    }
  }
  if ($setAttr)
    $sql .= substr($setAttr, 1);
  else
    $sql = '';
}

//--------------------------------------------------------------------------------------------
function deleteAction($bSilence = false)
{
  global $request;
  $n = str_replace('$', "'", $request["key"]);

  $sql = "delete from " . strtok(substr($n, 0, strpos($n, "where")), ",") . strstr($n, " where");
  //echo "sqlDelete=$sql".NL;return; 
  if (!jmysql_query($sql)) {
    if (!$bSilence)
      stop(__FILE__, __LINE__, "(" . jmysql_errno() . ") " . jmysql_error() . ": $sql");
    return false;
  }
  return true;
}

//--------------------------------------------------------------------------------------------
function saveAction($bSilence = false)
{
  global $request;
  $n = $request["list"];
  if (!$n)
    $n = str_replace('$', "'", $request["key"]);
  else
    $bList = true;
  $tab = strtok(substr($n, 0, strpos($n, "where")), ",");
  $where = strstr($n, "where");
  $sql = "update $tab set ";
  buildSql($sql, $bList);
  if (!$sql)
    return true;
  $sql .= " $where " . ($bList ? '' : "limit 1");
  //echo "sqlSave=$sql".NL;return;
  if (!jmysql_query($sql, 4)) {
    if (!$bSilence)
      stop(__FILE__, __LINE__, "(" . jmysql_errno() . ") " . jmysql_error() . ": $sql");
    return false;
  }
  return true;
}

//--------------------------------------------------------------------------------------------
function insertAction($bSilence = false)
{
  global $request, $seeRequest;
  $n = $request["key"];
  $tab = substr($n, 0, strpos($n, "where"));
  $sql = "insert into $tab set ";
  buildSql($sql);
  //echo "sqlInsert=$sql".NL;return;
  if (!($seeRequest ? jmysql_query($sql) : @jmysql_query($sql))) {
    if (!$bSilence)
      stop(__FILE__, __LINE__, "(" . jmysql_errno() . ") " . jmysql_error() . ":<br>\n $sql");
    return false;
  }
  return true;
}

//--------------------------------------------------------------------------------------------
function selectAction($bSilence = false)
{
  global $request, $action;
  switch ($action) {
    case "insert":
      return insertAction($bSilence);

    case "save":
      return saveAction($bSilence);

    case "delete":
      return deleteAction($bSilence);
  }
  return true;
}

?>