<style>
  img.combo{
    width:20px;height:24px;
    margin-top:2px;
    border-radius:3px;
    -moz-border-radius: 3px;
    -webkit-border-radius: 3px; 
  }
  span.combo {
    font-family:Calibri; font-size:12px;text-align:left;font-weight:normal;
    padding:10px 15px;display:none;
    margin-top:18px; margin-left:-19px;
    width:200px;
    position:absolute; color:#111;
    border:1px solid #DCA; background:#fffAF0;
    border-radius:4px;
    -moz-border-radius: 4px;
    -webkit-border-radius: 4px;

    -moz-box-shadow: 5px 5px 8px #CCC;
    -webkit-box-shadow: 5px 5px 8px #CCC;
    box-shadow: 5px 5px 8px #CCC; 
  }
  .combo_ok,.combo_close {width:70px;margin-top:15px;margin-right:5px;}
</style>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script>
  $(function () {
    $("img.combo").click(function () {
      $("span.combo").hide();
      $("#span_" + this.id).show();
      if (t = document.getElementById("input" + this.id.substr(5)))
        t.focus();
    });
    $("input.combo_close").click(function () {
      $(this.parentNode).hide();
    });
    //-------------------------------------------------------------------------------------
    $("input.combo_check").click(function () {
      s = this.checked;
      if (b = this.id)
        $(this).next("span").text((s ? "(Désélectionner" : "(Sélectionner") + " tout)");
      ar = [];
      bAll = true;
      k = (b ? b : this.className.substr(0, this.className.indexOf(' ')));
      $("input." + k).each(function () {
        if (b)
          this.checked = s;
        if (this.checked)
          ar.push(this.value.substr(this.value.indexOf("§") + 1));
        else
          bAll = false;
      });
      o = isNaN(k.charAt(11)) ? 11 : 12;
      document.getElementsByName("_sel" + k.substr(o))[0].value = !bAll && ar.length ? "_c=" + serialize(ar) : '';
    });
    //-------------------------------------------------------------------------------------
    $("input.combo_ok").click(function () {
      actionClick("");
    });
    //-------------------------------------------------------------------------------------
    $("input.sel_text").change(function () {
      o = isNaN(this.id.charAt(5)) ? 5 : 6;
      document.getElementsByName("_sel" + this.id.substr(o))[0].value = this.value;
    });
  });
</script>
