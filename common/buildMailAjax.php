<?php
//------------------------------------------------------------------------------------------------------
//ajax requests
$bFilter = function_exists("filterTemplate");
unset($filter);

//------------------------------------------------------------------
function getForwardedParams()
{
  global $request;
  foreach ($request as $k => $v)
    if (substr($k, 0, 5) == "_fwd_")
      $l .= ",$k:\"$v\"";
  return $l;
}

//Ajax request
switch ($action) {
  //------------------------------------------------------------------
  case "templateList":
    if ($bFilter && ($t = filterTemplate(1)))
      $filter = " where $t";
    echo "<fieldset style=margin-right:20px><legend>Modèles disponibles</legend>";
    $res = jmysql_query("select name from templates $filter");
    while ($tup = jmysql_fetch_row($res))
      echo "<div class=select_mod onclick=templateSelected(this)>$tup[0]</div>";
    echo "</fieldset>";
    exit();

  //------------------------------------------------------------------
  case "getMail":
    //dump($request);
    echo "res={mail:'" . addslashes(jmysql_result($r, 0, 0) . " &lt;" . jmysql_result($r, 0, 1)) . "&gt;',";
    echo "view:'";
    $bodyLine = array(
        0 => $request["data"]
    );
    $templateMail = 'viewmail.mail';
    $bNoComment = true;
    ob_start();
    include "genMailCommon.php";
    $l = addslashes(str_replace("\n", '', ob_get_contents()));
    ob_end_clean();
    echo "$l'};";
    exit();
}

include "head.php";
switch ($action) {
  //------------------------------------------------------------------
  case "getTemplate":
    if ($bFilter && ($t = filterTemplate(1)))
      $filter = " and $t";
    $res = jmysql_query("select subject,content,attaches from templates where name='" . addslashes($request["name"]) . "'$filter");
    if ($res && jmysql_num_rows($res)) {
      foreach ($request as $k => $v)
        if (substr($k, 0, 6) == "attach")
          unset($request[$k]);
      $request["subject"] = jmysql_result($res, 0, 0);
      $request["_val_body"] = jmysql_result($res, 0, 1);
      if ($t = unserialize(jmysql_result($res, 0, 2)))
        foreach ($t as $k => $v)
          $request[$k] = $v;
    }
    break;

  //------------------------------------------------------------------
  case "saveTemplate":
    if (!strlen($request["_val_body"])) {
      if ($bFilter && ($t = filterTemplate(1)))
        $filter = " and $t";
      jmysql_query("delete from templates where name='" . addslashes($request["name"]) . "'$filter");
      ?>
      <script>
        alert("Modèle supprimé");
      </script>
      <?php
      break;
    }
    foreach ($request as $k => $v)
      if (substr($k, 0, 6) == "attach")
        $s[$k] = $v;
    if ($s)
      $s = serialize($s);
    if ($bFilter && ($t = filterTemplate(2))) {
      $filter2 = ",$t";
      $filter3 = "," . filterTemplate(3);
    }
    jmysql_query("insert into templates (name, content, subject,attaches$filter2) values ('" . addslashes($request["name"]) . "', '" . $request["_val_body"] . "', '" . addslashes($request["subject"]) . "'," . ($s ? "'$s'" : "null") . $filter3 . ") on duplicate key update content=values(content), subject=values(subject),attaches=values(attaches)");
    break;

  //------------------------------------------------------------------
  case "sendMail":
    //dump($request);
    //dump($request["_val_body"]);
    $bodyLine = array(
        0 => str_replace("\r\n", '', str_replace('§', '&sect;', $request["_val_body"]))
    );
    if (!$templateMail)
      $templateMail = 'mailing.mail';
    if (!$genMail)
      $genMail = 'out/genMail';
    $subject = $request["subject"];
    $bResult = true;
    $request["sure"] = 'y';
    include "genMailCommon.php";
    //$bNoComment=false;
    if ($request["noSend"])
      $bTest = true;
    include "common/sendmails.php";
    exit();

  //------------------------------------------------------------------
  case "viewMail":
    //dump($request);
    ?>
    <script>
    <?php echo "var list=[" . $request["nums"] . "]" ?>;
      var current = -1;
      var data = "<?php echo addslashes(str_replace("\r\n", '', $request["_val_body"])) ?>";
      function get(o)
      {
        current += o ? -1 : +1;
        if (current >= list.length)
          current = 0;
        else if (current < 0)
          current = list.length - 1;
        $.post(location.pathName, {action: "getMail", num: list[current], data: data<?php echo getForwardedParams() ?>}, function (result) {
          try {
            eval(result);
          } catch (e) {
            alert('(' + e.message + '): ' + result);
            return;
          }
          $('#addr').html(res.mail);
          $('#viewMail').html(res.view);
        });
        document.getElementById('order').innerHTML = (current + 1) + ' de ' + list.length;
      }

    </script>
    <style>
      .viewMail{
        background: #e7f0f5;
        -moz-border-radius: 10px;
        -webkitborder-radius: 10px;
        border-radius: 10px;
        border: 3px double #000000;
        padding:10px;
        box-shadow: 1px 1px 12px #000000;
        width:60em;
        margin:auto;
        overflow-y:scroll;
      }
    </style>
    <?php
    echo "<title>Visualisation mail</title><body onload=get(0)></head>";
    dump($request);
    echo "<h2 style=width:100%;text-align:center>Visualisation de mail</h2>";
    echo "<div class=viewMail style=margin-bottom:20px><b>Pour : </b><div id=addr style=margin:10px;display:inline-block;font-size:small;min-width:40em;background:lightgray>test</div><input style=font-weight:bold type=button value='<' onclick=get(1)><span style=margin:10px id=order></span><input style=font-weight:bold type=button value='>' onclick=get(0)></div>";
    //createAction(-1,"Envoyer les mails","openWindow(\"genMail.php$basePath&data=\"+data)");
    echo "<div class=viewMail style=height:40em id=viewMail>";
    exit();
}
