<?php
define('NL', "<br>");
define('nl', "\n");
$isLocalHostForTest = strpos(__FILE__, "D:\\NetbeansProjects") !== false;
$bDevPhase = strpos(__FILE__, "/Jacques/") || $isLocalHostForTest;
include "connectDB.php";
//$bDevPhase=false;
$browserName = $_SERVER["HTTP_USER_AGENT"];
if ($t = stripos($browserName, 'Firefox/'))
  $browserName = 'firefox';
elseif ($t = stripos($browserName, 'MSIE/'))
  $browserName = 'ie';
elseif ($t = stripos($browserName, 'Trident/'))
  $browserName = 'ie';
elseif ($t = stripos($browserName, 'Edge/'))
  $browserName = 'edge';
elseif ($t = stripos($browserName, 'Chrome/'))
  $browserName = 'chrome';
elseif ($t = stripos($browserName, 'Safari/'))
  $browserName = 'safari';
if ($t) {
  $browserVersion = strtok(substr($_SERVER["HTTP_USER_AGENT"], $t), '/');
  $browserVersion = intval(strtok('.'));
}
error_reporting(E_ERROR | E_PARSE);
if (!$bDevPhase) {
  ini_set('display_errors', 0);
  set_error_handler("errorHandler", E_PARSE | E_COMPILE_ERROR | E_RECOVERABLE_ERROR | E_ERROR | E_CORE_ERROR);
} else
  ini_set('display_errors', 1);
//exit();
//$seeRequest=true;
//$bDevPhase=false;

$request = array_merge($_GET, $_POST);
if (get_magic_quotes_gpc())
  foreach ($request as $k => $v)
    if (strlen($v))
      $request[$k] = stripslashes($v);
$action = $request["action"];

$now = new DateTime();
unset($t);
if (!isset($yearSeason))
  $yearSeason = ($t = $request["season"]) ? $t + 2000 : $now->format("Y") + ($now->format("m-d") < NEW_SEASON_DATE ? -1 : 0);

session_start();
if ($bSession) {
  if (basename($currentPage = $_SERVER["SCRIPT_NAME"]) == "loadpage.php")
    $currentPage = dirname($currentPage) . "/" . $request["file"];
  if (!isset($_SESSION["pages"][$currentPage]))
    $_SESSION["pages"][$currentPage] = 1;
  else if ($request["action"] && ($c = $request["_checkRefresh_"]))
    if ($c != $_SESSION["pages"][$currentPage])
    //refresh unauthorized
      unset($request["action"]);
    else
      $_SESSION["pages"][$currentPage] ++;

  $id = $_SESSION["user"]["id"];
  if (!$id) {
    if (!$request["loginScript"])
      $request["loginScript"] = basename($_SERVER["SCRIPT_NAME"], ".php");
    $isIncludeLoginScript = true;
  } else {
    $bRoot = ($bOfficial = $id == "COM_jac.6QmdlR4D") || $id == "MEI_jac.7TZS9T6L" || $id == "DBT_jac.1Vtqk74D" || $id == "JUS_ced.wu1PPqK6";
    if (($bRoot || ($_SESSION["user"]["rights"] & 0x20)) && $request["user"] == "root")
      $id = $request["id"];
  }
} else {
  $id = $request["id"];
  $bRoot = ($bOfficial = $id == "COM_jac.6QmdlR4D") || $id == "MEI_jac.7TZS9T6L" || $id == "DBT_jac.1Vtqk74D" || $id == "JUS_ced.wu1PPqK6";
}

$currentDBLink = connectDB();
$bFraudPresent = jmysql_num_rows(jmysql_query("show tables like 'fraud'"));
if ($bFraudPresent && jmysql_result(jmysql_query("select count(*) from fraud where ip='" . $_SERVER['REMOTE_ADDR'] . "' and date='" . $now->format("Y-m-d") . "' and block=1"), 0)) {
  jmysql_query("update fraud set n=n+1 where ip='" . $_SERVER['REMOTE_ADDR'] . "' and date='" . $now->format("Y-m-d") . "'");
  notFound();
}


$basePath = $id ? "?id=$id" . ($t ? "&season=$t" : '') : ($t ? "?season=$t" : '');
$basePath1 = $basePath . (strlen($basePath) ? '&' : '?');
if (!$isIncludeLoginScript) {
  if (!$bRoot && $bOnlyRoot)
    stop(__FILE__, __LINE__, "Only root", null, true);
  if (!$id && $bIdMandatory)
    stop(__FILE__, __LINE__, "No ID", null, true);
  if (strlen($id) > 20)
    stop(__FILE__, __LINE__, "Fraude", null, true);
}

$months = array("Année", "janvier", "février", "mars", "avril", "mai", "juin", "juillet", "août", "septembre", "octobre", "novembre", "décembre");
$sMonths = array("An", "jan", "fév", "mar", "avr", "mai", "juin", "juil", "août", "sep", "oct", "nov", "déc");
$uMonths = array("ANNEE", "JANVIER", "FEVRIER", "MARS", "AVRIL", "MAI", "JUIN", "JUILLET", "AOUT", "SEPTEMBRE", "OCTOBRE", "NOVEMBRE", "DECEMBRE");
$orderMonths = array(1 => 5, 6, 7, 8, 9, 10, 11, 0, 1, 2, 3, 4);
$weekDays = array("dimanche", "lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi");
$sWeekDays = array("di", "lu", "ma", "me", "je", "ve", "sa");
$bMSIE = strpos($_SERVER["HTTP_USER_AGENT"], "MSIE") !== false;
$MSIE = $bMSIE ? "true" : "false";

define(CAT_ALL_SUBCAT, 0xFFFF);

define("CAT_FIRST", "0xFF0000=0");
define("CAT_COACH", 0x10000);
define("CAT_ASSIST", 0x20000);
define("CAT_PLAYER", 0x40000);
define("CAT_OTHERS", 0x80000);

define("CAT_REMP_COACH", (CAT_OTHERS+10));
define("CAT_BLESSE", (CAT_OTHERS+11));
define("CAT_SPONSOR", (CAT_OTHERS+60));
define("CAT_MUT_OUT", (CAT_OTHERS+90));
define("CAT_MUT_IN", (CAT_OTHERS+91));
define("CAT_STOP", (CAT_OTHERS+100));
define("CAT_STAGE", (CAT_OTHERS+110));

//---------------------------------------------------------------------------------------------------------------------------
function checkBrowserVersion($b)
{
  global $browserName, $browserVersion;
  foreach ($b as $k => $v) {
    $name = strtok($v, '.');
    $ver = strtok(' ');
    if ($name == $browserName)
      return !strlen($ver) || $browserVersion >= $ver;
  }
  return false;
}

//-------------------------------------------------------------------------------------------------
function getRefreshCnt($bComplete = false)
{
  global $currentPage;
  return $_SESSION["pages"][$currentPage];
}

//-------------------------------------------------------------------------------------------------
function notFound()
{
  header("Status: 404 Not Found", true, 404);
  ?>
  <!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
  <html><head>
      <title>404 Not Found</title>
    </head><body>
      <h1>Not Found</h1>
      <p>The requested URL <?php echo "http://" . $_SERVER["HTTP_HOST"] . $_SERVER["PHP_SELF"] ?> was not found on this server.</p>
      <hr>
      <address><?php echo $_SERVER["SERVER_SIGNATURE"] ?></address>
    </body></html>
  <?php
  exit();
}

//-------------------------------------------------------------------------------------------------    
function errorHandler($errno, $errstr, $errfile, $errline)
{
  //echo "<pre>errno=$errno\nerrstr=$errstr\nerrfile=$errfile\nerrline=$errline</pre>";
  if (!(error_reporting() & $errno))
  // This error code is not included in error_reporting
    return;

  //mail("lebrun48@gmail.com", "!! Error PHP", "errno=$errno\nerrstr=$errstr\nerrfile=$errfile\nerrline=$errline\n".getErrorDump());
  die("<br><div style='border:2px solid red;font-size:x-large'>Un problème technique est survenu.<br>Une solution sera apportée rapidement.<br>Veuillez réessayer plus tard.</div>");
}

//-------------------------------------------------------------------------------------------------
function SendError()
{
  global $bFraudPresent;
  if ($bFraudPresent) {
    $r = jmysql_query("select n,date from fraud where ip='" . $_SERVER['REMOTE_ADDR'] . "' and date='" . $now->format('Y-m-d') . "'");
    if (jmysql_num_rows($r)) {
      if ($com == "Fraude")
        jmysql_query("update fraud set block=1 where ip='" . $_SERVER['REMOTE_ADDR'] . "' and date='" . $now->format('Y-m-d') . "'");
      else {
        if ($com != "Fraude" && jmysql_result($r, 0) < 20)
          jmysql_query("update fraud set n=n+1 where ip='" . $_SERVER['REMOTE_ADDR'] . "' and date='" . $now->format('Y-m-d') . "'");
        else
          jmysql_query("update fraud set block=1 where ip='" . $_SERVER['REMOTE_ADDR'] . "' and date='" . $now->format('Y-m-d') . "'");
      }
      if (jmysql_result($r, 0) > 2)
        return false;
    } else
      jmysql_query("insert into fraud (ip,date,n,block) values ('" . $_SERVER['REMOTE_ADDR'] . "','" . $now->format('Y-m-d') . "',1," . ($com == "Fraude" ? "1)" : "0)"));
  }
  return true;
}

//-------------------------------------------------------------------------------------------------    
function stop($file, $line, $com = null, $overrideMsg = null, $bNotFound = false)
{
  global $bDevPhase, $request, $now, $bFraudPresent;
  if ($bDevPhase)
    die("<b>" . $file . '.' . $line . "</b> : $com <br> $overrideMsg");

  if (SendError() && $com) {
    $t = "ID: " . $request["id"] . "\nFile:$file\nLine:$line\nComment:$com\n";
    mail("lebrun48@gmail.com", "!! Error PHP", "$t\n" . getErrorDump());
  }

  if ($bNotFound)
    notFound();
  die("<br><div style='border:2px solid red;font-size:x-large'>" . ($overrideMsg ? $overrideMsg : "Un problème technique est survenu.<br>Une solution sera apportée rapidement.<br>Veuillez réessayer plus tard.") . "</div>");
}

//-------------------------------------------------------------------------------------------------    
function getErrorDump()
{
  global $request;
  $t = $_SERVER["HTTP_FROM"];
  $t1 = $_SERVER["HTTP_USER_AGENT"];
  if (stripos($t, "bingbot") !== false || stripos($t, "googlebot") !== false || stripos($t1, "ContextAd Bot") !== false)
    notFound();
  ob_start();
  dump($_SERVER, "server");
  dump($request, "request");
  dump($_SESSION, "session");
  $l .= ob_get_contents();
  ob_end_clean();
  return $l;
}

//-------------------------------------------------------------------------------------------------
function toHex(&$v)
{
  for ($i = 0; $i < strlen($v); $i++)
    $s .= sprintf("%02x", ord($v[$i]));
  echo "$v $s" . NL;
  return $s;
}

//-------------------------------------------------------------------------------------------------    
function toAsc(&$v)
{
  for ($i = 0; $i < strlen($v); $i += 2)
    $s .= ord(sscanf("%02x", $v[$i]));
  echo "$v $s" . NL;
  return $s;
}

//-------------------------------------------------------------------------------------------------    
function dump($obj, $com = null)
{
  echo "<pre>$com=";
  if (is_string($obj)) {
    $t = htmlspecialchars($obj);
    var_dump($t);
  } else
    var_dump($obj);
  echo "</pre>";
}

//-------------------------------------------------------------------------------------------------    
function creDate($dt, $offset)
{
  $d = clone($dt);
  $d->modify(($offset > 0 ? "+$offset" : $offset) . " day");
  return $d;
  //$t=mktime($dt->format("H"), $dt->format("i"), $dt->format("s"), $dt->format("n"), $dt->format("j"), $dt->format("Y"))+$offset*24*3600;
  //return new DateTime(date("Y-m-d H:m:s",$t));
}

//-------------------------------------------------------------------------------------------------    
function formatDate($dt, $ft = "W D M Y")
{
  global $weekDays, $sWeekDays, $months, $sMonths;
  for ($i = 0; $i < strlen($ft); $i++)
    switch ($ft[$i]) {
      case 'w':
        $s .= $sWeekDays[$dt->format("w")];
        break;

      case 'W':
        $s .= $weekDays[$dt->format("w")];
        break;

      case 'd':
        $s .= $dt->format("j");
        break;

      case 'D':
        $s .= $dt->format("d");
        break;

      case 'M':
        $s .= $months[$dt->format("n")];
        break;

      case 'm':
        $s .= $sMonths[$dt->format("n")];
        break;

      case 'n':
        $s .= $dt->format("m");
        break;

      case 'Y':
        $s .= $dt->format("Y");
        break;

      case 'y':
        $s .= substr($dt->format("Y"), -2);
        break;

      case 'H':
        $s .= $dt->format("H");
        break;

      case 'i':
        $s .= $dt->format("i");
        break;

      case 's':
        $s .= $dt->format("s");
        break;

      default:
        $s .= $ft[$i];
    }
  return $s;
}

//-------------------------------------------------------------------------------------------------
function getCategory($num, $bName = false)
{
  static $categories;
  if (!$categories) {
    $r = jmysql_query("select * from category where num<>0 and short is not null");
    while ($tup = jmysql_fetch_assoc($r)) {
      $n = $tup["num"];
      unset($tup["num"]);
      $categories[$n] = $tup;
    }
  }

  if (!is_numeric($num)) {
    foreach ($categories as $k => $v)
      if ($v["teamSite"]) {
        $t = unserialize($v["teamSite"]);
        foreach ($t as $k1 => $v1)
          if ($v1->div == $num)
            $ret[] = ($k + $k1) & 0xFFFF;
      }
    return $ret;
  }

  switch ($num & 0xFF0000) {
    case CAT_PLAYER:
      if ($t = ($num & 0xFFFF) % 10)
        $c = " " . chr(ord('A') + $t - 1);
      if ($bName)
        return $categories[$num - $t]["name"] . $c;
      $cat = $categories[$num - $t];
      $cat["name"] .= $c;
      $cat["short"] .= $c;
      $r = jmysql_query("select code from category where num=$num or num=" . ($num - $t) . " order by num");
      if ($t = jmysql_num_rows($r))
        $cat["code"] = $t > 1 ? jmysql_result($r, 1, 0) : jmysql_result($r, 0, 0);
      return $cat;

    case CAT_ASSIST:
    case CAT_COACH:
      if ($t = ($num & 0xFFFF) % 10)
        $c = " " . chr(ord('A') + $t - 1);
      $tm = (CAT_PLAYER | $num & 0xFFFF) - $t;
      if ($bName)
        return $categories[$num & 0xFF0000]["name"] . ' ' . $categories[$tm]["name"] . $c;
      $cat = $categories[$num & 0xFF0000];
      $cat["name"] .= ' ' . $categories[$tm]["name"] . $c;
      $cat["short"] .= ' ' . $categories[$tm]["short"] . $c;
      return $cat;

    default:
      return $bName ? $categories[$num]["name"] : $categories[$num];
  }
}

//-------------------------------------------------------------------------------------------------    
function getExistingCategories($cond, $bShort = true, $newCat = false, $allCat = null)
{
  
  global $seeRequest;
  //$seeRequest = 1;
  if (!isset($allCat))
    $allCat = $bShort;

  $sav_cond = $cond;
  for ($l = 0; $l <= ($newCat ? 1 : 0); $l++) {
    if ($l) {
      $cond = $sav_cond;
      $cond = str_replace("category", "ncategory", $cond);
    }
    $att = ($l ? 'n' : '') . "category";

    $r = jmysql_query("select $att from members where $cond $att<>0 group by $att");
    $p = $bShort ? "short" : "name";
    while ($tup = jmysql_fetch_row($r)) {
      $n = getCategory($tup[0]);
      $cat[$tup[0]] = $n[$p];
    }
    if (!$allCat)
      continue;
    for ($i = 2; $i <= 6; $i++) {
      $c = $i == 2 ? '' : $i - 1;
      if (stripos($cond, "category$c") !== false)
        $cond = str_replace("category$c", "category$i", $cond);
      $r = jmysql_query("select $att$i from members where $cond $att$i is not null group by $att$i");
      while ($tup = jmysql_fetch_row($r))
        if (!$cat[$tup[0]]) {
          $n = getCategory($tup[0]);
          $cat[$tup[0]] = $n[$p];
        }
    }
  }
  if ($cat)
    ksort($cat);
  return $cat;
}

function getID($name, $firstName, $length)
{
  for ($i = 0; $i < $length; $i++) {
    $encoded = rand(ord('0'), 122);
    if ($encoded > ord('9') && $encoded < ord('A'))
      $encoded -= 7;
    if ($encoded > ord('Z') && $encoded < ord('a'))
      $encoded -= 6;
    if ($encoded > ord('z'))
      $encoded -= 5;
    $res .= chr($encoded);
  }
  $replace = array("'"      => '-', ' '      => '-',
      '&lt;'   => '', '&gt;'   => '', '&#039;' => '', '&amp;'  => '',
      '&quot;' => '', 'À'      => 'A', 'Á'      => 'A', 'Â'      => 'A', 'Ã'      => 'A', 'Ä'      => 'Ae',
      '&Auml;' => 'A', 'Å'      => 'A', 'Ā'      => 'A', 'Ą'      => 'A', 'Ă'      => 'A', 'Æ'      => 'Ae',
      'Ç'      => 'C', 'Ć'      => 'C', 'Č'      => 'C', 'Ĉ'      => 'C', 'Ċ'      => 'C', 'Ď'      => 'D', 'Đ'      => 'D',
      'Ð'      => 'D', 'È'      => 'E', 'É'      => 'E', 'Ê'      => 'E', 'Ë'      => 'E', 'Ē'      => 'E',
      'Ę'      => 'E', 'Ě'      => 'E', 'Ĕ'      => 'E', 'Ė'      => 'E', 'Ĝ'      => 'G', 'Ğ'      => 'G',
      'Ġ'      => 'G', 'Ģ'      => 'G', 'Ĥ'      => 'H', 'Ħ'      => 'H', 'Ì'      => 'I', 'Í'      => 'I',
      'Î'      => 'I', 'Ï'      => 'I', 'Ī'      => 'I', 'Ĩ'      => 'I', 'Ĭ'      => 'I', 'Į'      => 'I',
      'İ'      => 'I', 'Ĳ'      => 'IJ', 'Ĵ'      => 'J', 'Ķ'      => 'K', 'Ł'      => 'K', 'Ľ'      => 'K',
      'Ĺ'      => 'K', 'Ļ'      => 'K', 'Ŀ'      => 'K', 'Ñ'      => 'N', 'Ń'      => 'N', 'Ň'      => 'N',
      'Ņ'      => 'N', 'Ŋ'      => 'N', 'Ò'      => 'O', 'Ó'      => 'O', 'Ô'      => 'O', 'Õ'      => 'O',
      'Ö'      => 'Oe', '&Ouml;' => 'Oe', 'Ø'      => 'O', 'Ō'      => 'O', 'Ő'      => 'O', 'Ŏ'      => 'O',
      'Œ'      => 'OE', 'Ŕ'      => 'R', 'Ř'      => 'R', 'Ŗ'      => 'R', 'Ś'      => 'S', 'Š'      => 'S',
      'Ş'      => 'S', 'Ŝ'      => 'S', 'Ș'      => 'S', 'Ť'      => 'T', 'Ţ'      => 'T', 'Ŧ'      => 'T',
      'Ț'      => 'T', 'Ù'      => 'U', 'Ú'      => 'U', 'Û'      => 'U', 'Ü'      => 'Ue', 'Ū'      => 'U',
      '&Uuml;' => 'Ue', 'Ů'      => 'U', 'Ű'      => 'U', 'Ŭ'      => 'U', 'Ũ'      => 'U', 'Ų'      => 'U',
      'Ŵ'      => 'W', 'Ý'      => 'Y', 'Ŷ'      => 'Y', 'Ÿ'      => 'Y', 'Ź'      => 'Z', 'Ž'      => 'Z',
      'Ż'      => 'Z', 'Þ'      => 'T', 'à'      => 'a', 'á'      => 'a', 'â'      => 'a', 'ã'      => 'a',
      'ä'      => 'ae', '&auml;' => 'ae', 'å'      => 'a', 'ā'      => 'a', 'ą'      => 'a', 'ă'      => 'a',
      'æ'      => 'ae', 'ç'      => 'c', 'ć'      => 'c', 'č'      => 'c', 'ĉ'      => 'c', 'ċ'      => 'c',
      'ď'      => 'd', 'đ'      => 'd', 'ð'      => 'd', 'è'      => 'e', 'é'      => 'e', 'ê'      => 'e',
      'ë'      => 'e', 'ē'      => 'e', 'ę'      => 'e', 'ě'      => 'e', 'ĕ'      => 'e', 'ė'      => 'e',
      'ƒ'      => 'f', 'ĝ'      => 'g', 'ğ'      => 'g', 'ġ'      => 'g', 'ģ'      => 'g', 'ĥ'      => 'h',
      'ħ'      => 'h', 'ì'      => 'i', 'í'      => 'i', 'î'      => 'i', 'ï'      => 'i', 'ī'      => 'i',
      'ĩ'      => 'i', 'ĭ'      => 'i', 'į'      => 'i', 'ı'      => 'i', 'ĳ'      => 'ij', 'ĵ'      => 'j',
      'ķ'      => 'k', 'ĸ'      => 'k', 'ł'      => 'l', 'ľ'      => 'l', 'ĺ'      => 'l', 'ļ'      => 'l',
      'ŀ'      => 'l', 'ñ'      => 'n', 'ń'      => 'n', 'ň'      => 'n', 'ņ'      => 'n', 'ŉ'      => 'n',
      'ŋ'      => 'n', 'ò'      => 'o', 'ó'      => 'o', 'ô'      => 'o', 'õ'      => 'o', 'ö'      => 'oe',
      '&ouml;' => 'oe', 'ø'      => 'o', 'ō'      => 'o', 'ő'      => 'o', 'ŏ'      => 'o', 'œ'      => 'oe',
      'ŕ'      => 'r', 'ř'      => 'r', 'ŗ'      => 'r', 'š'      => 's', 'ù'      => 'u', 'ú'      => 'u',
      'û'      => 'u', 'ü'      => 'ue', 'ū'      => 'u', '&uuml;' => 'ue', 'ů'      => 'u', 'ű'      => 'u',
      'ŭ'      => 'u', 'ũ'      => 'u', 'ų'      => 'u', 'ŵ'      => 'w', 'ý'      => 'y', 'ÿ'      => 'y',
      'ŷ'      => 'y', 'ž'      => 'z', 'ż'      => 'z', 'ź'      => 'z', 'þ'      => 't', 'ß'      => 'ss',
      'ſ'      => 'ss', 'ый'     => 'iy', 'А'      => 'A', 'Б'      => 'B', 'В'      => 'V', 'Г'      => 'G',
      'Д'      => 'D', 'Е'      => 'E', 'Ё'      => 'YO', 'Ж'      => 'ZH', 'З'      => 'Z', 'И'      => 'I',
      'Й'      => 'Y', 'К'      => 'K', 'Л'      => 'L', 'М'      => 'M', 'Н'      => 'N', 'О'      => 'O',
      'П'      => 'P', 'Р'      => 'R', 'С'      => 'S', 'Т'      => 'T', 'У'      => 'U', 'Ф'      => 'F',
      'Х'      => 'H', 'Ц'      => 'C', 'Ч'      => 'CH', 'Ш'      => 'SH', 'Щ'      => 'SCH', 'Ъ'      => '',
      'Ы'      => 'Y', 'Ь'      => '', 'Э'      => 'E', 'Ю'      => 'YU', 'Я'      => 'YA', 'а'      => 'a',
      'б'      => 'b', 'в'      => 'v', 'г'      => 'g', 'д'      => 'd', 'е'      => 'e', 'ё'      => 'yo',
      'ж'      => 'zh', 'з'      => 'z', 'и'      => 'i', 'й'      => 'y', 'к'      => 'k', 'л'      => 'l',
      'м'      => 'm', 'н'      => 'n', 'о'      => 'o', 'п'      => 'p', 'р'      => 'r', 'с'      => 's',
      'т'      => 't', 'у'      => 'u', 'ф'      => 'f', 'х'      => 'h', 'ц'      => 'c', 'ч'      => 'ch',
      'ш'      => 'sh', 'щ'      => 'sch', 'ъ'      => '', 'ы'      => 'y', 'ь'      => '', 'э'      => 'e',
      'ю'      => 'yu', 'я'      => 'ya'
  );
  $firstName = str_replace(array_keys($replace), $replace, $firstName);
  $name = str_replace(array_keys($replace), $replace, $name);
  $all = strtoupper(substr($name, 0, 3)) . '_' . strtolower(substr($firstName, 0, 3));
  return "$all.$res";
}

function includeTabs($template = 1, $width = 100)
{
  echo "<script src=/tools/tabcontent/tabcontent.js type=text/javascript></script>" . nl;
  echo "<link href=/tools/tabcontent/template$template/tabcontent.css rel=stylesheet type=text/css>" . nl;
  echo "<div style='width:$width%;margin: 0 auto;'>" . nl;
  echo "   <ul class=tabs data-persist=true>" . nl;
}

function insertEl(&$ar, $row)
{
  for ($i = sizeof($ar) - 1; $i > $row; $i--)
    $ar[$i + 1] = $ar[$i];
  $ar[$row + 1] = '';
  ksort($ar);
}

function remEl(&$ar, $row)
{
  for ($i = $row; $i < sizeof($ar); $i++)
    $ar[$i] = $ar[$i + 1];
  unset($ar[$i - 1]);
}

function delTree($dir)
{
  $files = array_diff(scandir($dir), array('.', '..'));
  foreach ($files as $file) {
    (is_dir("$dir/$file")) ? delTree("$dir/$file") : unlink("$dir/$file");
  }
  return rmdir($dir);
}

if ($isIncludeLoginScript)
  include "login.php";
?>
