<script>
//-------------------------------------------------------------------------------------------------
  function genericLoadEvent()
  {
<?php
global $request;
if ($request["scr"])
  echo "window.scrollTo(0," . $request["scr"] . ')' . nl;
if ($this->bExport) {
  echo "location.assign('" . $this->exportFileName . "')" . nl;
}
?>
  }

//-------------------------------------------------------------------------------------------------
  function generatePDF()
  {
<?php
if (isset($this->tabNb)) {
  ?>
      for (i = 0; i <=<?php echo $this->tabNb ?>; i++)
        if (document.getElementById("view" + i).style.display != "none")
          break;
  <?php
  echo "window.open(location.pathname+'" . buildAllParam($this, "action", "generatePDF") . "&tabNbr='+i)" . nl;
} else
  echo "window.open(location.pathname+'" . buildAllParam($this, "action", "generatePDF") . "')" . nl;
?>
    document.getElementById("mainForm").target = null;
  }

//-------------------------------------------------------------------------------------------------
  function exportExcel()
  {
<?php
$this->exportClick();
if (isset($this->tabNb)) {
  ?>
      for (i = 0; i <=<?php echo $this->tabNb ?>; i++)
        if (document.getElementById("view" + i).style.display != "none")
          break;
      actionClick('export', 'expTabNbr', i);
  <?php
} else
  echo "actionClick('export');" . nl;
?>
  }

//-------------------------------------------------------------------------------------------------
  function orderClick(key)
  {
    dir = '<?php echo $this->dir == "asc" ? "desc" : "asc" ?>';
    operation = document.getElementById("order");
    operation.value = key;
    document.getElementById("dir").value = key == '<?php echo $this->order ?>' ? dir : "asc";
    document.getElementById("action").value = 'order';
    document.getElementById("scr").value = document.body.scrollTop;
    document.getElementById("dummySubmit").click();
  }

//-------------------------------------------------------------------------------------------------
  function editClick(par)
  {
    els = document.getElementsByName("_select_");
    list = '';
    for (i = 0; i < els.length; i++)
      if (els[i].checked && els[i].value)
        list += "','" + els[i].value<?php echo ($this->sumKey ? ".substr(0,els[i].value.lastIndexOf('$'))" : '') ?>;
    operation = document.getElementById("operation");
    if (list)
    {
      if (typeof checkEditList == 'function' && checkEditList(list.substr(2) + "'"))
        return;
      operation.name = "list";
      operation.value = '<?php echo $this->buildKeyList() ?> in (' + list.substr(2) + "')";
    } else if (par.length)
    {
      operation.name = "key";
      operation.value = par;
    } else
    {
      operation.name = 'tableName';
      operation.value = '<?php echo strtok($this->tableName, ',') ?>';
    }
    document.getElementById("scr").value = document.body.scrollTop;
    document.getElementById("action").value = 'edit';
    document.getElementById("dummySubmit").click();
  }

//-------------------------------------------------------------------------------------------------
  function deleteClick(par, bAsk)
  {
    els = document.getElementsByName("_select_");
    list = '';
    for (i = 0; i < els.length; i++)
      if (els[i].checked && els[i].value)
        list += ",'" + els[i].value<?php echo ($this->sumKey ? ".substr(0,els[i].value.lastIndexOf('$'))" : '') ?> + "'";
    if (list) {
      if (typeof checkDeleteList == 'function' && checkDeleteList(list.substr(2) + "'"))
        return;
      par = '<?php echo $this->buildKeyList() ?> in (' + list.substr(1) + ')';
    }
    if (bAsk && !confirm("Es-tu sûr de vouloir supprimer " + (list ? 'ces enregistrements?' : 'cet enregistrement')))
      return;
    if (list && !confirm("ATTENTION: plusieurs enregistrements!! Confirme?"))
      return;
    operation = document.getElementById("operation");
    operation.name = "key";
    operation.value = par;
<?php
global $bSession;
if ($bSession)
  echo "$('#mainForm').append('<input type=hidden name=_checkRefresh_ value=" . getRefreshCnt() . ">');" . nl;
?>
    document.getElementById("scr").value = document.body.scrollTop;
    document.getElementById("action").value = 'delete';
    document.getElementById("dummySubmit").click();
  }

//-------------------------------------------------------------------------------------------------
  function viewAllClick()
  {
    document.getElementById("scr").value = document.body.scrollTop;
    document.getElementById("action").value = 'viewAll';
    document.getElementById("dummySubmit").click();
  }

//-------------------------------------------------------------------------------------------------
  function hideColClick(par)
  {
    operation = document.getElementById("operation");
    operation.name = "key";
    operation.value = par;
    document.getElementById("scr").value = document.body.scrollTop;
    document.getElementById("action").value = 'hideCol';
    document.getElementById("dummySubmit").click();
  }

//-------------------------------------------------------------------------------------------------
  function actionClick(action, oper, val)
  {
    operation = document.getElementById("operation");
    if (oper)
    {
      if (oper.substr(0, 5) == '_sel_' && (t = document.getElementsByName(oper)) && t.length)
        t[0].value = val;
      operation.name = oper;
      operation.value = val;
    }
    document.getElementById("scr").value = document.body.scrollTop;
    if (action) {
<?php
global $bSession;
if ($bSession)
  echo "$('#mainForm').append('<input type=hidden name=_checkRefresh_ value=" . getRefreshCnt() . ">');" . nl;
?>
      document.getElementById("action").value = action;
    }
    document.getElementById("dummySubmit").click();
  }

//-------------------------------------------------------------------------------------------------
  var bAll = {};
  function selectAll(name)
  {
    if (!name)
      name = "_select_";
    if (!(name in bAll))
      bAll[name] = true;

    $("[name='" + name + "']").each(function () {
      this.checked = bAll[name]
    });
    $("#" + name).attr("title", bAll[name] ? "Sélectionner Aucun" : "Sélectionner Tous");

    bAll[name] = !bAll[name];
    sumChecked(null, event, null, name);
  }

//-------------------------------------------------------------------------------------------------
  var lastIdx = {};
<?php
if ($this->nElements) {
  $t = '';
  foreach ($this->nElements as $k => $v)
    $t .= ",$k:$v";
  $t = substr($t, 1);
  echo "var nElts={ $t };" . nl;
}
?>
  function sumChecked(t, e, last, name)
  {
    var evt = e ? e : window.event;
    if (!(name in lastIdx))
      lastIdx[name] = 0;
    if (shift = evt.shiftKey ? 1 : 0)
      if (last < lastIdx[name]) {
        begin = last;
        end = lastIdx[name];
      } else {
        begin = lastIdx[name];
        end = last;
      }
    els = document.getElementsByName(name);
    bSum =<?php echo isset($this->sumKey) ? "true" : "false" ?>;
    nElements = 0;
    if (t) {
      els[last].checked = t.checked;
      nTemplates =<?php echo isset($this->nTemplates) ? $this->nTemplates : 0 ?>;
      if (typeof nElts !== "undefined" && typeof nElts[name] !== "undefined")
        nElements = nElts[name];
      for (i = 1; i < nTemplates; i++) {
        if ((n = i * nElements + last) < els.length)
          els[n].checked = t.checked;
        else
          break;
      }
    }

    if (shift) {
      for (i = begin + 1; i < end; i++) {
        els[i].checked = !els[i].checked;
        if (nElements)
          for (tab = 1; tab < nTemplates; tab++)
            if ((n = tab * nElements + i) < els.length)
              els[n].checked = !els[n].checked;
            else
              break;
      }
    }
    if (!nElements && bSum) {
      nElements = els.length;
      sumValueChecked = 0;
      for (i = 0; i < nElements; i++)
        if (els[i].checked && bSum)
          sumValueChecked += els[i].value.substr(els[i].value.lastIndexOf('$') + 1) / 1;
      dispSumChecked(sumValueChecked);
    }
    lastIdx[name] = last;

  }
//-------------------------------------------------------------------------------------
  function getCBLength(obj) {
    length = obj.length;
    if (typeof nElts[obj[0].name] !== "undefined")
      length = nElts[obj[0].name];
    return length;
  }

//-------------------------------------------------------------------------------------
  function serialize(mixed_value) {
    var val, key, okey,
            ktype = '', vals = '', count = 0,
            _utf8Size = function (str) {
              var size = 0,
                      i = 0,
                      l = str.length,
                      code = '';
              for (i = 0; i < l; i++) {
                code = str.charCodeAt(i);
                if (code < 0x0080) {
                  size += 1;
                } else if (code < 0x0800) {
                  size += 2;
                } else {
                  size += 3;
                }
              }
              return size;
            },
            _getType = function (inp) {
              var match, key, cons, types, type = typeof inp;

              if (type === 'object' && !inp) {
                return 'null';
              }
              if (type === 'object') {
                if (!inp.constructor) {
                  return 'object';
                }
                cons = inp.constructor.toString();
                match = cons.match(/(\w+)\(/);
                if (match) {
                  cons = match[1].toLowerCase();
                }
                types = ['boolean', 'number', 'string', 'array'];
                for (key in types) {
                  if (cons == types[key]) {
                    type = types[key];
                    break;
                  }
                }
              }
              return type;
            },
            type = _getType(mixed_value);

    switch (type) {
      case 'function':
        val = '';
        break;
      case 'boolean':
        val = 'b:' + (mixed_value ? '1' : '0');
        break;
      case 'number':
        val = (Math.round(mixed_value) == mixed_value ? 'i' : 'd') + ':' + mixed_value;
        break;
      case 'string':
        val = 's:' + _utf8Size(mixed_value) + ':"' + mixed_value + '"';
        break;
      case 'array':
      case 'object':
        val = 'a';
        /*
         if (type === 'object') {
         var objname = mixed_value.constructor.toString().match(/(\w+)\(\)/);
         if (objname == undefined) {
         return;
         }
         objname[1] = this.serialize(objname[1]);
         val = 'O' + objname[1].substring(1, objname[1].length - 1);
         }
         */

        for (key in mixed_value) {
          if (mixed_value.hasOwnProperty(key)) {
            ktype = _getType(mixed_value[key]);
            if (ktype === 'function') {
              continue;
            }

            okey = (key.match(/^[0-9]+$/) ? parseInt(key, 10) : key);
            vals += this.serialize(okey) + this.serialize(mixed_value[key]);
            count++;
          }
        }
        val += ':' + count + ':{' + vals + '}';
        break;
      case 'undefined':
      // Fall-through
      default:
        // if the JS object has a property which contains a null value, the string cannot be unserialized by PHP
        val = 'N';
        break;
    }
    if (type !== 'object' && type !== 'array') {
      val += ';';
    }
    return val;
  }

</script>
