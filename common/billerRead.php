<?php

//$bSimul=3;
//$seeRequest=1;
//dump($_FILES,"_FILES");
if ($_FILES["uploadAll"]) {
  foreach ($_FILES["uploadAll"]["error"] as $k => $v) {
    if ($v = $_FILES["uploadAll"]["error"][$k])
      alert("Une erreur s'est produite lors du téléchargement ($v) pour " . $_FILES["uploadAll"]["name"][$k]);
    else {
      $file = "tmp/" . $_FILES["uploadAll"]["name"][$k];
      if (!move_uploaded_file($_FILES["uploadAll"]["tmp_name"][$k], $file))
        alert("Une erreur s'est produite lors du transfert du fichier '" . $_FILES["uploadAll"]["name"][$k] . "'!");
      foreach ($numAccount as $k => $v)
        if (strpos($file, $v[FILE_NAME]) !== false) {
          $bAlready = false;
          insertFile($file, $k);
        }
      unlink($file);
    }
  }
}

echo "<form id=uploadForm action=" . $_SERVER["PHP_SELF"] . "$basePath method=post enctype='multipart/form-data'><input id=submit type=submit style=display:none>";
echo "<input onchange=document.getElementById('submit').click() multiple type=file name='uploadAll[]' id=uploadAll style=position:absolute;top:-20000px;></form>";
echo "Pour télécharger les fichiers d'extraits, <input type=button value='Clique ICI' onclick=document.getElementById('uploadAll').click()>";

function insertFile(&$file, $ac)
{
  global $separator, $bAlready, $numAccount, $IBAN_Account;
  $char = &$numAccount[$ac];
  if (!$separator && !($separator = $char[9]))
    $separator = ';';
  $h = fopen("$file", "r");
  echo "file=$file, accountLine=$ac" . NL;
  $state = ($len = strlen($char[HEADER])) == 0 ? 1 : 0;
  while (!feof($h)) {
    //get columns
    $l = fgets($h);
    if (($c = $l[strlen($l) - 1]) == "\n" || $c == "\r")
      $l = substr($l, 0, -1);
    if (($c = $l[strlen($l) - 1]) == "\n" || $c == "\r")
      $l = substr($l, 0, -1);
    //dump($l, "line before");
    if (function_exists("applyTranslation"))
      applyTranslation($l, $char[ACCOUNT], $file);
    //dump($l, "line after");
    if (!strlen($l))
      continue;
    switch ($state) {
      case 0:
        global $numAccount;
        if (substr($l, 0, $len) == $char[HEADER])
          $state = 1;
        break;

      case 1:
        $n = 0;
        $quoted = 0;
        unset($col);
        for ($i = 0; $i < strlen($l); $i++) {
          $c = $l[$i];
          if ($c == "\n" || $c == "\r")
            continue;
          //echo "sep=$separator, quoted=$quoted, c=$c".NL;
          switch ($quoted) {
            case 0:
              if ($c == $separator)
                $n++;
              else if (($quoted = $c == '"' ? 1 : 2) == 2)
                $col[$n] .= $c;
              break;

            case 1:
              if ($c == '"')
                switch ($l[$i + 1]) {
                  case '"':
                    $i++;
                    $col[$n] .= '"';
                    break;

                  case $separator:
                    $n++;
                    $i++;
                    $quoted = 0;
                    break;
                } else
                $col[$n] .= $c;
              break;

            case 2:
              if ($c == $separator) {
                $n++;
                $quoted = 0;
              } else
                $col[$n] .= $c;
              break;
          }
        }
        if (!($dt = $col[$char[DATE]]))
          continue;
        //Check date
        $d = strtok($dt, '/-');
        if (!is_numeric($d))
          continue;
        $m = strtok('/-');
        if (!is_numeric($m)) {
          global $sMonths;
          foreach ($sMonths as $k => $v)
            if (!strcasecmp(substr($m, 0, strlen($v)), $v)) {
              $m = $k;
              $k = 0;
              break;
            }
          if ($k) {
            echo "<span style=color:red;font-weight:bold>Mois incorrect:$m dans ligne: </span>$l" . NL;
            continue;
          }
        }
        $y = strtok('/-');
        if ($y < 2000)
          $y += 2000;
        $obj->date = sprintf("%d-%02d-%02d", $y, $m, $d);
        $obj->amount = str_replace(".", "", $col[$char[AMOUNT_I]]);
        if (strlen($obj->amount) == 0) {
          if ($char[AMOUNT_O] == -1) {
            echo "<span style=color:red;font-weight:bold>Pas de montant dans ligne: </span>$l" . NL;
            continue;
          }
          if (($obj->amount = str_replace(".", "", $col[$char[AMOUNT_O]])) > 0)
            $obj->amount = -$obj->amount;
        }
        $obj->amount = str_replace(",", ".", $obj->amount);
        $obj->amount = str_replace("€", "", $obj->amount);
        if (($obj->account = $char[ACCOUNT]) < 0) {
          foreach ($IBAN_Account as $k => $v)
            if ($v == $col[-$obj->account]) {
              $obj->account = $k;
              break;
            }
          if ($obj->account < 0)
            stop(__FILE__, __LINE__, "", $col[-$obj->account] . " n'est pas connu");
        }
        $obj->orig = $col[$char[ORIG]];
        $obj->detail = $char[DETAIL] != -1 ? jmysql_real_escape_string($col[$char[DETAIL]]) : null;
        $obj->accountNb = $char[NUM_ACCOUNT] != -1 ? jmysql_real_escape_string($col[$char[NUM_ACCOUNT]]) : null;
        if (function_exists("translateCols"))
          translateCols($obj);
        $sql = "select count(*) from " . TABLE_NAME . " where account=$obj->account " . ($obj->orig ? " and orig like '%" . jmysql_real_escape_string($obj->orig) . "%'" : '') . " and date='$obj->date' and (amount=$obj->amount or detail like '%(Total=" . $obj->amount . "€)%') and detail like '%$obj->detail%'";
        //Already exist?
        //dump($sql, "check");
        if (jmysql_result(jmysql_query($sql), 0)) {
          //fclose($h);
          if (!$bAlready)
            echo "<span style=color:red;font-weight:bold>Déjà présent:</span>$l" . NL;
          $bAlready = true;
          continue;
        }
        //store in DB
        if (!function_exists("insertDB") || insertDB($obj))
          ;
        insertLine($obj);
    }
  }
  fclose($h);
}

function insertLine(&$obj)
{
  global $currentYear, $defaultValue, $bAlready, $bSimul, $disableDate;
  if ($obj->date < $disableDate) {
    echo "<span style=color:red;font-weight:bold>Année clôturée:$obj->date</span>$l" . NL;
    return;
  }
  $sql = "insert into " . TABLE_NAME . " set account=$obj->account, date='$obj->date', orig='" . jmysql_real_escape_string($obj->orig) . "', amount=" . $obj->amount . ($obj->detail ? ", detail='" . jmysql_real_escape_string($obj->detail) . "'" : '') . "$defaultValue, season=" . (isset($currentYear) ? $currentYear - 2000 : substr($obj->date, 0, 4) - 2000) . (isset($obj->category) ? ", category=" . $obj->category : '') . (isset($obj->accountNb) ? ", accountNb='$obj->accountNb'" : '');
  echo "<span style=color:blue;font-weight:bold>Inserted:</span>$sql" . NL;
  jmysql_query($sql, $bSimul ? $bSimul : $bAlready * 2);
}
