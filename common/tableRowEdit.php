<?php
include_once "common/tableTools.php";

define('INTEGER_ONLY', 1);
define('NUMERIC_ONLY', 2);

class TableRowEdit
{

  public $title;
  public $colDef;
  public $saveClick = "saveClick";
  public $newClick = "saveClick";
  public $resetClick = "resetClick";

//-----------------------------------------------------------------------------------------------
  function __construct()
  {
    global $request;
    if ($request["list"])
      $this->bList = true;
    $this->bSave = $this->bNew = $this->bClearAll = $this->bEscape = true;
  }

  function applySave()
  {
    
  }

  function applyReset()
  {
    
  }

//-----------------------------------------------------------------------------------------------
  private function decodeRange($range, $ft, $bOptions = false)
  {
    $ret->bId = $range[0] == '*' || $range[1] == '*';
    $bNoEmpty = $range[0] == '-' || $range[1] == '-';
    $range = substr($range, $bNoEmpty + $bId);
    if ($bOptions) {
      if ($this->bList) {
        $ret->range[''] = "<span style=color:blue>(Inchangé)</span>";
        if (strpos($ft[2], "[mand]") === false)
          $ret->range["=null"] = "<span style=color:blue>(Supprimé)</span>";
      } else if (!$bNoEmpty)
        $ret->range[''] = '';
    }
    $range = strtok($range, '+');
    while ($range) {
      if (($ret->bLabel = $range[0] == '@') || $range[0] == '#') {
        //external array or DB
        $range = substr($range, 1);
        global $$range;
        if ($tr = $this->$range ? $this->$range : $$range)
          if (is_array($tr))
            foreach ($tr as $k => $v)
              $ret->range[$k] = $v;
          else {
            if (jmysql_num_rows($tr))
              jmysql_data_seek($tr, 0);
            while ($t = jmysql_fetch_row($tr))
              $ret->range[$t[0]] = $ret->bLabel || isset($t[1]) ? $t[1] : $t[0];
          }
      } else {
        while (($j = strpos($range, ";")) !== false || $range && ($j = strlen($range))) {
          $v = substr($range, 0, $j);
          $t = strpos($v, ':');
          $val = $t ? substr($v, $t + 1) : $v;
          $t = $t ? substr($v, 0, $t) : $v;
          $ret->range[$val] = $t;
          $range = substr($range, $j + 1);
        }
      }
      $range = strtok('+');
    }
    return $ret;
  }

//-----------------------------------------------------------------------------------------------
  function applyValue($att, $ft, $row, $bString = false)
  {
    $value = is_array($row) ? $row[$att] : $row;
    $type = "text";
    unset($disabled);
    if ($ft[3])
      $ch = " onchange=$ft[3]";
    if ($ft[2]) {
      for ($index = 0; $index < strlen($ft[2]); $index++) {
        if ($ft[2][$index] == '[') {
          $t = strpos($ft[2], ']', $index);
          $tok = substr($ft[2], $index + 1, $t - $index - 1);
          $index = $t;
          $tok = strtolower(strtok($tok, "("));
          $otherAttr = strtok('');
          $range = substr($otherAttr, 0, strrpos($otherAttr, ')'));
          $otherAttr = substr($otherAttr, strlen($range) + 1);
        } else
          $tok = strtolower($ft[2][0]);
        //dump($tok,"tok");
        static $dataListArray, $dataListNb = 0; //for hidden input linked to datalist
        switch ($tok) {
          case "default":
            if (!strlen($value))
              $value = $range;
            break;

          case "param":
            $param = " $range";
            break;

          case "mand":
            break;

          case "nl":
            $textAfter = "<br>";
            break;

          case "text-after":
            $textAfter = $range;
            break;

          case "display":
            $line .= "<input size=$ft[0] value='" . htmlspecialchars($value, ENT_QUOTES) . "' disabled>";
            break;

          case "disabled":
            $disabled = " disabled";
            break;

          case "help":
            $help = "<span style='border:1px solid black;border-radius:5px;background:#80CCCC;margin-left:10px;padding:2px;font-weight:bold' class=help id=itmark_$att>?</span>";
            $help .= "<span id=help_$att style='width:200px;visibility:hidden;background:#CCEBEB;position:absolute;border:1px solid lightgray;box-shadow:3px 3px 2px #888888;padding:5px'>" . $this->help[$att] . "</span>";
            break;

          case "int":case "1":case "2":case "dec":
            $st = " style=text-align:right$ch onkeypress=\"return keyPressed(event,'$tok[0]')\"";
            $type = "text";
            break;

          case "tab":
            $tab = " style=margin-left:${range}px;";
            break;

          case "email":
            $type = "email";
            break;

          case "datetime":
            $bTime = true;
            if ($value)
              $value = str_replace(' ', 'T', $value);
          case "date":
            global $now, $browserName;
            unset($max);
            unset($min);
            if ($range) {
              if (($min = strtok($range, ";")) && ($min[0] == '+' || $min[0] == '-'))
                $min = eval("return " . $now->format("Y") . $min . ';');
              if (($max = strtok(";")) && ($max[0] == '+' || $max[0] == '-'))
                $max = eval("return " . $now->format("Y") . $max . ';');
            }
            if (checkBrowserVersion(array("chrome.20", "edge.12", "firefox.57")))
              $line .= "<input$disabled$param$ch$tab name=_val_$att value='$value' type=" . ($bTime ? "datetime-local" : "date") . ($min ? " min=$min-01-01" : '') . ($max ? " max=$max-12-31" : '') . ">";
            else {
              global $months;
              $line .= "<select$disabled$param id=day_$att><option></option>";
              $t = substr($value, 8, 2);
              for ($j = 1; $j <= 31; $j++)
                $line .= "<option" . ($j == $t ? " selected=selected" : '') . ">" . sprintf("%02d", $j) . "</option>";
              $line .= "</select> / <select$disabled$param id=month_$att><option></option>";
              $t = substr($value, 5, 2);
              for ($j = 1; $j <= 12; $j++)
                $line .= "<option value=" . sprintf("%02d", $j) . ($j == $t ? " selected=selected" : '') . ">$months[$j]</option>";
              $line .= "</select> / <select$disabled$param id=year_$att><option></option>";
              $max = $max ? $max : $now->format("Y") + 90;
              $t = substr($value, 0, 4);
              for ($j = $min ? $min : $now->format("Y") - 90; $j <= $max; $j++)
                $line .= "<option" . ($j == $t ? " selected=selected" : '') . ">" . sprintf("%02d", $j) . "</option>";
              $line .= "</select>";
              if ($bTime) {
                $t = substr($value, 11, 2);
                $line .= " à <select$disabled$param id=hour_$att><option></option>";
                for ($j = 0; $j <= 23; $j++)
                  $line .= "<option" . (strlen($t) && $j == $t ? " selected=selected" : '') . ">" . sprintf("%02d", $j) . "</option>";
                $line .= "</select> h <select$disabled$param id=min_$att><option></option>";
                $t = substr($value, 14, 2);
                for ($j = 0; $j < 60; $j += 5)
                  $line .= "<option" . (strlen($t) && $j == $t ? " selected=selected" : '') . ">" . sprintf("%02d", $j) . "</option>";
                $line .= "</select>";
              }
              $line .= "<input type=hidden value='$value' name=_val_$att>   <input type=button value=Reset onclick=resetDate('$att')>";
            }
            break;

          case "checkbox":
            if (($t = strpos($range, ':')) !== false) {
              $v = substr($range, $t + 1);
              $t = substr($range, 0, $t);
            } else {
              $t = $range;
              unset($v);
            }
            $line .= "<input$disabled$param$ch$tab id=label_$att" . (isset($v) ? " value=$v" : '') . " type=checkbox name=_val_$att" . ($value == $v ? " checked>" : '>');
            unset($tab);
            $line .= "<label for=label_$att>$t</label>";
            break;


          case "radio":
            $decode = $this->decodeRange($range, $ft);
            $i = 0;
            foreach ($decode->range as $k => $v) {
              $line .= "<input$disabled$param$ch$tab type=radio id=label_${att}_" . ( ++$i) . " name=_val_$att value='$k'" . (count($value) && $value == $k ? " checked=checked" : '') . "><label$param for=label_${att}_$i>$v</label>$textAfter";
              unset($tab);
            }
            unset($textAfter);
            unset($tab);
            break;

          case "inputlist":
            $line .= "<input$disabled$param$tab autocomplete=on data-input=datalistInput$dataListNb list=${range}Id value=\"" . $dataListArray[$range]->range[$value] . "\"$ch><input id=datalistInput$dataListNb type=hidden name=_val_$att>";
            unset($tab);
            $dataListNb++;
            break;

          case "select":
          case "selectmultiple":
          case "datalist":
            global $request;
            $decode = $this->decodeRange($range, $ft, true);
            if ($bl = $tok[0] == 'd' && checkBrowserVersion(array("chrome.20", "edge.10", "firefox.4", "safari.12"))) {
              $attOnly = strtok($att, '[');
              $dataListArray[$attOnly] = $decode;
              $line .= "<input$disabled$param$tab autocomplete=on data-input=datalistInput$dataListNb list=${attOnly}Id value=\"" . $decode->range[$value] . "\"$ch><input id=datalistInput$dataListNb type=hidden name=_val_$att>";
              $dataListNb++;
              $line .= "<datalist id=${attOnly}Id>";
            } else
              $line .= "<select$disabled$param$tab$ch" . ($decode->bId ? " id=${att}Id" : '') . " name=_val_$att" . (($bMultiple = $tok[6] == 'm') ? "[] multiple" : '') . "$otherAttr>";
            if (sizeof($decode->range)) {
              if ($bMultiple && !is_array($value = unserialize($value)))
                $value[] = '';
              foreach ($decode->range as $k => $v)
                if ($bl)
                  $line .= "<option data-value='$k' value=\"$v\">";
                else
                  $line .= "<option value='$k'" . (!$this->bList && ($bMultiple && in_array("$k", $value, true) || !$bMultiple && $value == $k) ? " selected=selected" : '') . ">$v</option>";
            }
            $line .= "</$tok>";
            unset($tab);
            break;
        }
      }
    }
    if ($line) {
      $line .= $help;
      if ($bString)
        return $line;
      echo $line;
      return;
    }
    $st = eval("return \"" . str_replace('"', "\\\"", $st) . "\";");
    if ($ft[0]) {
      if ($ft[0] <= 100)
        $out = "<input$disabled$param name=_val_$att value=\"" . htmlspecialchars($value) . "\" type=$type size=" . (($t = $ft[0]) < 10 ? $t : intval($t / 1.5)) . " maxlength=$ft[0]$st$ch>";
      else
        $out = "<textarea$disabled$param name=_val_$att cols=100 rows=" . ((int) ($ft[0] / 100) + 1) . "$st$ch>" . htmlspecialchars($value) . "</textarea>";
      $out .= $textAfter;
      unset($textAfter);
      if ($this->bList)
        $out .= "<input type=checkbox name=_replace_${att}><b>Remplacer le contenu actuel</b>";
    }
    $out .= $help;
    if ($bString)
      return $out;
    echo $out;
  }

//-----------------------------------------------------------------------------------------------
  function buildSql($where)
  {
    return "select * from $where";
  }

//-----------------------------------------------------------------------------------------------
  function getTuple()
  {
    global $request;
    if ($this->bList)
      return null;
    if (!$request["key"]) {
      $this->bSave = false;
      $this->bInsert = true;
      //stop(__FILE__, __LINE__, "Pas d'idenfication de tuple");
      return;
    }
    $res = jmysql_query($this->buildSql(str_replace('$', "'", $request["key"])));
    if (!$res || jmysql_num_rows($res) != 1)
      stop(__FILE__, __LINE__, jmysql_error() . ": tuple '" . $request["key"]);
    return jmysql_fetch_assoc($res);
  }

//-----------------------------------------------------------------------------------------------
  function printLine($att, $ft, $row)
  {
    if (stripos($ft[2], "[mand]") !== false)
      $ft[1] .= "<span style=color:red;font-weight:bold;font-size:x-small>&nbsp;*</span>";

    static $prevFollow;
    if (!($follow = isset($ft["follow"])) || !$prevFollow)
      echo "<tr>";

    if (!$follow || $ft["follow"] == "table") {
      if ($ft[1])
        echo "<th style=padding-left:20px;" . $ft["titleStyle"] . ">$ft[1]</th>";
      echo "<td style=font-weight:normal;" . $ft["contentStyle"] . '>';
    } else if ($ft["follow"] == "collapse") {
      if (!$prevFollow)
        echo "<td>";
      if ($ft[1])
        echo "<span class=editRow style=padding-left:20px;" . $ft["titleStyle"] . ">$ft[1]</span>";
      echo "<span style=font-weight:normal;padding-left:5px" . $ft["contentStyle"] . '>';
      $end = "</span>";
    }
    $prevFollow = $follow;
    $this->applyValue($att, $ft, $row);
    echo $end;
  }

//-----------------------------------------------------------------------------------------------
  function editRow()
  {
    global $request;
    $this->row = $this->getTuple();
    getRequest($this->param);
    echo "<form autocomplete=off id=editForm action='" . $_SERVER["SCRIPT_NAME"] . buildGetParam($this->param) . "' method=post enctype='multipart/form-data'><input id=dummySubmit name=action type=submit style=display:none><input type=hidden name=_checkRefresh_ value=" . getRefreshCnt() . ">" . nl;
    if (!$request["key"] && !$request["list"])
      $request["key"] = $request["tableName"] . " where";
    buildDefaultInputs($this->param);
    // Edit window build
    if ($this->title)
      echo "<h2>" . $this->title . "</h2>";
    $bFirst = true;
    foreach ($this->colDef as $att => $ft) {
      if ($ft === null || is_string($ft)) {
        if (!$bFirst)
          echo "</table>";
        if ($bField)
          echo "</fieldset>";
        echo ($ft ? "<div style=$ft>" : '') . "<fieldset class=editField><legend class=editLegend>$att</legend>";
        $bField = true;
        if (!$bFirst)
          echo "<table id=edit>";
        continue;
      }
      if ($bFirst) {
        echo "<table id=edit>";
        $bFirst = false;
      }
      $this->printLine($att, $ft, $this->row);
    }
    echo "</table>" . ($bField ? "</div></fieldset>" : '') . "<br>";
    $this->buttons();
  }

//-----------------------------------------------------------------------------------------------
  function buildTranslate($name, $res)
  {
    echo "<script>\n";
    echo "var Tr$name={";
    if (is_array($res)) {
      foreach ($res as $k => $v)
        $l .= ",'$v':'$k'";
      echo substr($l, 1) . "};\n";
    } else {
      while ($t = jmysql_fetch_row($res))
        $l .= ",'$t[1]':'$t[0]'";
      echo substr($l, 1) . "};\n";
    }
    echo "</script>\n";
    return $res;
  }

//-----------------------------------------------------------------------------------------------
  function buttons()
  {
    require_once "tools.php";
    $offset = 20;
    if ($this->addButtonOffset == 1) {
      $this->addButton($offset);
      $offset += 120;
    }
    if ($this->bSave) {
      createAction(-1, "Sauvegarder", $this->saveClick . "('save')\" id=\"saveButton");
      $offset += 120;
    }
    if ($this->addButtonOffset == 2) {
      $this->addButton($offset);
      $offset += 120;
    }
    if (!$this->bList && $this->bNew) {
      createAction(-1, "Insérer nouveau", $this->newClick . "('insert')", 120);
      $offset += 140;
    }
    if ($this->addButtonOffset == 3) {
      $this->addButton($offset);
      $offset += 120;
    }
    if ($this->bClearAll) {
      createAction(-1, "Tout effacer", $this->resetClick . "()");
      $offset += 120;
    }
    if ($this->addButtonOffset == 4) {
      $this->addButton($offset);
      $offset += 120;
    }
    if ($this->bEscape) {
      createAction(-1, "Annuler", "document.location.assign('" . $_SERVER["SCRIPT_NAME"] . buildAllParam($this->param, "action", "escape") . "')");
      $offset += 120;
    }
    if ($this->addButtonOffset == 5) {
      $this->addButton($offset);
      $offset += 120;
    }
    echo "</form></body>" . nl;
    ?>
    <script>
    //-------------------------------------------------------------------------------------
      function resetDate(att) {
        $("#day_" + att).val('');
        $("#month_" + att).val('');
        $("#year_" + att).val('');
        $("#hour_" + att).val('');
        $("#min_" + att).val('');
      }
      $(".help").hover(function () {
        $("#help_" + this.id.substr(7)).css("visibility", "visible");
      }, function () {
        $("#help_" + this.id.substr(7)).css("visibility", "hidden");
      });

    //-------------------------------------------------------------------------------------
      function checkMand(o)
      {
        if (o === undefined || !o.length || $("[name='" + o[0].name + "']").css("visibility") == "hidden")
          return false;
        if (o.length > 1)
        {
          for (i in o)
            if (o[i].checked)
              return false;
          return true;
        }
        if (o[0].tagName == 'textarea')
          return !o[0].innerHTML.length;
        return !o[0].value.length;
      }

    //-------------------------------------------------------------------------------------
      var gParamSave;
      function saveClick(paramSave)
      {
        gParamSave = paramSave;
        //check datalist
        var objSet;
        $("input").each(function () {
          if (list = this.getAttribute("list")) {
            if (!this.disabled && this.value) {
              options = $("#" + list)[0].options;
              for (i in options)
                if (this.value == options[i].value) {
                  $("#" + this.dataset.input).val(options[i].dataset.value);
                  return;
                }
              objSet = this;
            }
          }
        });
        if (objSet) {
          msgBox("L'élement introduit n'est pas dans la liste", {click: function ()
            {
              objSet.focus();
              msgBoxClose();
            }
          });
          return;
        }

        $("[id^='day_']").each(function () {
          att = this.id.substr(4);
          y = $("#year_" + att).val();
          m = $("#month_" + att).val();
          d = $(this).val();
          var h, min;
          if (t = document.getElementById("hour_" + att)) {
            h = t.options[t.selectedIndex].value;
            min = $("#min_" + att).val();
          } else
            h = min = "";
          if ((y + m + d + h + min).length && (!y || !m || !d || t && (!h || !min))) {
            alert("La date n'est pas complète!");
            this.focus();
            return;
          }
          document.getElementsByName("_val_" + att)[0].value = y ? y + '-' + m + '-' + d + (t ? 'T' + h + ':' + min : '') : '';
        });

    <?php
    if (!$this->bList)
      foreach ($this->colDef as $k => $v) {
        if (stripos($v[2], "[mand]") !== false) {
          echo "o=document.getElementsByName('_val_$k');if(checkMand(o)){";
          echo "alert('Le champ \"" . addslashes($v[1]) . "\" est obligatoire!');";
          echo "o[0].focus();return;}" . nl;
        }
      }
    $this->applySave()
    ?>
        if (typeof userSaveClick == 'function' && userSaveClick(paramSave))
          return;
    <?php if ($t = $this->checkBeforeSave) {
      ?>
          $.post("<?php echo $t ?>", $("#editForm").serialize(), function (data) {
            if (!checkBeforeSave(data)) {
              submitForm();
            }
          });
    <?php } ?>
        submitForm();
      }

    //-------------------------------------------------------------------------------------
      function submitForm()
      {
        el = document.getElementById("dummySubmit");
        el.value = gParamSave;
        el.click();
      }

    //-------------------------------------------------------------------------------------
      function resetClick()
      {
        $("#editform select").each(function () {
          this.selectedIndex = 0;
        });
        $("#editform input:checked").attr("checked", false);
        $("#editform input").each(function (idx, elm) {
          if (this.type != 'hidden' && this.type != 'radio' && this.type != 'checkbox')
            this.value = '';
        });
        $("#editform textarea").html('');
    <?php $this->applyReset(); ?>
        $("#saveButton").css("visibility", "hidden");
      }

    //-------------------------------------------------------------------------------------
      function keyPressed(e, type)
      {
        if (window.event) // IE
          keynum = e.keyCode;
        else if (e.which) // Netscape/Firefox/Opera
          keynum = e.which;
        c = String.fromCharCode(keynum);
        s = "-0123456789";
        if (type == '2' || type == "d")
          s += '.';
        if (!(res = s.indexOf(c) != -1))
          alert("Des nombres seulement sont autorisés");
        return res;
      }


    </script>
    <?php
  }

}
