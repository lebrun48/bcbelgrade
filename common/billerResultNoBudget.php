<?php

//$seeRequest=1;
for ($loop = ($bPrint ? 0 : 1); $loop < 2; $loop++) {
  if ($bPrint)
    echo "<h1 style='" . ($loop ? ($bRoot ? "page-break-before:always;" : "padding-top:60px;border-top:1px solid black;") : '') . "color:RoyalBlue;text-align:center'>" . ($bDetail && $bPrint ? "Recettes " : "Bilan ") . substr(TABLE_NAME, 6) . (!$bCivil ? " saison $currentYear - " . ($currentYear + 1) : " année $currentYear") . "</h1>";
  echo "<table class=main style=margin:auto><tr><th style=width:200px>Poste</th><th style=width:100px>Etat " . ($m ? "de<br>$months[$m]" : ($bPreviousYear ? ($bCivil ? "année" : "saison") . "<br>" . ($season + 2000) : "au<BR>" . $now->format("d/m/Y"))) . "</th></tr>" . nl;
  //recettes
  displayNoBudget(true);

  //Dépenses
  if ($bPrint && $bDetail) {
    echo "</table class=main border=0px><h1 style='" . ($bRoot ? "page-break-before:always;" : "padding-top:60px;border-top:1px solid black;") . "color:RoyalBlue;text-align:center'>Dépenses " . substr(TABLE_NAME, 6) . (!$bCivil ? " saison $currentYear - " . ($currentYear + 1) : " année $currentYear") . "</h1>";
    echo "<table class=main style=margin:auto><tr><th style=width:200px>Poste</th><th style=width:100px>Etat " . ($m ? "de<br>$months[$m]" : ($bPreviousYear ? ($bCivil ? "année" : "saison") . "<br>" . ($season + 2000) : "au<BR>" . $now->format("d/m/Y"))) . "</th></tr>" . nl;
  }
  displayNoBudget(false);

  echo "</table><table class=main id=edit style=border:0;margin-top:1cm;width:300px;margin-left:auto;margin-right:auto>";
  echo "<tr style='border-bottom:1 solid #5d73a3'><td colspan=2 style=text-align:center;font-size:20px;padding-top:1cm;color:RoyalBlue>Bilan</td>";
  echo "<tr><td>Bilan</td><td style=color:" . ($result < 0 ? "red" : "darkgreen") . ";text-align:right>" . number_format($result, 0, ',', '.') . " €</td></tr>";

  //Bilan
  if (!$month && (!$bPrint || !$bDetail)) {
    echo "</table><table class=main id=edit style=border:0;margin-top:1cm;width:300px;margin-left:auto;margin-right:auto>";

    //Avoirs
    echo "<tr style='page-break-before:always;border-bottom:1 solid #5d73a3'><td colspan=2 style=text-align:center;font-size:20px;padding-top:1cm;color:RoyalBlue>Capital</td>";
    $sum1 = 0;
    $caisse = 0;
    foreach ($account as $k => $v) {
      if ($k >= 20 && $k < 30)
        continue;
      if (($bCash = substr($v, 0, 6) == "Caisse") && $v[strlen($v) - 1] != '+')
        if (abs($t = getSum(" account=$k and " . ($pCivil ? "date<='" . ($season + 2000) . "-12-31'" : "season<=$season"), false)) > 0.1)
          $ofCash += $t;
      if (abs($t = getSum(" account=$k$hidden and " . ($pCivil ? "date<='" . ($season + 2000) . "-12-31'" : "season<=$season"), false)) < 0.1)
        continue;
      if ($bCash) {
        $caisse += $t;
        continue;
      }
      $sum1 += $t;
      echo "<tr><td>$v</td><td style=color:" . ($t < 0 ? "red" : "darkgreen") . ";text-align:right>" . number_format($t, 0, ',', '.') . " €</td></tr>";
    }
    $sum1 += $caisse;
    if ($caisse) {
      echo "<tr><td>Caisse</td><td style=color:" . ($caisse < 0 ? "red" : "darkgreen") . ";text-align:right>" . number_format($caisse, 0, ',', '.') . " €</td></tr>";
      if ($bRoot && !$bOfficial) {
        echo "<tr style=text-align:right;font-size:small;font-style:italic><td style='padding:0 0 0 0'>Officielle</td><td style='padding:0 70 0 0;color:" . ($ofCash < 0 ? "red" : "darkgreen") . "'>" . number_format($ofCash, 0, ',', '.') . " €</td></tr>";
        echo "<tr style=text-align:right;font-size:small;font-style:italic><td style='padding:0 0 0 0'>Black</td><td style='padding:0 70 0 0;color:" . (($t = $caisse - $ofCash) < 0 ? "red" : "darkgreen") . "'>" . number_format($t, 0, ',', '.') . " €</td></tr>";
      }
    }
    //Virements internes
    $t = -getSum("season<=$season$hidden$resultCond and category&0xFF00=" . TRANSACTION_CATEGORY);
    if (abs($t) >= 0.01)
      echo "<tr><td>Virements internes</td><td style=color:" . ($t < 0 ? "red" : "darkgreen") . ";text-align:right>" . number_format($t, 0, ',', '.') . " €</td></tr>";
    $sum1 += $t;

    echo "<tr style='border-top:2px solid #5d73a3;font-size:large'><td style=color:RoyalBlue>Total avoirs</td><td style=color:" . ($sum1 < 0 ? "red" : "darkgreen") . ";text-align:right>" . number_format($sum1, 0, ',', '.') . " €</td></tr>";

    //TVA
    $t = 0;
    $dt = ($pCivil ? "date<='" . ($season + 2000) . "-12-31'" : "season<=$season") . ($bOfficial ? " and account<50" : '');
    if (isset($TVA)) {
      $t -= jmysql_result(jmysql_query("select sum(tva) from " . TABLE_NAME . " where TVA_status is null"), 0);
      if ($t)
        echo "<tr><td>TVA</td><td style=color:" . ($t < 0 ? "red" : "darkgreen") . ";text-align:right>" . number_format($t, 0, ',', '.') . " €</td></tr>";
      $sum1 += $t;
    }

    //Dettes/Créances
    $t = $t1 = 0;
    foreach ($category as $k => $v) {
      if ($v[0][0] == "Dettes")
        $t -= getSum("$dt and category&0xFF00=" . ($k * 256));
      else if ($v[0][0] == "Réserve")
        $t1 -= getSum("$dt and category&0xFF00=" . ($k * 256));
    }
    $sum1 += $t + $t1;
    if ($t)
      echo "<tr><td>" . ($t < 0 ? "Dettes" : "Créances") . "</td><td style=color:" . ($t < 0 ? "red" : "darkgreen") . ";text-align:right>" . number_format($t, 0, ',', '.') . " €</td></tr>";
    if ($t1)
      echo "<tr><td>Réserve</td><td style=color:" . ($t1 < 0 ? "red" : "darkgreen") . ";text-align:right>" . number_format($t1, 0, ',', '.') . " €</td></tr>";

    $t = 0;
    echo "<tr style='border-top:2px solid #5d73a3;font-size:large'><td style=color:RoyalBlue>Disponible</td><td style=color:" . (($t1 = $sum1 + ($t < 0 ? $t : 0)) < 0 ? "red;font-weight:bold" : "darkgreen") . ";text-align:right>" . number_format($t1, 0, ',', '.') . " €</td></tr></table>";
    //if ($t1<-300)
    //echo "<h2 style=width:100%;text-align:center;color:red><img width=100px src=attention.png style=vertical-align:middle>Rupture de paiement possible!";
  }
  $bDetail = true;
}

//-----------------------------------------------------------------------------------------------------
function displayNoBudget($bIncome)
{
  $par = 0;
  if ($bIncome)
    $type = "Recettes";
  else
    $type = "Dépenses";
  echo "<tr><td colspan=2 style='border-left:0px;border-right:0;border-bottom:2px solid #5d73a3;font-weight:bold;font-size:large;color:RoyalBlue" . (!$bIncome ? ";padding-top:10px" : '') . "'>$type</td></tr>";
  global $bRoot, $period, $m, $category, $aEnded, $amount, $catSerial, $bEnded, $sum, $sumUp, $season, $bDetail, $hidden, $month, $resultCond, $TVA, $bPrint, $subBud, $seeRequest;

  foreach ($category as $k => $v) {
    $s = getSum("$period$hidden$month$resultCond and category&0xFF00=" . ($k * 256));
    if (abs($s) < 0.01 || $bIncome && $s < 0 || !$bIncome && $s > 0)
      continue;
    echo "<tr class=parity$par><td class=tabLink><a onclick=biller(" . ($k * 256) . ")>" . $category[$k][0][0] . "</a><td style=text-align:right;color:" . ($s >= 0 ? 'black' : 'darkred') . ">" . number_format($s, 0, ',', '.') . " €</td></tr>";
    $par = 1 - $par;
    if ($bDetail && sizeof($category[$k]) > 1) {
      foreach ($category[$k] as $kk => $vv) {
        $dt = getSum("$period$hidden$month$resultCond and category=" . ($k * 256 + $kk));
        if (!$dt)
          continue;
        $ct = $catSerial[$k * 256 + $kk];
        echo "<tr class=parity$par style=font-size:small;font-style:italic;color:DarkSlateGray><td>$ct</td>";
        $par = 1 - $par;
        echo "<td style=padding-right:30px;color:" . ($dt < 0 ? 'darkred' : 'black') . ";text-align:right>" . number_format($dt, 2, ',', '.') . " €</td></tr>";
        echo "</tr>";
      }
    }
    //$v=main category budget, $s=main category situation, $y=main category for the month
    $t2 += $s;
    //echo "v=$v, sumup=$sumUp[$k]".NL;
  }
  echo "<tr style='border-top:2px solid black;border-bottom:2px solid black;font-weight:bold'><td>Total $type</td><td style=text-align:right;color:black>" . number_format($t2, 0, ',', '.') . " €</td></tr>" . nl;

  global $result;
  $result += $t2;
}
