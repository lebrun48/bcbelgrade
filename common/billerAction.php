<?php
//dump($request);
//$seeRequest=3;
//if (!$bRoot && (!function_exists("authorization") || !authorization())) 
//stop(__FILE__, __LINE__, "Not authorized");

switch ($action) {
  case "saveSplit":
    include "head.php";
    getRequest($obj);
    unset($obj->paramArray["ri"]);
    unset($obj->paramArray["val"]);
    echo "<body onload=opener.location.replace('" . TABLE_NAME . ".php" . buildAllParam($obj) . "');window.close()></body>";
    //echo "<body onload=opener.location.replace('".TABLE_NAME.".php".buildAllParam($obj)."')></body>";
    $tup = jmysql_fetch_assoc(jmysql_query("select * from " . TABLE_NAME . " where ri=" . $request["ri"]));
    $last = jmysql_result(jmysql_query("select max(subpart) from " . TABLE_NAME . " where date='" . $tup["date"] . "'"), 0);
    if ($last)
      $last = 10 * (1 + intval($last / 10));
    $tup["detail"] .= " (Total=" . sprintf("%0.2f", $tup["amount"]) . "€)";
    $val = explode('_', $request["val"]);
    $div = isset($resultCategory) ? 3 : 2;
    for ($i = 0; $i < sizeof($val); $i++) {
      $sql = "subpart=" . ($last + $i / $div + 1) . ", orig='[" . ($i / $div + 1) . "/" . (sizeof($val) / $div) . "] " . jmysql_real_escape_string($tup["orig"]) . "', category=$val[$i], amount=" . $val[++$i] . ", detail='" . jmysql_real_escape_string($tup["detail"]) . "'" . ($tup["num"] ? ",num=" . $tup["num"] : '');
      if (function_exists("saveSplitData"))
        $sql .= saveSplitData($tup);
      if (isset($resultCategory))
        $sql .= ", status=" . ($val[++$i] == -1 ? "null" : $val[$i]);
      if ($i == $div - 1)
        $sql = "update " . TABLE_NAME . " set $sql where ri=" . $request["ri"];
      else
        $sql = "insert into " . TABLE_NAME . " set $sql, account=" . $tup["account"] . ", date='" . $tup["date"] . "', season=" . $tup["season"] . (isset($tup["hidden"]) ? ",hidden=" . $tup["hidden"] : '');
      jmysql_query($sql);
    }
    exit();

  case "split":
    include "head.php";
    include_once "tools.php";
    echo "<title>Séparer</title></head><body>";
    $tup = jmysql_fetch_row(jmysql_query("select orig,amount,category" . (isset($resultCategory) ? ",status,detail" : '') . " from " . TABLE_NAME . " where ri=" . $request["ri"]));
    echo "<p style=color:black;font-weight:bold>$tup[0] <span style=color:gray>(total=" . sprintf("%0.2f", $tup[1]) . ")</span></p>" . nl;
    echo "<table border=0>";
    echo "<tr><td>Catégorie:" . (isset($resultCategory) ? "<BR>Bilan:<br>Equipe:" : '') . "</td><td><select onchange=statusChange() id=category>";
    foreach ($catSerial as $k => $v)
      echo "<option value=$k" . ($k == $tup[2] ? " selected=selected" : '') . ">" . (!$v ? "&lt;aucune&gt;" : $v) . "</option>";
    echo "</select>";
    if (isset($resultCategory)) {
      echo "<br><select id=status>";
      echo "<option value=-1>&lt;Aucune&gt;</option>";
      $step = -1;
      foreach ($resultCategory as $k => $v) {
        if ((int) ($k / 10) != $step) {
          if ($step != -1)
            echo "</optgroup>";
          $step = (int) ($k / 10);
          echo "<optgroup label='" . $resultCatName[$step] . "'>";
        }
        echo "<option value=$k" . (strlen($tup[3]) && $tup[3] == $k ? " selected=selected" : '') . ">$v</option>";
      }
      echo "</select><br>";
      if (($team = strpos($tup[4], " \$T:")) !== false)
        $team = substr($tup[4], $team + 4, strpos($tup[4], " \$T:", $team) - $team - 1);
      echo "<select name=team><option></option>";
      $teams = getExistingCategories("(category&" . CAT_PLAYER . ") and type<>0 and ", true);
      foreach ($teams as $v)
        echo "<option" . ($team == $v ? " selected=selected" : '') . ">$v</otion>";
      echo "</select>";
    }
    echo "</td><td><button onclick=insert()>====></button></td><td rowspan=2><div id=inserted></div></td></tr>" . nl;
    echo "<tr><td>Montant:</td><td><input id=input onkeyup=compute(this,document.getElementById('amount')) type=text value=" . sprintf("%0.2f", $tup[1]) . " size=23 maxlength=200><br><span id=amount style=color:blue;font-weight:bold>" . sprintf("%0.2f", $tup[1]) . "</span></td><td><button onclick=remove()><====</button></td></tr></table>" . NL . NL . nl;
    createAction(100, "Enregistrer", "save()");
    createAction(250, "Annuler", "window.close()");
    getRequest($obj);
    ?>
    <script type="text/javaScript">
      base=<?php echo $tup[1] ?>;
      function insert()
      {
      am=document.getElementById('amount');
      cat=document.getElementById('category');
      cat=cat.options[cat.selectedIndex];
      stat=document.getElementById('status');
      if (stat)
      stat=stat.options[stat.selectedIndex].value;
      v=am.innerHTML/1;
      ins=document.getElementById('inserted');
      n=document.createElement('div');
      n.setAttribute('id',cat.value+'_'+v<?php if (isset($resultCategory)) echo "+'_'+stat" ?>);
      n.setAttribute('style','color:black');
      n.innerHTML=cat.innerHTML+' ('+v.toFixed(2)+'€)';
      ins.appendChild(n);
      base-=v;
      document.getElementById('input').value=am.innerHTML=base.toFixed(2);
      }

      function save()
      {
      if (document.getElementById('amount').innerHTML/1 != 0)
      {
      alert ("La division n'est pas complète!");
      return;
      }
      ins=document.getElementById('inserted');
      p='';
      for (i=0; i<ins.childNodes.length; i++)
      {
      v=ins.childNodes[i];
      p+=(p ? '_': '')+v.id;
      }
      location.replace('<?php echo TABLE_NAME . ".php" . buildAllParam($obj, "action", "saveSplit") . "&val=" ?>'+p);
      } 

      function compute(obj, res)
      {
      if (!obj.value.length)
      {
      res.innerHTML='';
      return;
      } 
      res.innerHTML=eval(obj.value).toFixed(2);
      statusChange();
      }
      <?php
      if (isset($resultCategory)) {
        echo "allCat=[";
        foreach ($category as $k => $v)
          foreach ($v as $kk => $vv) {
            echo $sep . ($k * 256 + $kk);
            $sep = ',';
          }
        echo "];\nconvPos=[";
        unset($sep);
        foreach ($category as $k => $v)
          foreach ($v as $kk => $vv) {
            echo $sep . $vv[1];
            $sep = ',';
          }
        echo "];\nconvNeg=[";
        unset($sep);
        foreach ($category as $k => $v)
          foreach ($v as $kk => $vv) {
            echo $sep . $vv[2];
            $sep = ',';
          }
        echo "];" . nl;
      }
      ?>
      function statusChange()
      {
      <?php
      if (isset($resultCategory)) {
        ?>
        cat=document.getElementById('category');
        obj=document.getElementById('status');
        cat=cat.options[cat.selectedIndex].value;
        for (i in allCat)
        if (allCat[i]==cat)
        {
        v=document.getElementById('amount').innerHTML/1<0 ? convNeg[i]:convPos[i];
        if (v==-1)
        obj.selectedIndex=0;  
        for (j=0; j<obj.length; j++)
        if (obj.options[j].value==v)
        { 
        obj.selectedIndex=j;
        break;
        }
        }
        <?php
      }
      ?>
      }  

    </script>  
    <?php
    exit();

  case "updnum":
    //dump($request);
    if ($request["save"]) {
      include "head.php";
      echo "</head><body onload=window.close()>" . nl;
      foreach ($request as $k => $v)
        if (substr($k, 0, 3) == "num" && strlen($v))
          jmysql_query("update " . TABLE_NAME . "_num set $k=$v");
      echo "</body>" . NL;
      exit();
    }
    $tup = jmysql_fetch_assoc(jmysql_query("select * from " . TABLE_NAME . "_num"));
    echo "<form action=" . TABLE_NAME . ".php><input type=hidden name=id value=$id><input type=hidden name=save value=y><input type=hidden name=action value=updnum><table>";
    foreach ($tup as $k => $v)
      switch (substr($k, 0, 3)) {
        case "com":
          echo "<tr><td>$v:</td>";
          break;

        case "num":
          include "head.php";
          echo "<td style=text-align:right>$v.</td><td>Prochain numéro:<br><input type=text name=$k></td></tr>";
          break;
      }
    echo "</table><input type=submit value=Enregitrer></form>";
    exit();


  case "edit":
    include TABLE_NAME . "Edit.php";
    exit();

  case "insert":
    if ($request["_val_increment"]) {
      jmysql_query("update " . TABLE_NAME . "_num set num=num+1");
      unset($request["_val_increment"]);
    }
    $sql = "insert into " . TABLE_NAME . " set ";
    buildSql($sql);
    //echo "sql=$sql".NL;break;
    if (!jmysql_query($sql))
      echo "Error insert on sql: $sql" . NL . "Mysql error=" . jmysql_error() . NL;
    break;

  case "save":
    if ($request["_val_increment"]) {
      jmysql_query("update " . TABLE_NAME . "_num set num=num+1");
      unset($request["_val_increment"]);
    }
    saveAction();
    break;

  case "delete":
    deleteAction();
    break;

  case "classify":
    include "head.php";
    echo "<title>Classement</title></head><body>" . nl;
    include "tableEdit.php";
    $obj = new TableEdit;
    $obj->tableName = $request["tab"];
    $obj->colDef = array("ri");
    $cl = jmysql_query("select * from " . $request["tab"] . "_classify");
    while ($tup = jmysql_fetch_assoc($cl)) {
      if ($tup["content"])
        $obj->filter["orig"] = addslashes($tup["content"]);
      if ($tup["details"])
        $obj->filter["detail"] = addslashes($tup["details"]);
      else
        unset($obj->filter["detail"]);
      $sql = "update " . $request["tab"] . " set category=" . $tup["category"];
      if (isset($resultCategory)) {
        $sql1 = "$sql,status=" . $category[$tup["category"] / 256][$tup["category"] % 256][1] . " where category=0 and amount>0 and " . $obj->buildWhere();
        jmysql_query($sql1, 1);
        echo jmysql_affected_rows() . " tuples updated" . NL;
        $sql .= ",status=" . $category[$tup["category"] / 256][$tup["category"] % 256][2] . " where category=0 and amount<0 and " . $obj->buildWhere();
        jmysql_query($sql, 1);
        echo jmysql_affected_rows() . " tuples updated" . NL;
      } else {
        $sql .= " where category=0 and " . $obj->buildWhere();
        jmysql_query($sql, 1);
        echo jmysql_affected_rows() . " tuples updated" . NL;
      }
    }
    exit();
}
