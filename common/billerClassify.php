<?php

include "admin.php";
include "tableTools.php";
include $request["tab"] . "Conf.php";
//dump($request);
SelectAction();

if ($action == "edit") {
  include "head.php";
  echo "<title>" . substr(TABLE_NAME, 6) . " classify</title></head><body>" . nl;

  $col = array("content"  => array(50, "Orig contient :"),
      "details"  => array(50, "Detail contient :"),
      "category" => array(0, "Catégorie :"),
  );

  include "tableRowEdit.php";

  class BillerClassify extends TableRowEdit
  {

    //----------------------------------------------------------------------------------------------
    function applyValue(&$key, &$format, &$row)
    {
      if ($key == "category") {
        global $catSerial;
        echo "<select name=_val_category>";
        foreach ($catSerial as $k => $v)
          echo "<option value=$k" . ($row[$key] == $k ? " selected=selected" : '') . ">" . (!$v ? "&lt;aucune&gt;" : $v) . "</option>";
        echo "</select>";
        return;
      }
      return TableRowEdit::applyValue($key, $format, $row);
    }

  }

  $obj = new BillerClassify;
  $obj->title = "Edition de classeur";
  $obj->colDef = &$col;
  $obj->editRow();
  exit();
}

include "tableEdit.php";
include "head.php";
echo "<title>" . substr(TABLE_NAME, 6) . " classify</title></head><body onload=genericLoadEvent()>" . nl;

$col = array("content"  => array(50, "Orig", "width:100"),
    "details"  => array(50, "Details", "width:100"),
    "category" => array(-1, "Catégorie")
);

class BillerClassify extends TableEdit
{

//---------------------------------------------------------------------------------------------------
  public function applyDisplay(&$key, &$val, &$row, $idxLine)
  {
    switch ($key) {
      case "category":
        global $category;
        $dispValue = $category[$row[$key] / 256][0][0];
        if ($row[$key] % 256)
          $dispValue .= " > " . $category[$row[$key] / 256][$row[$key] % 256][0];
        break;
    }
    return TableEdit::applyDisplay($key, $val, $row, $idxLine, $dispValue);
  }

//---------------------------------------------------------------------------------------------------
  public function applyFilter(&$key, &$val)
  {
    switch ($key) {
      /* case "category":
        if ($val[0]=='=' && ($cat=substr($val,1))%256==0)
        return "$key&0xff00=".($cat/256)*256;
        break; */

      default:
        return TableEdit::applyFilter($key, $val);
    }
  }

//---------------------------------------------------------------------------------------------------
  function buildHelpArray($key, $where)
  {
    switch ($key) {
      case "category":
        global $catSerial;
        return $catSerial;
    }
  }

//---------------------------------------------------------------------------------------------------
  public function buildKey($row)
  {
    global $request;
    return $request["tab"] . "_classify where ri=" . $row["ri"];
  }

}

$obj = new BillerClassify;
$obj->tableId = "members";
$obj->colDef = &$col;
$obj->addCol = "ri";
$obj->tableName = TABLE_NAME . "_classify";
$obj->bInsertNew = true;
$obj->bHeader = true;

$obj->build();
?>  

