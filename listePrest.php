<?php
include "coach.php";
if ($request["last"])
  include "check.php";
include "head.php";
echo "<title>Liste Prestations</title></head><body><h1>Liste prestations</h1>";
if ($now->format("Ymd") < "${yearSeason}0801") {
  echo "Les premiers encodages ne commencent pas avant août!";
  exit();
}
echo "<p>" . $coach->prenom;
?>, tu peux visualiser ou modifier les prestations des semaines pr&eacute;c&eacute;dentes. Tes modifications seront prisent en compte dans ton prochain paiement.
</p>
<p>Clique sur la semaine &agrave; visualiser ou modifier 
</p>
<?php
$fin = mktime(0, 0, 0, 7, 20, $yearSeason);
echo "<ul>";
for ($i = $last; $i > $fin; $i -= 7 * 24 * 3600) {
  $dt = new DateTime('@' . $i);
  $prest = jmysql_result(jmysql_query("select count(*) from prestations where num=" . $coach->num . " AND date='" . $dt->format('Y-m-d') . "'"), 0);
  echo "<li><a href=prest_Club.php$basePath&begin=$i" . ($right == ROOT ? '&user=root' : '') . "> Semaine du " . $dt->format("d/m/Y") . " au ";
  $j = $i + 6 * 24 * 3600;
  $dt = new DateTime('@' . $j);
  echo $dt->format("d/m/Y") . "</a>";
  if ($prest == 0)
    echo "&nbsp; (Formulaire non enregistr&eacute;)";
  echo "</li>";
}
echo "</ul><br>";
include "tools.php";
createButton(120, "Retour Menu", "menu.php$basePath");
?>          
<br><br></body>
</html>