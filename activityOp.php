<?php
include "common/common.php";
include "head.php";
switch ($request["operation"]) {
  case "mails":
    echo "<title>Activity mail</title>";
    $ri = $request["activity"];
    $tup = jmysql_fetch_row(jmysql_query("select invited,owner from activities where ri=$ri"));
    ?>
    <!-- Place inside the <head> of your HTML -->
    <script src="/tools/ckeditor/ckeditor.js"></script>
    <script type="text/javascript">
      tinymce.init({
        selector: "textarea"
      });
    </script>

    <!-- Place this in the body of the page content -->
    <style>
      .selection {width:300;height:300;border:1px solid black;font-size:small;font-family:calibri;overflow-y:auto;cursor:pointer}
    </style>
    <script>
      function toSelect(obj)
      {
        if (obj.style.color == 'rgb(255, 255, 255)')
        {
          obj.style.color = "";
          obj.style.background = "";
        } else
        {
          obj.style.color = "#FFFFFF";
          obj.style.background = "#0099FF";
        }
      }
      function mail(ri, sel)
      {
        switch (sel)
        {
          case 0:
            li = '<?php echo $tup[1] ?>,';
            break;

          default:
            li = '';
            $("#selected").children().each(function () {
              if (sel == 2 || this.style.color != '')
                li += this.id + ',';
            });
            break;
        }

        if (li == '')
        {
          alert("Aucun sélectionné");
          return;
        }
        document.getElementsByName("_val_mails")[0].value = li;
        document.getElementById("submit").click();
      }

    </script>
    <?php
    include "tools.php";
    echo "</head><body>";
    $inv = unserialize($tup[0]);
    echo "Corps du mail à envoyé <b>(pas de 'Bonjour', ni signature)</b>:<br><br><form target=_blank method=post action=" . $_SERVER["SCRIPT_NAME"] . "?operation=send&activity=$ri><input id=submit type=submit style=display:none>";
    echo "<textarea class=ckeditor name=_val_body>"; // style=width:100%;height:200>";
    ?>
    <p>Je t'invite à participer à l'activité '§subject§'.</p>
    <script>
      CKEDITOR.replace('_val_body');
    </script>
    <?php
    echo "</textarea><br>Destinataire:<br>";
    echo "<table><tr><td><div class=selection id=selected>";
    if ($inv) {
      foreach ($inv->nums as $k => $v)
        $ar .= "$k,";
      $ar = substr($ar, 0, -1);
      $r = jmysql_query("select num, concat(name,' ',firstName)as fName,emailp from members where num in ($ar) order by fName");
      while ($tup = jmysql_fetch_row($r))
        echo "<div id=$tup[0]>$tup[1] &lt;" . ($tup[2][0] == '-' ? substr($tup[2], 1) : $tup[2]) . "&gt;</div>";
    }
    echo "</td><td style=vertical-align:middle>";
    createAction(20, "Envoyer mail à moi-même (test)", "mail($ri,0)");
    echo "<br><br>";
    createAction(20, "Envoyer mail aux sélectionné(s)", "mail($ri,1)");
    echo "<br><br>";
    createAction(20, "Envoyer mail à tous", "mail($ri,2)");
    echo "</td></tr></table><input type=hidden name=_val_mails value=''></form>";
    exit();

  case "send":
    include "head.php";
    echo "<title>Envoi des mails</title></head>"; //<body onload=window.close()>";

    $l = substr($request["_val_mails"], 0, -1);
    $to = '§name§ §firstName§ <§emailF§>';
    $tup = jmysql_fetch_row(jmysql_query("select title,groupNm,owner from activities where ri=" . ($ri = $request["activity"])));
    $subject = ($t = $tup[1]) ? $t : $tup[0];
    //$subject="test";
    $headerLine = 'Bonjour §firstName§';
    $lastLine = "<p>Bonne journée.</p>";
    $bodyLine[0] = $request["_val_body"];
    if (!strpos($bodyLine[0], "§hRef§") && !strpos($bodyLine[0], "&sect;hRef&sect;"))
      $bodyLine[0] .= "<p>Clique <a href=§hRef§>ici</a> pour accéder à l'activité \"$subject\" et indiquer tes disponibilités.</p>";
    $hRef = "http://" . $_SERVER["HTTP_HOST"] . "/members/" . basename($_SERVER["SCRIPT_NAME"]) . "?id=§id§&activity=$ri";
    $query = "select id,name,firstName,ifnull(emailF,emailP) as emailF from members where num in ($l)";
    $owner = jmysql_fetch_row(jmysql_query("select name,firstName,ifnull(emailF,emailP) as emailF from members where num=$tup[2]"));
    $from = "$owner[1] $owner[0] <$owner[2]>";
    $signing = "$owner[1] $owner[0]";
    $request["sure"] = 'y';
    $bTest = true;
    $bResult = true;
    //$bDumpMail=true;
    $fileMail = "mail.test";
    include "common/genMailCommon.php";
    include "common/sendmails.php";
    exit();
}

function checkRights()
{
  
}

//------------------------------------------------------------------------------------------
include "memberData.php";
include "activitySel.php";
?>
<style>
  table.activities
  {
    font-family: Calibri;
    border-collapse:collapse;
  }
  table.activities td 
  {
    font-size:0.9em;
    padding:0px 2px 0px 2px;
    border:3px solid white;
  }

</style>
<?php
$group = ($t = $request["activity"]) ? jmysql_result(jmysql_query("select groupNm from activities where ri=$t"), 0) : $request["group"];
if ($group) {
  $r = jmysql_query("select ri,date from activities where date>='" . $now->format("Y-m-d") . "' and groupNm='$group' order by date");
  //echo "<h2 style=width:100%;margin:auto>$group</h2>";
  echo "<title>Activité '$group'</title>";
  echo "</head><body>";
  includeTabs();
  $tab = 0;
  while ($tup = jmysql_fetch_row($r)) {
    $dt = new DateTime($tup[1]);
    $dt = $dt->format("d/m/Y");
    echo "<li><a href=#view$tab>$dt</a></li>";
    $ri[$tab] = $tup[0];
    if (++$tab == 10)
      break;
  }
  echo "</ul>";
  echo "<div class=tabcontents>";
  for ($kk = 0; $kk < $tab; $kk++) {
    echo "<div id=view$kk>";
    buildActivity($ri[$kk]);
    echo "</div>";
  }
  echo "</div>";
  shareMsg($group);
} else {
  echo "<title>Activité</title>";
  echo "</head><body>";
  buildActivity($t = $request["activity"]);
  echo "<br>";
  shareMsg($t);
}
exit();

