<?php

include "common/common.php";
include "head.php";

$comity = array("Conseil d'administration" => array(
        array("Laurent ZINTZ", "Président", ""),
        array("Eric Tillieux", "Président d'honneur", ""),
        array("Cédric Juste", "Trésorier", ''),
        array("Francis Davin", "Secrétaire", ''),
        array("Nicolas Delpire", "Secrétaire adjoint", "(Gestion calendrier)"),
        array("Thierry Briot", "Administrateur", "(Manager)"),
        array("Philippe Aigret", "Administrateur", '(Relations avec la presse)'),
    ),
    "Direction technique"      => array(
        array("Stéphan Baczai", "Maxi basket", ""),
        array("Danielle Adam", "Mini basket", ''),
    ),
    "Membres du comité"        => array(
        array("Michel Vanderperre", "Membre", "(comité sportif)"),
        array("Danielle Adam", "Membre", "(Comité sportif)"),
        array("Sélim BECA", "Membre", "(Comité sportif)"),
        array("Guillaume PIRON", "Membre", "(Cellule sponsoring)"),
        array("Stéphanie PINEUX ", "Membre", "(Gestion des équipements)"),
        array("Marie DES TOUCHES", "Membre", "(Représentante section dames)"),
        array("Johanne Smets", "Membre", "(Maintenance site internet)"),
        array("Jérôme FURNEMONT", "Membre", "(Organisations et festivités)"),
        array("Vincent DUSSART", "Membre", "(Organisations et festivités)"),
        array("Laurent HANOT", "Membre", "(Organisations et festivités)"),
        array("Stéphanie LEGRAND", "Membre", "(Organisations et festivités)"),
        array("Olivier ROBIN", "Membre", "(Organisations et festivités)"),
        array("David ROBIN", "Membre", "(Organisations et festivités)"),
        
    ),
);

function printMember($member)
{
  echo "<span style=font-family:Arial;font-size:19px;font-weight:bold;color:#1a3c0a>$member[1]: </span>";
  if ($bHide = $member[0][0] == '-')
    $member[0] = substr($member[0], 1);
  $tup = jmysql_fetch_assoc(jmysql_query("select num, sex, ifnull(phP,phF) as phF, ifnull(emailP,emailF) as emailF, address, concat(cp, ' ', town) as city,visibility from members where concat(firstName,' ',name) like '$member[0]%'"));
  echo "<span  style=font-family:Arial;font-size:14px;font-weight:bold;color:#1a3c0a>" . $member[0] . " $member[2]</span></br>";
  $f = "/members/pictures/N_" . $tup["num"] . ".jpg";
  $file = file_exists(BASE_PATH . $f) ? $f : ($tup["sex"] == "M" ? "man.png" : "woman.png");
  echo "<table><tr><td><img src=$file width=93px></td>";
  if (!$bHide)
    echo "<td style=padding-left:20px;padding-top:30px;font-weight:bold;vertical-align:top>Tél<br>E-Mail<br>Adresse</td><td style=vertical-align:top;padding-top:30px;padding-left:10px>" . ($tup["visibility"][1] != 'N' ? $tup["phF"] : '') . "<br>" . ($tup["visibility"][0] != 'N' ? "<a href='mailto:" . $tup["emailF"] . "'>" . $tup["emailF"] . "</a>" : '') . "<br>" . $tup["address"] . "<br>" . $tup["city"] . "</td>";
  echo "</tr></table>";
}

echo "<meta content=text/html; charset=ISO-8859-1 http-equiv=content-type>";

foreach ($comity as $k => $v) {
  echo "<div style='border-radius:5px;background:#E9EDE8;font-family:Arial;padding:10px;margin:30px 0px 30px 0px'><h1>$k</h1><table width=100%>";
  $i = 0;
  foreach ($v as $v1) {
    echo (!($i % 2) ? "<tr>" : '') . "<td style=padding-bottom:20px>";
    printMember($v1);
    echo "</td>" . ($i % 2 ? "</tr>" : '');
    $i++;
  }
  echo "</table></div><hr>";
}

    