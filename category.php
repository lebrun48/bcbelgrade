<?php
$bSession=true;
include "admin.php";
include "common/tableTools.php";
//dump($request,"request");
//$seeRequest=1;
$colors = array
    ("FFFFFF", "D3D3D3", "808000", "BA3838", "7FFFD4", "8A2BE2", "DEB887", "5F9EA0", "7FFF00", "D2691E", "FF7F50", "6495ED", "008B8B", "008000", "FF8C00",
    "E9967A", "8FBC8F", "2F4F4F", "00CED1", "1E90FF", "FF00FF", "FFD700", "FF69B4", "F0E68C", "FF4500", "FFB6C1", "CD853F", "FFFF00", "4169E1", "00FF7F");

if ($bRoot && $admin["num"] == 173)
  unset($bRoot);

if ($action == "save" || $action == "insert") {
  //$seeRequest=3;
  $bUpd = $action == "save" && $request["_val_num"] && $request["_val_num"] != ($old = substr($request["key"], strpos($request["key"], "where num=") + 10));
  $t = unserialize(jmysql_result(jmysql_query("select teamSite from " . $request["key"]), 0));
  foreach ($request as $k => $v) {
    if (substr($k, 0, 8) == "_val_del") {
      unset($t[$k[8]]);
      unset($request[$k]);
    } else if (substr($k, 0, 8) == "_val_set") {
      if ($v)
        $t[$k[8]]->set = $v;
      else
        unset($t[$k[8]]->set);
      unset($request[$k]);
    } else if (substr($k, 0, 8) == "_val_div") {
      if ($v)
        $t[$k[8]]->div = $v;
      unset($request[$k]);
    } else if (substr($k, 0, 8) == "_val_col") {
      if ($v)
        $t[$k[8]]->color = $v;
      unset($request[$k]);
    }
  }
  $request["_val_teamSite"] = $t ? serialize($t) : '';
}
//dump($request,"request");
selectAction();
if ($bUpd) {
  //update code everywhere
  $sav = $seeRequest;
  //$seeRequest=1;
  echo "<h2>Updating category number...</h2>";
  if ($request["_val_num"] & CAT_PLAYER) {
    $new = $request["_val_num"] & 0xFFFF;
    $old1 = intval(($old & 0xFFFF) / 10);
    jmysql_query("update calendar set team=$new+(team mod 10) where team div 10=$old1");
    $r = jmysql_query("select team,team_view,num from coach");
    while ($tup = jmysql_fetch_row($r)) {
      unset($v);
      unset($v1);
      $t = strtok($tup[0], '-');
      $bUpd = false;
      while ($t) {
        if (intval($t / 10) == (int) $old1) {
          $bUpd = true;
          $v .= "-" . ($new + ($t % 10)) . '-';
        } else
          $v .= "-$t-";
        $t = strtok('-');
      }
      $t = strtok($tup[1], '-');
      while ($t) {
        if (intval($t / 10) == $old1)
          $v1 .= "-" . ($new + ($t % 10)) . '-';
        else
          $v1 .= "-$t-";
        $t = strtok('-');
      }
      if ($bUpd)
        jmysql_query("update coach set team='$v'" . ($v1 ? ",team_view='$v1'" : '') . " where num=$tup[2]");
    }
    jmysql_query("update com_presences set num=$new+(num mod 10) where num div 10=$old1");
    for ($i = 0; $i < 6; $i++) {
      $cat = "category" . ($i ? $i + 1 : '');
      jmysql_query("update members set $cat=" . CAT_PLAYER . "+$new+(($cat&0xFFFF) mod 10) where $cat div 10=" . intval($old / 10));
      jmysql_query("update members set $cat=" . CAT_COACH . "+$new+(($cat&0xFFFF) mod 10) where $cat div 10=" . intval((CAT_COACH + ($old & 0xFFFF)) / 10));
      jmysql_query("update members set $cat=" . CAT_ASSIST . "+$new+(($cat&0xFFFF) mod 10) where $cat div 10=" . intval((CAT_ASSIST + ($old & 0xFFFF)) / 10));
      jmysql_query("update members set n$cat=" . CAT_PLAYER . "+$new+((n$cat&0xFFFF) mod 10) where n$cat div 10=" . intval($old / 10));
      jmysql_query("update members set n$cat=" . CAT_COACH . "+$new+((n$cat&0xFFFF) mod 10) where n$cat div 10=" . intval((CAT_COACH + ($old & 0xFFFF)) / 10));
      jmysql_query("update members set n$cat=" . CAT_ASSIST . "+$new+((n$cat&0xFFFF) mod 10) where n$cat div 10=" . intval((CAT_ASSIST + ($old & 0xFFFF)) / 10));
    }
    jmysql_query("update pay_out set team=$new+(team mod 10) where team div 10=$old1");
    jmysql_query("update presences set team=$new+(team mod 10) where team div 10=$old1");
    jmysql_query("update training set team=$new+(team mod 10) where team div 10=$old1");
  } else {
    for ($i = 0; $i < 6; $i++) {
      $cat = "category" . ($i ? $i+1 : '');
      jmysql_query("update members set $cat=" . $request["_val_num"] . " where $cat=$old");
      jmysql_query("update members set n$cat=" . $request["_val_num"] . " where n$cat=$old");
    }
  }
  $seeRequest = $sav;
}

if ($action == "edit") {
  include "common/tableRowEdit.php";
  include "head.php";

  $col = array("order"    => array(0, "Ordre"),
      "num"      => array(0, "Numéro"),
      "short"    => array(20, "Abgrégé"),
      "name"     => array(50, "Nom"),
      "disabled" => array(0, "Inactive"),
      "ageMin"   => array(0, "Age Minimum"),
      "ageMax"   => array(0, "Age Maximum"),
      "code"     => array(0, "Code cotisation"),
      "teamSite" => array(0, "Série fédération"),
  );

  echo '<title>category';
  echo '</title></head><body>';

  function cmp($a, $b)
  {
    return $a->nb - $b->nb;
  }

  class CategoryEdit extends TableRowEdit
  {

    function applyValue(&$key, &$format, &$row)
    {
      switch ($key) {
        case "order":
          echo "<input type=text id=order value=" . ($row["num"] >> 16) . ">";
          return;

        case "num":
          echo "<input type=text name=_val_num value=" . ($row[$key] & 0xFFFF) . ">";
          return;

        case "code":
          echo "<select name=_val_code><option value=0" . ($row[$key] ? '' : " selected=selected") . ">&nbsp;</option>";
          $r = jmysql_query("select descr,num from code_paie");
          while ($tup = jmysql_fetch_row($r))
            echo "<option value=$tup[1]" . ($row[$key] == $tup[1] ? " selected=selected" : '') . ">$tup[0]</option>";
          echo "</select>";
          return;

        case "disabled":
          echo "<input name=_val_disabled type=hidden value=$row[$key]><input type=checkbox onclick=\"document.getElementsByName('_val_disabled')[0].value=this.checked ? 1:0\"" . ($row[$key] ? " checked=chekcked>" : '>');
          break;

        case "ageMin":
        case "ageMax":
          echo "<select name=_val_$key>";
          for ($i = 4; $i <= 20; $i++)
            echo "<option value=$i" . ($row[$key] == $i ? " selected=selected" : '') . ">$i</options>";
          echo "<option value='=null'" . (!$row[$key] ? " selected=selected" : '') . ">Indéfini</option></select>&nbsp;An";
          break;

        case "teamSite";
          //dump($row);
          if ($row[$key])
            $t = unserialize($row[$key]);
          //dump($t);
          $r = jmysql_query("select category&0xFFFF from members where type<>0 and category&" . CAT_PLAYER . " and (category&0xFFFF) div 10=" . intval(($row["num"] & 0xFFFF) / 10) . " group by category");
          while ($tup = jmysql_fetch_row($r)) {
            if (!isset($t[$tup[0] % 10]))
              $t[$tup[0] % 10]->set = '';
          }

          if ($t) {
            ksort($t);
            global $bRoot;
            if (!$bRoot) {
              echo "<input type=hidden value=" . $row["num"] . " name=_val_num>";
              foreach ($t as $k => $v) {
                echo $nl . "<b>" . ($k ? "Equipe <u><i>" . chr(ord('A') + $k - 1) : '') . "</u></i>:";
                echo "  série </b><input name=_val_set$k type=text size=1 maxlength=1" . ($v->set ? " value=$v->set>" : '>');
                $nl = "<BR>";
              }
              break;
            }

            //dump($t);
            global $colors;
            $r = jmysql_query("select category from members where type<>0 and category&" . CAT_PLAYER . "<>0 group by category");
            while ($tup = jmysql_fetch_row($r)) {
              $c = getCategory($tup[0]);
              if ($c = unserialize($c["teamSite"]))
                foreach ($c as $k1 => $v1)
                  if ($v1->color)
                    $already[$v1->color] = $tup[0] + $k;
            }
            foreach ($t as $k => $v) {
              echo $nl . "Rang <u><i>" . ($k ? chr(ord('A') + $k - 1) : "&lt;aucun&gt;") . "</u></i>";
              echo "  Série <input name=_val_set$k type=text size=1 maxlength=1" . ($v->set ? " value=$v->set>" : '>');
              echo "  Division <input name=_val_div$k type=text size=50 maxlength=50" . ($v->div ? " value='$v->div'>" : '>');
              echo "  <span id=color$k" . ($v->color ? " style=background-color:#" . $colors[$v->color] : '') . ">Couleur <input name=_val_col$k type=hidden" . ($v->color ? " value=$v->color" : '') . "></span>";
              echo "<table>";
              for ($i = 0; $i < 5; $i++) {
                echo "<tr style=height:.5cm>";
                for ($j = 0; $j < 6; $j++) {
                  $n = $i * 6 + $j;
                  echo "<td" . ($already[$n] ? " title='Déjà assigné à " . getCategory($already[$n], true) . "'" : '') . " onclick=setColor($n,'" . $colors[$n] . "',$k) style=text-align:center;width:.5cm;background-color:#" . $colors[$n] . ">" . ($already[$n] ? 'X' : '') . "</td>";
                }
                echo "</tr>";
              }
              echo "</table>";
              echo "  Supprimer <input type=checkbox name=_val_del$k value=1>";
              $nl = "<BR>";
            }
          }
          break;
      }
      TableRowEdit::applyValue($key, $format, $row);
    }

    function applySave()
    {
      global $bRoot;
      if ($bRoot) {
        ?>

        v=document.getElementsByName("_val_num")[0];
        if (v.value % 10) {
        alert("Le Numéro doit être un multiple de 10!!");
        v.focus();
        return;
        }
        v.value=v.value/1+(document.getElementById("order").value<<16);
        <?php
      }
    }

  }

  $ed = new CategoryEdit;
  if (!$bRoot) {
    unset($col["order"]);
    unset($col["num"]);
    //unset($col["disabled"]);
    unset($col["ageMin"]);
    unset($col["ageMax"]);
    //unset($col["code"]);
    $ed->bNew = $ed->bClearAll = false;
  }
  $ed->title = "Donnée de table category";
  $ed->colDef = &$col;
  $ed->editRow();
  ?>
  <script type="text/javaScript">
    function setColor(i,col,k)
    {
    document.getElementById('color'+k).style.backgroundColor='#'+col;
    document.getElementsByName('_val_col'+k)[0].value=i;
    }
  </script>
  <?php
  exit();
}

include "common/tableEdit.php";

$col = array("order"    => array(0, "Ordre", "width:10", null, '@'),
    "num"      => array(0, "Numéro", "width:20"),
    "short"    => array(0, "Abgrégé", "width:150"),
    "name"     => array(20, "Nom", "width:200"),
    "disabled" => array(-1, "Act."),
    "ageMin"   => array(0, "Age Minimum"),
    "ageMax"   => array(0, "Age Maximum"),
    "code"     => array(0, "Code cotisation"),
    "teamSite" => array(0, "Série Fédé."),
);

include "head.php";
//table itself
echo '<title>category';
echo '</title></head><body onload=genericLoadEvent()>';

class Category extends TableEdit
{

//---------------------------------------------------------------------------------------------------
  public function buildKey(&$row)
  {
    return "category where num=" . $row["num"];
  }

//---------------------------------------------------------------------------------------------------
  function removeImg($row)
  {
    global $bRoot;
    if (!$bRoot)
      return;
    return parent::removeImg($row);
  }

//---------------------------------------------------------------------------------------------------
  function applyDisplay(&$key, &$val, &$row, $idxLine)
  {
    $v = null;
    switch ($key) {
      case "order":
        $v = $row["num"] >> 16;
        break;

      case "num":
        $v = ($row["num"] & 0xFFFF) . " (" . $row["num"] . ')';
        break;

      case "code":
        if ($row[$key])
          $v = jmysql_result(jmysql_query("select descr from code_paie where num=$row[$key]"), 0, 0);
        else
          $v = "";
        break;

      case "disabled":
        $v = $row[$key] ? '' : 'X';
        break;

      case "teamSite":
        if ($row[$key]) {
          $t = unserialize($row[$key]);
          global $colors, $bRoot;
          foreach ($t as $k => $v1) {
            $v .= ($v1->color ? "<div style=width:100%;background-color:#" . $colors[$v1->color] . ">" : '') . ($k ? "Eq " . chr(ord('A') + $k - 1) : '') . ($v1->set ? ": Série '$v1->set'" : '') . ($bRoot ? "; Div '$v1->div'" : "&nbsp;") . ($v1->color ? "</div>" : '');
          }
        }
        break;

      default:
        if (substr($key, 0, 4) == "cost" && $row["code"])
          $v = "<span>" . number_format($row[$key], 2, ',', '.') . "€</span>";
    }
    return TableEdit::applyDisplay($key, $val, $row, $idxLine, $v);
  }

  //---------------------------------------------------------------------------------------------------
  function buildHelpArray($key, $where)
  {
    if ($key == "disabled")
      return array(0 => "active", 1 => "inactive");
  }

}

$category = new Category;
if (!$bRoot) {
  unset($col["order"]);
  unset($col["num"]);
  //unset($col["disabled"]);
  unset($col["ageMin"]);
  unset($col["ageMax"]);
  //unset($col["code"]);
  if (!$category->filter)
    $category->filter["disabled"] = "_c=" . serialize(array('0'));
  $category->defaultWhere = "num&" . CAT_PLAYER . " and ";
  $category->addCol = "num";
}
$category->colDef = &$col;
$category->tableName = "category";
$category->tableId = "members";
$category->bHeader = true;
if (!$category->order)
  $category->order = "num";
$category->build();
?>
