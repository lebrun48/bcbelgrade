<?php

include_once "common/common.php";
include_once "common/Engine.php";

class EncodePrest extends Engine
{

  public function initialize()
  {
    global $query;
    $this->res = jmysql_query($query);
    if (!$this->res) {
      include_once "tools.php";
      alert("$query : " . jmysql_error());
      exit();
    }
  }

  public function getEntry()
  {
    return $this->row = jmysql_fetch_assoc($this->res);
  }

  public function getValue($key)
  {
    if ($key == "bcc") {
      global $bcc;
      return $bcc ? "Bcc: $bcc\n" : '';
    }
    if (function_exists("specific") && ($v = specific($key, $this->row)) !== null)
      return $v;
    global $$key;
    if ($key != "bodyLine" && $key != "id" && isset($$key))
      return $$key;
    switch ($key) {
      case "bodyLine":
        global $bodyLine;
        return ($t = ${$key}[$this->row["name"]]) || ($t = ${$key}[$this->row["name"] . ' ' . $this->row["firstName"]]) || $t === "" || ($t = ${$key}[0]) ? $t : "";

      case "from":
        return "Jacques Meirlaen <newbc.belgrade@skynet.be>";

      case "signing":
        return "<pre style=color:gray;><i>Jacques Meirlaen<br>" .
                "Tr&eacute;sorier NewBC Alsavin-Belgrade<br>" .
                "GSM:0479/960842</i></pre>";

      case "attachedHeader":
        global $request;
        foreach ($request as $k => $v)
          if ($this->bAttached = substr($k, 0, 6) == "attach")
            return "Content-Type: multipart/mixed; boundary=\"" . ($this->uid = "----" . md5(uniqid(time()))) . "\"\n\n" .
                    "This is a multi-part message in MIME format\n" .
                    "--$this->uid\n";
        return '';

      case "attachedContent":
        global $request;
        if ($this->bAttached) {
          $this->fwrite($this->hOutput, "\n\n--$this->uid");
          foreach ($request as $k => $v)
            if (substr($k, 0, 6) == "attach")
              $this->fwrite($this->hOutput,
                            "\nContent-Type: application/octet-stream; name=\"$v\"\n" . // use different content types here
                      "Content-Transfer-Encoding: base64\n" .
                      "Content-Disposition: attachment; filename=\"$v\"\n\n" .
                      chunk_split(base64_encode(file_get_contents("mailAttach/$k"))) . "--$this->uid");
          $this->fwrite($this->hOutput, "--\n");
        }
        return '';
    }
    return $this->row[$key];
  }

}

$gen = new EncodePrest;
$gen->template = "template/" . ($templateMail ? $templateMail : "mail.mail");
$gen->run();
?>
