<?php

$bOnlyRoot = true;
include "common/common.php";
include "common/tableTools.php";

if ($action == "edit") {
  include "head.php";
  include "common/tableRowEdit.php";
  $col = array("product"   => array(50, "Produit"),
      "unitNbr"   => array(5, "Nombre d'unité dans 1 produit", INTEGER_ONLY),
      "unitPrice" => array(5, "Prix unité", "[money]")
  );

  class TableEdit extends TableRowEdit
  {

    function applyValue(&$att, &$format, &$row)
    {
      switch ($att) {
        default:
          TableRowEdit::applyValue(&$att, &$format, &$row);
          return;
      }
    }

  }

  $obj = new TableEdit();
  $obj->title = "Edition de produits";
  $obj->colDef = &$col;
  $obj->editRow();
  exit();
}
SelectAction();


$col = array("product"   => array(-1, "Produit", "width:150px"),
    "unitNbr"   => array(0, "Nbr unité", "width:70px", "[alr]"),
    "unitPrice" => array(0, "Prix unité", "width:70px", "[money]")
);

include "common/tableEdit.php";
include "head.php";
echo "<title>Produits</title></head><body onload=genericLoadEvent()>";

class Table extends TableEdit
{

//--------------------------------------------------------------------------------------------------
  public function buildKey(&$row)
  {
    return "products where ri=" . $row["ri"];
  }

//--------------------------------------------------------------------------------------------------
  public function applyDisplay(&$key, &$val, &$row, $idxLine)
  {
    switch ($key) {
      default:
        return TableEdit::applyDisplay($key, $val, $row, $idxLine);
    }
  }

}

$obj = new Table;
$obj->colDef = &$col;
$obj->tableId = "members";
$obj->tableName = "products";
$obj->addCol = "ri";
$obj->bHeader = true;
$obj->bInsertNew = true;

$obj->build();




