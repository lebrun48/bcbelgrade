<?php

$bSession = true;
include_once "admin.php";
if ($admin["rights"] != 0 && !($admin["rights"] & 0x0020))
  stop(__FILE__, __LINE__, "No Rights", null, true);
define('TABLE_NAME', "billerNBCB");
define('TITLE_BILLER_CLUB', "NBCBAB");

$enterprise->name = NAME_CLUB;
$enterprise->num = TVA_CLUB;
$enterprise->address = ADDRESS_CLUB;
$enterprise->responsible = TREASURER_NAME_CLUB . "<br>Trésorier";
$enterprise->logo = LOGO_CLUB;

$IBAN_Account = array(5 => "BE88143101737341", 6 => "BE47144890236980");

$account = array(
    5  => "Fintro CC",
    6  => "Fintro Ep",
    0  => "Belfius CC",
    1  => "Belfius Ep",
    7  => "ING CC",
    2  => "(Belfius I)",
    3  => "(Fintro CC)",
    4  => "(Fintro Ep)",
    9  => "Bons d'achats",
    10 => "Caisse",
    11 => "Caisse ESB",
    20 => "Patrimoine",
    21 => "Compte TVA",
    50 => "Caisse +",
    51 => "DB +",
    52 => "Banque+",
    55 => "Coffre+",
    54 => "C. Boutique",
);


define('FILE_NAME', 0);
define('HEADER', 1);
define('ACCOUNT', 2);
define('DATE', 3);
define('ORIG', 4);
define('DETAIL', 5);
define('AMOUNT_I', 6);
define('AMOUNT_O', 7);
define('NUM_ACCOUNT', 8);
//nméro de séquence	Date d'exécution	Date valeur	Montant	Devise du compte	CONTREPARTIE DE LA TRANSACTION	Détails	Numéro de compte
//        0               1                 2        3       4                          5                       6       7
$numAccount = array(//En-tête   ,account,date,tiers,detail,montant+,Montant-,N° compte
    array("file", "Num", -7, 1, 5, 6, 3, -1, -1),
    array("BE67 0682 2338 7387", "Compte;", 0, 1, 5, 14, 10, -1, 4),
    array("BE09 0882 0989 4857", "Compte;", 1, 1, 5, 14, 10, -1, -1),
    array("BE40 0835 9289 9163", "Compte;", 2, 1, 5, 14, 10, -1, -1),
    array("BE85142402363806", "\"ANNEE +", 3, 1, 6, 10, 3, -1, -1),
    array("BE30143853416411", "\"ANNEE +", 4, 1, 6, 10, 3, -1, -1),
    array("377-0379617-93", "Numéro", 52, 4, 8, 9, 6, -1, -1),
    array("ESB", "Tiers", 11, 1, 0, 4, 5, 2, -1)
);

$resultCatName = array(0 => "Dépenses", 1 => "Recettes", 5 => "Avoirs", 6 => "Dettes", 7 => "Droits", 8 => "Engagements");
$resultCategory = array(//Dépenses
    0   => "Marchandises",
    1   => "Rémunérations",
    2   => "Services et biens divers",
    3   => "Autres dépenses",
    //Recettes
    100 => "Cotisations",
    101 => "Dons et legs",
    102 => "Subsides",
    103 => "Autres recettes",
    //Avoirs
    599 => "Immeubles (terrains,...) ",
    500 => "<span style=font-weight:normal;margin-left:10px>- appartenant en pleine propriété</span>",
    501 => "<span style=font-weight:normal;margin-left:10px>- autres</span>",
    598 => "Machines ",
    502 => "<span style=font-weight:normal;margin-left:10px>- appartenant en pleine propriété</span>",
    503 => "<span style=font-weight:normal;margin-left:10px>- autres</span>",
    597 => "Mobilier et matériel roulant ",
    504 => "<span style=font-weight:normal;margin-left:10px>- appartenant en pleine propriété</span>",
    505 => "<span style=font-weight:normal;margin-left:10px>- autres</span>",
    506 => "Stocks",
    507 => "Créances",
    508 => "Placements de trésorerie",
    509 => "Liquidités",
    510 => "Autres avoirs",
    //Dettes
    600 => "Dettes financières",
    601 => "Dettes fournisseurs",
    602 => "Dettes à l'égard des membres",
    603 => "Dettes fiscales sal. et soc.",
    604 => "Autres dettes",
    //Droits
    700 => "Subsides promis",
    701 => "Dons promis",
    702 => "Autres droits",
    //Engagements
    800 => "Hypothèques et promesses d'hyp.",
    801 => "Garanties données",
    802 => "Autres engagements");


// 3th value indicates team cost:
// -1 global cost for all teams only for this category
// -2 global cost for all teams only for category and sub categories
// other>0 cost for team nb only for this category 
// other<0 cost for team nb for this category and sub categories 
$category = array(
    0  => array(0 => array("", -1, -1)),
    1  => array(0 => array("Entraineurs", 103, 2)),
    2  => array(0 => array("Cotisations", 100, 100)),
    3  => array(0 => array("Location Salles", 2, 2)),
    4  => array(0  => array("Manifestations", 103, 0),
        13 => array("Stage rentrée", 103, 2),
        14 => array("Stage Xmas", 103, 2),
        18 => array("Soirée Xmas", 103, 0),
        1  => array("Stage Pâques", 103, 2),
        27 => array("Repas photo club", 103, 0),
        6  => array("St Nicolas", 103, 0),
        23 => array("Raclette", 103, 0),
        28 => array("Repas boulettes", 103, 0),
        10 => array("Gaufres", 103, 0),
        5  => array("Lasagnes", 103, 0),
        15 => array("Marche ADEPS", 103, 0),
        26 => array("Playoffs", 103, 0),
        8  => array("Tournois", 103, 0),
        19 => array("Coupe prov.", 103, 0),
        20 => array("BAB/BIP/BEN", 103, 0),
        21 => array("Spectacle", 103, 0, 1150),
        22 => array("Fête basket", 103, 0),
        9  => array("Tombola", 103, 0),
        17 => array("3 contre 3", 103, 0),
        25 => array("All Star Games", 103, 0),
        11 => array("Vins Alsavins", 103, 0),
        12 => array("Boutique", 103, 0),
        16 => array("Autre", 103, 0),
        3  => array("Gibier", 103, 0),
        2  => array("BBQ", 103, 0),
        24 => array("JRJ", 103, 0),
        4  => array("Choucroute", 103, 0),
        7  => array("VTT", 103, 0)),
    5  => array(0 => array("Buvette", 103, 0),
        1 => array("Recettes", 103, 103),
        2 => array("Marchandises", 0, 0),
        3 => array("Déf. bénévoles", 2, 2),
        4 => array("Gérant", -1, -1),
        5 => array("Autre", 103, 0),
        6 => array("Part séniors", 103, 0)),
    6  => array(0 => array("Arbitres", 2, 2)),
    7  => array(0 => array("Matériel-Equipement", 0, 2, -1),
        1 => array("Réserve remplacement", -1, -1)),
    8  => array(0  => array("Adminsitratif", 2, 2, -2),
        1  => array("Secrétariat", 103, 2),
        2  => array("Comptabilité/trésorerie", 103, 2),
        3  => array("Poste", 2, 2),
        4  => array("Dir. Technique", 103, 2),
        6  => array("Portail et site Internet", 2, 2),
        7  => array("Matériel-Entretien", 2, 2),
        8  => array("Banque", 103, 2),
        9  => array("Assurances", 2, 2),
        10 => array("Autre", 103, 3),
        11 => array("Pertes/profits", 103, 3)),
    9  => array(0 => array("Sponsors", 103, 2, -151),
        1 => array("Rétrocession", -1, -1),
        2 => array("Panneaux", 103, 2)),
    10 => array(0 => array("AWBB", 102, 2),
        1 => array("Transferts", 103, 2),
        2 => array("Subsides", 102, 102),
        3 => array("Amendes", 2, 2),
        4 => array("Frais", 2, 2),
        5 => array("Licence col.", 2, 2),
        6 => array("Compensations", 103, 2)),
    11 => array(0 => array("Virements internes", -1, -1),
        1 => array("Attente", -1, -1),
        2 => array("Compte", -1, -1),
        3 => array("Bons Mestdagh", -1, -1)),
    12 => array(0 => array("Subsides", 102, 102),
        1 => array("Ville Namur", 102, 102, 100),
        2 => array("Ville Namur: jeunes", 102, 102),
        3 => array("Région", 102, 102)),
    13 => array(0  => array("Séniors", -1, -1, -100),
        10 => array("Défraiements coach", 103, 2),
        11 => array("Défraiements assistant", 103, 2),
        1  => array("Défraiements joueurs", 103, 2),
        12 => array("Arbitres", 103, 2),
        13 => array("Salles", 103, 2),
        16 => array("AWBB", 103, 2),
        3  => array("Festivités", 103, 0),
        4  => array("Stage rentrée", 103, 2),
        5  => array("Play-Off", 103, 0),
        6  => array("Déplacement car", 2, 2),
        7  => array("Kiné", 2, 2),
        9  => array("Entr. Phy.", 2, 2),
        8  => array("Restauration", 2, 2),
        2  => array("Entrées", 103, 103),
        14 => array("Buvette", 103, 103),
        15 => array("Subsides", 103, 103)),
    14 => array(0 => array("Dettes", -1, -1),
        3 => array("Caution", 602, 602)),
    20 => array(0 => array("Réserve", -1, -1),
        1 => array("Equipement", -1, -1)),
    15 => array(0 => array("Cadeaux St Nicolas", 103, 2)),
    17 => array(0 => array("Déf. Bénévoles", 103, 2)),
    16 => array(0 => array("Report", -1, -1)),
    19 => array(0 => array("TVA", -1, -1),
        1 => array("Acompte", -1, -1)),
);

$subCategory["D2 Nationale"] = array(
    1  => array(0 => array("Défraiements joueurs+T1+T2", 103, 2)),
    2  => array(0 => array("Entrées", 103, 103)),
    3  => array(0 => array("Festivités", 103, 0)),
    4  => array(0 => array("Stage de rentrée", 103, 2)),
    5  => array(0 => array("Play-Off", 103, 0)),
    6  => array(0 => array("Déplacement car", 2, 2)),
    7  => array(0 => array("Kiné", 2, 2)),
    8  => array(0 => array("Restauration fin de match", 2, 2)),
    9  => array(0 => array("Entraineur Physique.", 2, 2)),
    10 => array(0 => array("Défraiements coach", 103, 2)),
    11 => array(0 => array("Défraiements assistant", 103, 2)),
    12 => array(0 => array("Arbitres", 103, 2)),
    13 => array(0 => array("Location salles", 103, 2)),
    16 => array(0 => array("Fédération AWBB", 103, 2)),
    14 => array(0 => array("Buvette", 103, 103)),
    15 => array(0 => array("Subsides", 103, 103))
);
$subCategory["D3 Nationale"] = array(
    1  => array(0 => array("Défraiements joueurs", 103, 2)),
    2  => array(0 => array("Entrées", 103, 103)),
    3  => array(0 => array("Festivités", 103, 0)),
    4  => array(0 => array("Stage de rentrée", 103, 2)),
    5  => array(0 => array("Play-Off", 103, 0)),
    6  => array(0 => array("Déplacement car", 2, 2)),
    7  => array(0 => array("Kiné", 2, 2)),
    8  => array(0 => array("Restauration fin de match", 2, 2)),
    9  => array(0 => array("Entraineur Physique.", 2, 2)),
    10 => array(0 => array("Défraiements coach", 103, 2)),
    11 => array(0 => array("Défraiements assistant", 103, 2)),
    12 => array(0 => array("Arbitres", 103, 2)),
    13 => array(0 => array("Location salles", 103, 2)),
    16 => array(0 => array("Fédération AWBB", 103, 2)),
    14 => array(0 => array("Buvette", 103, 103)),
    15 => array(0 => array("Subsides", 103, 103))
);


$TVAType = array(1  => "6%",
    2  => "12%",
    3  => "21%",
    44 => "Prestations intracomm.",
    45 => "Co-contractant",
    46 => "Marchandises intracomm.",
    47 => "Exportations hors CEE",
    48 => "Crédits intracomm.",
    49 => "Autres Crédits sur ventes",
    81 => "Marchandises",
    82 => "Biens et services",
    83 => "Investissements",
    84 => "Crédits s/acq. intracomm.",
    85 => "Autres Crédits sur achats",
    86 => "Marchandises intracomm.",
    87 => "Hors CEE / Co-contractant",
    88 => "Prestations intracomm."
);
$TVA = array(1 => 6, 12, 21);

if (($bOfficial = $request["official"] == "true") || ($bOfficial = $admin["num"] == 507)) {
//if (($bOfficial=true) || ($bOfficial=$admin["num"]==507))
  foreach ($account as $k => $v) {
    if ($k >= 50)
      unset($account[$k]);
  }
  unset($category[9][1]);
  unset($category[5][4]);
  $resultCond = " and account<20";
  $t = $now->format("md");
} else
  $resultCond = " and (account<20 or account>=30)";

$disableDate = ($now->format('Y') - ($now->format("md") > "0401" ? 0 : 1)) . "-01-01";

foreach ($category as $k => $v)
  foreach ($v as $k1 => $v1) {
    $catSerial[$k * 256 + $k1] = ($k1 ? "--- " : "") . $v1[0];
  }
if ($subCategory)
  foreach ($subCategory as $k2 => $v2)
    foreach ($v2 as $k => $v)
      foreach ($v as $k1 => $v1)
        $catSerial[$k2][$k * 256 + $k1] = ($k1 ? "--- " : "") . $v1[0];


$currentYear = $yearSeason;
define(TRANSACTION_CATEGORY, 11 * 256);

