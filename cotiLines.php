<?php

$firstLine = array(
    "first"       => "<p>Votre club du " . NAME_CLUB . " vous remercie pour la confiance que vous lui accordez et vous souhaite une excellente saison $yearSeason-" . ($yearSeason + 1) . ".</p>" .
    "<p>Nous vous invitons � r�gler le montant de la cotisation suivant le tableau ci-dessous." .
    "�Test:!retPrest1� Nous vous rappelons que �Test:period0�le paiement doit �tre effectu� avant le <u>15 ao�t</u> afin de b�n�ficier de la r�duction.�Else�la r�duction pour paiement unique n'est plus applicable apr�s le 15 ao�t.�Tend��Tend�" .
    " Vous pouvez trouver les r�gles de paiement des cotisations au dos de cette feuille</p>",
    "ok"          => "<p>Vous �tes en r�gle de cotisation.</p>",
    "limit"       => "<p>Nous arrivons � la date limite pour le paiement�trancheNumNT�. Nous vous prions de vous mettre en r�gle en versant le montant indiqu� dans le tableau ci-dessous.</p>",
    "rappel"      => "<p>A ce jour et sauf erreur de ma part, le paiement de cotisation�trancheNum� n'a pas encore �t� per�u ou est partiellement incomplet. Nous vous prions de vous mettre en r�gle en versant le montant indiqu� dans le tableau ci-dessous.</p>�Test:noPay�<p>Nous vous rappelons que la r�duction pour paiement unique n'est plus applicable apr�s le 15 ao�t.</p>�Tend�",
    "new"         => "<p>Nous souhaitons la bienvenue � �allName� au " . NAME_CLUB .
    "�Test:young� et esp�rons qu'" .
    "�Test:!news��Test:M�il�Else�elle�Tend� trouvera beaucoup de plaisir dans la pratique de son sport" .
    "�Else��Test:M�ils�Else�elles�Tend� trouveront beaucoup de plaisir dans la pratique de leur sport�Tend�" .
    "�Tend�.</p>" .
    "<p>Nous vous invitons � r�gler le montant de la cotisation �Test:period0�pour le 15 ao�t �Tend�suivant le tableau ci-dessous.  Vous pouvez trouver les r�gles de paiement des cotisations au dos de cette feuille</p>",
    "wait"        => "<p>Votre situation est en attente, le tableau ci-dessous n'est qu'indicatif.</p>",
    "last"        => "<p>�Test:veryLast�<b><u>Dernier rappel.</u></b> �Tend�La date limite pour le paiement de la derni�re tranche de cotisation est d�pass�e. Selon nos comptes et sauf erreur de notre part, le paiement complet de la cotisation n'a pas encore �te effectu�. Nous vous prions de vous mettre en r�gle en versant le montant indiqu� dans le tableau ci-dessous.</p>" .
    "�Test:veryLast�<p>Nous vous rappelons qu'un retard de paiement de plus de 15 jours apr�s cette lettre entrainera des frais administratifs d'un montant de 10� (voir r�glement au dos de cette feuille)</p>�Tend�",
    "recommended" => "<p>Nous vous prions de vous mettre en r�gle de cotisation en versant le montant indiqu� dans le tableau ci-dessous.</p>" .
    "<p>Le paiement n'�tant toujours pas effectu� suite au dernier rappel, nous vous informons qu'un montant de 10,00� pour frais administratifs a �t� ajout� suivant le point 7 du r�glement (voir r�glement au dos de cette feuille)</p>",
);

$rule = '<h3>R�gles de paiement des cotisations.</h3><i><ol>' .
        "<li>Une caution de " . CAUTION_VALUE . "� par famille est � r�gler avant de recevoir l'�quipement (except� pour BABIES). Cette caution sera rembours�e <u>uniquement</u> en cas de d�part du club apr�s retour de l'�quipement complet et en ordre. La d�sinscription au club pour la saison <u>" . ($yearSeason + 1) . '-' . ($yearSeason + 2) . "</u> doit �tre signal�e <u>avant le 01 juin " . ($yearSeason + 1) . "</u> sinon une retenue sur la caution sera effectu�e en vue de r�cup�rer les frais accasionn�s par une r�inscription inutile � la f�d�ration. Le remboursement de la caution est effectu� � partir du 15 juin.</li>" .
        "<li>Chaque famille b�n�ficie d'un bon d'achat de 10�. Ce bon sera d�duit lors d'une participation � une activit� extra-sportive telle que vente lasagnes, gaufres, ainsi qu'aux repas organis�s � l'exception du repas \"St Nicolas\".</li>" .
        "<li>La cotisation est � r�gler en 1 fois ou en 3 fois (2 fois pour BABIES). Toutefois, le paiement echelonn� ne sera pas accord� si les �ch�ances n'ont pas �t� respect�es durant la saison pr�c�dente.</li>" .
        "<li>Le paiement unique b�n�ficie d'une r�duction de +/-6% et doit �tre r�gl� au plus tard pour le <u>15 ao�t $yearSeason</u>. Au-del� de cette date la r�duction n'est plus applicable.</li>" .
        "<li>Les dates pour le paiement �chelonn� sont fix�es aux 15 ao�t, 15 novembre et 15 janvier " . ($yearSeason + 1) . " pour la derni�re tranche. Pour les BABIES, les dates de paiement sont fix�es au 15 novembre et 15 janvier " . ($yearSeason + 1) . ".</li>" .
        "<li>Les familles b�n�ficient d'une r�duction de 25% pour le deuxi�me et 50% pour le troisi�me inscrit et suivants.</li>" .
        "<li>Afin de pouvoir participer aux matches, le joueur devra �tre en ordre de cotisation en ayant effectu� au moins un premier paiement.</li>" .
        "<li>En cas de non paiement dans les 15 jours qui suivent l'envoi du dernier rappel (indiqu� en gras soulign� sur le document), un montant de 10,00� sera ajout� pour frais administratifs.</li>" .
        "</ol></i>"
;


$verser = array(
    "ok"      => "<p>Vous �tes actuellement en r�gle de cotisation. �nextPay�</p>",
    "wait"    => "<p>Ne rien payer, compte en attente...</p>",
    "partiel" => "<p>�Test:singleFam�Veuillez verser le montant de <b>�Format:%.02f;singleFam� �</b> pour paiement�Test:period0� unique�Else� du solde de la cotisation�Tend� �tranche� au compte <b style=white-space:nowrap>N� BE67 0682 2338 7387</b> <b style=white-space:nowrap>(BIC: GKCC BE BB)</b> en indiquant en communication le <b>Nom et Pr�nom</b> du joueur.</p>" .
    "�Else�Vous �tes en r�gle de cotisation. Ne rien payer.</p>" .
    "�Tend�" .
    "�Test:retPrest�<p>Si aucun paiement n'a �t� per�u pour le 15 ao�t �Year�, une retenue sera effectu�e sur le d�fraiement du " .
    "�Test:coach�coach �coach�" .
    "�Else�joueur�Tend�" .
    ".</p>�Tend�"
);

$sig = '<p style="position:relative; left:350px">Bien � vous.<br><br>Pour le comit�, le tr�sorier.<br><img src=sig.gif width=168 height=95><br>Jacques Meirlaen.</p>';
?>