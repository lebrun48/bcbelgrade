<?php
include "common/tableRowEdit.php";
include "head.php";
//table itself
echo '<title>Editer cotisation';

$rowEdit = array("num"       => array(6, "Numéro : "),
    "name"      => array(0, "NOM Prénom : "),
    "status"    => array(0, "Status : "),
    "caution"   => array(0, "Caution : "),
    "comment"   => array(200, "Commentaire : "),
    "code_cat"  => array(0, "Catégorie : "),
    "previous"  => array(0, "Suivant(e) de : "),
    "firstLine" => array(500, "Première ligne : "),
    "verser"    => array(200, "Verser : ")
);
if ($yearSeason < 2011) {
  unset($rowEdit["firstLine"]);
  unset($rowEdit["verser"]);
}
if ($request["list"]) {
  unset($rowEdit["num"]);
  unset($rowEdit["name"]);
  unset($rowEdit["caution"]);
  unset($rowEdit["comment"]);
  unset($rowEdit["previous"]);
}

class CotisationsEd extends TableRowEdit
{

  //---------------------------------------------------------------------------------------------
  function buildSql($where)
  {
    return "select cotisations.*,concat(members.name,' ',members.firstName) as name,members.category,members.caution,members.status from members,$where and members.num=cotisations.num";
  }

  //---------------------------------------------------------------------------------------------
  function applyValue(&$key, &$val, &$row)
  {
    switch ($key) {
      case "num":
        $this->num = $row["num"];
        echo '<div name="num">' . $this->num . "</div>";
        break;

      case "name":
        $cat = getCategory($row["category"]);
        echo $row["name"] . " (" . $cat["name"] . ")" . nl;
        $this->bNoCaution = jmysql_result(jmysql_query("select noCaution from code_paie where num=" . (($t = $row["code_cat"]) ? $t : $cat["code"])), 0);
        break;

      case "status":
        echo '<select name="_val_status">';
        global $statusDef;
        if ($this->bList)
          echo "<option selected=selected></option>";
        foreach ($statusDef as $k => $v)
          echo "<option value=$k" . (!$this->bList && ($row[$key] & 0x0F) == $k ? ' selected="selected"' : '') . '>' . $v . '</option>';
        echo '</select>' . nl;
        echo "<input type=checkbox " . ($row[$key] & 0x80 ? "checked=checked" : null) . " id=keepRed>Garder la réduction";
        echo "<input type=checkbox " . ($row[$key] & 0x40 ? "checked=checked" : null) . " id=countStatus>Prendre en compte le status";
        echo "<input type=checkbox " . ($row[$key] & 0x20 ? "checked=checked" : null) . " id=recommended>Frais non paiement";
        echo "<input type=checkbox " . ($row[$key] & 0x10 ? "checked=checked" : null) . " id=singleOnly>Paiement unique seulement";
        break;

//Caution
//bit 0x00 Frère ou soeur ou non payée
//bit 0x01 Payée
//bit 0x02 Remboursée
//bit 0x04 Payée sur versement
//bit 0x08 
//bit 0x10 Pas de caution
//bit 0x20 Caution obligatoire
      case "caution":
        $n = $row["caution"] & 0xF0;
        echo '<input type="hidden" name="_val_caution">';
        echo '<input type="radio" name="cautionSt" value="16"' . ($n == 16 ? ' checked="checked"' : '') . '>Pas de cautions';
        echo '<input type="radio" name="cautionSt" value="0"' . ($n == 0 && $row["previous"] ? ' checked="checked"' : '') . '>Frère';
        echo '<input type="radio" name="cautionSt" value="0"' . ($n == 0 && !$row["previous"] ? ' checked="checked"' : '') . '>Premier';
        echo '<input type="radio" name="cautionSt" value="32"' . ($n == 32 ? ' checked="checked"' : '') . '>Caution obligatoire<br>';
        $n = $row["caution"] & 0x0F;
        echo '<input type="radio" name="cautionPaie" value="0"' . ($this->bNoCaution || $n == 0 && $row["previous"] ? ' checked="checked"' : '') . '>&lt; Sans valeur &gt;';
        echo '<input type="radio" name="cautionPaie" value="1"' . ($n == 1 ? ' checked="checked"' : '') . '>Caution déjà payée';
        echo '<input type="radio" name="cautionPaie" value="4"' . ($n == 4 ? ' checked="checked"' : '') . '>Caution payée sur versement';
        echo '<input type="radio" name="cautionPaie" value="0"' . (!$this->bNoCaution && $n == 0 && !$row["previous"] ? ' checked="checked"' : '') . '>Caution non payée';
        echo '<input type="radio" name="cautionPaie" value="2"' . ($n == 2 ? ' checked="checked"' : '') . '>Caution remboursée<br>';
        break;

      case "valid":
        echo "<input id=valid type=checkbox" . ($row["caution"] & 0x08 ? " checked=checked" : '') . " value=1>";
        break;

      case "code_cat":
        $r = jmysql_query("select num, descr from code_paie");
        echo '<select name="_val_code_cat">';
        echo '<option value="">&lt; Défaut &gt;</option>';
        while ($v = jmysql_fetch_row($r))
          echo '<option value="' . $v[0] . '"' . ($row[$key] == $v[0] ? ' selected="selected"' : '') . '>' . $v[1] . '</option>';
        echo '</select>' . nl;
        break;

      case "previous":
        echo '<select name="_val_' . $key . '">';
        $res = jmysql_query("select num,name,firstName from members where num in (select num from cotisations) order by name");
        echo '<option value=""' . ($row[$key] ? ' selected="selected"' : '') . '>&nbsp;</option>';
        while ($r = jmysql_fetch_row($res))
          echo '<option value="' . $r[0] . '"' . ($r[0] == $row[$key] ? ' selected="selected"' : '') . '>' . $r[1] . " " . $r[2] . '</option>';
        echo '</select>' . nl;
        break;


      default:
        TableRowEdit::applyValue($key, $val, $row);
    }
  }

  //---------------------------------------------------------------------------------------------
  function applySave()
  {
    if (!$this->bList) {
      echo "bNoCaution=" . intval($this->bNoCaution) . ";\n"; {
        ?>
        caution=document.getElementsByName('_val_caution')[0];
        optSt=document.getElementsByName('cautionSt');
        optPaie=document.getElementsByName('cautionPaie');
        statusObj=document.getElementsByName('_val_status')[0];
        status=statusObj.options[statusObj.selectedIndex].value;
        optStIdx=-1;
        optPaieIdx=-1;
        for(i=0; i<optSt.length; i++)
          if (optSt[i].checked)
          {
          optStIdx=i;
          break;
          }
          for(i=0; i<optPaie.length; i++)
            if (optPaie[i].checked)
            {
            optPaieIdx=i;
            break;
            }
            if (bNoCaution && optStIdx==3)
            {
            alert("Pas de caution pour cette catégorie");
            return;
            }
            if (optStIdx<=1  && optPaieIdx!=0)
            {
            alert("S'il n'y a pas de caution, le paiement doit être <Sans valeur>");
              return;
              }
              if (!bNoCaution && optStIdx>1 && optPaieIdx==0)
              {
              alert("La caution ne peut être <Sans valeur> pour un paiement de caution à faire");
                return;
                }
                if (optStIdx==1 && !document.getElementsByName('_val_previous')[0].value)
                {
                alert("Le premier de la famille n'est pas indiqué. Il ne peut donc pas être frère");
                return;
                }
                if (optStIdx==2 && document.getElementsByName('_val_previous')[0].value)
                {
                alert("Le premier est déjà indiqué, il ne peut donc pas s'agir du premier");
                return;
                }
                if (optStIdx==2 && document.getElementsByName('_val_previous')[0].value)
                {
                alert("Le premier est déjà indiqué, il ne peut donc pas s'agir du premier");
                return;
                }
                if (!bNoCaution && status>0 && status<9 && optPaieIdx==3)
                {
                alert("La caution doit être payée s'il y a déjà eu un paiement");
                return;
                }
                if (!bNoCaution && status==0 && optPaieIdx==2) 
                {
                alert("La caution ne peut être payée sur versement s'il n'y a pas eu de paiement");
                return;
                }

                bPayIn=<?php
        $r = jmysql_query("select sum(amount) from pay_in where num=$this->num and (descr=3 or descr=2)");
        echo ($r && jmysql_result($r, 0) < 0 ? "true;" : "false;") . nl
        ?>
                if (status==0 && bPayIn)
                {
                alert("Un paiement est présent dans pay_in, il ne peut donc pas avoir 'Rien payé'");
                return;
                }       
                caution.value=optSt[optStIdx].value/1+optPaie[optPaieIdx].value/1;
                optSt[optStIdx].checked=false;   
                optPaie[optPaieIdx].checked=false;
                if (document.getElementById('keepRed').checked)
                {
                status=status/1-128;
                statusObj.options[statusObj.selectedIndex].value=status;
                }
                if (document.getElementById('countStatus').checked)
                {
                status=status/1+64;
                statusObj.options[statusObj.selectedIndex].value=status;
                }
                if (document.getElementById('recommended').checked)
                {
                status=status/1+32;
                statusObj.options[statusObj.selectedIndex].value=status;
                }
                if (document.getElementById('singleOnly').checked)
                {
                status=status/1+16;
                statusObj.options[statusObj.selectedIndex].value=status;
                }
                <?php
              }
            }
          }

          public $bNoCaution;

        }

        echo '<title>Cotisations';
        echo '</title></head><body>';
        $ed = new CotisationsEd;
        $ed->title = "Etat des cotisations";
        $ed->colDef = &$rowEdit;
        $ed->editRow();
        echo "<br><br>";

        createAction(140, "Insérer/modifier pay_in", "openWindow()", 240);
        ?>
        <script type="text/javaScript">
          var win=null;
          function openWindow()
          {
          loc="payIn.php<?php echo $basePath1 ?>action=check&n="+document.getElementsByName('num')[0].innerHTML;
          if (win && !win.closed)
          win.location.replace(loc);
          else
          win=window.open(loc, "_blank",  "scrollbars=yes,left=800,width=700,height=800,titlebar=no,menubar=no,status=no,location=no");
          win.focus();
          return false;
          }

        </script>
