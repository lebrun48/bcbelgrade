<?php

$bIdMandatory = true;
include "common/common.php";
include "DetailPay.php";
include "cotiLines.php";
include "templateClub.php";
$bill = jmysql_fetch_assoc(jmysql_query("select name,firstName,address,concat(cp,' ',town) as city, billAddress, bill.*, client.* from bill join client on client.num=bill.client join members on members.num=bill.client where bill.ri=" . $request["bill"]));
//dump($bill);

$pdf = new HeaderClub();
$pdf->AddPage();
$pdf->Template();

//Address
$pdf->SetTitle("Facture " . $bill["billNum"]);
$pdf->SetSubject("Facture " . $bill["billNum"]);
$pdf->SetFont('Arial', '', 12);
$pdf->SetXY(120, 35);
$pdf->SetLeftMargin(120);
if ($t = utf8_decode($bill["society"]))
  $pdf->Write(5, "$t\n");
if ($t = utf8_decode($bill["firstName"])) {
  $pdf->Write(5, $bill["sex"] == 'F' ? "Mme. " : "Mr. ");
  $pdf->Write(5, "$t " . utf8_decode($bill["name"]) . nl);
}
if ($bill["billAddress"])
  $pdf->Write(5, utf8_decode($bill["billAddress"]));
else {
  $pdf->Write(5, utf8_decode($bill["address"]) . "\n");
  $pdf->Write(5, utf8_decode($bill["city"]));
}

$amount = $bill["amount"];
//date
$pdf->SetLeftMargin(50);
$dt = new DateTime($bill["date"]);
$pdf->SetXY(140, 90);
$pdf->Cell(50, 5, CITY_CLUB . ", le " . utf8_decode(formatDate($dt, "D M Y")), '', 1);
$pdf->SetY(100);
$pdf->SetFont('', 'B');
$pdf->Write(7, ($amount < 0 ? "NOTE DE CREDIT" : "FACTURE") . " N� " . intval($bill["billNum"] / 100) . '/' . sprintf('%02d', $bill["billNum"] % 100) . nl);
if ($t = $bill["numTVA"])
  $pdf->Write(7, "TVA client: BE$t\n");

//description
$pdf->SetY(130);
$pdf->SetFillColor(0xCA, 0xCE, 0xEE);
$pdf->SetDrawColor(0xBF);
$pdf->SetFont('', 'B');
$pdf->Cell(90, 7, "DESIGNATION", "LTRB", 0, 'C', true);
$pdf->Cell(50, 7, "MONTANT ", "TRB", 1, 'C', true);

$pdf->SetFont('', '');
$pdf->CellHTML(90, 15, "   " . str_replace("\n", "<br>", utf8_decode($bill["descr"])), "LR", 0);
$pdf->Cell(50, 15, number_format($amount, 2, ',', '.') . " �", "R", 1, 'R');
$pdf->Cell(90, 50, '', "LRB", 0);
$pdf->Cell(50, 50, '', "LRB", 1);
if ($t = $bill["tva"]) {
  $pdf->Cell(90, 10, "TVA $t%   ", "R", 0, 'R');
  $pdf->Cell(50, 10, number_format($amount * $t / 100, 2, ',', '.') . " �", "RB", 1, 'R');
}
$pdf->SetFont('', 'B');
$pdf->Cell(90, 10, "TOTAL A PAYER  ", "R", 0, 'R');
$pdf->Cell(50, 10, number_format($amount * (1 + $t / 100), 2, ',', '.') . " �", "RB", 1, 'R');

//fin
$pdf->SetFont('', '');
$pdf->SetY(240);
if ($amount > 0) {
  $pdf->Write(10, "Paiement: R�ception facture\n");
  $pdf->WriteHTML("<b>IBAN:</b> BE67 0682 2338 7387<br><br>");
  $pdf->WriteHTML(($t = utf8_decode($bill["comment"])) ? $t : "Nous vous remercions pour votre soutien.");
} else
  $pdf->WriteHTML("En votre faveur : <b>" . number_format(-$amount * (1 + $t / 100), 2, ',', '.') . " �</b>\n");

$pdf->Output($bill["billNum"] . ($amount < 0 ? "Note de cr�dit " : " Facture ") . str_replace('.', '', ($t = utf8_decode($bill["society"])) ? " $t" : utf8_decode($bill["name"]) . ' ' . utf8_decode($bill["firstName"][0])) . ".pdf", 'I');
