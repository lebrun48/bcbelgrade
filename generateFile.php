<?php
include "common.php";
include "tools.php";
include "Engine.php";
include "head.php";
?>
<title>Générate files     
</title>  
</head>
<body onload="onloadEvent()">
  <script type="text/javaScript">
    function onloadEvent()
    {
    <?php
    if (!($table = $_GET["table"])) {
      ?>
      table=prompt("Entre le nom de la table de données", "vgen");
      if (!table)
      {
      alert("Nom de fichier invalide");
      return;
      }
      <?php
    } else
      echo "table=$table;" . nl;

    if (!($template = $_GET["template"])) {
      ?>
      template=prompt("Entre le nom du fichier template", "template/");
      if (template=="template/" || !template)
      {
      alert("Nom de fichier invalide");
      return;
      }
      <?php
    }

    if (!$table || !$template)
      echo 'location.assign(location.pathname+"?table="+table+"&template="+template);' . nl;
    ?>
    }
  </script>
  <?php
  if (!$table || !$template)
    exit();

  if (!file_exists($template)) {
    alert("Fichier $template n'existe pas");
    exit();
  }

  class GenerateFile extends Engine
  {

    public function initialize()
    {
      global $table;
      $this->res = jmysql_query("select * from $table");
      if (!$this->res) {
        alert("Erreur table : " . jmysql_error());
        exit();
      }
    }

    public function getEntry()
    {
      return $this->row = jmysql_fetch_assoc($this->res);
    }

    public function getValue($key)
    {
      return $this->row[$key];
    }

  }

  $gen = new GenerateFile;
  $gen->template = $template;
  $gen->run();
  echo "Fichier(s) généré(s)"
  ?>


