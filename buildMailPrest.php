<?php

include "common/common.php";
include "payOutData.php";
//dump($request);
$month = $request["month"];

$requestedParams["month"] = $month;
$r = jmysql_query("SELECT count(date),date from prestations where status=$month group by date");
while ($tup = jmysql_fetch_row($r)) {
  $dateEnd = new DateTime($tup[1]);
  if ($tup[0] < 10 || isset($dateBeg))
    continue;
  $dateBeg = new DateTime($tup[1]);
}
$dateEnd = creDate($dateEnd, 6);
$dateEnd = $dateEnd->format("d/m/Y");
$dateBeg = $dateBeg->format("d/m/Y");

//------------------------------------------------------------------------------------------------------
function checkRights()
{
  global $id, $bRoot;
  if (!$bRoot)
    stop(__FILE__, __LINE__, "invalid id", true);
}

//------------------------------------------------------------------------------------------------------
function specific($key, $row)
{
  switch ($key) {
    case "season":
      global $request;
      return ($t = $request["season"]) ? "&season=$t" : '';

    case "emailF":
      $m = isset($row["emailP"]) ? $row["emailP"] : $row["emailF"];
      if ($m[0] == '-')
        $m = substr($m, 1);
      return $m;


    case "type":
      if (jmysql_result(jmysql_query("select count(*) from coach where num=" . $row["num"]), 0))
        return "prestations";
      if ($row["category"] & CAT_PLAYER)
        return "joueur";
      return jmysql_result(jmysql_query("select descr from pay_out where num=" . $row["num"] . " and month=0 and type&0x3F<>" . COACH_DESC . " and descr is not null"), 0);
  }

  return null;
}

//------------------------------------------------------------------------------------------------------
//Ajax request
switch ($action = $request["action"]) {
  case "getMail":
    //get single mail name
    $r = jmysql_query("select concat(name,' ',firstName),concat(emailF,',',emailM,',',emailP) from members where num=" . $request["num"]);
    //Get single mail data
    $query = "select id,name,firstName from members where num=" . $request["num"];
    break;

  case "sendMail":
    //dump($request);
    $query = "select category,num,name,firstName,id,emailP,emailF from members where type and num in (" . $request["nums"] . ") order by name";
    $to = "§name§ §firstName§ <§emailF§>";
    //$bTest=true;
    //$bSendResult=true;
    //$bDumpMail=true;
    $bResult = true;
    break;

  case "viewMail":
    checkRights();
    $r = jmysql_query("select concat(name,' ',firstName),concat(emailF,',',emailM,',',emailP) from members where num=" . ($num = ($t = $request["num"]) ? $t : strtok($request["nums"], ",")));
    //Get single mail data
    $query = "select id,name,firstName from members where num=$num";
    break;
}
//------------------------------------------------------------------------------------------------------
//other Ajax request (do not change)
if ($action)
  include "common/buildMailAjax.php";

//mailing variables must be here
?>
<script>

  function getVariables()
  {
    return [];
  }
</script>
<?php

checkRights();
//------------------------------------------------------------------------------------------------------
//main
//$from must contain the from 
//dump($request);
$from = "Jacques Meirlaen &lt;newbc.belgrade@skynet.be&gt;";
//$nums must contain the list
$nums = $request["list"];
//$res must contain the dest name(s)
$res = jmysql_query("select concat(name,' ',firstName) from members where num in ($nums)");
//*otherFunction is added after mail
$otherFunction = "<input type=checkbox name=noSend value=true checked>Tester (pas d'envois)";

//------------------------------------------------------------------------------------------------------
//always at the end  (do not change)
include "common/buildMail.php";






