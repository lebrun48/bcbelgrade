<?php

include_once "common/common.php";
if ($logAs = $request["logAs"]) {
  header("Location:login.php?logAs=$logAs&loginScript=" . basename($_SERVER["SCRIPT_NAME"], ".php"));
  exit();
}

//$seeRequest=1;
if (empty($id)) {
  if (!$bAcceptNoId)
    stop(__FILE__, __LINE__, "Invalid URL!", null, true);
} else {
  $res = jmysql_query("select num from members where id='$id'");
  if (!$res || !jmysql_num_rows($res))
    stop(__FILE__, __LINE__, "Invalid URL!", null, true);
  $num = jmysql_result($res, 0);
  $res = jmysql_query("select admin.*, members.name, members.firstName from admin,members where admin.num=$num and members.num=$num");
  if ($res && jmysql_num_rows($res) == 1)
    $admin = jmysql_fetch_assoc($res);
  $r = jmysql_query("select team, team_view from coach where num=$num");
  if ($r && jmysql_num_rows($r) == 1) {
    if (!$admin) {
      jmysql_query("insert into admin set num=$num, rights=2,attributes='picture,category,AWBB_id,year,name,firstName,emailF,emailM,emailP,address,cp,town,phP,phF,phM,birth,dateIn', conditions='type:1:', visible='picture,category,AWBB_id,year,name,firstName,emailF,emailM,emailP,address,cp,town,phF,phM,birth,dateIn', sort='name'");
      $admin = jmysql_fetch_assoc(jmysql_query("select * from admin where num=$num"));
    }
    if (!$admin["categories"] && !($admin["rights"] & 0x04)) {
      $tok = strtok(jmysql_result($r, 0, 0) . jmysql_result($r, 0, 1), "-");
      while ($tok) {
        $admin["categories"] .= "," . (CAT_PLAYER | $tok);
        $tok = strtok("-");
      }
      $admin["categories"] = "category in (".CAT_REMP_COACH . $admin["categories"] . ") or category&".CAT_FIRST." or category&" . CAT_COACH;
    }
  } else if (!$admin)
    stop(__FILE__, __LINE__, "Not administrator!", null, true);

  if ($admin["categories"] == "ALL")
    $admin["categories"] = null;
}

function getRealCoach($team)
{
  $stok = strtok($team, '-');
  while ($stok) {
    $r = jmysql_query("select id,firstName,name,num from members where type<>0 and (category=" . CAT_COACH . "|$stok or category2=" . CAT_COACH . "|$stok or category3=" . CAT_COACH . "|$stok)");
    if ($r)
      $ret[] = jmysql_fetch_assoc($r);
    $stok = strtok('-');
  }
  return $ret;
}

?>
