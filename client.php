<?php

$bRootOnly = true;
include "common/common.php";
include "common/tableTools.php";

switch ($action) {
  case "edit":
    include "common/tableRowEdit.php";
    include "head.php";
    //$seeRequest=1;

    $col = array("society"     => array(50, "Société"),
        "numTVA"      => array(12, "N° TVA"),
        "firstName"   => array(20, "Prénom"),
        "name"        => array(30, "Nom"),
        "emailF"      => array(50, "Email"),
        "address"     => array(100, "Adresse", '[Mand]'),
        "cp"          => array(10, "Code Postal", '[Mand]'),
        "town"        => array(50, "Ville", '[Mand]'),
        "billAddress" => array(200, "Adresse facturation"),
        "phF"         => array(20, "N° tél."),
        "phM"         => array(20, "N° GSM"),
    );

    echo '<title>Client edit';
    echo '</title></head><body>';

    class Row extends TableRowEdit
    {

      //-----------------------------------------------------------------------------------------------
      function buildSql($where)
      {
        return "select members.*, client.* from members left join client on client.num=members.num " . substr($where, strpos($where, "where"));
      }

    }

    $ed = new Row;
    $ed->title = "Donnée de table category";
    $ed->colDef = &$col;
    $ed->editRow();
    exit();

  case "save":
  case "insert":
    $num = $action[0] == 's' ? substr($request["key"], strpos($request["key"], '=') + 1) : jmysql_result(jmysql_query("select max(num)+1 from members"), 0);

    //$seeRequest=1;
    $sql = jmysql_result(jmysql_query("select count(*) from client where num=$num"), 0) ? "update client set " : "insert into client set num=$num, ";
    $attr["_val_society"] = $request["_val_society"];
    $attr["_val_numTVA"] = $request["_val_numTVA"];
    $attr["_val_billAddress"] = $request["_val_billAddress"];
    buildSql($sql, null, $attr);
    jmysql_query($sql . ($sql[0] == 'u' ? " where num=$num" : ''));
    if ($action[0] == 'i') {
      $request["_val_num"] = $num;
      $request["_val_category"] = $request["val_nCategory"] = CAT_SPONSOR;
      $request["_val_id"] = getID(($t = $request["_val_name"]) ? $t : $request["_val_society"], $request["_val_firstName"], 8);
      $request["_val_sex"] = 'M';
      $request["_val_dateIn"] = $now->format("Y-m-d");
    }
    unset($request["_val_society"]);
    unset($request["_val_numTVA"]);
    unset($request["_val_billAddress"]);
    break;
}
selectAction();

include "common/tableEdit.php";

$col = array("society" => array(1, "Société",),
    "name"    => array(1, "Nom", '', '', "concat(firstName, ' ', name)"),
    "address" => array(1, "Adresse"),
    "city"    => array(1, "Ville", '', '', "concat(cp,' ',town)"),
    "numTVA"  => array(1, "TVA"),
);

include "head.php";
echo '<title>Clients';
echo '</title></head><body onload=genericLoadEvent()>';

//$seeRequest=true;

class Table extends TableEdit
{

//---------------------------------------------------------------------------------------------------
  function buildFrom()
  {
    return "members left join client on members.num=client.num";
  }

//---------------------------------------------------------------------------------------------------
  public function removeImg($row)
  {
    
  }

//---------------------------------------------------------------------------------------------------
  public function formatTD($key, $row)
  {
    if ($key == "society") {
      global $basePath;
      return "class=tabLink onclick=window.open('bill.php$basePath&_sel_client=" . $row["num"] . "','_blank')";
    }
  }

//---------------------------------------------------------------------------------------------------
  function applyDisplay($key, $val, $row, $idxLine)
  {
    global $basePath;
    $disp = $row["type"] ? null : "<span style=color:gray>";
    if ($key == "society" && !$disp)
      return parent::applyDisplay($key, $val, $row, $idxLine, "<span style=color:green><u>$row[$key]</u></span>");
    return parent::applyDisplay($key, $val, $row, $idxLine, $disp ? "$disp$row[$key]</span>" : null);
  }

}

$obj = new Table;
if (!$obj->order)
  $obj->order = "type desc,society";
$obj->colDef = &$col;
$obj->tableName = "members,client";
$obj->defaultWhere .= "(members.category=".CAT_SPONSOR;
for ($i = 2; $i <= 6; $i++)
  $obj->defaultWhere .= " or members.category$i=".CAT_SPONSOR;
$obj->defaultWhere .= ") and";
$obj->tableId = "members";
$obj->bHeader = true;
$obj->bInsertNew = true;
$obj->addCol = "members.num,type";
$obj->primKey = "num";
$obj->build();



