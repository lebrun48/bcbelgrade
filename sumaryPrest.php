<?php include "head.php" ?>
<title>Résumé Prestations          
</title>    
</head>     
<body><h1>Résumé des prestations</h1>

  <?php

  $back = array(7  => "#8A5C00",
      6  => "#996600",
      5  => "#A37519",
      4  => "#AD8533",
      3  => "#B8944D",
      2  => "#C2A366",
      1  => "#CCB280",
      12 => "#D6C299",
      11 => "#E0D1B2",
      10 => "#EBE0CC",
      9  => "#F5F0E6"
  );
  include "common/common.php";
  $fin = mktime(0, 0, 0, 7, 25, $yearSeason);
  $last = time();
  $dt = new DateTime('@' . $last);
  $last -= (intval($dt->format('w')) + 6) * 24 * 3600;
  $res = jmysql_query("select coach.num,team from coach left join members on members.num=coach.num where (category&" . CAT_COACH . " or category2&" . CAT_COACH . " or category3&" . CAT_COACH . ") order by name");
  echo '<table class=main id=members style=table-layout:fixed;width:100%><tr><th>Date</th>';
  $i = 0;
  $n = (int) (100 / jmysql_num_rows($res));
  while ($row = jmysql_fetch_assoc($res)) {
    $nm = jmysql_fetch_assoc(jmysql_query("select name, firstName,id from members where num=" . $row["num"]));
    $num[$i]->num = $row["num"];
    $num[$i]->id = $nm["id"];
    $num[$i]->team = explode("-", $row["team"]);
    $num[$i]->name = $nm["name"] . ' ' . $nm["firstName"][0] . '.';
    foreach ($num[$i]->team as $k => $v)
      if (empty($v) || intval($val / 10) == 95)
        unset($num[$i]->team[$k]);
    $i++;
    echo "<th style=max-width:$n%;overflow:hidden>" . $nm["name"] . ' ' . $nm["firstName"][0] . ".</th>";
  }
  echo "</tr>";
  $day = array("lu", "ma", "me", "je", "ve", "sa", "di");
  $parity = 0;
  for ($d = $last; $d > $fin; $d -= 7 * 24 * 3600) {
    $dt = new DateTime('@' . $d);
    $date = $dt->format('Y-m-d');
    foreach ($num as $i => $val) {
      $res = jmysql_query("select prest, remp0, remp1, remp2, remp3, remp4, remp5, remp6,status from prestations where date='$date' and num=" . $val->num);
      if ($bPresent[$i] = jmysql_num_rows($res)) {
        $row[$i] = jmysql_fetch_assoc($res);
        $prest[$i] = $row[$i]["prest"];
      } else {
        unset($row[$i]);
        $prest[$i] = "       ";
      }
    }

    $dd = $d + 6 * 24 * 3600;
    if ($season = $request["season"])
      $season = "&season=$season";
    for ($j = 6; $j >= 0; $j--) {
      $dt = new DateTime('@' . $dd);
      $dd -= 24 * 3600;
      echo "<tr class=\"parity$parity\"><td>" . $day[$dt->format('N') - 1] . '.' . $dt->format("d/m") . "</td>";
      $parity = 1 - $parity;
      foreach ($bPresent as $i => $b) {
        echo "<td" . (($t = $row[$i]["status"]) > 0 ? " style=background:$back[$t]" : '') . " onclick=";
        echo "window.open('prest_Club.php?id=" . $num[$i]->id . "&begin=$d&user=root$season','_blank');";
        foreach ($num[$i]->team as $v)
          echo "window.open('presencesClub.php?id=" . $num[$i]->id . "&begin=$d&team=" . $v . "&user=root$season','_blank');";
        echo " class=tabLink title=\"" . $num[$i]->name . "\">" . $prest[$i][$j];
        if ($row[$i]["remp$j"])
          echo '(' . $row[$i]["remp$j"] . ")";
        echo "</td>" . nl;
      }
      echo "</tr>" . nl;
    }
  }
  echo "</table>";
  ?>
