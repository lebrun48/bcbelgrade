<?php

class MemberData
{

  public $name;
  public $firstName;
  public $category;
  public $categoryName;
  public $num;
  public $id;

  function __construct()
  {
    global $id;
    if (!($this->id = $id))
      stop(__FILE__, __LINE__, "ID parameter not present", null, true);

    $res = jmysql_query("select name,firstname,category,num from members where id='" . $this->id . "' and (type or id='DBT_jac.1Vtqk74D' or id='COM_jac.6QmdlR4D')");
    if (!$res || jmysql_num_rows($res) == 0)
      stop(__FILE__, __LINE__, "Invalid ID: " . jmysql_error(), null, true);

    $row = jmysql_fetch_assoc($res);
    $this->name = $row["name"];
    $this->firstName = $row["firstname"];
    $this->category = $row["category"];
    $this->num = $row["num"];
  }

}

$member = new MemberData();
?>