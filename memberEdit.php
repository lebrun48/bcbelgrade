<?php
//$seeRequest=1;
include "head.php";
$col = array("picture"    => array(0, "Photo: "),
    "category"   => array(0, "Catégorie principale <span style=\"color:red\">(*)</span>:"),
    "category2"  => array(0, "Catégorie secondaire 1 :"),
    "category3"  => array(0, "Catégorie secondaire 2 :"),
    "ncategory"  => array(0, "Nouvelle catégorie principale :"),
    "ncategory2" => array(0, "Nouvelle catégorie secondaire 1 :"),
    "ncategory3" => array(0, "Nouvelle catégorie secondaire 2 :"),
    "category4"  => array(0, "Autres catégories :"),
    "remark"     => array(100, "Remarque :"),
    "caution"    => array(0, "Caution :"),
    "AWBB_id"    => array(15, "N° AWBB :"),
    "licence"    => array(15, "Licence coach :"),
    "name"       => array(40, "Nom <span style=\"color:red\">(*)</span> :"),
    "firstName"  => array(20, "Prénom <span style=\"color:red\">(*)</span> :"),
    "sex"        => array(20, "Sexe <span style=\"color:red\">(*)</span> :"),
    "phP"        => array(15, "GSM :"),
    "emailP"     => array(40, "EMail :"),
    "emailF"     => array(40, "EMail parents 1 :"),
    "emailM"     => array(40, "EMail parents 2 :"),
    "phF"        => array(15, "GSM parent 1 :"),
    "phM"        => array(15, "GSM parent 2 :"),
    "phFix"      => array(15, "Tél Fixe :"),
    "address"    => array(60, "Adresse :"),
    "cp"         => array(4, "Code Postale :"),
    "town"       => array(20, "Localité :"),
    "birth"      => array(0, "Date de naissance :"),
    "birthLoc"   => array(20, "Lieu de naisssance :"),
    "natNum"     => array(20, "Numéro National :"),
    "color"      => array(0, "Couleur :"),
    "dateIn"     => array(0, "Date entrée :", "[Date]"),
    "extern"     => array(0, "Affiliation AWBB :"),
    "toSelect"   => array(0, "Ajouter dans sélection :")
);
?>
<title>Edition membre          
</title>    
</head>   
<?php
include "common/tableRowEdit.php";
$category = jmysql_query("select num,name from category where num<>0 and short is not null" . ($type ? " and disabled=0" : '') . " order by num");
$teams = getExistingCategories("category&" . CAT_PLAYER . " and type<>0 and", false, true, true);
$nteams = getExistingCategories("category&" . CAT_PLAYER . " and type<>0 and", false, true, false);
if (!$bRoot) {
  unset($col["toSelect"]);
  unset($col["numEquip"]);
  unset($col["caution"]);
}
if ($request["list"]) {
  unset($col["picture"]);
  unset($col["AWBB_id"]);
  unset($col["name"]);
  unset($col["firstName"]);
  unset($col["emailF"]);
  unset($col["emailM"]);
  unset($col["emailP"]);
  unset($col["address"]);
  unset($col["cp"]);
  unset($col["town"]);
  unset($col["phM"]);
  unset($col["phF"]);
  unset($col["phP"]);
  unset($col["phFix"]);
  unset($col["birth"]);
  unset($col["natNum"]);
  unset($col["birthLoc"]);
  unset($col["awbb"]);
  unset($col["licence"]);
}

class MemberEdit extends TableRowEdit
{

//------------------------------------------------------------------------------------------------------------
  function getTuple()
  {
    global $now, $yearSeason, $admin;
    $this->otherCat = jmysql_query("select num,name from category where num&".CAT_OTHERS." or num&".CAT_FIRST." and short is not null and disabled=0 order by num");

    $this->row = TableRowEdit::getTuple();
    if ($this->bNewCat = $this->bSave && $now->format("Y") == ($yearSeason + 1) && $now->format("m-d") < NEW_SEASON_DATE) {
      $ch = "changeCategory('" . $this->row["ncategory"] . "','1','n');changeCategory('" . $this->row["ncategory2"] . "','2','n');changeCategory('" . $this->row["ncategory3"] . "','3','n')";
      if ($this->bList) {
        unset($this->colDef["category"]);
        unset($this->colDef["category2"]);
        unset($this->colDef["category3"]);
      } else {
        $ch .= ";changeCategory('" . $this->row["category"] . "','1','')";
        if (!$this->row["category2"])
          unset($this->colDef["category2"]);
        else
          $ch .= ";changeCategory('" . $this->row["category2"] . "','2','')";
        if (!$this->row["category3"])
          unset($this->colDef["category3"]);
        else
          $ch .= ";changeCategory('" . $this->row["category3"] . "','3','')";
      }
    } else {
      unset($this->colDef["ncategory"]);
      unset($this->colDef["ncategory2"]);
      unset($this->colDef["ncategory3"]);
      $ch = "changeCategory('" . $this->row["category"] . "','1','');changeCategory('" . $this->row["category2"] . "','2','');changeCategory('" . $this->row["category3"] . "','3','')";
    }
    echo "<body onload=$ch>" . nl;
//dump($this->row);
    return $this->row;
  }

//----------------------------------------------------------------------------------------------------------------
  function applyValue(&$att, &$format, &$row)
  {
    switch ($att) {
      case "category":
      case "category2":
      case "category3":
      case "ncategory":
      case "ncategory2":
      case "ncategory3":
        global $category, $admin, $teams, $nteams;
        if (($bNew = $att[0] == 'n' ? "n" : null) && ($admin["rights"] & 1) && $att == "ncategory")
          $category = jmysql_query("select num,name from category where num<>0 and short is not null and disabled=0 and num&" . CAT_PLAYER . " order by num");
        if (!($nb = substr($att, $bNew ? 9 : 8, 1)))
          $nb = 1;
        if ($row[$att] & CAT_PLAYER)
          $cat = $row[$att] - ($row[$att] & 0xFFFF) % 10;
        else if ($row[$att] & (CAT_COACH | CAT_ASSIST)) {
          $team = $row[$att] & 0xFFFF;
          $cat = $row[$att] - $team;
          $team |= CAT_PLAYER;
        } else
          $cat = $row[$att];
        $disab = $admin["rights"] != 0 && $this->bNewCat && !$bNew ? "disabled=disabled" : '';
        echo "<select $disab onchange=changeCategory(this.options[this.selectedIndex].value,$nb," . ($bNew ? "'n'" : "''") . ") name=_val_$att>";
        echo "<option></option>";
        if ($this->bList)
          echo "<option value='=null'>&lt;Supprimer&gt</option>";
        for ($i = 0; $i < jmysql_num_rows($category); $i++) {
          $tmp = jmysql_result($category, $i, "num");
          if ($tmp >= (CAT_OTHERS | 450) && ($att == "category" || $att == "ncategory"))
            continue;
          echo '<option value="' . $tmp . '" ' . ($cat == $tmp ? 'selected=selected>' : '>') . jmysql_result($category, $i, "name") . "</option>";
        }
        echo "</select><select $disab id=${bNew}teams$nb><option value=0></option>";
        $ar = "${bNew}teams";
        foreach ($$ar as $k => $v)
          echo "<option value=$k" . ($team == $k ? " selected=selected" : '') . ">$v</option>" . nl;
        echo "</select><select $disab id=${bNew}range$nb><option value=0></option>";
        $range = ($row[$att] & 0xFFFF) % 10;
        for ($i = 1; $i < 8; $i++)
          echo "<option value=$i" . ($range == $i ? " selected=selected>" : '>') . chr(ord('A') + $i - 1) . "</option>";
        echo "</select>" . nl;
        if (!$bNew) {
          echo "<span id=equip$nb>&nbsp;&nbsp;N° maillot:<select name=_val_numEquip" . substr($att, $bNew ? 9 : 8) . "><option></option>";
          for ($i = 4; $i <= 30; $i++)
            echo "<option" . ($row["numEquip" . substr($att, $bNew ? 9 : 8)] == $i ? " selected=selected" : '') . ">$i</option>";
          echo "</select></span>";
        }
        break;

      case "category4":
        $format[2] = '[Select(#otherCat)]';
        echo parent::ApplyValue("category4", $format, $row);
        jmysql_data_seek($this->otherCat, 0);
        echo parent::ApplyValue("category5", $format, $row);
        jmysql_data_seek($this->otherCat, 0);
        echo parent::ApplyValue("category6", $format, $row);
        break;

      case "caution":
//Caution                           
//bit 0x01 Payée                    
//bit àx02 Remboursée               
//bit 0x04 Payée sur versement      
//bit 0x08                          
//bit 0x10 Pas de caution           
//bit 0x20 Caution obligatoire      
        echo "<select name=_val_caution><option></option>";
        echo "<option value=0" . (isset($row[$att]) && $row[$att] == 0 ? " selected=selected" : '') . ">Rien payé ou famille ou baby</option>";
        echo "<option value=1" . ($row[$att] == 1 ? " selected=selected" : '') . ">Payé</option>";
        echo "<option value=2" . ($row[$att] == 2 ? " selected=selected" : '') . ">Remboursée</option>";
        echo "<option value=4" . ($row[$att] == 4 ? " selected=selected" : '') . ">Payée sur versement</option>";
        echo "<option value=8" . ($row[$att] == 8 ? " selected=selected" : '') . ">A un frère ou pas en ordre de cotisation ou d'équipement...</option>";
        echo "<option value=16" . ($row[$att] == 16 ? " selected=selected" : '') . ">Pas de caution</option>";
        echo "<option value=32" . ($row[$att] == 32 ? " selected=selected" : '') . ">Caution obligatoire</option></select>";
        break;

      case "sex":
        echo "<select name=_val_sex><option" . (!strlen($row[$att]) ? " selected=selected" : '') . "></option><option" . ($row[$att] == 'M' ? " selected=selected" : '') . " value=M>Masculin</option><option" . ($row[$att] == 'F' ? " selected=selected" : '') . " value=F>Féminin</option></select>" . nl;
        break;

      case "picture":
        if (file_exists($_SERVER["DOCUMENT_ROOT"] . "/members/pictures/N_" . $row["num"] . ".jpg"))
          echo '<img src="/members/pictures/N_' . $row["num"] . '.jpg" height="200" widt="156">';
        break;

      case "color":
        echo '<input type="hidden" name="_val_birth">';
        global $color, $request;
        if ($this->bList)
          $this->type = jmysql_result(jmysql_query("select count(*) from " . $request["list"] . " and type=0"), 0) ? 0 : -1;
        else
          $this->type = $this->bSave ? $row["type"] & 0x0F : 1;
        if ($this->bSave && $this->type == 0)
          echo "La couleur ne peut être changée pour un membre supprimé.";
        else {
          echo "<select name=_val_type style=background-color:" . $color[$this->type] . ";width:100 onchange=changeColor(this)>";
          if ($this->bList)
            echo "<option value=''>&lt;Inchangée&gt;</option>";
          foreach ($color as $i => $v)
            if ($i > 0)
              echo "<option value=$i style=background-color:$v" . ($this->type == $i ? " selected=selected" : '') . ">&nbsp;&nbsp;&nbsp;</option>";
          echo "\n</select>\n";
        }
        break;

      case "extern":
        $type = $row["extern"];
        echo '<input type="radio" name="_val_extern" value="=null" id="awbb1" ' . (!$this->bList && !$type ? 'checked="checked" ' : '') . '><label style="cursor:pointer" for="awbb1">Au ' . NAME_CLUB . '</label>';
        echo '<input type="radio" name="_val_extern" value=1 id="awbb2" ' . (!$this->bList && $type ? 'checked="checked" ' : '') . '><label style="cursor:pointer" for="awbb2">Dans un autre club</label>' . nl;
        break;

      case "birth":
        if (!$row[$att]) {
          $y = 0;
          $m = 0;
          $d = 0;
        } else {
          $dt = new dateTime($row[$att]);
          $y = $dt->format('Y');
          $m = $dt->format('m');
          $d = $dt->format('d');
        }
        echo "\n<select name=\"birth_day\">\n";
        for ($i = 0; $i <= 31; $i++)
          echo '<option value="' . ($i ? sprintf("%02d", $i) : ' ') . '" ' . ($i == $d ? 'selected="selected"' : '') . '>' . ($i ? $i : ' ') . '</option>';
        echo "\n</select>\n<select name=\"birth_month\">\n";
        global $months;
        for ($i = 0; $i <= 12; $i++)
          echo '<option value="' . ($i ? sprintf("%02d", $i) : ' ') . '" ' . ($i == $m ? 'selected="selected"' : '') . '>' . ($i ? $months[$i] : " ") . '</option>';
        $last = new DateTime();
        $last = $last->format('Y') - 2;
        echo "\n</select>\n<select name=\"birth_year\">\n";
        echo '<option value=" " ' . (!$y ? 'selected="selected">' : '>') . ' </option>';
        for ($i = $last; $i >= 1930; $i--)
          echo '<option value="' . $i . '" ' . ($i == $y ? 'selected="selected">' : '>') . $i . '</option>';
        echo "\n</select>\n";
        break;

      case "toSelect":
        $type = $row["toSelect"];
        echo '<input type="radio" name="_val_toSelect" value=1 id="toSel1" ' . (!$this->bList && $type ? 'checked="checked" ' : '') . '><label style="cursor:pointer" for="toSel1">A sélectionner</label>';
        echo '<input type="radio" name="_val_toSelect" value="=null" id="toSel2" ' . (!$this->bList && !$type ? 'checked="checked" ' : '') . '><label style="cursor:pointer" for="toSel2">Ne pas sélectioner ou déjà dans les cotisations.</label>' . nl;
        break;

      case "emailP":
        TableRowEdit::ApplyValue($att, $format, $row);
        echo "<span style=margin-left:10px>Non publique :</span>";
        echo "<input type=checkbox id=emVisibility" . ($row["visibility"][0] == 'N' ? ' checked>' : ">");
        echo "<input type=hidden name=_val_visibility>";
        break;

      case "phP":
        TableRowEdit::ApplyValue($att, $format, $row);
        echo "<span style=margin-left:10px>Non publique :</span>";
        echo "<input type=checkbox id=phVisibility" . ($row["visibility"][1] == 'N' ? ' checked>' : ">");
        break;

      default:
        TableRowEdit::ApplyValue($att, $format, $row);
    }
  }

}

$memberEdit = new MemberEdit();
$memberEdit->title = "Edition de membre";
$memberEdit->colDef = &$col;
$memberEdit->editRow();
?>
<script>

  function userSaveClick(paramSave)
  {
    for (j = 0; j < 2; j++)
    {
      n = j == 0 ? '' : 'n';
      for (i = 1; i < 4; i++)
      {
        if (obj = document.getElementsByName("_val_" + n + "category" + (i > 1 ? i : ''))[0])
        {
          obj = obj.options[obj.selectedIndex];
          if (obj.value)
          {
            if (obj.value &<?php echo CAT_PLAYER ?>)
            {
              o1 = document.getElementById(n + "range" + i);
              o1 = o1.options[o1.selectedIndex];
              obj.value = obj.value / 1 + o1.value / 1;
            } else if (obj.value &<?php echo CAT_COACH | CAT_ASSIST ?>)
            {
              o1 = document.getElementById(n + "teams" + i);
              o1 = o1.options[o1.selectedIndex];
              if (o1.value == 0)
              {
                alert("Les catégories entraineurs et assistants doivent avoir une équipes");
                return;
              }
              obj.value = obj.value / 1 + ((o1.value / 1) & 0xFFFF);
            }
          }
        }
      }
    }
    if ( !<?php echo $memberEdit->bList*1?>) {
      $("[name='_val_visibility']").val((document.getElementById("emVisibility").checked ? 'N' : 'Y') + (document.getElementById("phVisibility").checked ? 'N' : 'Y'));
      if (!document.getElementsByName("_val_name")[0].value || !document.getElementsByName("_val_firstName")[0].value || (obj = document.getElementsByName("_val_category")) && obj.length && !obj[0].value || !document.getElementsByName("_val_sex")[0].value)
      {
        alert("La catégorie principale, le nom, le prénom et le sexe sont obligatoires");
        return true;
      }
      el = document.getElementsByName("_val_natNum")[0];
      if (el.value.length)
        el.value = "='" + el.value + "'";
      birthday = document.getElementsByName("_val_birth")[0];
      el = document.getElementsByName("birth_day")[0];
      d = el.value;
      el.name = '';
      el = document.getElementsByName("birth_month")[0];
      m = el.value;
      el.name = '';
      el = document.getElementsByName("birth_year")[0];
      y = el.value;
      el.name = '';
      if (d && m && y)
        birthday.value = y + '-' + m + '-' + d;
    }
  }

  function changeColor(el)
  {
    color = [<?php
foreach ($color as $v) {
  echo "$sep'$v'";
  $sep = ", ";
}
?>];
    el.style.backgroundColor = color[el.selectedIndex];
  }

  function changeCategory(cat, nb, n)
  {
    document.getElementById(n + "teams" + nb).style.display = "none";
    if (equip = document.getElementById(n + "equip" + nb))
      equip.style.display = "none";
    document.getElementById(n + "range" + nb).style.display = "none";
    if (cat && (cat &<?php echo CAT_PLAYER ?>))
    {
      document.getElementById(n + "range" + nb).style.display = "inline";
      if (equip)
        equip.style.display = "inline";
    } else if (cat &<?php echo CAT_COACH | CAT_ASSIST ?>)
      document.getElementById(n + "teams" + nb).style.display = "inline";
    //  else if (n!='n'&&cat>=(<?php echo CAT_OTHERS | 100 ?>))
    //    alert('Si un membre est mis en "Arrêt", il sera mis automatiquement dans la liste des supprimés');
  }

</script>                                                  
</form>
</body>
</html>
