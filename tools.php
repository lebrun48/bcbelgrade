<?php
define("BUT_COL", '#8ba8bd');
define("BUT_COL_HOVER", 'lightgreen');
?>
<style type="text/css">
  .button
  {border-style:solid;border-width:6px;border-bottom-width:1px;border-top-width:1px;border-radius:4px;margin:4px;font-weight:bold;border-color:<?php echo BUT_COL ?>;background-color:<?php echo BUT_COL ?>;color:black;cursor:pointer;text-align:center;display:inline}
  .button:hover
  {border-color:<?php echo BUT_COL_HOVER ?>;background-color:<?php echo BUT_COL_HOVER ?>;color:darkgreen;box-shadow: 5px 5px 2px #888888;}
</style>

<?php

function createButton($pos, $txt, $href, $width = 100)
{
  global $request;
  if ($request["action"] != "generatePDF")
    echo '<a class=button name="button" href="' . $href . '"' . ($pos != -1 ? " margin-left:${pos}px;" : '') . '; width:' . $width . 'px; text-align:center;">' . $txt . "</a>\n";
}

function createAction($pos, $txt, $onClick, $width = 100)
{
  global $request;
  if ($request["action"] != "generatePDF")
    echo '<div class=button name="button" onclick="' . $onClick . '" style=cursor:pointer' . ($pos != -1 ? ';margin-left:' . $pos . 'px;' : null) . ' width:' . $width . 'px;"><u>' . $txt . "</u></div>\n";
}

function alert($msg)
{
  global $request;
  if ($request["action"] != "generatePDF") {
    echo '<script type="text/javaScript">' . nl;
    echo 'alert("' . $msg . '")' . nl;
    echo '</script>' . nl;
  }
}
?>
