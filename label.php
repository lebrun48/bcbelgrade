<?php

include "common/common.php";
if (!$_GET["cols"]) {
  include "head.php";
  echo "<form action=label.php method=get target=_blank>";
  echo "Nombre d'étiquettes par lignes: <input name=cols type=text size=1 maxlength=2 value=4><br>";
  echo "En-tête: <input type=text name=head size=30 maxlength=30 value='Aux parents de'><br>";
  echo "<select multiple=multiple id=selection><option></option>";
  $r = jmysql_query("select num, concat(name,' ',firstName) from members where type<>0 order by name");
  while ($tup = jmysql_fetch_row($r))
    echo "<option value=$tup[0]>$tup[1]</option>";
  echo "</select>";
  echo "<br>Default sql:<br>";
  echo "select num, concat(name,' ',firstName) from members where type<>0 order by name<br>";
  echo "New sql: <textarea name=sql cols=200 rows=3></textarea>";
  echo " a partir de la position <input name=start size=1 type=text value=0><br>";
  echo "<input type=radio name=noaddr>Sans adresses<br>";
  echo "<input type=radio name=addr>Avec adresses<br>";
  echo "<input type=submit value=Exécuter>";
  exit();
}

include_once BASE_PATH . "/tools/fpdf/fpdf.php";
define('FPDF_FONTPATH', BASE_PATH . '/tools/fpdf/font');
$pdf = new FPDF;
$pdf->SetAutoPageBreak(false, 0);
$pdf->SetMargins(0, 0, 0);
$pdf->SetFont("Arial", '', 10);

function printLine(&$tup)
{
  global $pdf, $i, $cols, $bAddr;
  if (!($p = $i % (11 * $cols))) {
    $pdf->AddPage();
  }
  $pdf->SetLeftMargin($x = $cols == 3 ? 10 + 200 / $cols * ($p % $cols) : 8 + 200 / $cols * ($p % $cols));
  $pdf->SetXY($x, 12 + 280 / 11 * intval($p / $cols));
  //echo $tup["name"].", x=".$pdf->GetX().", y=".$pdf->GetY().NL;
  $head = $_GET["head"];
  if ($head && ($tup["category"] >= (CAT_PLAYER | 400) && $tup["category"] <= (CAT_PLAYER | 1000) || $tup["category"] >= (CAT_PLAYER | 1250) && $tup["category"] < (CAT_PLAYER | 1500))) {
    $pdf->SetFontSize(8);
    $pdf->Write(3, "$head\n");
  }
  $pdf->SetFont('', !$bAddr ? 'B' : '', 10);
  $pdf->Write(5, ($t = $tup["name"]) . (strlen($t) >= 13 ? "\n" : ' ') . $tup["firstName"] . "\n");
  if ($bAddr) {
    $pdf->SetFontSize($cols > 3 && strlen($tup["address"]) > 22 ? 8 : 10);
    $pdf->Write(5, $tup["address"] . "\n");
    $pdf->SetFontSize($cols > 3 && strlen($tup["town"]) > 15 ? 8 : 10);
    $pdf->Write(5, $tup["cp"] . ' ' . $tup["town"]);
  } else {
    $pdf->SetFont('', 'I');
    $pdf->Write(5, $tup ? utf8_decode(getCategory($tup["category"], true)) : '');
  }
  $i++;
}

$bAddr = isset($_GET["addr"]);



$i = 0;
$cols = $_GET["cols"];
if ($_GET["list"]) {
  while ($i != $_GET["start"])
    printLine($tup);
  $r = jmysql_query("select num,category,name,firstName,address,cp,town from members where num in (" . $_GET["list"] . ") order by name", 1);
} else
  $r = jmysql_query("select category,name,firstName,address,cp,town from members where category&" . CAT_PLAYER . " and type<>0 order by category,name");
//$r=jmysql_query("select members.num,category,name,firstName,address,cp,town from members,cotisations where (cotisations.num=members.num and cotisations.previous is null) and type<>0 order by category,name");
//$r=jmysql_query("select category,name,firstName,address,cp,town from members,cotisations where (cotisations.num=members.num and cotisations.previous is null) and type<>0 and members.num=746 order by category,name");
//$r=jmysql_query("select members.num,category,name,firstName,address,cp,town from members,cotisations where (cotisations.num=members.num and cotisations.previous is null or category=".CAT_SPONSOR." or category2=".CAT_SPONSOR." or category3=".CAT_SPONSOR.") and type<>0 order by category,name");
//$r=jmysql_query("select category.name as catName, members.category, members.name, members.firstName, members.address, members.cp,members.town from cotisations,members,category where members.num=cotisations.num and category.num=members.category and members.type<>0 and cotisations.previous is null order by members.category,members.name");
while ($i != $_GET["start"])
  printLine($tup);
while ($tup = jmysql_fetch_assoc($r)) {
  foreach ($tup as $k => $v)
    $tup[$k] = utf8_decode($tup[$k]);
  printLine($tup);
}

$pdf->Output("labels.pdf", 'I');
