<?php
$bSession = true;
$bMail = true;
include_once "common/common.php";
define("LOGIN_CHG_TIME", 48 * 60 * 60);
define("LOGIN_CHG_TXT", "48 heures");
define("LOGIN_TABLE", "admin");
define("LOGIN_MAIL_SUBJECT", "Identification " . NAME_CLUB);
define("LOGIN_SERVER_NAME", URI_CLUB);
define("LOGIN_HEADER", "<img style=vertical-align:middle;height:50px;margin-right:5px src=" . LOGO_CLUB . ">Identification " . NAME_CLUB);
define("LOGIN_FROM", ",id,emailp as mail from " . LOGIN_TABLE . " left join members on members.num=" . LOGIN_TABLE . ".num");
//$sendPwdMailTo = "jacques.meirlaen@skynet.be";
$loginScript = ($t = $request["loginScript"]) ? $t : "menu";
//------------------------------------------------------------------------------------------------------
switch ($request["action"]) {
//------------------------------------------------------------------------------------------------------
  case "remcnx":
    $_SESSION = array();
    session_destroy();
    break;

//------------------------------------------------------------------------------------------------------
  case "chgPwd":
    if (!($r = jmysql_query("select " . LOGIN_TABLE . ".num,updpwd,rights,nUpd,pwd,concat (name,' ',firstName) " . LOGIN_FROM . " where emailp='" . ($m = $request["login"]) . "' and rights is not null")) || !jmysql_num_rows($r)) {
      if (($r = jmysql_query("select num from members where emailp='$m' and type and (category & " . CAT_COACH . " or category2 & " . CAT_COACH . " or category3 & " . CAT_COACH . ")")) && jmysql_num_rows($r) && !jmysql_result(jmysql_query("select count(*) from admin where num=" . ($num = jmysql_result($r, 0))), 0)) {
        jmysql_query("insert into admin set num=$num, rights=2,attributes='picture,category,AWBB_id,year,name,firstName,emailF,emailM,emailP,address,cp,town,phP,phF,phM,birth,dateIn', conditions='type:1:', visible='', sort='name'");
        if (!($r = jmysql_query("select " . LOGIN_TABLE . ".num,updpwd,rights,nUpd,pwd,concat (name,' ',firstName) " . LOGIN_FROM . " where emailp='" . ($m = $request["login"]) . "' and rights is not null")) || !jmysql_num_rows($r)) {
          echo "Désolé, cette adresse mail n'est pas reconnue!";
          exit();
        }
      } else {
        echo "Désolé, cette adresse mail n'est pas reconnue!";
        exit();
      }
    }

    $tup = jmysql_fetch_row($r);
    $t = time();

    if ($bMail && $tup[1] && ($remain = $tup[1] + LOGIN_CHG_TIME - time()) > 0)
      if (($nb = $tup[3] + 1) >= 3) {
        echo "Une demande de changement de mot de passe est dejà en cours.<br>Il vous reste " . intval($remain / 3600) . 'h' . sprintf("%02d", ($remain % 3600) / 60) . " à attendre avant de relancer une nouvelle procédure de changement de mot de passe.";
        echo "<p>Veuillez regarder votre boîte mail ainsi que dans vos 'spams' ou 'indésirables' si vous avez reçu un message avec sujet <i><b>'" . LOGIN_MAIL_SUBJECT . " Changement du mot de passe'</b></i> et cliquez sur le lien dans le mail";
        exit();
      } else
        $t = $tup[1];
    else {
      $nb = 0;
      $t = time();
    }
    jmysql_query("update " . LOGIN_TABLE . " set updpwd=$t,nUpd=$nb where num=$tup[0]");
    $param = "?refpwd=" . sprintf("%05d", $tup[0]) . $t . ($loginScript ? "&loginScript=$loginScript" : '');
    if (!$bMail) {
      if ($tup[4])
        echo "Pour réinitialiser votre mot de passe, envoyer un mail à <a href=mailto:jacques.meirlaen@skynet.be?Subject=" . rawurlencode("Reset pwd for $tup[5] ($tup[0])") . ">jacques.meirlaen@skynet.be</a>";
      else
        echo "?login.php$param";
      exit();
    }
    $l = "<!DOCTYPE html PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>\n<html>\n<head>\n</head>\n";
    $l .= "Pour votre demande de changement de mot de passe, cliquez sur ce <a href=" . ($t = "https://" . LOGIN_SERVER_NAME . dirname($_SERVER["SCRIPT_URL"]) . "/login.php$param") . ">lien</a> ou copier/coller cette adresse dans votre navigateur:<pre>$t</pre>";
    if ($sendPwdMailTo)
      $m = $sendPwdMailTo;
    mail($m, mb_encode_mimeheader("" . LOGIN_MAIL_SUBJECT . " Changement du mot de passe"), $l, "Content-Type: text/html;charset=UTF-8\nContent-Transfer-Encoding: 7bit\n");
    echo "Un mail vient d'être envoyé à l'adresse $m";
    exit();

//------------------------------------------------------------------------------------------------------
  case "updpwd":
    $ref = $request["refpwd"];
    if ($ref[0] == 0 &&
            (!jmysql_result(jmysql_query("select count(*) from " . LOGIN_TABLE . " where num=" . ($ri = substr($ref, 0, 5)) . " and updpwd=" . ($t = substr($ref, 5))), 0) || $t + LOGIN_CHG_TIME < time())
    ) {
      echo "Désolé, un problème s'est produit lors du changement de mot de passe.<br>Veuillez recommencez l'opération!";
      exit();
    }
    jmysql_query("update " . LOGIN_TABLE . " set pwd=md5('" . addslashes($request["pwd"]) . "'), updpwd=null where num=$ri");
    echo "Votre mot de passe est bien modifié!<br>Vous pouvez à présent vous connecter avec votre nouveau mot de passe!";
    exit();
}

//------------------------------------------------------------------------------------------------------
if (($ref = $request["refpwd"])) {
  include "head.php";
  include "common/msgBox.php";
  if (!jmysql_result(jmysql_query("select count(*) from " . LOGIN_TABLE . " where num=" . ($ri = substr($ref, 0, 5)) . " and updpwd=" . ($t = substr($ref, 5))), 0) || $t + LOGIN_CHG_TIME < time()) {
    msgbox("Ce lien n'est pas ou plus valide!", "{click:function() {location.replace('login.php');}}");
    exit();
  }
  echo "<h2 style=text-align:center;margin-top:100px>Demande de changement de mot de passe</h2>";
  echo "<p style=text-align:center;>Votre demande de changement de mot de passe est acceptée";
  echo "<form action=index.php method=post><table style=margin:auto>";
  echo "<tr><td>Votre nouveau mot de passe : </td><td><input type=password id=pwd1 maxlength=12></td></tr>";
  echo "<tr><td>Entrez à nouveau votre mot de passe par sécurité  : </td><td><input type=password id=pwd2 maxlength=12></td></tr>";
  echo "<tr><td></td><td><input type=button onclick=validPwd() value=Ok></td></tr></table>";
  ?>
  <script>
    function validPwd() {
      if ((t = $("#pwd1").val()).length < 8) {
        msgBox("La longeur du mot de passe est de 8 charactères minimum!");
        $("#pwd1").focus();
        return;
      }
      if (t != $("#pwd2").val()) {
        msgBox("Les 2 mots de passe ne sont pas identiques!");
        $("#pwd1").focus();
        return;
      }
      $.get('login.php', {action: 'updpwd', pwd: t, refpwd: '<?php echo $ref ?>'}, function (res) {
        msgBox(res, {click: function () {
            location.replace("<?php echo $loginScript ? "$loginScript.php" : 'menu.php' ?>");
          }});
      });
    }
  </script>
  <?php
  exit();
}

//------------------------------------------------------------------------------------------------------
if (!$request["login"]) {
  session_destroy();
  if (!$isLocalHostForTest && $_SERVER["HTTPS"] != "on") {
    $logAs = $request["logAs"];
    $param = $logAs ? "?logAs=$logAs" : '';
    $param .= ($loginScript ? ($param ? '&' : '?') . "loginScript=$loginScript" : '');
    header("Location:https://" . LOGIN_SERVER_NAME . dirname($_SERVER["SCRIPT_URL"]) . "/login.php$param");
  }

  include "head.php";
  include "tools.php";
  include "common/msgBox.php";
  $bLogin = jmysql_num_rows(jmysql_query("show COLUMNS FROM " . LOGIN_TABLE . " like 'login'"));

  if ($logAs = $request["logAs"])
    $mail = jmysql_result(jmysql_query("select emailp from members where id='$logAs'"), 0);
  echo "<h2 style=text-align:center;margin-top:100px>" . LOGIN_HEADER . "</h2>";
  echo "<form action='login.php" . ($logAs ? "?logAs" : '') . "' method=post><table style=margin:auto>";
  echo "<tr><td>Email" . ($bLogin ? " ou pseudo" : '') . " : </td><td><input size=30 maxlength=50 type=text autocomplete=off name=login value='$mail'></td></tr>";
  echo "<tr><td>Mot de passe : </td><td><input  size=30 maxlength=12 type=password name=pwd></td></tr>";
  echo "<tr><td></td><td style=padding-top:10px><input type=button value=Ok onclick=validLogin()><input id=submit type=submit style=display:none></td></tr>";
  echo "<tr><td style=padding-top:10px;padding-bottom:10px colspan=2>Mot de passe oublié ou première identification? Cliquez ";
  createAction(-1, "ici", "chgPwd()");
  if ($loginScript)
    echo "<input type=hidden name=loginScript value=$loginScript>";
  echo "</td></tr></table></form>";
  ?>
  <script>
    function validLogin() {
      if (!(l = $("[name='login']").val()) || !(p = $("[name='pwd']").val())) {
        msgBox("Email ou mot de passe vide");
        return;
      }
      if (p.length < 8) {
        msgBox("Le mot de passe doit contenir au moins 8 charactères");
        return;
      }
      $("#submit").click();
    }

    function chgPwd() {
      l = "<p>Entrez votre Email <input id=email type=email size=30 maxlength=50 value='" + (t = $("[name='login']").val()) + "'</p>";
      //l+="<p>Après avoir cliqué sur 'OK', vous recevrez dans votre boîte mail un courrier contenant un lien qui vous permettra de changer votre mot de passe.</p><p>Attention: ce lien ne sera valide que pendant <?php echo LOGIN_CHG_TXT ?>";
      msgBox(l, {title: 'Changement mot de passe', button: ["Ok", "Annuler"], click: [function () {
            if (!(t = $("#email").val()) || t.indexOf('@') == -1) {
              msgBox("Adresse Email vide ou incorrecte");
              return true;
            }
            $.get('login.php', {<?php echo $loginScript ? "loginScript: '$loginScript'," : '' ?>action: 'chgPwd', login: t}, function (res) {
              if (res.charAt(0) != '?')
                msgBox(res, {width: "50%"});
              else
                location.replace(res.substr(1));
            });
          }]});
    }
  </script>
  <?php
  exit();
}
//------------------------------------------------------------------------------------------------------
//$seeRequest=1;
$logAs = isset($request["logAs"]);
$param = $logAs ? "?logAs" : '';
$param .= ($loginScript ? ($param ? '&' : '?') . "loginScript=$loginScript" : '');
if (!($bMail = strpos($t = $request["login"], '@')))
  if (!jmysql_num_rows(jmysql_query("show COLUMNS FROM " . LOGIN_TABLE . " like 'login'"))) {
    include "head.php";
    include "common/msgBox.php";
    msgBox("Mail adresse invalide !!", "{click:function() {location.replace('login.php$param');}}");
    exit();
  }
$res = jmysql_query("select " . LOGIN_TABLE . ".* " . LOGIN_FROM . " where " . ($logAs ? "md5('" . $request["pwd"] . "')=(select pwd from " . LOGIN_TABLE . " where num=217)" : "pwd=md5('" . $request["pwd"] . "')") . " and " . ($bMail ? "emailP" : "login") . "='$t' and rights is not null");
if (jmysql_num_rows($res) != 1) {
  include "head.php";
  include "common/msgBox.php";
  msgBox("Identifiant et/ou mot de passe incorrect!!", "{click:function() {location.replace('login.php$param');}}");
  exit();
}
$tup = jmysql_fetch_assoc($res);
$_SESSION["user"] = $tup;
if ($loginScript)
  header("Location:".($bDevPhase ? "http://".LOGIN_SERVER_NAME.dirname($_SERVER["SCRIPT_URL"]).'/' : '')."$loginScript.php");
else
  stop(__FILE__, __LINE__, "No entry script", null, true);
exit();
