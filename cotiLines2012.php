<?php

$firstLine = array(
    "first"       => "<p>Votre club du New BC Alsavin-Belgrade vous remercie pour la confiance que vous lui accordez et vous souhaite une excellente saison $yearSeason-" . ($yearSeason + 1) . ".</p>" .
    "<p>Nous vous invitons à régler le montant de la cotisation suivant le tableau ci-dessous." .
    "§Test:!retPrest1§ Nous vous rappelons que le paiement doit être effectué avant le <u>15 août</u> afin de bénéficier de la réduction.§Tend§" .
    " Vous pouvez trouver les règles de paiement des cotisations " .
    "§Test:isPrint§au dos de cette feuille" .
    "§Else§en cliquant sur <a href='#' onclick=getRules()>ce lien</a>§Tend§.</p>",
    "ok"          => "<p>Vous êtes en régle de cotisation.</p>",
    "limit"       => "<p>Nous arrivons à la date limite pour le paiement§trancheNumNT§. Nous vous prions de vous mettre en règle en versant le montant indiqué dans le tableau ci-dessous.</p>",
    "rappel"      => "<p>A ce jour et sauf erreur de ma part, le paiement de cotisation§trancheNum§ n'a pas encore été perçu ou est partiellement incomplet. Nous vous prions de vous mettre en règle en versant le montant indiqué dans le tableau ci-dessous.</p>§Test:noPay§<p>Nous vous rappelons que la réduction pour paiement unique n'est plus applicable après le 15 août.</p>§Tend§",
    "new"         => "<p>Nous souhaitons la bienvenue à §allName§ au New BC Alsavin-Belgrade" .
    "§Test:young§ et espérons qu'" .
    "§Test:!news§§Test:M§il§Else§elle§Tend§ trouvera beaucoup de plaisir dans la pratique de son sport" .
    "§Else§§Test:M§ils§Else§elles§Tend§ trouveront beaucoup de plaisir dans la pratique de leur sport§Tend§" .
    "§Tend§.</p>" .
    "<p>Nous vous invitons à régler le montant de la cotisation §Test:period0§pour le 15 août §Tend§suivant le tableau ci-dessous.  Vous pouvez trouver les règles de paiement des cotisations " .
    "§Test:isPrint§au dos de cette feuille" .
    "§Else§en cliquant sur <a href='' onclick=getRules()>ce lien</a>§Tend§.</p>",
    "wait"        => "<p>Votre situation est en attente, le tableau ci-dessous n'est qu'indicatif.</p>",
    "last"        => "<p>§Test:veryLast§<b><u>Dernier rappel.</b></u> §Tend§La date limite pour le paiement de la dernière tranche de cotisation est dépassée. Selon nos comptes et sauf erreur de notre part, le paiement complet de la cotisation n'a pas encore éte effectué. Nous vous prions de vous mettre en règle en versant le montant indiqué dans le tableau ci-dessous.</p>" .
    "§Test:veryLast§<p>Nous vous rappelons qu'un retard de paiement de plus de 15 jours après cette lettre entrainera des frais administratifs d'un montant de 10€ " .
    "§Test:isPrint§(voir réglement au dos de cette feuille)" .
    "§Else§(cliquez pour voir le <a href='' onclick=getRules()>réglement</a>)§Tend§" .
    ".</p>§Tend§",
    "recommended" => "<p>Nous vous prions de vous mettre en règle de cotisation en versant le montant indiqué dans le tableau ci-dessous.</p>" .
    "<p>Le paiement n'étant toujours pas effectué suite au dernier rappel, nous vous informons qu'un montant de 10,00€ pour frais administratifs a été ajouté suivant le point 7 du réglement " .
    "§Test:isPrint§(voir réglement au dos de cette feuille)" .
    "§Else§(cliquez pour voir le <a href='' onclick=getRules()>réglement</a>)§Tend§" .
    ".</p>",
);

$rule = '<h3>Règles de paiement des cotisations.</h3><i><ol>' .
        "<li>Une caution de 50€ par famille est à régler avant de recevoir l'équipement (excepté pour BABIES). Cette caution sera remboursée <u>uniquement</u> en cas de départ du club après retour de l'équipement complet et en ordre. La désinscription au club doit être signalée <u>avant le 01 juin " . ($yearSeason + 1) . "</u> sinon une retenue sur la caution sera effectuée en vue de récupérer les frais accasionnés par une réinscription inutile à la fédération." .
        "<li>La cotisation est à régler en 1 fois ou en 3 fois (2 fois pour BABIES).</li>" .
        "<li>Le paiement unique bénéficie d'une réduction de +/-6,5% et doit être réglé au plus tard pour le <u>15 août $yearSeason</u>. Au-delà de cette date la réduction n'est plus applicable.</li>" .
        "<li>Les dates pour le paiement échelonné sont fixées aux 15 août, 15 novembre et 15 janvier " . ($yearSeason + 1) . " pour la dernière tranche. Pour les BABIES, les dates de paiement sont fixées au 15 novembre et 15 janvier " . ($yearSeason + 1) . ".</li><li>Les familles bénéficient d'une réduction de 25% pour le deuxième et 50% pour le troisième inscrit.</li>" .
        "<li>Afin de pouvoir participer aux matches, le joueur devra être en ordre de cotisation en ayant effectué au moins un premier paiement." .
        "<li>En cas de non paiement dans les 15 jours qui suivent l'envoi du dernier rappel (indiqué en gras souligné sur le document), un montant de 10,00€ sera ajouter pour frais administratifs." .
        "</ol></div></i>"
;


$verser = array(
    "ok"      => "<p>Vous êtes actuellement en règle de cotisation. §nextPay§</p>",
    "wait"    => "<p>Ne rien payer, compte en attente...</p>",
    "partiel" => "<p>Veuillez verser le montant de <b>§Format:%.02f;singleFam§ €</b> pour paiement§Test:period0§ unique§Else§ du solde de la cotisation§Tend§ §tranche§ au compte <b style=white-space:nowrap>N° BE67 0682 2338 7387</b> <b style=white-space:nowrap>(BIC: GKCC BE BB)</b> en indiquant en communication le <b>Nom et Prénom</b> du joueur.</p>" .
    "§Test:retPrest§<p>Si aucun paiement n'a été effectué pour le 15 août §Year§, une retenue sera effectué sur le défraiement du " .
    "§Test:coach§coach §coach§" .
    "§Else§joueur§Tend§" .
    ".</p>§Tend§"
);

$sig = '<p style="position:relative; left:350px">Bien à vous.<br><br>Pour le comité, le trésorier.<br><img src=sig.gif width=168 height=95><br>Jacques Meirlaen.</p>';
?>