<?php
$bSession=true;
include "admin.php";
include "head.php";
include 'tools.php';

$seeRequest = 1;
if (!$request["season"]) {
  ?>
  <script>
    alert("Exécution sur la saison courante!!");
  </script>
  <?php
  exit();
}
if (!isset($request["action"])) {
  ?>
  <script>
    function launch()
    {
      val = 0;
      $(":checked").each(function () {
        val |= $(this).val();
      });
      location.assign("nextYear.php<?php echo $basePath ?>&action=" + val);
    }</script>
  <?php
  echo "<input type=checkbox value=255>Tout" . NL;
  echo "<input type=checkbox value=1>Vider les tables" . NL;
  echo "<input type=checkbox value=2>Mise à jour des membres" . NL;
  echo "<input type=checkbox value=4>Mise à jour des cotisations" . NL;
  echo "<input type=checkbox value=64>Vider table pay_in + 'firstLines'" . NL;
  echo "<input type=checkbox value=8>Mise à jour du calendrier" . NL;
  echo "<input type=checkbox value=16>Mise à jour des entrainements" . NL;
  echo "<input type=checkbox value=32>Mise à jour pay_out" . NL;
  echo "<input type=checkbox value=128>Mise à jour coaches" . NL;
  echo NL . "<input type=button value=OK onclick=launch()>" . NL;
  exit();
}

$val = $request["action"];



//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
if ($val & 0x02) {
  echo "Updating members..." . NL;
  if (!jmysql_result(jmysql_query("select count(*) from members where nCategory<>category and type<>0"), 0)) {
    echo "DB déjà mise à jour!!" . NL;
    exit();
  }
  jmysql_query("update members set category=nCategory where nCategory<>".CAT_MUT_OUT." and nCategory<>".CAT_STOP." and type<>0");
  jmysql_query("update members set category2=nCategory2 where type<>0");
  jmysql_query("update members set category3=nCategory3 where type<>0");
  jmysql_query("update members set category3=nCategory where (nCategory=".CAT_MUT_OUT." or nCategory=".CAT_STOP.") and type<>0");
  jmysql_query("update members set type=0, ncategory=category where (nCategory=".CAT_MUT_OUT." or nCategory=".CAT_STOP.") and type<>0");
  jmysql_query("update members set status=0 where status is not null and status<>10 and type<>0");
  jmysql_query("update members set caution=1 where caution=4 and type<>0");
  jmysql_query("update members set equipement=null,numEquip=null,numEquip2=null,numEquip3=null,equipSt=null,equipSt2=null,equipSt3=null where  type<>0");
}

//-------------------------------------------------------------------------------------------------
if ($val & 0x04) {
  echo "Updating cotisations..." . NL;
  if (!jmysql_result(jmysql_query("select count(*) from members where (status<>0 or status <>10) and type<>0"), 0)) {
    echo "DB déjà mise à jour!!" . NL;
    exit();
  }
  $r = jmysql_query("select num from members where (category&" . CAT_PLAYER . " or category2&" . CAT_PLAYER . " or category3&" . CAT_PLAYER . ") and num not in (select num from cotisations) and type<>0");
  if (!$r)
    stop(__FILE__, __LINE__, "slqError:" . jmysql_error());
  while ($row = jmysql_fetch_row($r)) {
    jmysql_query("insert into cotisations set num=$row[0]", 1);
    jmysql_query("update members set status=10,caution=0 where num=$row[0] and status is null", 1);
  }

  $r = jmysql_query("select cotisations.num,previous,caution from cotisations left join members on members.num=cotisations.num where members.type=0 or !(category&0x40000)");
  while ($row = jmysql_fetch_row($r)) {
    $p = jmysql_query("select num from cotisations where previous=$row[0]", 1);
    if (jmysql_num_rows($p)) {
      jmysql_query("update cotisations set previous=" . ($row[1] ? $row[1] : "null") . " where num=" . ($p = jmysql_result($p, 0)), 1);
      jmysql_query("update members set caution=$row[2] where num=$p", 1);
      jmysql_query("update members set caution=0 where num=$row[0]", 1);
    }

    jmysql_query("delete from cotisations where num=$row[0]", 1);
  }
  if ($val & 0x40) {
    jmysql_query("TRUNCATE TABLE pay_in");
    jmysql_query("update cotisations set comment=null,firstLine=null,verser=null,prepaid=0", 1);
  }
}

//-------------------------------------------------------------------------------------------------
if ($val & 0x08) {
  echo "Updating calendar..." . NL;
  jmysql_query("delete from calendar where date<'$yearSeason-" . NEW_SEASON_DATE . "'");
  jmysql_query("ALTER TABLE `calendar` DROP `rowid`");
  jmysql_query("ALTER TABLE `calendar` ADD `rowid` SMALLINT NOT NULL AUTO_INCREMENT FIRST, ADD PRIMARY KEY (`rowid`)");
}

//-------------------------------------------------------------------------------------------------
if ($val & 0x10) {
  echo "Updating training..." . NL;
  jmysql_query("delete from training where grid>=0");
  jmysql_query("update training set grid=0");
  jmysql_query("ALTER TABLE `training` DROP `ri`");
  jmysql_query("ALTER TABLE `training` ADD `ri` SMALLINT NOT NULL AUTO_INCREMENT FIRST, ADD PRIMARY KEY (`ri`)");
}

//-------------------------------------------------------------------------------------------------
if ($val & 0x20) {
  //-------------------------------------------------------------------------------------------------
  echo "Updating pay_out..." . NL;
  include "payOutData.php";
  if (!jmysql_result(jmysql_query("select count(*) from pay_out where month<>0"), 0)) {
    alert("Table déjà faite!");
    exit();
  }

  jmysql_query("delete from pay_out where type=" . YEAR_ACCOUNT);
  $r = jmysql_query("select pay_out.num,concat(name,' ',firstName) from pay_out,members where (pay_out.type=" . DISTR_ACCOUNT . " or pay_out.type=" . ACCOUNT . " and amount is not null) and pay_out.month=0 and members.num=pay_out.num");
  while ($tup = jmysql_fetch_row($r)) {
    $sum = 0;
    $sum += jmysql_result(jmysql_query("select sum(amount) from pay_out where num=$tup[0] and month>=1 and month<=7 and (type=" . DISTR_ACCOUNT . " or type=" . ACCOUNT . ")"), 0);
    $t = jmysql_fetch_row(jmysql_query("select count(ndep), sum(ndep) from prestations where num=$tup[0] and status>=1 and status<=7"));
    if (!$t[0]) {
      $r1 = jmysql_query("select count(pres),group_concat(pres) from presences where num=$tup[0] and substr(date,6,2)>='01' and substr(date,6,2)<='07'");
      $t = jmysql_fetch_row($r1);
      $t[1] = substr_count($t[1], "1");
    }
    $r1 = jmysql_query("select amount from pay_out where num=$tup[0] and month=0 and (type=" . MOVE . " or type=" . HOME . ")");
    $home = jmysql_num_rows($r1) ? jmysql_result($r1, 0) : 0;
    $r1 = jmysql_query("select amount from pay_out where num=$tup[0] and month=0 and type=" . PREST_FICT);
    if ($r1 && jmysql_num_rows($r1)) {
      $t[1] += jmysql_result($r1, 0) * 4;
    }
    $sumKm = $t[0] * 30 + $t[1] * $home;
    echo jmysql_error();
    if ($sum + $sumKm == 0)
      continue;
    $sql = "insert into pay_out set type=" . YEAR_ACCOUNT . ", num=$tup[0], amount=$sum, detail=$sumKm, month=0";
    jmysql_query($sql);
    echo jmysql_error($link);
    echo "$tup[1] terminé" . NL;
  }

  jmysql_query("delete from pay_out where month<>0");
  jmysql_query("ALTER TABLE `pay_out` DROP `ri`");
  jmysql_query("ALTER TABLE `pay_out` ADD `ri` SMALLINT NOT NULL AUTO_INCREMENT FIRST, ADD PRIMARY KEY (`ri`)");
}

//-------------------------------------------------------------------------------------------------
if ($val & 0x01) {
  echo "Truncating tables..." . NL;
  jmysql_query("TRUNCATE TABLE com_presences");
  jmysql_query("TRUNCATE TABLE events");
  jmysql_query("TRUNCATE TABLE memberHistory");
  jmysql_query("TRUNCATE TABLE presences");
  jmysql_query("TRUNCATE TABLE prestations");
  jmysql_query("TRUNCATE TABLE stages");
}

//-------------------------------------------------------------------------------------------------
if ($val & 0x80) {
  echo "Updating coaches..." . NL;
  jmysql_query("update coach set end=1");
}
