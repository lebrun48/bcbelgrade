<?php

include "common/common.php";
include "head.php";
?>
<title>Get clubs          
</title>    
</head>   
<body>
  <?php

//---------------------------------------------------------------------------------------------------------
  function dispEl(&$t, $bCont = false, $bRec = false)
  {
    static $level = 0;
    $tab = str_repeat(" - ", $level);
    echo $tab . $t->length . NL;
    for ($i = 0; $i < $t->length; $i++) {
      echo $tab . $t->item($i)->nodeName . "[$i]=" . $t->item($i)->nodeValue . NL;
      if ($bRec && $t->item($i)->childNodes->length) {
        $level++;
        dispEl($t->item($i)->childNodes, $bCont, true);
      }
    }

    if ($level) {
      $level--;
      return;
    }
    if (!$bCont)
      exit();
  }

  echo NL . "<big><u><b>Coordonnées clubs</b></u></big>" . NL;
  $document = new DOMDocument();
  $base = "http://awbb.be/index.php?option=com_content&view=article&id=2015&Itemid=187";
  @$document->loadHTML(file_get_contents($base));
  $t = $document->childNodes->item(1)->childNodes->item(1)->childNodes->item(13)->childNodes->item(1)->childNodes->item(1)->childNodes->item(1)->childNodes->item(9)->childNodes->item(1)->childNodes->item(4)->childNodes->item(2)->childNodes;
//dispEl($t,false,true);
  for ($i = 0; $i < $t->length; $i += 2) {
    $url = "http://awbb.be" . $t->item($i)->childNodes->item(0)->getAttribute("href");
    echo "url=$url" . NL;
    $reg = new DOMDocument();
    @$reg->loadHTML(file_get_contents($url));
    $t1 = $reg->childNodes->item(1)->childNodes->item(1)->childNodes->item(13)->childNodes->item(1)->childNodes->item(1)->childNodes->item(1)->childNodes->item(15)->childNodes->item(5)->childNodes->item(11)->childNodes->item(1)->childNodes->item(3)->childNodes->item(1)->childNodes->item(1)->childNodes->item(0)->childNodes;
    //dispEl($t1,false,true);
    for ($j = 0; $j < $t1->length; $j++) {
      //$t2=$t1->item($j)->getElementsByTagName("a");
      //dispEl($t2, true);
      //continue;
      $url = $t1->item($j)->getElementsByTagName("a")->item(0)->getAttribute("href");
      echo "url=$url" . NL;
      $clUrl = new DOMDocument();
      @$clUrl->loadHTML(file_get_contents($url));
      $club->url = substr($url, strrpos($url, '/') + 5);
      if (!$clUrl->childNodes->length) {
        echo '<div style="color:red; font-weight:bold">pas de club à cet URl:' . $url . "</div>" . NL;
        continue;
      }
      $t2 = $clUrl->childNodes->item(1)->childNodes->item(1)->childNodes->item(1)->getElementsByTagName("table")->item(0)->childNodes;
      //dispEl($t2);
      //dispEl($t2->item(0)->childNodes,true);
      $club->num = trim($t2->item(0)->childNodes->item(2)->nodeValue);
      $club->name = test(trim($t2->item(0)->childNodes->item(4)->nodeValue));
      $nAddr = 0;
      for ($k = 0; $k < $t2->length; $k++) {
        if (!strcasecmp(substr(ltrim($t2->item($k)->nodeValue), 0, 5), "salle"))
          if (($a = trim(test($t2->item($k)->childNodes->item(2)->nodeValue))) && strlen($a) > 5)
            $club->address[$nAddr++] = $a;
        if (!$nAddr && !strcasecmp(substr(ltrim($t2->item($k)->nodeValue), 0, 9), "terrain 1"))
          if (($a = trim(test($t2->item($k)->childNodes->item(2)->nodeValue))) && strlen($a) > 5)
            $club->address[$nAddr++] = $a;
        if (!strcasecmp(substr(ltrim($t2->item($k)->nodeValue), 0, 9), "terrain 2"))
          if (($a = trim(test($t2->item($k)->childNodes->item(2)->nodeValue))) && strlen($a) > 5)
            $club->address[$nAddr++] = $a;
        if (!strcasecmp(substr(ltrim($t2->item($k)->nodeValue), 0, 12), "Couleur des "))
          if (($a = trim(test($t2->item($k)->childNodes->item(2)->nodeValue))) && strlen($a) > 3)
            $club->jersey = $a;
      }
      dump($club, "club");
      $res = jmysql_query("select * from clubs where num=" . $club->num);
      if ($res && jmysql_num_rows($res)) {
        $row = jmysql_fetch_assoc($res);
        $sql = "update clubs set ";
        if ($row["name"] != $club->name)
          $sql .= "name='" . addslashes($club->name) . "', ";
        if ($row["address"] != $club->address[0])
          $sql .= "address='" . addslashes($club->address[0]) . "', ";
        if ($club->address[1] && $row["address2"] != $club->address[1])
          $sql .= "address2='" . addslashes($club->address[1]) . "', ";
        if ($row["jersey"] != $club->jersey)
          $sql .= "jersey='" . addslashes($club->jersey) . "', ";
        if ($sql[strlen($sql) - 2] == ',') {
          $sql[strlen($sql) - 2] = ' ';
          $sql .= "where num=$club->num";
          if ($row["updated"])
            echo "<div style=color:orange>le club devrait être modifié par sql=$sql</div>" . NL;
          else {
            //echo "sql=$sql".NL;
            jmysql_query($sql, 1);
          }
        }
      } else {
        $sql = "insert into clubs set url='$club->url', num=" . $club->num . ", name='" . addslashes($club->name) . "', address='" . addslashes($club->address[0]) . "'" . ($club->address[1] ? ", address2='" . addslashes($club->address[1]) . "'" : '') . ($club->jersey ? ", jersey='" . addslashes($club->jersey) . "'" : '');
        //echo $sql.NL;
        jmysql_query($sql, 1);
      }
      $club = null;
    }
  }

  function test($str)
  {
    return utf8_decode($str);
  }
  