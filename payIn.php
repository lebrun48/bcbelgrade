<?php
include "admin.php";
include "common/tableTools.php";

$descr = array("Réduction", "Supplément", "Chèques", "Versement", "Transfert", "Cotisation", "Remboursement");

selectAction();
if ($action == "edit") {
  include "head.php";
  include "tools.php";
  include "common/tableRowEdit.php";

  $rowEdit = array("num"    => array(6, "Numéro : "),
      "name"   => array(0, "NOM Prénom : "),
      "amount" => array(8, "Montant : "),
      "descr"  => array(20, "Description : "),
      "detail" => array(200, "Détails : ")
  );

  class PayInEd extends TableRowEdit
  {

    //-------------------------------------------------------------------------------
    function applySave()
    {
      ?>
      el=document.getElementsByName('_val_descr')[0];
      am=document.getElementsByName('_val_amount')[0].value;
      if (el.value==1 || el.value==5 || el.value==6)
      {
      if (am<=0)
      {
      alert("Le montant doit être positif pour ce type d'opération!");
      return;
      } 
      }
      else if (am>=0)
      {
      alert("Le montant doit être négatif pour ce type d'opération!");
      return;
      }
      <?php
    }

    //-------------------------------------------------------------------------------
    function getTuple()
    {
      global $request;
      if ($m = $request["member"]) {
        unset($request["member"]);
        return array("num" => $m);
      }
      return TableRowEdit::getTuple();
    }

    //-------------------------------------------------------------------------------
    function ApplyValue(&$key, &$val, &$row)
    {
      switch ($key) {
        case "num":
          echo $row["num"];
          break;

        case "name":
          echo '<select name="_val_num">';
          $res = jmysql_query("select num,name,firstName from members where num in (select num from cotisations) order by name");
          while ($r = jmysql_fetch_row($res))
            echo '<option value="' . $r[0] . '"' . ($r[0] == $row["num"] ? ' selected="selected"' : '') . '>' . $r[1] . " " . $r[2] . '</option>';
          echo '</select>' . nl;
          break;

        case "amount":
          $res = jmysql_query("select members.category, members.dateIn, code_paie.first+code_paie.second+code_paie.third, cotisations.previous,status,code_paie.single,category.short from members,category,code_paie,cotisations where members.num=" . $row["num"] . " and (category.num=members.category or category.num=members.category-((members.category&0xFFFF) mod 10)) and code_paie.num=category.code and cotisations.num=members.num");
          if (jmysql_num_rows($res) > 1)
            while (($r = jmysql_fetch_row($res)) && !empty($r[6]));
          else
            $r = jmysql_fetch_row($res);
          $num = $r[3];
          while ($num) {
            $n++;
            $num = jmysql_result(jmysql_query("select previous from cotisations where num=$num"), 0);
          }
          $dt = new DateTime($r[1]);
          global $yearSeason;
          ?>
          <script type="text/javascript">
            function  computeReduction()
            {
              dt = new Date(<?php echo $dt->format("Y") . "," . $dt->format("n") . "," . $dt->format("j") . ",12,0,0" ?>);
              d = prompt("Entre la date d'arrivée", '<?php echo $dt->format("d/m/Y") ?>');
              sp = d.split('/');
              dt = new Date(sp[2], sp[1], sp[0], 12, 0, 0, 0);
              beg = new Date(<?php echo $yearSeason . ', 9, ' . ($r[0] == 240 ? 10 : 1) ?>, 12, 0, 0);
              dif = dt.getTime() - beg.getTime();
              wk = parseInt(dif / 1000 / 3600 / 24 / 7);
              am =<?php echo (($r[4] == 4 ? $r[5] : $r[2]) - 15) * (1 - $n * 0.25) ?>;
              dif = wk * am / 38;
              alert("Réduction (" + wk + " semaines) de " + am + "=" + dif);
            }
            function  computeAbsence()
            {
              d = prompt("Entre la date début d'absence (j/m/a)");
              sp = d.split('/');
              beg = new Date(sp[2], sp[1], sp[0], 12, 0, 0, 0);
              d = prompt("Entre la date fin d'absence (j/m/a)");
              sp = d.split('/');
              if (!d || sp[1] >= 6)
              {
                sp[0] = 30;
                sp[1] = 5;
                sp[2] =<?php echo ($yearSeason + 1) ?>;
              }
              end = new Date(sp[2], sp[1], sp[0], 12, 0, 0, 0);
              dif = end.getTime() - beg.getTime();
              wk = parseInt(dif / 1000 / 3600 / 24 / 7);
              dif = wk *<?php echo ($r[4] == 4 ? $r[5] : $r[2]) * (1 - $n * 0.25) ?> / 39 - 12;
              alert("Réduction (" + wk + " semaines)=" + dif);
            }
          </script>
          <?php
          TableRowEdit::ApplyValue($key, $val, $row);
          echo "&nbsp;&nbsp;";
          createAction(-1, "Calc. Réd.", "computeReduction()");
          createAction(-1, "Calc. Abs", "computeAbsence()");
          break;

        case "descr":
          echo '<select name="_val_descr">';
          global $descr;
          foreach ($descr as $k => $v)
            echo '<option value="' . $k . '"' . ($row[$key] == $k ? ' selected="selected"' : '') . '>' . $v . '</option>';
          echo '</select>' . nl;
          break;

        default:
          TableRowEdit::ApplyValue($key, $val, $row);
      }
    }

  }

  echo '<title>pay_in';
  echo '</title></head><body>';
  //dump($request);
  $ed = new PayInEd;
  $ed->title = "Donnée de table pay_in";
  $ed->colDef = &$rowEdit;
  $ed->editRow();
  exit();
} else if ($action == "check") {
  if (!($num = $request["n"]))
    stop(__FILE__, __LINE__, "No mum specified");

  if (jmysql_result(jmysql_query("select count(*) from pay_in where num=$num"), 0) == 0)
    header("location:payIn.php${basePath1}action=edit&member=$num&key=pay_in+where&_sel_name=" . jmysql_result(jmysql_query("select name from members where num=$num"), 0));
  else
    header("location:payIn.php${basePath1}_sel_name=" . jmysql_result(jmysql_query("select name from members where num=$num"), 0));
  exit();
}

//main table
include "common/tableEdit.php";

$col = array("num"    => array(4, "Num", "width:30", "text-align:right", "pay_in.num"),
    "name"   => array(20, "NOM Prénom", "width:150", "", "members.name"),
    "descr"  => array(0, "Description", "width:100", null, "pay_in.descr"),
    "amount" => array(6, "Montant", "width:70", "text-align:right", "pay_in.amount"),
    "detail" => array(10, "Détails", "width:800", null, "pay_in.detail")
);

//table itself
include "head.php";
echo '<title>pay_in';
echo '</title></head><body>';

class PayIn extends TableEdit
{

//---------------------------------------------------------------------------------------------------
  function printNumLine($num, &$row)
  {
    
  }

//---------------------------------------------------------------------------------------------
  public function applyDisplay(&$key, &$val, &$row, $idxLine)
  {
    switch ($key) {
      case "name":
        return $this->formatValue($row["name"] . " " . $row["firstName"], $val);

      case "descr":
        global $descr;
        return $this->formatValue($descr[$row[$key]], $val);

      case "amount":
        return $this->formatValue(sprintf("%.02f €", $row[$key]), $val);

      default:
        return TableEdit::applyDisplay($key, $val, $row, $idxLine);
    }
  }

//---------------------------------------------------------------------------------------------
  public function applyFilter(&$key, &$val)
  {
    switch ($key) {
      case "descr":
        global $descr;
        $sep = "descr in (";
        foreach ($descr as $k => $v)
          if (stripos($v, $val) !== false) {
            $t .= $sep . $k;
            $sep = ",";
          }
        if ($t)
          return $t . ')';
        return "descr=-1";

      default:
        return TableEdit::applyFilter($key, $val);
    }
  }

//---------------------------------------------------------------------------------------------
  function buildKey(&$row)
  {
    return "pay_in where ri=" . $row["ri"];
  }

}

$PayIn = new PayIn;
$PayIn->colDef = &$col;
$PayIn->tableName = "pay_in,members";
$PayIn->tableId = "members";
$PayIn->addCol = "ri,firstName";
$PayIn->defaultWhere = "members.num=pay_in.num AND ";
$PayIn->bHeader = true;
$PayIn->build();
?>
