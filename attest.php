<?php

include "common/common.php";
include "templateClub.php";

$cont[] = "<p>Je, soussigné Jacques Meirlaen, trésorier du club de basket New BC Alsavin-Belgrade, atteste avoir perçu le montant de 300 euros (trois cents) en date du 3 septembre 2015 pour le paiement de la cotisation 2015-2016 de Monsieur De Moor Nicolas inscrit en catégorie U21.</p>";

class Letter extends HeaderNBCB
{

  function Header()
  {
    $this->Template();
    $this->SetAutoPageBreak(true, 20);
    $this->SetMargins(50, 15, 20);
    $this->SetY(15);
  }

}

//$pdf = new Letter();
$pdf = new Letter();
$pdf->AddPage();
$pdf->SetFont("Arial", '', 28);
$pdf->SetXY(80, 60);
$pdf->WriteHTML("<u>Attestation</u>");
$pdf->Ln(20);
$pdf->SetFont("Arial", '', 13);
$pdf->SetAutoPageBreak(true, 20);
foreach ($cont as $v)
  $pdf->WriteHTML($v);
$pdf->SetLeftMargin(110);
$pdf->Ln(10);
$pdf->Write(4, "Fait à Belgrade le 13 octobre 2015");
$pdf->Ln(25);
$pdf->Write(4, "Jacques Meirlaen");
$pdf->Output("Attestation.pdf", 'I');

