<?php
$bOnlyRoot = true;
include "common/common.php";
include "head.php";

switch ($request["action"]) {
  case "reset":
    jmysql_query("update members set caution=2 where num in (" . $request["list"] . ")");
    break;
}

include "tools.php";
echo "<table class=main><tr><th><span class=button id=allNone>Tous</span></th><th>Nom Prénom</th><th style=width:70px>Date départ</th><th>Status</th><th style=width:150>Compte</th><th>Nom compte</th><th>Commmunication</tr>";
$r = jmysql_query("select name,firstName,dateOut,status&0x0F,num,equipement,equipSt from members where (type=0 or not (category&" . CAT_PLAYER . ")) and caution&5<>0 order by name");
while ($tup = jmysql_fetch_row($r)) {
  unset($prev);
  $r1 = jmysql_query("select concat(name,' ',firstName) from cotisations,members where previous=$tup[4] and members.num=cotisations.num");
  if ($r1 && jmysql_num_rows($r1))
    $prev = "<br>Frère:" . jmysql_result($r1, 0);
  echo "<tr><td><input type=checkbox value=1 name=_sel_$tup[4]></td><td>Remboursement caution $tup[0] $tup[1]$prev</td><td>" . ($tup[2] > "$yearSeason-" . NEW_SEASON_DATE ? "<span style=color:red>$tup[2]</span>" : $tup[2]) . "</td><td><span" . ($tup[3] < 3 ? " style=color:red" : '') . ">$tup[3]</span><span  style=margin-left:3px" . (!$tup[6] ? ";color:red>NOK" : '>OK') . " $tup[5]</span></td><td>";

  $col1 = $col2 = $col3 = '';
  $r1 = jmysql_query("select accountNb,orig,detail from billerNBCB where detail like '%$tup[0]%' and detail like '%$tup[1]%' and accountNb is not null group by accountNb order by date desc");
  while ($ac = jmysql_fetch_row($r1)) {
    $col1 .= "$ac[0]</br>";
    $col2 .= "$ac[1]</br>";
    $col3 .= "$ac[2]</br>";
  }
  echo substr($col1, 0, -5) . "</td><td>" . substr($col2, 0, -5) . "</td><td>" . substr($col3, 0, -5) . "</td></tr>";
}
echo "</table>";
createAction(-1, "Mettre à 'Rembourser'", "setReimbursement()");
?>
<script>
  $(function () {
    $("#allNone").click(function () {
      if (this.innerHTML == "Tous") {
        this.innerHTML = "Aucun";
        $(":checkbox").each(function () {
          this.checked = true;
        });
      } else {
        this.innerHTML = "Tous";
        $(":checkbox").attr("checked", false);
      }
    });
  });


  function setReimbursement() {
    var list = '';
    $(":checked").each(function () {
      list += ',' + this.name.substr(5);
    });
    if (!list) {
      alert("Aucun sélectionné");
      return;
    }
    location.replace("checkCaution.php<?php echo $basePath1 ?>action=reset&list=" + list.substr(1));
  }
</script>    

