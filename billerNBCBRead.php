<?php

include "billerNBCBConf.php";
$currentYear = $yearSeason;
$defaultValue = ",hidden=0";
//$bSimul=3;
include "common/billerRead.php";

function translateCols(&$obj)
{
  switch ($obj->account) {
    case 2: case 3: case 5: case 6: //FORTIS
      if (($i = stripos($obj->detail, " EXECUTE LE")) || ($i = stripos($obj->detail, " DATE VALEUR")))
        $obj->detail = substr($obj->detail, 0, $i);
      if (preg_match("/BE\d+|\d+/", $obj->orig)) {
        $obj->accountNb = $obj->orig;
        $obj->orig = '';
      }
      if ($i = preg_match("/(.*) (PAS DE COMMUNICATION|COMMUNICATION\s?:\s?(.*))/", $obj->detail, $matches)) {
        $obj->detail = $matches[3] ? $matches[3] : '';
        if (!$obj->orig) {
          $obj->orig = $matches[1];
          if (preg_match("/(.*) BE\d*/", $obj->orig, $matches))
            $obj->orig = $matches[1];
        }
      } else if (!$obj->orig) {
        $obj->orig = preg_match("/(.*) BE\d+/", $obj->detail, $matches) ? $matches[1] : $obj->detail;
        $obj->detail = '';
      }
      return;

    case 52: //ING
    case 53:
      do
        $obj->orig = str_replace('  ', ' ', $obj->orig, $n); while ($n);
      do
        $obj->detail = str_replace('  ', ' ', $obj->detail, $n); while ($n);
      if (strpos($obj->orig, "Décompte de frais") === 0) {
        $obj->orig = strtok($obj->orig, "+-");
        return;
      }

      strtok($obj->orig, "+-");
      $obj->orig = strtok(null);
      $obj->orig = substr($obj->orig, strpos($obj->orig, ' ', 1) + 1);
      $i = strpos($obj->orig, " Communication:");
      if ($i !== false)
        $obj->orig = substr($obj->orig, 0, $i);
      $i = strpos($obj->orig, ",");
      if ($i !== false)
        $obj->orig = substr($obj->orig, 0, $i);
      $obj->orig = str_replace("Vers: ", '', $obj->orig);
      $obj->orig = str_replace("De: ", '', $obj->orig);
      return;
  }
}
