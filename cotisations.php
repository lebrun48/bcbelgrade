<?php
$bSession=true;
include "admin.php";
include "common/tableTools.php";
include "cotiConf.php";
//dump($request,"req");
//$seeRequest=3;
switch ($action) {
  case "save":
    if ($n = $request["list"])
      $bList = true;
    else
      $n = str_replace('$', "'", $request["key"]);
    $n = str_replace("cotisations.", '', $n);
    $where = strstr($n, "where");
    $t = $request["_val_status"];
    jmysql_query("update members set " . (isset($t) ? "status=$t" : '') . (!$bList ? ", caution=" . $request["_val_caution"] : '') . " $where");
    unset($request["_val_caution"]);
    unset($request["_val_status"]);
    break;

  case "delete":
    //$seeRequest=1;
    if (!($list = $request["list"]))
      $list = substr($request["key"], strpos($request["key"], "=") + 1);
    $num = strtok($list, "(),");
    while ($num) {
      $prev = jmysql_result(jmysql_query("select previous from cotisations where num=$num"), 0);
      $r = jmysql_query("select num from cotisations where previous=$num");
      if (jmysql_num_rows($r))
        jmysql_query("update cotisations set previous=" . (strlen($prev) ? $prev : "null") . " where num=" . jmysql_result($r, 0, 0));
      $num = strtok("(),");
    }
    break;
}

selectAction();
//unset($seeRequest);
switch ($action) {
  case "edit":
    include "cotisationsEdit.php";
    exit();

  case "single":
    $payDate[0] = new dateTime("$yearSeason-09-15");
    break;

  case "family":
    //$seeRequest=1;
    $r = jmysql_query("select cotisations.num, address,name,firstName from cotisations,members where members.num=cotisations.num and previous is null and (status=10 or status=0)");
    while ($tup = jmysql_fetch_assoc($r)) {
      //dump($tup);
      $r1 = jmysql_query("select members.num, concat(members.name,' ',members.firstName,'. Adresse=',members.address ) from cotisations,members where cotisations.num<>" . $tup["num"] . " and previous is null and members.num=cotisations.num " . ($l ? "and cotisations.num not in (" . substr($l, 0, -1) . ")" : '') . " and (emailF is not null and emailF='" . $tup["emailF"] . "' or emailM is not null and emailM='" . $tup["emailM"] . "' or address='" . jmysql_real_escape_string($tup["address"]) . "')");
      $l .= $tup["num"] . ",";
      while ($tup1 = jmysql_fetch_row($r1)) {
        $ar[] = $tup["num"];
        $ar[] = $tup1[0];
        echo $tup1[1] . NL . "<span style=color:blue>" . $tup["name"] . " " . $tup["firstName"] . ". Adresse:" . $tup["address"] . "</span>" . NL;
      }
    }
    if (sizeof($ar)) {
      $request["_sel_cotisations.num"] = "_c=" . serialize($ar);
      $request["order"] = "name,category";
    } else
      echo "<h3>Pas de nouvelle famille trouvée</h3>";
    break;
}
include "common/tableEdit.php";
//$seeRequest=true;

$col = array("name"     => array(20, "NOM Prénom", "min-width:180; width:180"),
    "status"   => array(-1, "Status", "min-width:80; width:80"),
    "rest"     => array(0, "Reste", "min-width:60; width:60", "text-align:right", "@"),
    "rest1"    => array(0, "1", "min-width:60; width:60", "text-align:right", "@"),
    "rest2"    => array(0, "2", "min-width:60; width:60", "text-align:right", "@"),
    "rest3"    => array(0, "3", "min-width:60; width:60", "text-align:right", "@"),
    "caution"  => array(-1, "Caution", '', "text-align:center"),
    "comment"  => array(0, "Commentaire", "min-width:100; width:100", null, "cotisations.comment"),
    "category" => array(-1, "Catégorie", "min-width:150; width:100"),
    "previous" => array(0, "Précédent", "min-width:100; width:100"),
);

$others = "cotisations.num,members.id,members.firstName,cotisations.code_cat,members.type";
if ($bRoot)
  $visible = array("name", "status", "to_pay", "paid", "rest", "rest1", "rest2", "rest3", "caution", "comment", "category", "previous");
else
  $visible = array("name", "status", "to_pay", "paid", "rest", "rest1", "rest2", "rest3", "caution", "comment", "category");

if ($_GET["check"]) {
  $r = jmysql_query("select num from members where category&" . CAT_PLAYER . " and num not in (select num from cotisations) and type<>0" . ($_GET["check"] == "all" ? '' : " and (AWBB_id is not null or (category&0xFFFF) div 10 =95)"));
  if (!$r)
    stop(__FILE__, __LINE__, "slqError:" . jmysql_error());

  while ($row = jmysql_fetch_row($r)) {
    jmysql_query("insert into cotisations set num=$row[0]", 1);
    jmysql_query("update members set status=10,caution=0 where num=$row[0] and status is null", 1);
    $checkNums .= ",$row[0]";
  }
}

if ($bRoot) {
  $r = jmysql_query("select * from memberHistory order by num");
  while ($tup = jmysql_fetch_assoc($r)) {
    $r1 = getCategory($tup["oldCategory"]);
    if ($r1["code"])
      $memberHistory[$tup["num"]] = htmlspecialchars("Ancienne catégorie: '" . $r1["name"] . "' changée le " . $tup["date"], ENT_QUOTES);
  }
}

//-----------------------------------------------------------------------------------------------------------------------
class Cotisations extends TableEdit
{

  public $bNoCaution;
  public $pay;
  public $toPay;

//-----------------------------------------------------------------------------------------
  public function addHeaderFunction()
  {
    global $bRoot;
    if ($bRoot) {
      global $request;
      if (isset($request["single"]))
        echo "<a href=# onclick=actionClick('normal','','')><img height=40 width=40 style=border-style:none src=single.jpg title='Voir normal'></a>" . nl;
      else
        echo "<a href=# onclick=actionClick('single','','')><img height=40 width=40 style=border-style:none src=single.jpg title='Voir en paiement unique'></a>" . nl;
    }
  }

//-----------------------------------------------------------------------------------------
  public function buildKey(&$row)
  {
    return "cotisations where cotisations.num=" . $row["num"];
  }

//-----------------------------------------------------------------------------------------
  public function applyStyle(&$row)
  {
    global $bDeleted;
    return "color:black" . (!$bDeleted && !$row["type"] ? "; text-decoration:line-through" : '');
  }

//---------------------------------------------------------------------------------------------------
  function printNumLine($num, &$row)
  {
    global $basePath1;
    if (!$row["num"])
      return TableEdit::printNumLine($num, $row);
    global $id;
    return "<a style=color:black target=_blank href=membersClub.php${basePath1}action=filter&_sel_num=%3D" . $row["num"] . ">$num</a>";
  }

//-----------------------------------------------------------------------------------------
  public function printLine(&$row)
  {
    //Already printed or removed do nothing
    if ($this->coti->bAlready[$row["num"]])
      return;

    //Entry must be the first in family
    $prev = $row["previous"];
    $loop = 10;
    while ($prev) {
      if (! --$loop) {
        echo __LINE__.": boucle détecté avec $prev";
        exit();
      }
      //echo "prev=$prev".NL;
      if ($t = jmysql_result(jmysql_query("select previous from cotisations where num=$prev"), 0))
        $prev = $t;
      else {
        $row = jmysql_fetch_assoc($this->buildSql("$this->defaultWhere cotisations.num=$prev"));
        $this->printLine($row);
        return;
      }
    }

    //First in family
    global $bDeleted;
    unset($this->coti->totFam);
    $this->coti->n = 0;
    $this->coti->getSQL($row);
    if ($row["type"] && !$bDeleted)
      parent::printLine($row);
    $loop = 10;
    while ($t = $this->coti->row["next"]) {
      if (! --$loop) {
        echo __LINE__.": boucle détecté avec $prev";
        exit();
      }
      $this->coti->n++;
      $row = jmysql_fetch_assoc($this->buildSql("$this->defaultWhere cotisations.num=$t"));
      $this->coti->getSQL($row);
      parent::printLine($row);
    }
    global $bRoot;
    if ($this->coti->bTest("isFamily")) {
      echo '<tr><td class=fam colspan=' . ($bRoot ? 4 : 2) . ' style="text-align:center">Total famille</td>';
      echo '<td class=fam style="text-align:right;color:gray">' . sprintf("%0.2f", $this->coti->totFam["single"]) . '</td>';
      if ($this->coti->bTest("bThreeTimes")) {
        echo '<td class=fam style="text-align:right;color:darkcyan">' . $this->coti->getLine("§firstFam§") . '</td>';
        echo '<td class=fam style="text-align:right;color:darkcyan">' . $this->coti->getLine("§secondFam§") . '</td>';
        echo '<td class=fam style="text-align:right;color:darkcyan">' . $this->coti->getLine("§thirdFam§") . '</td>';
      }
      echo '<td class=fam colspan="' . ($bRoot ? 4 : 3) . '"></td></tr>';
    }
  }

//-----------------------------------------------------------------------------------------
  public function terminate()
  {
    global $bRoot;
    $nCoti = jmysql_result(jmysql_query("select count(*) from cotisations"), 0);
    echo '<tr class="havy-border" style="color:black"><td colspan=' . ($bRoot ? 4 : 2) . ' style="text-align:center"><b>Caution: ' . $this->nCautionPaid . "/" . $this->nCautionToPay . ' (' . ($this->nCautionPaid * CAUTION_VALUE) . "€)     Nb cotisations: $this->numLine/$nCoti</b></td>";
    echo '<td class="alr" style="font-weight:bold">' . sprintf("%.02f €", $this->sumRest[0]) . "</td>";
    for ($i = 1; $i < 4; $i++)
      echo '<td class="alr" style="color:blue">' . sprintf("%.02f €", $this->sumRest[$i]) . '</td>';
    echo '<td colspan="' . ($bRoot ? 5 : 3) . '"></td></tr>';
    echo '<tr class="havy-border" style="color:black"><td colspan=' . ($bRoot ? 4 : 2) . ' style="text-align:center"><b>Total sans cautions</b></td>';
    echo '<td class="alr" style="font-weight:bold">' . sprintf("%.02f €", $this->sumRest[0] - $this->sumCautionToPay) . '</td>';
    for ($i = 1; $i < 4; $i++)
      echo '<td class="alr" style="color:blue">' . sprintf("%.02f €", $this->sumRest[$i] - ($i == 1 ? $this->sumCautionToPay : 0)) . '</td>';
    echo '<td colspan="' . ($bRoot ? 5 : 3) . '" style="text-align:center; font-weight:bold; color:crimson">Cautions non payées: ' . sprintf("%.02f €", $this->sumCautionToPay) . ' (' . ($this->sumCautionToPay / CAUTION_VALUE) . ')</td></tr>';
  }

//-----------------------------------------------------------------------------------------
  function buildHelpArray(&$key, &$where)
  {
    switch ($key) {
      case "status":
        global $statusDef;
        return $statusDef;

      case "category":
        return getExistingCategories("category&" . CAT_PLAYER . " and type<>0 and", false);

      case "caution":
        $a['P'] = "Pay ";
        $a['F'] = "Fam.";
        $a['V'] = "Vers ";
        $a['X'] = "Nok";
        $a['R'] = "Remb.";
        return $a;
    }
    return TableEdit::buildHelpArray($key, $where);
  }

//-----------------------------------------------------------------------------------------
  public function applyDisplay(&$key, &$val, &$row, $idxLine)
  {
    switch ($key) {
      case "sel":
        return $this->formatValue("<input type=checkbox checked=checked name=select value=" . $row["num"] . '>', $val);

      case "name":
        global $nums;
        $nums .= ',' . $row["num"];
        global $memberHistory, $basePath;
        if (isset($memberHistory[$row["num"]]))
          return $this->formatValue("<span name=blinked title='" . $memberHistory[$row["num"]] . "'><a style=font-weight:bold;color:red class=tabLink onclick=openWindow('cotiPDF.php$basePath&id=" . $row["id"] . "')>" . $row["name"] . " " . $row["firstName"] . "</a></span>", $val, $row["name"] . " " . $row["firstName"]);
        return $this->formatValue("<a class=tabLink onclick=openWindow('cotiPDF.php$basePath&id=" . $row["id"] . "')>" . $row["name"] . " " . $row["firstName"] . "</a>", $val, $row["name"] . " " . $row["firstName"]);

      case "category":
        return TableEdit::applyDisplay($key, $val, $row, $idxLine, getCategory($row[$key], true));

      case "status":
        global $statusDef;
        return TableEdit::applyDisplay($key, $val, $row, $idxLine, $statusDef[$row["status"] & 0x0F]);

      case "previous":
        if ($row[$key])
          return TableEdit::applyDisplay($key, $val, $row, $idxLine, jmysql_result(jmysql_query("select concat(name,' ',substr(firstName,1,1), '.') from members where num=$row[$key]"), 0));
        break;

      case "rest":
        $dispVal = !($rest = $this->coti->getLine("§singleTot§")) ? '' : "<div style=font-weight:bold" . ($rest < 0 ? ';color:red' : '') . ">$rest</div>";
        $this->sumRest[0] += $rest;
        return parent::applyDisplay($key, $val, $row, $idxLine, $dispVal);

      case "rest1":
        $dispVal = !($rest = $this->coti->getLine("§firstTot§")) ? '' : "<div style=font-weight:bold" . ($rest < 0 ? ';color:red' : '') . ">$rest</div>";
        $this->sumRest[1] += $rest;
        return parent::applyDisplay($key, $val, $row, $idxLine, $dispVal);

      case "rest2":
        $dispVal = !($rest = $this->coti->getLine("§secondTot§")) ? '' : "<div style=font-weight:bold" . ($rest < 0 ? ';color:red' : '') . ">$rest</div>";
        $this->sumRest[2] += $rest;
        return parent::applyDisplay($key, $val, $row, $idxLine, $dispVal);

      case "rest3":
        $dispVal = !($rest = $this->coti->getLine("§thirdTot§")) ? '' : "<div style=font-weight:bold" . ($rest < 0 ? ';color:red' : '') . ">$rest</div>";
        $this->sumRest[3] += $rest;
        return parent::applyDisplay($key, $val, $row, $idxLine, $dispVal);

//Caution
//bit 0x00 Frére ou soeur ou non payée
//bit 0x01 Payée
//bit 0x02 Remboursée
//bit 0x04 Payée sur versement
//bit 0x08 
//bit 0x10 Pas de caution
//bit 0x20 Caution obligatoire
      case "caution":
        if ($this->coti->pay[5] || $row["caution"] & 0x10)
          $dispValue = 'N';
        else if ($row["caution"] & 0x01) {
          $dispValue = 'P';
          $this->nCautionPaid++;
          $this->nCautionToPay++;
        } else if ($row["caution"] & 0x04) {
          $this->nCautionPaid++;
          $this->nCautionToPay++;
          $dispValue = 'V';
        } else if ($row["previous"])
          $dispValue = 'F';
        else {
          $dispValue = "<div style=\"color:red;font-weight:bold\">";
          if ($row["caution"] & 0x02)
            $dispValue .= 'R';
          else {
            $this->nCautionToPay++;
            $dispValue .= 'X';
          }
          $dispValue .= "</div>";
        }
        return TableEdit::applyDisplay($key, $val, $row, $idxLine, $dispValue);
    }
    return TableEdit::applyDisplay($key, $val, $row, $idxLine);
  }

//-----------------------------------------------------------------------------------------
  public function applyFilter(&$key, &$val)
  {
    switch ($key) {
      case "category":
        return TableEdit::buildFilter("members.category", $val);

      case "status":
        return TableEdit::buildFilter("(members.status&0x0F)", $val);

      case "name":
      case "previous":
      case "cotisations.num":
        return TableEdit::applyFilter($key, $val);

//Caution
//bit 0x00 Frére ou soeur ou non payée
//bit 0x01 Payée
//bit 0x02 Remboursée
//bit 0x04 Payée sur versement
//bit 0x08 
//bit 0x10 Pas de caution
//bit 0x20 Caution obligatoire
      case "caution":
        $val = unserialize(substr($val, 3));
        $wh = '(';
        foreach ($val as $v)
          switch ($v) {
            case "X":
              $wh .= "(members.category&0xFFFF<950 or members.category&0xFFFF>=1000) and ((caution&0x1f)=0 and previous is null or (caution&0x2F)=0x20) or ";
              break;

            case "F":
              $wh .= "caution=0 and previous is not null or ";
              break;

            case "V":
              $wh .= "caution&0x04 or ";
              break;

            case "P":
              $wh .= "caution&0x01 or ";
              break;

            case "R":
              $wh .= "caution&0x02 or ";
              break;
          }
        return substr($wh, 0, -4) . ')';

      default:
        return "1";
    }
  }

}

//-----------------------------------------------------------------------------------------------------------------------------
include "DetailPay.php";

class Coti extends DetailPay
{

  function getSQL($row)
  {
    parent::getSQL($row);
    unset($this->tot);
    while ($this->bTest("lineTab")) {
      $this->getLine("§single§");
      if ($this->bTest("bThreeTimes")) {
        $this->getLine("§first§");
        $this->getLine("§second§");
        $this->getLine("§third§");
      }
    }
  }

  function initialize()
  {
    $this->date = time();
    parent::initialize();
    $this->nLevel = -1;
  }

}

//------------------------------------------------------------------------------------------
//$seeRequest=1;
$cotisations = new Cotisations;
$cotisations->colDef = &$col;
$cotisations->addCol = &$others;
$cotisations->visible = &$visible;
$cotisations->tableName = "cotisations,members";
$cotisations->tableId = "members";
$cotisations->defaultWhere = "members.num=cotisations.num and";
$cotisations->coti = new Coti;
$cotisations->coti->initialize();
if ($bDeleted = (bool) $_GET["deleted"])
  $cotisations->defaultWhere .= " members.type=0 and";
if ($_GET["check"] && $checkNums)
  $cotisations->defaultWhere .= " cotisations.num in (" . substr($checkNums, 1) . ") and";
if ($bRoot) {
  $cotisations->bHeader = true;
  $cotisations->bPrintNumLine = true;
  $cotisations->selKey = "num";
  $cotisations->bExportAvailable = true;
  $cotisations->exportFileName = "cotisations.xls";
}
$cotisations->nums = ',';

if ($action == "single") {
  $cotisations->paramArray["single"] = "y";
  $request["single"] = "y";
} else if ($action == "normal") {
  unset($cotisations->paramArray["single"]);
  unset($request["single"]);
}

include "head.php";
//table itself
echo '<title>Cotisations';
echo '</title></head><body onload="genericLoadEvent()">';

//Build list first
$r = jmysql_query("select cotisations.num from $cotisations->tableName where $cotisations->defaultWhere " . $cotisations->buildwhere());
while ($t = jmysql_fetch_row($r)) {
  //dump($t);
  $cotisations->bIn[$t[0]] = true;
}
$cotisations->build();

if ($bRoot) {
  include_once ("tools.php");
  createAction(-1, "Insérer nouveaux", "insertNew()", 120);
  createAction(-1, "Générer 1 fichier", "generateFiles(1)");
  createAction(-1, "Générer tous", "generateFiles(2)");
  createAction(-1, "Générer mail", "generateFiles(3)");
  //createButton(500, $bDeleted ? "Voir affiliés" : "Voir supprimés", $_SERVER["PHP_SELF"].$basePath.(!$bDeleted ? "&deleted=y":null),150);
  createButton(700, "Famille", $_SERVER["PHP_SELF"] . "$basePath&action=family", 120);
}
?>
<script type="text/javaScript">
  var elmts=new Array();
  var bShow=1;
  function loadEvent()
  {
  <?php if ($bMSIE) {
    ?>
    spans=document.getElementsByTagName('span');
    for (i=0; i<spans.length; i++)
    if (spans[i].name=='blinked')
    elmts.push(spans[i]);
    <?php
  } else
    echo "elmts=document.getElementsByName('blinked');" . nl
    ?>
  if (elmts)
  setTimeout("blink()",200+300*bShow);
  genericLoadEvent();
  }

  function blink()
  {
  bShow=1-bShow;
  for (i=0; i<elmts.length; i++)
  {
  if (bShow)
  elmts[i].style.visibility='visible';
  else
  elmts[i].style.visibility='hidden';
  }
  setTimeout("blink()", 200+300*bShow);
  }

  function insertNew()
  {
  //all=!confirm("Seulement ceux qui sont affiliés?");
  all=true;
  location.assign(location.href+(all ? "&check=all":"&check=only"));
  }

  function generateFiles(type)
  {
  els=document.getElementsByName("_select_");
  nums='';
  for (i=0;i<els.length; i++)
  if (els[i].checked)
  nums+=','+els[i].value;
  if (!nums)
  nums='<?php echo $nums ?>';
  if (type==3)
  window.open("buildMailCoti.php<?php echo $basePath ?>&list="+nums.substr(1,nums.length-1),'_blank');
  else  
  window.open("cotiPDF.php<?php echo $basePath ?>&nums="+nums.substr(1,nums.length-1)+"&action="+type,'_blank');
  }

  var win=null;
  function openWindow(loc)
  {
  if (win && !win.closed)
  win.location.replace(loc);
  else
  win=window.open(loc, "_blank",  "scrollbars=yes,left=800,width=740,height=800,titlebar=no,menubar=no,status=no,location=no");
  win.focus();
  return false;
  }

</script>

