<?php

define('EVENT_NUM', 0);
define('EVENT', "St Nicolas");
//-------------------------------------------------------------------------------------------------------------------------------------
$selTitle_19 = array(1 => "Volaille Sambre&Meuse<br>Enfant",
    "Rôti porc fumé<br>Enfant",
    "Volaille Sambre&Meuse<br>Adulte",
    "Rôti porc fumé<br>Adulte",
);

$selAmount_19 = array(1 => 10, 10, 17, 17, 0, 0);
$maxSel_19 = array(1 => 0, 0, 0, 0, 0, 0);
$comment_19 = array(1 => "orange", "cyan", "rouge", "bleu", "bleu", "vert");

//-------------------------------------------------------------------------------------------------------------------------------------
$selTitle_18 = array(1 => "Pennes J/Fr<br>Enfant",
    "Farfalles bolo<br>Enfant",
    "Poulet estragnon<br>Adulte",
    "Filet porc<br>Adulte",
    "Dessert<br>Mousse",
    "Dessert<br>Fruits"
);

$selAmount_18 = array(1 => 10, 10, 17, 17, 0, 0);
$maxSel_18 = array(1 => 0, 0, 0, 0, 0, 0);
$comment_18 = array(1 => "blanc", "orange", "jaune", "rouge", "bleu", "vert");

//-------------------------------------------------------------------------------------------------------------------------------------
$selTitle_15 = array(1 => "Pilon poulet<br>Enfant",
    "Porchetta<br>Enfant",
    "1/2 poulet<br>Adulte",
    "Porchetta<br>Adulte"
);

$selAmount_15 = array(1 => 9, 11, 14, 17);
$maxSel_15 = array(1 => 0, 0, 0, 0);

//-------------------------------------------------------------------------------------------------------------------------------------

$selTitle = array(1 => "Pilon poulet<br>Enfant",
    "Cochon de lait<br>Enfant",
    "1/2 poulet<br>Adulte",
    "Cochon de lait<br>Adulte"
);

$selAmount = array(1 => 8, 10, 12, 15);
$maxSel = array(1 => 0, 0, 0, 0);
