<?php

include "admin.php";
include "head.php";
$mysqlOp = 3;

//---------------------------------------------------------------------------------------------------------
function updCalendar(&$cal)
{
  global $yearSeason, $listNum, $now, $mysqlOp;
  //dump($cal, "cal");
  $d = strtok($cal->day, "-/");
  $m = strtok("-/");
  $y = strtok("-/");
  if (!$y)
    $y = $yearSeason + ($m < 7 ? 1 : 0);
  $h = strtok($cal->hr, ".:");
  $mm = strtok(".:");
  $dt = new DateTime("$y-$m-$d $h:$mm:00");
  //Store in list to see if not remove
  if ($dt >= $now)
    $listNum .= $listNum ? ",'$cal->num'" : "'$cal->num'";
//  $res=jmysql_query("select * from calendar where num='".$cal->num."'");
//  if (jmysql_num_rows($res)==0)
//  {
  $sql = "insert into calendar set num='" . $cal->num . "', team=" . $cal->team . ", home='" . $cal->home . "', away='" . $cal->away . "', date='" . $dt->format("Y-m-d H:i:s") . "', lastUpdated=NOW(), type=" . $cal->type . ($cal->comment ? ", comment='" . $cal->comment . "'" : '');
  jmysql_query($sql, $mysqlOp);
  /*  }
    else
    {
    if ($now->format("Y-m-d")<"$yearSeason-09-01")
    {
    $sql="update calendar set team=$cal->team, home='$cal->home', away='$cal->away', date='".$dt->format("Y-m-d H:i:s")."', type=$cal->type where num='$cal->num'";
    jmysql_query($sql,$mysqlOp);
    }
    else
    {

    //check what has changed
    $row=jmysql_fetch_assoc($res);
    $upd=0;
    $dtdb=new DateTime($row["date"]);
    if ($dtdb!=$dt)
    {
    $sql="date='".$dt->format("Y-m-d H:i:s")."'";
    $upd|=0x01;
    $com=$dtdb->format("d/m/Y H:i");
    }
    if ($row["home"]!=$cal->home)
    {
    $sql.=($upd ? ',': '')."home='".$cal->home."'";
    $com.=" à ".$row["home"];
    $upd|=0x02;
    }
    if ($row["away"]!=$cal->away)
    {
    $sql.=($upd ? ',': '')."away='".$cal->away."'";
    $com.=" à ".$row["away"];
    $upd|=0x04;
    }
    if ($upd)
    {
    $sql="update calendar set $sql, updated=$upd, comment='".($cal->comment ? $cal->comment.". " : '')."Auparavant: $com', lastUpdated=NOW() where num='".$cal->num."' and updated&0x80=0";
    jmysql_query($sql,$mysqlOp);
    }
    }
    } */
  $cal = null;
}

echo "<title>Import calendrier CP</title><body>" . nl;

$res = jmysql_query("select * from Calendar_CP");

while ($tup = jmysql_fetch_assoc($res)) {
  //dump($tup);
  if (!$tup["num"]) {
    if ($tup["home"] && !is_numeric($tup["home"][0])) {
      $teamName = $tup["home"];
      $team = getCategory($teamName);
      dump($team, "for '$teamName'");
      if ($team) {
        foreach ($team as $v)
          if (!$teams[$v]) {
            jmysql_query("delete from calendar where team=$v and type=0", $mysqlOp);
            $teams[$v] = true;
          }
      }
    }
    continue;
  }
  if (!$tup["hour"] || !is_numeric($tup["hour"][0]) || !strcasecmp($tup["home"], "bye") || !strcasecmp($tup["away"], "bye") || stripos($tup["away"], "FF général") || stripos($tup["home"], "FF général") || stripos($tup["away"], "forfait") || stripos($tup["home"], "forfait"))
    continue;

  if (stripos($tup["home"], KEY_CLUB) === false && stripos($tup["away"], KEY_CLUB) === false)
    continue;

  if (!$team) {
    echo "<div style='font-weight:bold; color:red'>La catégorie $teamName n'est pas enregistrée</div>" . NL;
    continue;
  }
  $i = 0;
  if (sizeof($team) > 1) {
    $bFound = false;
    foreach ($team as $i => $v) {
      $c = KEY_CLUB . " " . chr(ord('a') + $v % 10 - 1);
      if (stripos($tup["home"], "$c") !== false) {
        $bFound = true;
        break;
      }
    }
    if (!$bFound)
      foreach ($team as $i => $v) {
        $c = KEY_CLUB . " " . chr(ord('a') + $v % 10 - 1);
        if (stripos($tup["away"], "$c") !== false)
          break;
      }
  }

  $cal->num = $tup["num"];
  $cal->day = $tup["date"];
  $cal->hr = $tup["hour"];
  $cal->home = $tup["home"];
  $cal->away = $tup["away"];
  $cal->type = 0;
  $cal->team = $team[$i];

  updCalendar($cal);
  //$team=null;
}

  
  
