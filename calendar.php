<?php
$bSession = true;
include "common/common.php";
if ($action == "getLocation") {
  //$seeRequest=1;
  if (!$request["date"]) {
    if (!($r = jmysql_query("select home, hallAddr from calendar where rowid=" . $request["ri"])) || jmysql_num_rows($r) != 1) {
      echo "Désolé, l'adresse est inconnue pour cette rencontre.";
      exit();
    }
    $row = jmysql_fetch_row($r);
    if (is_numeric($row[1])) {
      echo file_get_contents("http://www.cpnamur.be/Jacques/utf8/calendar.php?action=getLocation&club=$row[1]");
      exit();
    }
    echo "<div style=margin:10px;font-size:15px;color:blue;>$row[0]</div>" . str_replace("\n", "<br>", $row[1]);
    exit();
  }
  echo file_get_contents("http://www.cpnamur.be/Jacques/utf8/calendar.php?action=getLocation&date=" . urlencode($request["date"]) . "&num=" . $request["num"] . "&club=" . KEY_CLUB);
  exit();
}
?>
<style>
  .locationButton:hover{background:gray;border-color:black}
  .locationButton {height:10px;padding:2px;cursor:pointer;vertical-align:middle}
</style>

<?php
$bAcceptNoId = true;
include "admin.php";
include "common/tableTools.php";
include "calendarData.php";
$activityTypes = array(CHAMPIONSHIP_NAT  => "Champ. Nat.",
    CHAMPIONSHIP_REG  => "Champ. Rég.",
    CHAMPIONSHIP_PROV => "Champ. Prov.",
    NAT_CUP           => "Coupe Nat.",
    REG_CUP           => "Coupe Rég.",
    PROV_CUP          => "Coupe Prov.",
    FRIENDLY          => "Amical",
    TOURNAMENT        => "Tournoi club",
    EXT_TOURNAMENT    => "Tournoi extérieur",
    BIP_BAP_BEN       => "BAB-BIP-BEN",
    PLAY_OFF          => "Play off",
    STAGE             => "Stage club",
    ACTIVITY          => "Activité club",
    OTHER             => "Information");
//dump($request,"request");

$rights = $id ? $admin["rights"] : -1;

if ($request["order"] == "day,hour")
  stop(0, 0, null, null, true);

//dump($request);
switch ($action) {
  case "edit":
    include "calendarEdit.php";
    exit();

  case "insert":
    $request["_val_lastUpdated"] = "=NOW()";
    unset($request["_val_keep"]);
    $request["_val_updated"] = $request["_val_extern"] ? 0x10 : 0;
    unset($request["_val_extern"]);
    insertAction();
    if ($request["_val_type"] <= 4) {
      $dt = new DateTime($request["_val_date"]);
      $window = "date like '" . $dt->format("Y-m-d") . "%'";
    }
    if ($request["_val_color"] == "FFFFFF")
      $request["_val_color"] = "=null";
    else
      $request["_val_color"] = "=0x" . $request["_val_color"];
    break;

  case "save":
    //check what has changed
    /*
      updated=
      0x01 date updated
      0x02 home updated
      0x04 away updated
      0x10 extern
      0x40 Row deleted
     */
    if ($request["_val_color"] == "FFFFFF")
      $request["_val_color"] = "=null";
    else
      $request["_val_color"] = "=0x" . $request["_val_color"];
    if ($request["key"]) {
      $row = jmysql_fetch_assoc(jmysql_query("select * from " . $request["key"]));
      $dtdb = new DateTime($row["date"]);
      $dt = new DateTime($request["_val_date"]);
      $upd = 0;
      if (!$request["_val_keep"] && $now->format("ymd") > "${yearSeason}0820") {
        if ($dtdb != $dt) {
          $upd |= 0x01;
          $com = $dtdb->format("d/m/Y H:i");
          $window = "date like '" . $dt->format("Y-m-d") . "%' or date like '" . $dtdb->format("Y-m-d") . "%'";
        } else
          $window = "date like '" . $dt->format("Y-m-d") . "%'";

        if ($row["home"] != $request["_val_home"]) {
          $com .= " & " . $request["_val_home"];
          $upd |= 0x02;
          $request["_val_clubNb"] = '';
        }
        if ($row["away"] != $request["_val_away"]) {
          $com .= " & " . $request["_val_away"];
          $upd |= 0x04;
        }
        if ($upd) {
          if (!$request["_val_comment"])
            $request["_val_comment"] = "Auparavant: $com";
          else if (stripos($request["_val_comment"], "Auparavant") === false)
            $request["_val_comment"] .= " (Auparavant: $com)";
        }
      }
      if ($request["_val_extern"])
        $upd |= 0x10;
      $request["_val_updated"] = $upd;
      unset($request["_val_extern"]);
      unset($request["_val_keep"]);
      $request["_val_lastUpdated"] = "=NOW()";
    }
    saveAction();
    break;

  case "delete":
    $cond = substr($request["key"], strpos($request["key"], "where"));
    $window = "date like '" . substr(jmysql_result(jmysql_query("select date from calendar $cond"), 0), 0, 10) . "%'";
    $sql = "update calendar set updated=updated+0x40 $cond";
    //echo "sql=$sql".NL;break;
    jmysql_query($sql);
    break;

  case "suppress":
    $cond = substr($request["key"], strpos($request["key"], "where"));
    $window = "date like '" . substr(jmysql_result(jmysql_query("select date from calendar $cond"), 0), 0, 10) . "%'";
    $sql = "delete from calendar $cond";
    //echo "sql=$sql".NL;break;
    jmysql_query($sql);
    break;
}


include "common/tableEdit.php";

$col = array("team"    => array(-1, "Equipe (Série)", "min-width:220px; width:220px"),
    "type"    => array(-1, "Activité", "min-width:80px; width:80px"),
    "num"     => array(8, "N° match", "min-width:60px; width:60px", "text-align:right"),
    "field"   => array(0, "T", "min-width:15px; width:15px", "text-align:center"),
    "day"     => array(-1, "Jour", "min-width:70px; width:70px", "text-align:center", "substr(date,1,10)"),
    "hour1"   => array(0, "Heure", "min-width:40px; width:40px", "text-align:center", "substr(date,12,5)"),
    "hour2"   => array(0, "Heure", "min-width:40px; width:40px", "text-align:center", "substr(date,12,5)"),
    "home"    => array(25, "Visité", "min-width:160px; width:160px"),
    "away"    => array(25, "Visiteur", "min-width:160px; width:160px"),
    "results" => array(0, "Scores", "min-width:60px; width:60px", "text-align:center"),
    "comment" => array(0, "Remarque"),
    "info"    => array(0, "Info (secrétaire)"),
);

if (!isset($request["sel"])) {
  $tup = jmysql_fetch_row(jmysql_query("select max(date), min(date) from calendar"));
  $dtMin = new DateTime($tup[1]);
  $dtMax = new DateTime($tup[0]);
  if ($now < $dtMin)
    $request["sel"] = 2;
  else if ($now > $dtMax && !$request["list"] && !$request["season"] && $dtMax->format('Y') > ($yearSeason + 1)) {
    if ($request["id"]) {
      header("Location:calendar.php$basePath&season=" . ($yearSeason - 1999));
      exit();
    }
    echo "<h1>Le calendrier de cette saison est terminé.<br>Le calendrier de la saison " . ($yearSeason + 1) . "-" . ($yearSeason + 2) . " sera disponible le 01 juillet " . ($yearSeason + 1);
    exit();
  }
}

class Calendar extends TableEdit
{
  /*
    //--------------------------------------------------------------------------------------
    function printLine(&$row)
    {
    static $curDate;
    if ($row["day"]!=$curDate && $this->order=="day" && !$this->filter["team"])
    {
    global $id;
    if ($curDate)
    echo "<tr><td colspan=".(sizeof($this->colDef)+isset($id))." style='text-align:center; color:#8BA8BD'>".str_repeat('-', 150)."</td></tr>";
    $curDate=substr($row["day"],0,10);
    }
    return TableEdit::printLine($row);
    }
   */

//---------------------------------------------------------------------------------------------------
  function buildSql($where)
  {
    if ($this->bCheck || $this->rowids->days) {
      //dump($this->rowids);
      if (!$this->rowids)
        $where = "date is null";
      else {
        $where = '(';
        foreach ($this->rowids->days as $key => $val) {
          $where .= "${sep}date like '$key%'";
          $sep = " or ";
        }
        $where .= ')';
      }
      $this->order = "day";
      $this->defaultWhere = null;
    }
    return TableEdit::buildSql($where);
  }

//---------------------------------------------------------------------------------------------------
  function buildKey(&$row)
  {
    return "calendar where rowid=" . $row["rowid"];
  }

//---------------------------------------------------------------------------------------------------
  function buildHelpArray(&$key, &$where)
  {
    switch ($key) {
      case "team":
        $r = jmysql_query("select team from calendar $where");
        while ($tup = jmysql_fetch_row($r))
          $cat[$tup[0]] = getCategory(CAT_PLAYER | $tup[0], true);
        $where = str_replace("by team", "by team1", $where);
        $r = jmysql_query("select team1 from calendar $where");
        while ($tup = jmysql_fetch_row($r))
          $cat[$tup[0]] = getCategory(CAT_PLAYER | $tup[0], true);
        if ($cat)
          ksort($cat);
        return $cat;

      case "type":
        global $activityTypes;
        return $activityTypes;

      case "day":
        global $sWeekDays;
        global $sMonths;
        $r = jmysql_query("select substring(date,1,10) as day from " . $this->tableName . $where);
        while ($tup = jmysql_fetch_row($r)) {
          $dt = new DateTime($tup[0]);
          $t[$tup[0]] = $sWeekDays[$dt->format("w")] . ' ' . $dt->format("d") . "-" . $sMonths[$dt->format("n")];
        }
        return $t;
    }
    return TableEdit::buildHelpArray($key, $where);
  }

//---------------------------------------------------------------------------------------------------
  function applyStyle(&$row)
  {
    global $rights;
    if ($rights == 0 && ($t = $row["color"]))
      $style = "background-color:#" . sprintf("%06X;", $t);
    if ($row["updated"] & 0x40)
      $style .= "text-decoration:line-through; ";
    return $style . TableEdit::applyStyle($row);
  }

//---------------------------------------------------------------------------------------------------
  function applyDisplay($key, $val, $row, $idxLine)
  {
    $dispValue = null;
    switch ($key) {
      case "team":
        $cat = getCategory($row["team"] ? CAT_PLAYER | $row["team"] : 0);
        if ($cat["teamSite"]) {
          $set = unserialize($cat["teamSite"]);
          $set = $set[$row["team"] % 10]->set;
          if ($set)
            $set = " (série $set)";
          else
            unset($set);
        }
        $dispValue = $cat["name"] . $set;
        if ($this->rowids->ri[$row["rowid"]] & 0x01) {
          TableEdit::storeDisplay($key, $val, $row, $idxLine, $dispValue);
          return "<td style='font-weight:bold; $val[3]'><span name=blinked>$dispValue</span></td>";
        }
        global $now, $activityTypes;
        if ($row["type"] == CHAMPIONSHIP_PROV && $row["num"][0] == 5 && ($t = substr($row["num"], 1, 2)) <= "70") {
          TableEdit::storeDisplay($key, $val, $row, $idxLine, $dispValue);
          return "<td><span title=Classement onclick='getRanking(" . ($t * 10) . ")' class=tabLink>$dispValue</span></td>";
        }
        if ($row[$key]) {
          TableEdit::storeDisplay($key, $val, $row, $idxLine, $dispValue);
          return "<td>$dispValue</td>";
        }
        TableEdit::storeDisplay($key, $val, $row, $idxLine, $dispValue);
        break;

      case "type":
        global $activityTypes;
        $dispValue = $activityTypes[$row[$key]];
        break;

      case "day":
        $dt = new DateTime($row[$key]);
        global $sWeekDays;
        global $sMonths;
        if ($row["updated"] & 0x01)
          $chg = 'font-weight:bold;color:red;';
        $dispValue = $sWeekDays[$dt->format("w")] . ' ' . $dt->format("d") . "-" . $sMonths[$dt->format("n")];
        $row["hour1"] = "<span style='$chg" . ($this->rowids->ri[$row["rowid"]] & 0x02 ? "; font-weight:bold' name=blinked" : "'") . ">" . $row["hour1"] . "</span>";
        $row[$key] = substr($row[$key], 0, 10) . '%';
        TableEdit::storeDisplay($key, $val, $row, $idxLine, $dispValue);
        if ($this->rowids->ri[$row["rowid"]] & 0x01)
          return "<td style='font-weight:bold; $val[3]; $chg'><span name=blinked>$dispValue</span></td>";
        return "<td style='$chg; $val[3]'><span>$dispValue</span></td>";

      case "hour1":
      case "hour2":
        global $rights;
        if ($rights == 0) {
          $dispValue = '';
          if (($t = $row["field"]) && $t <= 2) {
            if ($key[4] == $t)
              $dispValue = $row["hour1"];
          } else if ($key[4] == 1) {
            $this->write("<td colspan=2 style=$val[3]>" . $row["hour1"] . "</td>", true);
            return;
          } else
            return;
        }
        break;

      case "home":
      case "away":
        if ($row["type"] == ACTIVITY || $row["type"] == STAGE || $row["type"] == OTHER) {
          if ($key == "away")
            return;
          $col = $row["type"] == ACTIVITY ? "blue" : ($row["type"] == STAGE ? "#CD5C5C" : "#4169E1");
          $v[3] = "font-weight:bold;color:$col' colspan='4";
          TableEdit::storeDisplay($key, $v, $row, $idxLine, $row["comment"]);
          return "<td colspan=4 style=font-weight:bold;color:$col>" . $row["comment"] . "</td>";
        }
        $v[0] = $val[0];
        if ($bLocal = stripos($row[$key], KEY_CLUB) !== false)
          $v[3] = "font-weight:bold; color:green";
        if ($key == "home") {
          if ($row["hallAddr"])
            $dispValue = "<img class=locationButton src=location.png onclick=\"getLocation(" . $row["rowid"] . ")\">" . $row["home"];
          else if (!$bLocal && strcasecmp($row["home"], "bye") && $row["type"] == CHAMPIONSHIP_PROV)
            $dispValue = "<img class=locationButton src=location.png onclick=\"getLocation('" . $row["date"] . "'," . $row["num"] . ")\">" . $row["home"];
        }
        return parent::applyDisplay($key, $v, $row, $idxLine, $dispValue);

      case "results":
        if ($row["type"] == ACTIVITY || $row["type"] == STAGE || $row["type"] == OTHER)
          return;
        if ($v = &$row[$key])
          if (stripos($row["home"], KEY_CLUB) !== false)
            $v = '<b>' . strtok($v, '- ') . "</b> - " . strtok("- ");
          else
            $v = strtok($v, '- ') . ' - <b>' . strtok("- ") . "</b>";
        break;

      case "field":
        if ($this->rowids->ri[$row["rowid"]] & 0x02) {
          TableEdit::storeDisplay($key, $v, $row, $idxLine, $dispValue);
          return "<td style='font-weight:bold; $val[3]'><span name=blinked>$row[$key]</span></td>";
        }
        break;

      case "comment":
        if ($row["type"] == ACTIVITY || $row["type"] == STAGE || $row["type"] == OTHER)
          return;
    }
    return TableEdit::applyDisplay($key, $val, $row, $idxLine, $dispValue);
  }

  //--------------------------------------------------------------------------------------------
  function buildFilter($col, &$val)
  {
    if ($col == "team")
      return "(" . parent::buildFilter($col, $val) . " or " . parent::buildFilter("team1", $val) . ")";
    return parent::buildFilter($col, $val);
  }

  //----------------------------------------------------------------------------------------------------
  function applyOrder()
  {
    switch ($this->order) {
      case "day":
        $this->order = "date";
        return;

      default:
        return "order by $this->order $this->dir,date asc";
    }
  }

//----------------------------------------------------------------------------------------------------
  public function deleteClick($row)
  {
    return "deleteRow('" . $this->buildKey($row) . "')";
  }

}

$calendar = new Calendar;
$calendar->addCol = "rowid, date, hallAddr, clubNb, updated,color";
$calendar->tableId = "members";
$calendar->colDef = &$col;
$calendar->tableName = "calendar";
$calendar->bInsertNew = true;

if ($rights != 0) {
  unset($col["field"]);
  unset($col["hour2"]);
  unset($col["info"]);
} else {
  $calendar->selKey = "rowid";
  $col["hour1"][1] .= "<br>T1";
  $col["hour2"][1] .= "<br>T2";
  $calendar->bHeader = true;
  $calendar->bExportAvailable = true;
  $calendar->exportFileName = "calendrier_$yearSeason-" . ($yearSeason + 1) . ".xls";
  $calendar->bPrintNumLine = true;
}

if ($request["list"]) {
  $calendar->defaultWhere .= " rowid in (" . substr($request["list"], 1) . ") AND ";
  $sel = 4;
} else
  $sel = $request["sel"];

$beg = new DateTime();
$t = mktime(3, 0, 0, $beg->format('n'), $beg->format('j'), $beg->format('Y')) - ($beg->format("N") - 1) * 24 * 3600;
$beg = new DateTime('@' . $t);
$end = new DateTime('@' . ($t + 7 * 24 * 3600));
if (!isset($sel))
  $sel = !jmysql_result(jmysql_query("select count(*) from calendar where date>'" . $beg->format("Y-m-d H:i:s") . "' AND date<'" . $end->format("Y-m-d H:i:s") . "'"), 0) ? 2 : 0;
if (!$calendar->filter["day"])
  switch ($sel) {
    case 0:
      $dt = "date>'" . $beg->format("Y-m-d H:i:s") . "' AND date<'" . $end->format("Y-m-d H:i:s") . "'";
      $calendar->defaultWhere .= ($rights != 0 ? "$dt AND type=" . ACTIVITY . " OR " : '') . "$dt and";
      break;

    case 1:
      $beg = new DateTime();
      $t = mktime(3, 0, 0, $beg->format('n'), $beg->format('j'), $beg->format('Y')) - ($beg->format("N") - 1) * 24 * 3600 - 7 * 24 * 3600;
      $beg = new DateTime('@' . $t);
      $end = new DateTime('@' . ($t + 7 * 24 * 3600));
      $dt = " date>'" . $beg->format("Y-m-d H:i:s") . "' AND date<'" . $end->format("Y-m-d H:i:s") . "'";
      $calendar->defaultWhere .= ($rights != 0 ? "$dt AND type=" . ACTIVITY . " OR " : '') . "$dt and";
      break;

    case 2:
      $t = mktime(3, 0, 0);
      $beg = new DateTime('@' . $t);
      $dt = " date>'" . $beg->format("Y-m-d H:i:s") . "'";
      $calendar->defaultWhere .= ($rights != 0 ? "$dt AND type=" . ACTIVITY . " OR " : '') . "$dt and";
      break;

    case 3:
      $calendar->defaultWhere .= ($rights != 0 ? "date>='" . $now->format("Y-m-d 00:00:00") . "' AND type=" . ACTIVITY . " or" : '');
  }

if (!$calendar->order)
  $calendar->order = "day";

include "head.php";
//table itself
echo "<title>Calendrier $yearSeason-" . ($yearSeason + 1);
echo '</title></head><body onload="loadEvent()">';
include_once "tools.php";

if ($id)
  echo "<h2><img src=" . LOGO_LITTLE_CLUB . " style=vertical-align:middle>Calendrier " . NAME_CLUB . "</h2>";
if (!$request["list"]) {
  ?>
  <span class=button <?php if ($sel == 2) echo "style='box-shadow: 5px 5px 2px black'" ?> onclick="location.replace('<?php echo $_SERVER["SCRIPT_NAME"] . buildAllParam($calendar, "sel", "2") ?>')">A partir d'aujourd'hui</span>
  <span class=button <?php if ($sel == 0) echo "style='box-shadow: 5px 5px 2px black'" ?> onclick="location.replace('<?php echo $_SERVER["SCRIPT_NAME"] . buildAllParam($calendar, "sel", "0") ?>')">Cette semaine</span>
  <span class=button <?php if ($sel == 1) echo "style='box-shadow: 5px 5px 2px black'" ?> onclick="location.replace('<?php echo $_SERVER["SCRIPT_NAME"] . buildAllParam($calendar, "sel", "1") ?>')">Semaine passée</span>
  <span class=button <?php if ($sel == 3) echo "style='box-shadow: 5px 5px 2px black'" ?> onclick="location.replace('<?php echo $_SERVER["SCRIPT_NAME"] . buildAllParam($calendar, "sel", "3") ?>')">Saison complète</span>
  <?php
}

if ($rights == 0)
  echo "<a href=" . $_SERVER["REQUEST_URI"] . (!$_GET["action"] ? "&action=check" : null) . "><img src=check.png title='Vérifier le calendrier' style=margin-left:15px;width:30px height:30px border=none></a>";
if ($window) {
  include "calendarCheck.php";
  $calendar->rowids = checkCalendar($window, false);
} else if ($action == "check") {
  include "calendarCheck.php";
  $calendar->rowids = checkCalendar("date>=NOW()", false);
  $calendar->bCheck = true;
} else if ($id) {
  include "calendarCheck.php";
  $calendar->rowids = checkCalendar("date>=NOW()");
  unset($calendar->rowids->days);
}

if ($calendar->rowids)
  echo "<span name=blinked style='font-weight:bold; color:red'><img src=attention.jpeg width=20 height=20 style=margin-right:10> Il y a une incohérence dans le calendrier aux éléments qui clignotent</span>" . nl;

//echo "<span name=blinked><img style=float:left src=new.png height=40 width=40></span><span style=font-weight:bold>Cliquez sur le nom de l'Úquipe dans la 1<sup>er</sup> colonne pour voir son classement!</span>".NL;

$calendar->build();

if ($admin && $admin["rights"] == 0 && ($t = $calendar->filter["day"])) {
  $t = unserialize(substr($t, 3));
  if (sizeof($t) == 1)
    createAction(150, "Imprimer feuille des matches", "window.open('calendarSheet.php?day=$t[0]')", 250);
}

include "common/msgBox.php";
?>
<script>

  function getLocation(key, num)
  {
    if (!num)
      $.get("calendar.php", {action: 'getLocation', ri: key}, function (data) {
        msgBox(data, {dragDrop: false, width: '500px', title: "Adresse salle"});
      });
    else
      $.get("calendar.php", {action: 'getLocation', date: key, num: num, club: '<?php echo KEY_CLUB ?>'}, function (data) {
        msgBox(data, {dragDrop: false, width: '500px', title: "Adresse salle"});
      });
  }


  var elmts = new Array();
  var bShow = 1;
  function loadEvent()
  {
<?php if ($bMSIE) {
  ?>
      spans = document.getElementsByTagName('span');
      for (i = 0; i < spans.length; i++)
        if (spans[i].name == 'blinked')
          elmts.push(spans[i]);
  <?php
} else
  echo "elmts=document.getElementsByName('blinked');" . nl
  ?>
    if (elmts)
      setTimeout("blink()", 200 + 300 * bShow);
    genericLoadEvent();
  }

  function blink()
  {
    bShow = 1 - bShow;
    for (i = 0; i < elmts.length; i++)
    {
      if (bShow)
        elmts[i].style.visibility = 'visible';
      else
        elmts[i].style.visibility = 'hidden';
    }
    setTimeout("blink()", 200 + 300 * bShow);
  }

  var win = null;
  function getClub(n)
  {
    loc = 'http://documents.awbb.be/clubs/mat/club' + n;
    if (win && !win.closed)
      win.location.replace(loc);
    else
      win = window.open(loc, "_blank", "scrollbars=yes,left=0,width=800,height=600,titlebar=no,menubar=no,status=no,location=no");
    win.focus();
    return false;
  }
  function getRanking(n)
  {
    loc = 'ranking.php?team=' + n;
    if (win && !win.closed)
      win.location.replace(loc);
    else
      win = window.open(loc, "_blank", "scrollbars=yes,left=400,width=600,height=500,titlebar=no,menubar=no,status=no,location=no");
    win.focus();
    return false;
  }

  function deleteRow(key)
  {
    if (confirm("Es-tu sûr de vouloir supprimer cet enregistrement?"))
    {
      operation = document.getElementById("operation");
      operation.name = "key";
      operation.value = key;
      document.getElementById("scr").value = document.body.scrollTop;
      document.getElementsByName("action")[0].value = 'delete';
      if (confirm("De façon définitive (Ok) ou simplement en le barrant (Annuler)?"))
        document.getElementsByName("action")[0].value = 'suppress';
      else
        document.getElementsByName("action")[0].value = 'delete';
      document.getElementById("dummySubmit").click();
    }
  }


</script>
