<?php
include "admin.php";
include "common/tableTools.php";
//dump($request);
//$seeRequest=true;
//                   0=>Name       1=>Begin                    2=>End                      3=>nbcb 4=>out 5=>meal 6=>soup
$stages = array(array("août mini", new DateTime("2020-08-24"), new DateTime("2020-08-28"), 14, 15, 0, 0),
    array("août maxi", new DateTime("2015-08-17"), new DateTime("2015-08-21"), 0, 0, 0, 0),
    array("Xmas", new DateTime("2020-12-23"), new DateTime("2020-12-27"), 23.75, 27.5, 0, 0),
    array("carnaval", new DateTime("2013-04-08"), new DateTime("2013-04-08")),
    array("Pâques mini", new DateTime("2016-03-29"), new DateTime("2016-04-01"), 12.5, 13.75, 0, 0),
    array("Pâques maxi", new DateTime("2013-04-08"), new DateTime("2013-04-08")),
);

$noStage[2] = array(2 => true);
$stage = $request["stage"];
$nDays = 0;
for ($t = new DateTime($stages[$stage][1]->format("Y-m-d")); $t <= $stages[$stage][2]; $t->modify("+1 day"))
  $nDays++;
switch ($action) {
//----------------------------------------------------------------------------------
  case "insert":
    if ($request["_val_name"]) {
      $dt = $request["_val_birth_year"] . '-' . $request["_val_birth_month"] . '-' . $request["_val_birth_day"];
      $name = addslashes($request["_val_name"]);
      $fName = addslashes($request["_val_firstName"]);
      if (jmysql_result(jmysql_query("select count(num) from members where name='$name' and firstname='$fName' and birth='$dt'"), 0)) {
        include "tools.php";
        alert("Ce joueur est déjà dans nos membres!");
        break;
      }
      $request["user"] = jmysql_result(jmysql_query("select max(num) from members"), 0) + 1;
      $sql = "insert into members set num=" . $request["user"] . ", name='" . addslashes($name) . "', firstname='" . addslashes($fName) . "',id='" . getID($name, $fName, 8) . "', category=524300, sex='" . $request["_val_sex"] . "', " . (($t = $request["_val_emailF"]) ? "emailF='$t', " : '') . (($t = $request["_val_address"]) ? "address='" . addslashes($t) . "', " : '') . (($t = $request["_val_cp"]) ? "cp='$t', " : '') . (($t = $request["_val_town"]) ? "town='" . addslashes($t) . "', " : '') . "birth='$dt', dateIn=NOW(),type=1,extern=1";
      jmysql_query($sql);
      unset($request["_val_name"]);
      unset($request["_val_firstName"]);
      unset($request["_val_sex"]);
      unset($request["_val_emailF"]);
      unset($request["_val_address"]);
      unset($request["_val_cp"]);
      unset($request["_val_town"]);
      unset($request["_val_birth_day"]);
      unset($request["_val_birth_month"]);
      unset($request["_val_birth_year"]);
    }
    jmysql_query("insert into stages set stage=$stage, num=" . $request["user"]);
    $request["key"] = "stages where ri=" . jmysql_result(jmysql_query("select ri from stages where stage=$stage and num=" . $request["user"]), 0);
    unset($request["user"]);
  case "save":
    $request["_val_days"] = "='";
    for ($i = 0; $i < $nDays; $i++) {
      $request["_val_days"] .= $request["_val_day$i"];
      unset($request["_val_day$i"]);
    }
    $request["_val_days"] .= "'";
    saveAction();
    break;

//-------------------------------------------------------------------------------------------------
  case "delete":
    deleteAction();
    break;


//----------------------------------------------------------------------------------
  case "edit":
    include "common/tableRowEdit.php";
    include "head.php";
    echo "<title>Stage édition</title></head><body onload=dayChange(true)>" . nl;
    //dump($request);

    $col = array("name"      => array(40, "Nom: ", "[Mand]"),
        "firstName" => array(20, "Prénom: ", "[Mand]"),
        "birth"     => array(0, "Date naissance: ", "[Mand]"),
        "sex"       => array(0, "Sexe:", "[Mand]"),
        "address"   => array(60, "Adresse :"),
        "cp"        => array(4, "Code Postale :"),
        "town"      => array(20, "Localité :"),
        "emailF"    => array(40, "EMail :"),
        "days"      => array(0, "Jours :"),
        "price"     => array(10, "Prix stage :", 2, "priceChange(1)"),
        "meal"      => array(10, "Repas :", 2, "priceChange(2)"),
        "toPay"     => array(0, "A payer :"),
        "account"   => array(10, "Payé sur compte :", 2),
        "cash"      => array(10, "Payé en caisse :", 2),
        "comment"   => array(100, "Commentaire :")
    );

    class StageEdit extends TableRowEdit
    {

      //------------------------------------------------------
      function getTuple()
      {
        global $request, $nDays, $stages, $stage, $noStage;
        $this->bClearAll = false;

        if (!$request["key"]) {
          $d = 0;
          for ($t = clone($stages[$stage][1]); $t <= $stages[$stage][2]; $t->modify("+1 day")) {
            if (!$noStage[$stage][$d]) {
              $row["days"] .= '1';
              $nRealDays++;
            } else
              $row["days"] .= '4';
            $d++;
          }
          $row["meal"] = 0;
          $row["meal_soup"] = 0;
          $this->bSave = false;
          if (!$request["user"]) {
            $row["price"] = $nRealDays * $stages[$stage][4];
            $row["extern"] = 1;
            return $row;
          }
        }

        $this->colDef["name"][0] = 0;
        $this->colDef["name"][1] = "Nom :";
        $this->colDef["firstName"][0] = 0;
        $this->colDef["firstName"][1] = "Prénom :";
        unset($this->colDef["birth"]);
        unset($this->colDef["sex"]);
        unset($this->colDef["address"]);
        unset($this->colDef["cp"]);
        unset($this->colDef["town"]);
        unset($this->colDef["emailF"]);
        if (!$request["key"] && $request["user"]) {
          $row = array_merge($row, jmysql_fetch_assoc(jmysql_query("select name, firstName,extern from members where num=" . $request["user"])));
          $row["price"] = $nRealDays * $stages[$stage][3 + $row["extern"]];
          return $row;
        }
        $this->bNew = false;
        $r = TableRowEdit::getTuple();
        $request["user"] = $r["num"];
        $r = array_merge($r, jmysql_fetch_assoc(jmysql_query("select name,firstName,extern from members where num=" . $r["num"])));
        return $r;
      }

//-----------------------------------------------------------------------------------------
      function applyValue(&$key, &$format, &$row)
      {
        switch ($key) {
          case "name":
          case "firstName":
            if (!$row[$key])
              break;
            $this->colDef[$key][2] = '';
            echo "<span style=color:blue;font-weight:bold>$row[$key]</span>";
            return;

          case "birth":
            echo "<select name=_val_birth_day><option></option>";
            for ($i = 1; $i <= 31; $i++)
              echo "<option>" . sprintf("%02d", $i) . "</option>";
            echo "</select><select name=_val_birth_month><option></option>";
            global $months;
            for ($i = 0; $i <= 12; $i++)
              echo '<option>' . sprintf("%02d", $i) . '</option>';
            global $yearSeason;
            echo "</select><select name=_val_birth_year><option></option>";
            for ($i = $yearSeason - 4; $i >= $yearSeason - 20; $i--)
              echo "<option>$i</option>";
            echo "</select>";
            return;

          case "sex":
            echo "<select name=_val_sex><option></option><option value=M>Masculin</option><option value=F>Féminin</option></select>" . nl;
            return;

          case "days":
            echo "<table border=0px><tr><th></th>";
            global $stages, $stage, $nDays, $sWeekDays;
            for ($t = $stages[$stage][1]; $t <= $stages[$stage][2]; $t->modify("+1 day")) {
              echo "<th style='color:blue;border-left:1px solid black;border-bottom:1px solid black'>" . $sWeekDays[$t->format('w')] . NL . $t->format("d/m") . "</th>";
            }
            echo "</tr><tr><td style=color:blue>Présent</td>";
            for ($i = 0; $i < $nDays; $i++)
              echo "<td style='border-left:1px solid black;text-align:center'><input onchange=dayChange() type=radio name=_val_day$i value=1" . (($row[$key][$i] & 3) == 1 ? " checked" : '') . ($row[$key][$i] & 4 ? " disabled" : '') . "></td>";
            echo "</tr><tr><td style=color:blue>1/2 jour</td>";
            for ($i = 0; $i < $nDays; $i++)
              echo "<td style='border-left:1px solid black;text-align:center'><input onchange=dayChange() type=radio name=_val_day$i value=2" . (($row[$key][$i] & 3) == 2 ? " checked" : '') . ($row[$key][$i] & 4 ? " disabled" : '') . "></td>";
            echo "</tr><tr><td style=color:blue>Absent</td>";
            for ($i = 0; $i < $nDays; $i++)
              echo "<td style='border-left:1px solid black;text-align:center'><input onchange=dayChange() type=radio name=_val_day$i value=" . ($row[$key][$i] & 4) . (($row[$key][$i] & 3) == 0 ? " checked" : '') . "></td>";
            echo "</tr></table>";
            ?>
            <script type="text/javaScript">
              var update=[true,true]; 
              function dayChange(bCheck)
              {
              s=s1=0;
              for (i=0;i<<?php echo $nDays ?>; i++)
              {
              t=document.getElementsByName('_val_day'+i);
              for (j=0; j<t.length; j++)
              if (t[j].checked)
              {
              s+=t[j].value==2 ? 0.5:t[j].value/1;
              s1+=t[j].value==2 ? 0:t[j].value/1;
              break;
              }
              }
              if ((t=document.getElementsByName('_val_price')[0]).value && t.value!=(t1=s*<?php echo $stages[$stage][3 + $row["extern"]] ?>) && bCheck)
              update[0]=false;
              else if (update[0])
              t.value=t1;
              t=document.getElementsByName('_val_meal_soup'); 
              for (j=0; j<t.length; j++)
              if (t[j].checked)
              {
              s=s1*(t[j].value==0 ? 0: (t[j].value==1 ? <?php echo $stages[$stage][5] . ":" . $stages[$stage][6] ?>));  
              break;
              }
              if ((t=document.getElementsByName('_val_meal')[0]).value && t.value!=s && bCheck)
              update[1]=false;
              else if (update[1])
              t.value=s;
              priceChange();
              }

              function priceChange(idx)
              {
              if (idx)
              update[idx-1]=false;
              t=document.getElementsByName('_val_price')[0].value/1+document.getElementsByName('_val_meal')[0].value/1;
              document.getElementById('toPay').innerHTML=t.toFixed(2)+" €";
              } 
            </script>
            <?php
            return;

          case "meal":
            echo "<input type=radio name=_val_meal_soup value=0" . ($row["meal_soup"] == 0 ? " checked=checked" : '') . " onChange=dayChange()>Rien<input type=radio name=_val_meal_soup value=1" . ($row["meal_soup"] == 1 ? " checked=checked" : '') . " onChange=dayChange()>Repas<input type=radio name=_val_meal_soup value=2" . ($row["soup"] == 2 ? " checked=checked" : '') . " onChange=dayChange()>Soupe&nbsp;&nbsp;";
            TableRowEdit::applyValue($key, $format, $row);
            return;

          case "toPay":
            echo "<span style=color:blue;font-weight:bold id=toPay></span>";
        }
        TableRowEdit::applyValue($key, $format, $row);
      }

    }

    $obj = new StageEdit;
    $obj->title = "Inscription stage " . $stages[$stage][0];
    $obj->colDef = &$col;
    $obj->editRow();
    return;

  case "genAttest":
    header("Content-Type:text/html; charset=UTF-8", true);
    include "genAttest.php";
    exit();
}

//------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------
include "common/tableEdit.php";
include "head.php";
echo "<title>Stage " . $stages[$stage][0] . "</title>";
echo "</title></head><body onload=genericLoadEvent()>" . nl;

$col = array("name"     => array(1, "Nom", 'width:180px;min-width:180px', '', "concat(members.name,' ',members.firstName)"),
    "category" => array(-1, "Catégorie", 'width:100px;min-width:100px', null, "members.category"),
    "year"     => array(0, "An", null, "text-align:center", "mid(members.birth,3,2)")
);

$i = 0;
for ($t = new DateTime($stages[$stage][1]->format("Y-m-d")); $t <= $stages[$stage][2]; $t->modify("+1 day")) {
  $col["day$i"] = array(0, $sWeekDays[$t->format('w')] . NL . $t->format("d/m"), '', 'text-align:center', '@');
  $i++;
}
$col["price"] = array(0, "Stage", 'width:50px;min-width:50px', "[money]", "stages.price");
$col["meal"] = array(0, "Repas", 'min-width:90px;width:90px', "[money]", "stages.meal");
$col["toPay"] = array(0, "A payer", 'width:50px;min-width:50px', '[money];font-weight:bold', "@");
$col["account"] = array(0, "Compte", 'width:50px;min-width:50px', "[money]", "stages.account");
$col["cash"] = array(0, "Caisse", 'width:50px;min-width:50px', "[money]", "stages.cash");
$col["rest"] = array(0, "Reste", 'width:50px;min-width:50px', 'text-align:right;font-weight:bold', "@");
$col["comment"] = array(0, "Commentaire", '', '', "stages.comment");

class Stages extends TableEdit
{

//---------------------------------------------------------------------------------------------------
  function printNumLine($num, &$row)
  {
    if (!$row["num"])
      return TableEdit::printNumLine($num, $row);
    global $basePath1;
    return "<a style=color:black target=_blank href=membersClub.php${basePath1}action=filter&_sel_num=%3D" . $row["num"] . ">$num</a>";
  }

//---------------------------------------------------------------------------------------------------
  function applyDisplay(&$key, &$val, &$row, $idxLine)
  {
    $dispValue = null;
    switch ($key) {
      case "category":
        $dispValue = getCategory($row["category"], true);
        break;

      case "price":
        $this->sum[0] += $row["toPay"] = $row[$key];
        break;

      case "meal":
        $dispValue = $row["meal_soup"] == 0 ? '' : ($row["meal_soup"] == 1 ? "Repas: " : "Soupe: ") . number_format($row[$key], 2, ',', '.') . " €";
        $row["toPay"] += $row[$key];
        $this->sum[1] += $row[$key];
        break;

      case "account":
      case "cash":
        $row["toPay"] -= $row[$key];
      case "toPay":
        $this->sum[$key == "cash" ? 4 : ($key == "toPay" ? 2 : 3)] += $row[$key];
        break;

      case "rest":
        $dispValue = $row["toPay"] == 0 ? '' : "<span style=color:" . ($row["toPay"] < 0 ? "red>" : "blue>") . number_format($row["toPay"], 2, ',', '.') . " €</span>";
        $this->sum[5] += $row["toPay"];
        break;

      default:
        if (substr($key, 0, 3) == "day") {
          switch ($row["days"][$key[3]] & 3) {
            case 0:
              $dispValue = "<span style=color:red;font-weight:bold>X</span>";
              break;

            case 1:
              if ($row["meal_soup"]) {
                $t = "\$this->" . ($row["meal_soup"] == 1 ? "meal" : "soup") . "[$key[3]]++;";
                eval($t);
              }
              $this->pres[$key[3]] ++;
              $dispValue = "P";
              break;

            case 2:
              $dispValue = "<span style=color:blue>½</span>";
              $this->pres[$key[3]] ++;
              break;
          }
        }
    }
    return TableEdit::applyDisplay($key, $val, $row, $idxLine, $dispValue);
  }

//---------------------------------------------------------------------------------------------------
  function buildHelpArray(&$key, &$where)
  {
    switch ($key) {
      case "category":
        $r = jmysql_query("select members.category from members,stages $where");
        while ($tup = jmysql_fetch_row($r))
          $tab[$tup[0]] = $tup[0] ? getCategory($tup[0], true) : "&lt;Externe&gt;";
        return $tab;
    }

    return TableEdit::buildHelpArray($key, $where);
  }

//---------------------------------------------------------------------------------------------------
  function buildKey(&$row)
  {
    return "stages where ri=" . $row["ri"];
  }

//---------------------------------------------------------------------------------------------------
  function applyFilter(&$key, &$val)
  {
    switch ($key) {
      case "name":
        return $this->buildFilter("concat(name,' ',firstName)", $val);

      case "category":
        return $this->buildFilter("members.category", $val);
    }
  }

//---------------------------------------------------------------------------------------------------
  function terminate()
  {
    if (!$this->pres[0])
      return;
    global $nDays;
    $this->write("<tr><th class=bottom colspan=" . (4 - $this->bExport) . " style=text-align:center>Total</th>");
    for ($i = 0; $i < $nDays; $i++)
      $this->write("<th class=bottom>" . $this->pres[$i] . "</th>");
    for ($i = 0; $i < sizeof($this->sum); $i++)
      $this->write("<th class=bottom style=text-align:right>" . number_format($this->sum[$i], 2, ',', '.') . " €</th>");
    $this->write("<th class=bottom></th></tr><tr><th class=bottom colspan=4 style=text-align:right>Repas</th>");
    for ($i = 0; $i < $nDays; $i++)
      $this->write("<th class=bottom>" . $this->meal[$i] . "</th>");
    $this->write("</tr><tr><th class=bottom colspan=4 style=text-align:right>Soupes</th>");
    for ($i = 0; $i < $nDays; $i++)
      $this->write("<th class=bottom>" . $this->soup[$i] . "</th>");
  }

}

echo "<h2 style=width:100%;text-align:center>Stage " . $stages[$stage][0] . "</h2>";
$y = $now->format("Y");
if ($stage == 0 or $stage == 4)
  $y = " and mid(birth,1,4)>" . ($y - 13);
else if ($stage == 1 or $stage == 5)
  $y = " and mid(birth,1,4)<" . ($y - 11) . " and mid(birth,1,4)>" . ($y - 21);
else
  $y = " and mid(birth,1,4)>" . ($y - 21);
echo "<span style=font-size:14;font-weight:bold>Choisis un nom à insérer: </span><select name=_val_num onchange=actionClick('edit','user',this.value)>";
echo "<option></option><option value='' style=color:red>&lt;Extérieur ou nouveau&gt;</option>";
$r = jmysql_query("select num, category, concat(members.name,' ',members.firstName) from members where type<>0 and category=524300$y and num not in (select num from stages where stage=$stage) order by name");
echo "<optgroup label=Stagiaires>";
while ($tup = jmysql_fetch_row($r)) {
  $c = getCategory($tup[1]);
  $c = $c["short"];
  echo "<option value=$tup[0]>$tup[2]</option>";
}
$r = jmysql_query("select num, category, concat(members.name,' ',members.firstName) from members where type<>0 and category&" . CAT_PLAYER . "$y and num not in (select num from stages where stage=$stage) order by name");
echo "<optgroup label='Du club'>";
while ($tup = jmysql_fetch_row($r)) {
  $c = getCategory($tup[1]);
  $c = $c["short"];
  echo "<option value=$tup[0]>$tup[2]. ($c)</option>";
}

echo "</select></span>" . nl;

$obj = new Stages;
$obj->bExportAvailable = true;
$obj->exportFileName = str_replace('û', 'u', str_replace('â', 'a', str_replace(' ', '_', $stages[$stage][0] . ".xls")));
$obj->colDef = &$col;
$obj->bHeader = true;
$obj->bPrintNumLine = true;
$obj->defaultWhere = "stage=$stage and members.num=stages.num and";
$obj->tableName = "stages,members";
$obj->addCol = "stages.ri,stages.num,stages.days,stages.meal_soup";
if (!$obj->order) {
  $obj->order = "ri";
  $obj->dir = "desc";
}
$obj->tableId = "members";

$obj->build();
if ($bRoot)
  createButton(-1, "Générer attestations", "stages.php${basePath1}action=genAttest&stage=$stage");


