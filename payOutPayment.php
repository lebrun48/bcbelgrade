<?php
$bSession = true;
include "common/common.php";
include "payOutData.php";
include "head.php";
include "memberData.php";
echo '<title>Paiements';
echo '</title></head><body>' . nl;

//$seeRequest=1;
if (!jmysql_result(jmysql_query("select count(*) from pay_out where num=$member->num and type&0x3F<" . PLAYER_DESC), 0) || $request["user"] != "root" && jmysql_result(jmysql_query("select count(*) from pay_out where num=$member->num and type=" . NO_MAIL), 0))
  stop(__FILE__, __LINE__, "ended", null, true);
$y = $now->format('Y');
$m = $now->format("m");

if ($month = $_GET["month"]) {
  echo "<h2>Détail du paiement de " . $months[$month] . " pour " . $member->name . " " . $member->firstName . "</h2>";
  $last = jmysql_result(jmysql_query("select max(date) from prestations where status=$month"), 0);
  $dt = new DateTime($last);
  $last = mktime(12, 0, 0, $dt->format("m"), $dt->format("d"), $dt->format("Y"));
  if (jmysql_result(jmysql_query("select count(*) from pay_out where num=" . $member->num . " and type=" . PREST . " and month=0"), 0) != 0 && !jmysql_result(jmysql_query("select end from coach where num=" . $member->num), 0)) {
    $first = '<img src=attention.jpeg style=width:20px> Semaine(s) où il n\'y a pas d\'encodage:<ul>';
    $beg = $last - 4 * 7 * 24 * 3600;
    for ($d = $beg; $d <= $last; $d += 7 * 24 * 3600) {
      $dt = new DateTime('@' . $d);
      if ($dt->format("Y-m") < "$yearSeason-08")
        continue;
      $res = jmysql_query("select count(*) from prestations where num=" . $member->num . " AND date='" . $dt->format('Y-m-d') . "'");
      if (jmysql_result($res, 0) == 0) {
        echo "$first<li>Semaine du <a href=# onClick=viewWeek('$basePath&begin=$d')>" . $dt->format('d/m/Y') . "</a></li>";
        $first = null;
      }
    }
    if (!$first)
      echo "</ul>";
  }

  echo "<table class=main>";
  echo '<tr><th style=height:0px>Description</th><th style=height:0px;width:60px>Montants</td><th style=height:0px>&nbsp</th>' . nl;
  $i = 1;
  $res = jmysql_query("select * from pay_out where num=" . $member->num . " and month=$month order by type,ri desc");
  $bFirstSanction = true;
  while ($row = jmysql_fetch_assoc($res)) {
    $amount = $row["amount"];
    //echo "$member->name $member->firstName:";dump($row);
    switch ($row["type"]) {
      case PLAYER_SANCTION:
      case PLAYER_SANCTION_PAID:
        if ($bFirstSanction) {
          $bFirstSanction = false;
          echo "<tr style='border-top:4 double black;border-bottom:4 double black'><td><b>Sous-Total</b></td><td></td><td style=\"text-align:right\"><b>" . sprintf("%.02f €", $sum) . "</b></td></tr>";
        }
      case PREST:
      case MOVE:
      case PLAYER_PRESENCE:
      case PLAYER_BONUS:
      case PLAYER_BONUS_WIN:
      case PLAYER_MATCH:
      case MONTHLY:
      case PLAYER_MONTHLY:
      case PLAYER_ABSENT:
      case ONE_SHOT:
        echo "<tr><td>" . $row["descr"] . "</td><td style=text-align:right" . ($amount < 0 ? ";color:red" : null) . ">" . sprintf("%.02f €", $amount) . "</td>";
        if ($row["detail"])
          echo "<td style=text-align:center>" . $row["detail"] . "</td>";
        echo "</tr>";
        $sum += $amount;
        break;

      /*    case ACCOUNT:
        $sum=$account=$amount;
        break; */

      case MONEY:
        $sum = $money = $amount;
        break;

      case DISTR_ACCOUNT:
      case ACCOUNT:
        $account = $amount;
        $sum = $row["detail"];
        break;
    }
  }

  echo "<tr><th style=height:0px>Total</td><th style=height:0px;text-align:right" . ($sum < 0 ? ";color:red>" : ">") . sprintf("%.02f €", $sum) . "</td></tr></table>";
  if (jmysql_result(jmysql_query("select count(*) from prestations where num=$member->num and status=$month"), 0)) {
    echo '<i><small>(*) Semaine(s) du lundi ';
    $r = jmysql_query("select date, nprest from prestations where num=" . $member->num . " and status=$month");
    $sep = "";
    while ($r1 = jmysql_fetch_row($r)) {
      $dt = new DateTime($r1[0]);
      echo "$sep<a href=# onClick=viewWeek('$basePath&begin=" . mktime(12, 0, 0, $dt->format("n"), $dt->format("j"), $dt->format("Y")) . "')>";
      echo $dt->format("d/m/Y") . "</a>";
      $sep = ", &nbsp;";
    }
    echo "<br></small></i>";
  }

  if ($sum < 0)
    echo "<p>Pas de paiement, <b>" . sprintf("%.02f €", -$sum) . "</b> est déduit sur le montant du mois prochain.</p>";
  else if ($sum == 0)
    echo "<p>pas de paiement.</p>";
  else if ($money)
    echo "<p>Paiement de <b>" . sprintf("%.02f €", $sum) . "</b> en liquide.</p>";
  else if ($account != $sum)
    echo "<p>Sur ce montant, <b>" . sprintf("%.02f €", $account) . "</b> est versé sur le compte et <b>" . sprintf("%.02f €", $sum - $account) . "</b> est payé en liquide.</p>";
  else
    echo "<p>Paiement de <b>" . sprintf("%.02f €", $sum) . "</b> sur compte.</p>";
}

include_once "tools.php";
createButton(100, " Voir le résumé des paiements et prestations", "payOutSumary.php$basePath" . ($_GET["user"] == "root" ? "&user=root" : null), 350);
?>
<script type="text/javaScript">
  function otherMonth(m)
  {
  location.assign(location.pathname+'<?php echo $basePath ?>&month='+m);
  }

  var win=null;
  function viewWeek(p)
  {
  var loc="prest_Club.php"+p+"&skip=y";
  if (win && !win.closed)
  win.location.replace(loc);
  else
  win=window.open(loc, "_blank",  "scrollbars=yes,left=800,width=700,height=800,titlebar=no,menubar=no,status=no,location=no");
  win.focus();
  return false;
  }
</script>

