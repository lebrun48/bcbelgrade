<?php

//dump($request);
//$seeRequest=1;
//dump($admin,"admin");

if (!($cond = unserialize($admin["conditions"])))
  $cond["type"] = $type = 1;
else
  $type = $cond["type"];
switch ($action) {
  case "order":
    if ($id != -1)
      jmysql_query("update admin set sort='" . $request["order"] . "' where num=$num");
    $admin["sort"] = $request["order"];
    break;

  case "filter":
    getRequest($obj);
    $obj->filter["type"] = $type;
    if ($n = $obj->filter["num"]) {
      unset($obj->filter["num"]);
      $t = serialize($obj->filter);
      $obj->filter["num"] = $n;
    } else
      $t = serialize($obj->filter);
    if ($id != -1)
      jmysql_query("update admin set conditions='$t' where num=$num");
    $cond = $obj->filter;
    break;

  case "suppress":
    jmysql_query("update members set type=" . (1 - $type) . ", dateOut=" . ($type ? "NOW()" : "NULL") . " where num" . ($request["key"] ? "=" . $request["key"] : " in (" . $request["list"] . ")"));
    if (!$type) {
      jmysql_query("update members set category2=null where num" . ($request["key"] ? "=" . $request["key"] : " in (select num from members where " . $request["list"] . ") and category2>=" . (CAT_OTHERS | 100)));
      jmysql_query("update members set category3=null where num" . ($request["key"] ? "=" . $request["key"] : " in (select num from members where " . $request["list"] . ") and category3>=" . (CAT_OTHERS | 100)));
    }
    break;

  case "delete":
    $t = $request["key"] ? "=" . $request["key"] : " in (" . $request["list"] . ")";
    if (jmysql_result(jmysql_query("select count(*) from bill where client$t"), 0)) {
      include_once "tools.php";
      alert("Une facture existe pour ce(s) client(s)");
    } else {
      jmysql_query("delete from members where num$t");
      jmysql_query("delete from client where num$t");
      jmysql_query("delete from cotisations where num$t");
    }
    break;

  case "viewType":
    $type = 1 - $type;
    $cond["type"] = $type;
    jmysql_query("update admin set $s conditions='" . serialize($cond) . "' where num=$num");
    break;

  case "edit":
    include "memberEdit.php";
    exit();

  case "insert":
    $request["_val_num"] = jmysql_result(jmysql_query("select max(num)+1 from members"), 0);
    $request["_val_id"] = getId($request["_val_name"], $request["_val_firstName"], 8);
    $dt = new DateTime();
    $request["_val_dateIn"] = $dt->format('Y-m-d');
    if ($request["_val_category"] & CAT_PLAYER) {
      $request["_val_status"] = 10;
      $request["_val_caution"] = 0;
    } else {
      unset($request["_val_caution"]);
      unset($request["_val_status"]);
    }
    if (!selectAction(true))
      if (jmysql_errno() == 1062) {
        ?>
        <script type="text/javascript">
          alert("Il est impossible de créer un nouveau membre avec le même Nom, Prénom et Adresse qu'un autre membre");
        </script>
        <?php

      } else
        stop(__FILE__, __LINE__, "Erreur sql:" . jmysql_error());
    break;
    if (!isset($request["_val_ncategory"])) {
      $request["_val_ncategory"] = $request["_val_category"];
      $request["_val_ncategory2"] = $request["_val_category2"];
      $request["_val_ncategory3"] = $request["_val_category3"];
    }

  case "save":
    //dump($request);
    if ($request["key"]) {
      $tup = jmysql_fetch_row(jmysql_query("select category,substr(id,1,7) from " . $request["key"]));
      if (($t1 = $request["_val_category"]) && ($t = $tup[0]) != $t1) {
        if (!($t1 & CAT_PLAYER) && jmysql_result(jmysql_query("select count(*) from " . str_replace("members", "cotisations", $request["key"])), 0)) {
          ?>
          <script>
            alert("Une cotisation existe pour ce membre.\nIl faut d'abord supprimer sa cotisation avant de changer sa catégorie principal!");
          </script>
          <?php

          break;
        }
        jmysql_query("insert into memberHistory set " . substr($request["key"], strpos($request["key"], "where ") + 6) . ", oldCategory=$t");
      }
    }
    if (!isset($request["_val_ncategory"])) {
      $request["_val_ncategory"] = $request["_val_category"];
      $request["_val_ncategory2"] = $request["_val_category2"];
      $request["_val_ncategory3"] = $request["_val_category3"];
    } else
      $b = $request["_val_ncategory"] == (CAT_OTHERS | 90) || $request["_val_ncategory"] == (CAT_OTHERS | 100);
    if (($b1 = $request["_val_category"] == (CAT_OTHERS | 100)) || $request["_val_category2"] == (CAT_OTHERS | 100) || $request["_val_category3"] == (CAT_OTHERS | 100) || $b) {
      if (!$b)
        $request["_val_type"] = 0;
      if (!jmysql_result(jmysql_query("select dateOut from " . $request["key"]), 0))
        $request["_val_dateOut"] = "=NOW()";
      if ($b1) {
        unset($request["_val_category"]);
        unset($request["_val_ncategory"]);
        $request["_val_category3"] = $request["_val_ncategory3"] = (CAT_OTHERS | 100);
      }
    }
    selectAction();
    break;
}
?>