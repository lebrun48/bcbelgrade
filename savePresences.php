<?php

include "presences.php";
//dump($request);
//$seeRequest=1;
$nPres = array();
$maxDay = (int) ((time() - $begin) / 24 / 3600) + 1;
$team = $request["team"];
$teamFull = $team | CAT_PLAYER;
foreach ($request as $key => $val) {
  if (!strncmp($key, "Player", 6)) {
    $pres = "0000000";
    $sanction = null;
    $player = $val;
    $bPresent = jmysql_result(jmysql_query("SELECT count(*) FROM presences WHERE num=" . $player . " AND date='" . $date . "' and team=$team"), 0);
    //echo "bPresent=".$bPresent.", pres=".$pres.NL;
  } else if (!strncmp($key, "CotiState", 9)) {
    if (!$bPresent) {
      $cotiState = str_repeat ($val, 7);
    }
  } else if (!strncmp($key, "Num", 3)) {
    if ($player && (int) $val) {
      $i = substr($key, strpos($key, '_') + 1);
      if ($i == 1)
        $i = null;
      $sql = "UPDATE members SET numEquip$i=";
      $sql .= (int) $val == 0 ? "NULL" : (int) $val;
      //echo $sql." WHERE num=".$player.NL;
      jmysql_query($sql . " WHERE num=" . $player);
    }
  } else if (!strncmp($key, "Presence", 8)) {
    if (($pos = $key[strpos($key, '_', 8) + 1]) > $maxDay) {
      $str = "presencesClub.php$basePath&team=$team" . ($bList ? "&begin=$begin" : "");
      header("refresh:3; url=$str");
      include "head.php";
      echo "</head>";
      echo "<body>";
      echo "<p>Tu as enregistré des présences dans le futur, tu t'es certainement trompé de semaine...</p>";
      echo "<p>Tu vas être redirigé dans 3 sec vers l'encodage des présences pour corriger.</p>";
      echo "<p>Sinon <a href=\"$str\">clique ici</a>.</p>";
      exit();
    }
    $pres[$pos] = '1';
  } else if (!strncmp($key, "Sanction", 8) && $val) {
    $sanction .= dechex(128 + $key[strpos($key, '_', 8) + 1]);
    $tok = strtok($val, ',');
    while ($tok) {
      $sanction .= sprintf("%02x", $tok);
      $tok = strtok(',');
    }
  } else if (!strncmp($key, "End", 3)) {
    //Remove player from team
    jmysql_query("UPDATE members SET type=0, dateOut=NOW() WHERE num=" . $player);
    $r = jmysql_query("select name, firstName from members where num=$player");
    $t = "Numéro Membre: $player\n";
    $t .= "Nom joueur: " . jmysql_result($r, 0, 0) . " " . jmysql_result($r, 0, 1) . nl;
    $t .= "Equipe: $teamName\n";
    $t .= "Nom coach: " . $coach->nom . " " . $coach->prenom . nl;
    $t .= "Raison: " . $request["Raison" . substr($key, 3)];
    mail("newbc.belgrade@skynet.be, bcb1702@skynet.be", "Joueur supprimé", $t);
  } else if (!strncmp($key, "Raison", 6)) {
    //Dernière valeur => doit être enregistré
    if (empty($val) && strpos($pres, '1') === FALSE && !$sanction) {
      if ($bPresent) {
        jmysql_query("DELETE FROM presences WHERE date='" . $date . "' AND num=$player and team=$team");
      }
    } else {
      //record number of presence
      for ($i = 0; $i < 7; $i++)
        $nPres[$i] += $pres[$i];
      $sql = $bPresent ? "UPDATE presences SET " : "INSERT INTO presences SET date='" . $date . "', num=" . $player . ", team=$team, ";
      $sql .= empty($val) ? "reason=NULL" : "reason='" . addslashes($val) . "'";
      $sql .= ", pres='" . $pres . "'";
      $sql .= ", sanction=" . ($sanction ? '0x' . $sanction : 'null');
      if ($bPresent)
        $sql .= " WHERE date='" . $date . "' AND num=$player and team=$team";
      //echo $sql.NL;
      else {
        $sql .= ", COTI_STATE='$cotiState'";
      }
      if (!jmysql_query($sql))
        stop(__FILE__, __LINE__, "Important problème de sauvetage!!" . jmysql_error());
    }
  } else if (!strncmp($key, "newPlayer", 9)) {
    if ($player = $val) {
      //Insert new player
      $t = jmysql_fetch_row(jmysql_query("select category,category2,category3,name,firstName from members where num=$player"));
      if (!$t[1] || !$t[2]) {
        jmysql_query("update members set category" . ($t[1] ? '3' : '2') . "=$teamFull where num=$player");
        $t = "Nom joueur: $t[3]\n";
        $t .= "Prénom joueur: $t[4]\n";
        $t .= "Equipe: $teamName\n";
        $t .= "Nom operateur: " . $coach->nom . " " . $coach->prenom;
        mail("newbc.belgrade@skynet.be, bcb1702@skynet.be", "Nouveau joueur", $t);
      }
    }
    $bPresent = false;
    $pres = "0000000";
    $sanction = null;
  } else if ($key == "Com")
  //Save com
    jmysql_query("INSERT INTO com_presences SET updTime='" . $now->format("Y-m-d") . "', com=" . (empty($val) ? "NULL" : "'" . addslashes($val) . "'") . ",date='$date',num=$team ON DUPLICATE KEY UPDATE com=values(com),updTime=values(updTime)");
}
$day = array("lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi");
for ($i = 0; $i < 7; $i++)
  if ($nPres[$i] == 1) {
    $str = "presencesClub.php$basePath&team=$team" . ($bList ? "&begin=$begin" : "");
    header("refresh:5; url=$str");
    include "head.php";
    echo "</head>";
    echo "<body>";
    echo "<p>Hum...le $day[$i], tu as fais un entrainement avec 1 seul joueur.</p>";
    echo "<p>C'est sans doute une erreur et tu vas être redirigé dans 5 sec vers l'encodage des présences pour corriger.</p>";
    echo "<p>Sinon <a href=\"$str\">clique ici</a>.</p>";
    exit();
  }
include "check.php";
switch ($bList) {
  case 0:$url = "menu.php$basePath";
    break;
  case 1:$url = "listePrest.php$basePath";
    break;
  case 2:$url = "sumaryPres.php$basePath&team=$team";
    break;
}
header("Location: $url");
exit();
?>