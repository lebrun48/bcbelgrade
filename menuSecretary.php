<?php

$bSession = true;
include "admin.php";
if ($admin["num"] != 63 && $admin["num"] != 724 && !$bRoot)
  stop(__FILE__, __LINE__, "Pas de droit pour $id", null, true);
if ($bCoach = jmysql_result(jmysql_query("select count(*) from members where num=" . $admin["num"] . " and (category&(" . CAT_COACH . "|" . CAT_ASSIST . ") or category2&(" . CAT_COACH . "|" . CAT_ASSIST . ") or category3&(" . CAT_COACH . "|" . CAT_ASSIST . "))"), 0))
  include "coach.php";
include "head.php"
?>
<title>Menu Secrétaire     
</title>  
</head>
<?php

echo "<body><img src=" . LOGO_LITTLE_CLUB . ">" . nl;
echo "<h1 align=middle>Menu secrétaire";
echo "<input type=button style=margin-left:30px value=Déconnection onclick=location.replace('login.php?action=remcnx&loginScript=" . basename($_SERVER["SCRIPT_NAME"], ".php") . "')></h1>";
echo "<table class=training style=margin:auto><tr><th style=min-width:300px>Saison $yearSeason - " . ($yearSeason + 1) . "</th></tr>" . nl;
echo "<tr><td class=tabLink><a target=_blank href=calendar.php$basePath>Calendrier</a></td>" . nl;
echo "<tr><td class=tabLink><a target=_blank href=getCalendar.php$basePath>Check calendrier</a></td>" . nl;
echo "<tr><td class=tabLink><a target=_blank href=clubs.php$basePath>Adresses des clubs</a></td>" . nl;
echo "<tr><td class=tabLink><a target=_blank href=membersClub.php$basePath>Membres</a></td>" . nl;
echo "<tr><td class=tabLink><a target=_blank href=category.php$basePath>Série fédé pour équipes</a></td>" . nl;
echo "<tr><td class=tabLink><a target=_blank href=training.php$basePath>Grilles entrainements</a></td>" . nl;
echo "<tr><td class=tabLink><a target=_blank href=cotisations.php$basePath>Cotisations</a></td>" . nl;
echo "<tr><td class=tabLink><a target=_blank href=template.php>Modèle lettre " . SHORT_NAME_CLUB . "</a></td>" . nl;
if ($bCoach) {
  $realCoach = getRealCoach($coach->team);
  if ($realCoach) {
    $bSingle = true;
    foreach ($realCoach as $k => $v)
      if ($v["num"] != $coach->num)
        $bSingle = false;
    if ($bSingle)
      echo "<tr><td class=tabLink><a target=_blank href=listePrest.php$basePath" . ($right == ROOT ? "&user=root" : "") . ">Encodage des présences/prestations</a></td></tr>";
    else {
      echo "<tr><td>Encodage des présences/prestations<ul>" . nl;
      foreach ($realCoach as $k => $v)
        echo "<li><a target=_blank href=listePrest.php?id=" . $v["id"] . ($right == ROOT ? "&user=root" : "") . ">Pour " . $v["firstName"] . ' ' . $v["name"] . "</a>";
      echo "</ul></td></tr>";
    }
  }
}

include "menuEvents.php";
if ($t = getEvents())
  echo "<tr><td>$t</td></tr>" . nl;

echo "<tr><td class=tabLink><a target=_blank href=team.php$basePath>Composition d'équipes</a></td>" . nl;

echo "<tr><td class=tabLink><a target=_blank href=sumaryAllAttendances.php$basePath>Résumé des présences</a></td>" . nl;

echo "</table>" . nl;
