<?php

include "admin.php";
include "head.php"
?>
<title>Résumé Présences     
</title>  
</head>  
<body>
  <?php

//$seeRequest=1;
  include "sumaryPresCom.php";
  if (!$bRoot)
    echo "<p>Lorsqu'un \"<b><u style=\"color:red; font-family: 'times new roman', Arial, Helvetica, sans-serif;\">X</u></b>\" souligné et en gras apparaît dans le tableau des présences, cela signifie qu'une raison a été renseignée pour l'absence. Pour voir cette raison il suffit de déplacer la souris sur le \"<b><u style=\"color:red; font-family: 'times new roman', Arial, Helvetica, sans-serif;\">X</u></b>\" et la raison apparaîtra après 1 seconde.</p>" . nl;
  else {
    include "tools.php";
    $bViewAll = $_GET["viewAll"];
    createButton(-1, $bViewAll ? "Voir sans arrêts" : "Voir avec arrêts", $bViewAll ? $_SERVER["SCRIPT_NAME"] . $basePath : $_SERVER["REQUEST_URI"] . "&viewAll=y");
  }
  $last = time() - ($now->format('N') - 1) * 24 * 3600;
  $resPres = jmysql_query("select team from presences group by team");
  //$resPres = jmysql_query("select team from training group by team");
  while ($rowPres = jmysql_fetch_row($resPres)) {
    $team = $rowPres[0];
    $c = getRealCoach($team);
    if (!$c[0])
      continue;
    $r = jmysql_query("select * from coach where num=" . $c[0]["num"]);
    $coach->team = jmysql_result($r, 0, "TEAM");
    $coach->teamView = jmysql_result($r, 0, "TEAM_VIEW");
    $coach->end = jmysql_result($r, 0, "END");
    $coach->order = jmysql_result($r, 0, "TRI");
    $coach->assemble = jmysql_result($r, 0, "ASSEMBLE");
    $coach->num = jmysql_result($r, 0, "NUM");
    $r = jmysql_query("select firstName,name,id from members where num=" . $coach->num);
    $coach->prenom = jmysql_result($r, 0, "firstName");
    $coach->nom = jmysql_result($r, 0, "name");
    $coach->id = jmysql_result($r, 0, "id");
    $val = strtok($coach->team, '-');
    unset($teamName);
    while ($val) {
      if (!$coach->assemble && $val != $team) {
        $val = strtok("-");
        continue;
      }
      $cat = getCategory($val | CAT_PLAYER, true);
      if (!$cat)
        stop(__FILE__, __LINE__, "Impossible d'extraire le nom de l'équipe");
      $teamName .= "$cat / ";
      if ($coach->assemble)
        $val = strtok("-");
      else
        break;
    }
    $teamName = substr($teamName, 0, -3);
    echo "<h4>Résumé présences équipe '$teamName' ($coach->nom $coach->prenom)</h4>";
    $id = $coach->id;
    buildSumaryPres($bRoot, $bViewAll, true);
  }
