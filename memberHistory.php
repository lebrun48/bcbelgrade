<?php

include "admin.php";
include "common/tableTools.php";
include "common/tableEdit.php";
include "head.php";
echo '<title>Membres history';
echo '</title></head><body>';
selectAction();
$col = array("name"     => array(0, "Nom Prénom", "width:150", '', "concat(members.name,' ',members.firstName)"),
    "category" => array(0, "Ancienne catégorie", "width:100"),
    "date"     => array(0, "Date changement", "width:100"),
);

class MemberHistory extends TableEdit
{

  function printLine(&$row)
  {
    $row["category"] = getCategory($row["category"], true);
    TableEdit::PrintLine($row);
  }

}

//table itself
$obj = new MemberHistory;
$obj->colDef = &$col;
$obj->tableName = "memberHistory,members";
$obj->defaultWhere = "members.num=memberHistory.num and";
$obj->addCol = "memberHistory.ri";
$obj->bHeader = true;
$obj->selKey = "ri";

$obj->build();
?>
