<?php

$bIdMandatory = true;
include "common/common.php";
include "common/tableTools.php";
//$seeRequest=1;
//dump($request);
switch ($action) {
  case "edit":
    include "common/tableRowEdit.php";
    include "head.php";

    $col = array("client"     => array(0, "Client", "[Mand][Select(#client)]"),
        "seasonYear" => array(0, "Saison", "[Mand]"),
        "date"       => array(0, "Date Facture", "[Mand][Date]"),
        "billNum"    => array(10, "N° Facture", "[Mand]"),
        "descr"      => array(500, "Description", "[Mand]"),
        "amount"     => array(15, "Montant (HTVA)", "[Mand]"),
        "tva"        => array(0, "Taux TVA", "[Mand][Select(Pas de TVA:0;6%:6;12%:12;21%:21)]"),
        "comment"    => array(100, "Commentaire"),
        "sent"       => array(0, "Envoyée", "[Radio(Oui:1;Non:)]"),
        "paid"       => array(0, "Payée", "[Radio(Oui:1;Non:)]"),
        "retroVal"   => array(10, "Rétrocession"),
        "retroMade"  => array(0, "Rétro payée", "[Radio(Oui:1;Non:)]"),
        "deleted"    => array(0, "Supprimée", "[checkbox]"),
    );
    if ($bOfficial) {
      unset($col["retroVal"]);
      unset($col["retroMade"]);
    }

    echo '<title>Facture';
    echo '</title></head><body>';

    class Table extends TableRowEdit
    {

      function getTuple()
      {
        global $now;
        $this->client = jmysql_query("select client.num,ifnull(society,concat(name,' ',firstName))as name from client,members where members.num=client.num order by type desc,name");
        $tup = parent::getTuple();
        if (!$tup["date"])
          $tup["date"] = $now->format("Y-m-d");
        if (!$tup["billNum"]) {
          $max = jmysql_result(jmysql_query("select max(billNum) from bill"), 0);
          $tup["billNum"] = intval($max / 100) != $now->format("Y") ? $now->format("Y") * 100 + 1 : $max + 1;
          $tup["seasonYear"] = jmysql_result(jmysql_query("select max(seasonYear) from bill"), 0);
        }
        return $tup;
      }

      function ApplyValue($key, $val, $row)
      {
        switch ($key) {
          case "seasonYear":
            $r = jmysql_query("select seasonYear from bill group by seasonYear");
            echo "<select name=_val_seasonYear>";
            while ($t = jmysql_fetch_row($r)) {
              $m = $t[0];
              echo "<option value=$m" . ($m == $row[$key] || !$row && $m == $max ? " selected>" : '>') . (2000 + $m) . '-' . (2001 + $m) . "</option>";
              $m++;
            }
            echo "<option value=$m" . ($m == $row[$key] ? " selected>" : '>') . (2000 + $m) . '-' . (2001 + $m) . "</option></select>";
            return;
        }

        parent::ApplyValue($key, $val, $row);
      }

    }

    $ed = new Table;
    $ed->title = "Donnée de Facture";
    $ed->colDef = &$col;
    $ed->editRow();
    exit();


  case "insert":
    $max = jmysql_result(jmysql_query("select max(billNum) from bill"), 0);
    $request["_val_billNum"] = intval($max / 100) < ($y = $now->format("Y")) ? $y * 100 + 1 : $max + 1;
    $request["_val_date"] = $now->format("Y-m-d");
    break;

  case "save":
    if (!$request["_val_deleted"])
      $request["_val_deleted"] = '=null';
    break;

  case "delete":
    $n = str_replace('$', "'", $request["key"]);
    jmysql_query("update " . strtok(substr($n, 0, strpos($n, "where")), ",") . " set deleted=1" . strstr($n, " where"));
    unset($action);
    break;

  case "listing":
    include "head.php";
    echo "<title>Listing client TVA</title></head><body>";
    echo "<h3 style=width:100%;text-align:center>Listing client TVA</h3>";
    echo "<table style=width:50%;margin:auto class=main><tr><th>Nom</th><th>N° TVA</th><th>Montant</th><th>Total TVA</th></tr>";
    $r = jmysql_query("select ifnull(society,concat(name,' ',firstName)),numTVA,sum(amount),sum(amount*tva/100) as s from bill,client,members where client.num=bill.client and members.num=bill.client and numTVA is not null and deleted is null and sent is not null and substr(bill.date,1,4)=$yearSeason group by numTVA");
    while ($tup = jmysql_fetch_row($r)) {
      echo "<tr><td>$tup[0]</td><td>" . str_replace('.', '', $tup[1]) . "</td><td style=text-align:right>" . ($t1 = sprintf("%0.2f", $tup[2])) . " €</td><td style=text-align:right>" . ($t2 = sprintf("%0.2f", $tup[3])) . " €</td></tr>";
      $sum1 += $t1;
      $sum2 += $t2;
    }
    echo "<tr><th></th><th></th><th>" . sprintf("%0.2f €", $sum1) . "</th><th>" . sprintf("%0.2f €", $sum2) . "</th></tr>";
    exit();
}

selectAction();


include "common/tableEdit.php";

$col = array("society"    => array(1, "Client", '', '', "ifnull(society,concat(name,' ',firstName))"),
    "date"       => array(1, "Date facture", '', '[date]'),
    "seasonYear" => array(-1, "Saison", '', ''),
    "billNum"    => array(1, "N° facture"),
    "amount"     => array(1, "Montant HTVA", '', '[money]'),
    "tva"        => array(1, "TVA"),
    "sent"       => array(-1, "E"),
    "paid"       => array(-1, "P"),
    "retroVal"   => array(1, "Rétrocession", '', '[money]'),
    "retroMade"  => array(-1, "R"),
    "net"        => array("1", "Net", '', '[money]', 'amount-ifnull(retroVal,0)'),
);

include "head.php";
echo '<title>Factures';
echo '</title></head><body onload=genericLoadEvent()>';

class Table extends TableEdit
{

//--------------------------------------------------------------------------------------------
  function applyStyle($row)
  {
    return parent::applyStyle($row) . ($row["deleted"] ? ";text-decoration:line-through;" : '');
  }

//---------------------------------------------------------------------------------------------------
  function applyDisplay($key, $val, $row, $idxLine)
  {
    global $basePath;
    switch ($key) {
      case "billNum":
        return parent::applyDisplay($key, $val, $row, $idxLine, "<a target=_blank href=billBuilder.php$basePath&bill=" . $row["ri"] . ">$row[$key]</a>");
      case "sent": case "paid": case "retroMade":
        return parent::applyDisplay($key, $val, $row, $idxLine, $row[$key] ? "X" : '');

      case "amount":
        if ($row["deleted"])
          break;
        $this->$key += $row[$key];
        if ($row["paid"])
          $this->paid += $row[$key];
        break;

      case "retroVal":
        if ($row["deleted"])
          break;
        $this->$key += $row[$key];
        if ($row["retroMade"])
          $this->retro += $row[$key];
        break;
    }

    return parent::applyDisplay($key, $val, $row, $idxLine);
  }

//-----------------------------------------------------------------------------------------------------
  function terminate()
  {
    global $bRoot, $bOfficial;
    $t = $bRoot ? 5 : 4;
    echo "<tr><th class=bottom colspan=$t>Total</th>" . sprintf("<th class=bottom style=text-align:right>%0.2f €</th><th class=bottom colspan=3></th>", $this->amount);
    if (!$bOfficial)
      echo sprintf("<th class=bottom style=text-align:right>%0.2f €</th><th class=bottom></th><th class=bottom style=text-align:right>%0.2f €</th>", $this->retroVal, $this->amount - $this->retroVal);
    echo "</tr><tr><th class=bottom colspan=$t>Déjà payé</th>" . sprintf("<th class=bottom style=text-align:right>%0.2f €</th><th class=bottom colspan=3>", $this->paid);
    if (!$bOfficial)
      echo sprintf("</th><th class=bottom style=text-align:right>%0.2f €</th><th class=bottom></th><th class=bottom style=text-align:right>%0.2f €</th>", $this->retro, $this->paid - $this->retro);
    echo "</tr><tr><th class=bottom colspan=$t>Reste</th>" . sprintf("<th class=bottom style=text-align:right>%0.2f €</th><th class=bottom colspan=3></th>", $this->amount - $this->paid);
    if (!$bOfficial)
      echo sprintf("<th class=bottom style=text-align:right>%0.2f €</th><th class=bottom></th><th class=bottom style=text-align:right>%0.2f €</th>", $this->retroVal - $this->retro, $this->amount - $this->paid - $this->retroVal + $this->retro);
    echo "</tr>";
  }

//---------------------------------------------------------------------------------------------------
  function buildFrom()
  {
    return "bill left join client on client.num=bill.client join members on bill.client=members.num";
  }

}

if (!isset($request["_sel_seasonYear"]))
  $request["_sel_seasonYear"] = "_c=a:1:{i:0;s:2:\"" . ($yearSeason - 2000) . "\";}";
$obj = new Table;
$obj->colDef = &$col;
$obj->tableName = "bill";
$obj->tableId = "members";
if ($bRoot)
  $obj->bHeader = true;
$obj->bInsertNew = true;
$obj->addCol = "bill.ri,deleted";
$obj->primKey = "ri";
if (!$obj->order)
  $obj->order = "billNum";
if ($bOfficial) {
  unset($col["retroVal"]);
  unset($col["retroMade"]);
  unset($col["net"]);
  $obj->defaultWhere = "billNum>2000 and ";
}
if ($bRoot) {
  include "tools.php";
  createButton(-1, "Listing TVA", "bill.php$basePath1" . "action=listing\" target=\"_blank");
}
$obj->build();



