<?php include "head.php" ?>
<title>Résumé Présences     
</title>  
</head>  
<body>
  <?php

  include "common/common.php";
  include "memberData.php";
  include "sumaryPresCom.php";

  echo "<p>Lorsqu'un \"<b><u style=\"color:red; font-family: 'times new roman', Arial, Helvetica, sans-serif;\">X</u></b>\" souligné et en gras apparaît dans le tableau des présences, cela signifie qu'une raison a été renseignée pour l'absence. Pour voir cette raison il suffit de déplacer la souris sur le \"<b><u style=\"color:red; font-family: 'times new roman', Arial, Helvetica, sans-serif;\">X</u></b>\" et la raison apparaîtra après 1 seconde.</p>" . nl;
  $fin = $_GET["begin"];
  $last = $_GET["end"];
  echo "<h4>Résumé présences pour $member->firstName $member->name</h4>";
  $player = "num=$member->num and";
  $team = strtok($_GET["team"], ",");
  while ($team) {
    echo "<h4>Equipe " . getCategory($team | CAT_PLAYER, true) . "</h4>";
    buildSumaryPres();
    $team = strtok(",");
  }
