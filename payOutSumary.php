<?php

$bSession = true;
include "common/common.php";
include "payOutData.php";
include "head.php";
include "memberData.php";
echo '<title>Résumé prestations et paiements';
echo '</title></head><body>' . nl;
$bOld = $request["season"];
//$bOfficial=true;

if (!jmysql_result(jmysql_query("select count(*) from pay_out where num=$member->num and type&0x3F<" . PLAYER_DESC), 0) || $request["user"] != "root" && (jmysql_result(jmysql_query("select count(*) from pay_out where num=$member->num and type=" . NO_MAIL), 0) || !jmysql_result(jmysql_query("select count(*) from pay_out where num=$member->num and type<>" . COACH_DESC), 0)))
  stop(__FILE__, __LINE__, "ended", null, true);
echo "<h1>Résumé des prestations pour " . $member->firstName . " " . $member->name . "</h1>";
if (!$bOfficial)
  echo "<p>Clique sur le mois pour voir les détails du paiement</p>";
echo "<table class=main><tr><th style=height:0px>Mois</th><th style=height:0px>Nbre prestations</th><th style=height:0px>" . ($bOfficial ? "Montants</th>" : "Sur compte</th><th style=height:0px>En liquide</th><th style=height:0px>Total mois</th>") . "</tr>";
$lim = $now->format('n');
$trMonths = $orderMonths;
asort($trMonths);
foreach ($trMonths as $month => $vv) {
  if ($month == 8 || !$bRoot && !jmysql_result(jmysql_query("select count(*) from pay_out where type=" . ENCLOSE_MONTH . " and month=$month"), 0, 0))
    continue;
  $nPrest = jmysql_result(jmysql_query("select sum(nprest) from prestations where num=" . $member->num . " and status=$month"), 0);
  $sumPrest += $nPrest;
  $res = jmysql_query("select type, amount from pay_out where num=" . $member->num . " and month=$month order by type");
  $sum = 0;
  $bPresent = false;
  while ($pay = jmysql_fetch_row($res)) {
    switch ($pay[0]) {
      case PLAYER_SANCTION_PERC:
      case PLAYER_SANCTION_PERC_AUTO:
        break;

      case DISTR_ACCOUNT:
      case ACCOUNT:
        $n = $sum - $pay[1];
        $sm = $n;
        $bPresent = true;
        if ($pay[1] < 0)
          $pay[1] = 0;
        echo "<tr class=parity$par><td>" . (!$bOfficial ? "<a href=payOutPayment.php$basePath&month=$month" . ($bRoot ? "&user=root>" : '>') : '') . "$months[$month] " . ($month >= 8 ? $yearSeason : $yearSeason + 1) . "</a></td>";
        $par = 1 - $par;
        echo '<td style="text-align:center">' . $nPrest . '</td><td style="text-align:right">' . sprintf("%.02f €", $pay[1]) . '</td>' . (!$bOfficial ? "<td style=text-align:right>" . sprintf("%.02f €", $n) . '</td>' : '');
        $sumAccount += $pay[1];
        $sm += $pay[1];
        $sumMoney += $n;
        break;

      /*     case ACCOUNT:
        $sm=$n=0;
        if ($pay[1]<0)
        $pay[1]=0;
        echo  "<tr class=parity$par><td><a href=payOutPayment.php?id=$member->id&month=$month>$months[$month] ".($month>=8 ? $yearSeason : $yearSeason+1)."</a></td>";
        $par=1-$par;
        echo '<td style="text-align:center">'.$nPrest.'</td><td style="text-align:right">'.sprintf("%.02f €", $pay[1]).'</td><td style="text-align:right">0.00 €</td>';
        $sumAccount+=$pay[1];
        $sm+=$pay[1];
        $bPresent=true;
        break; */

      case MONEY:
        $n = $pay[1];
        echo "<tr class=parity$par><td><a href=payOutPayment.php$basePath&month=$month>$months[$month] " . ($month >= 8 ? $yearSeason : $yearSeason + 1) . ($bRoot ? "&user=root" : '') . "</a></td>";
        $par = 1 - $par;
        echo '<td style="text-align:center">' . $nPrest . '</td><td style="text-align:right">0.00 €</td><td style="text-align:right">' . sprintf("%.02f €", $n) . '</td>';
        $bPresent = true;
        $sumMoney += $n;
        $sm = $n;
        break;

      default:
        $sum += $pay[1];
        break;
    }
  }
  if ($bPresent && !$bOfficial)
    echo '<td style="text-align:right; font-weight:bold">' . sprintf("%.02f €", $sm) . '</td></tr>';
  if (!$bOld && $month == $lim)
    break;
}
echo "<tr><th style=height:0px>Total saison</th><th style=height:0px;text-align:center>$sumPrest</th><th style=height:0px;text-align:right>" . sprintf("%.02f €", $sumAccount) . "</th>" . ($bOfficial ? '' : "<th style=height:0px;text-align:right>" . sprintf("%.02f €", $sumMoney) . "</th><th style=height:0px;text-align:right>" . sprintf("%.02f €", $sumMoney + $sumAccount) . "</th>") . "</tr>";
