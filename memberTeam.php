<?php

include "common/common.php";
include "head.php";
echo "<title>Composition d'équipe</title></head><body>\n";
include "common/tableTools.php";
include "common/tableEdit.php";
//dump($request,"request");
//$seeRequest=1;
$r = getExistingCategories("category&" . CAT_PLAYER . " and type<>0 and ", false, false, true);
if (!($team = $request["viewteam"]))
  $team = key($r);
echo "<select style='font-family:verdana;font-size:25px;font-weight:bold' onchange=\"location.assign('memberTeam.php${basePath1}viewteam='+(this.options[this.selectedIndex].value))\">";
foreach ($r as $k => $v)
  echo "<option value=$k" . ($team == $k ? " selected=selected" : '') . ">$v</option>";

echo "</select><br><br>";
$sub = ($team & 0xFFFF) % 10;
$file = "/members/teams/$yearSeason/" . jmysql_result(jmysql_query("select short from category where num=" . ($team - $sub)), 0) . ($sub ? "_" . chr(ord('A') + $sub - 1) : '') . ".jpg";
//dump($file);
if (file_exists($_SERVER["DOCUMENT_ROOT"] . $file))
  echo "<img src=$file style='border: 1px solid lightgray;padding:5px;width:700px'><br><br>";
echo "<div style=background:#E9EDE8;border-radius:5px;padding:20px><h2 style=text-family:Arial>Composition d'équipe</h2>";
$col = array("num"       => array(0, "N°", "width:40px"),
    "picture"   => array(0, "Photo", "min-width:40px;width:40px", "text-align:center", '@'),
    "name"      => array(0, "Nom", "min-width:200px;width:200px", "font-weight:bold"),
    "firstName" => array(0, "Prénom", "min-width:200px;width:200px", "font-weight:bold"),
    "year"      => array(0, "Année", "min-width:50px;width:50px", "text-align:center", "birth"),
    "numEquip"  => array(0, "N° maillot", "min-width:100px;width:100px", "text-align:center")
);
if (!$bRoot)
  unset($col["num"]);

class MemberTeam extends TableEdit
{

  public function applyDisplay(&$key, &$val, &$row, $idxLine)
  {
    switch ($key) {
      case "picture":
        if (file_exists($_SERVER["DOCUMENT_ROOT"] . "/members/pictures/N_" . $row["num"] . ".jpg"))
          return $this->formatValue('<img src="/members/pictures/N_' . $row["num"] . '.jpg" height=50px>', $val);
        return $this->formatValue("<div style=height:50px></div>", $val);

      case "year":
        return $this->formatValue(substr($row[$key], 2, 2), $val);

      case "numEquip":
        global $team;
        for ($i = 1; $i <= 3; $i++)
          if ($row["category" . ($t = $i == 1 ? '' : "$i")] == $team) {
            if (!($dispVal = $row["numEquip$t"]))
              $dispVal = '';
            break;
          }
    }
    return TableEdit::applyDisplay($key, $val, $row, $idxLine, $dispVal);
  }

}
?>
<style>
  #team {margin-left:20px}
</style>
<?php

$obj = new MemberTeam;
$obj->colDef = &$col;
$obj->tableName = "members";
if (!$obj->order)
  $obj->order = "name";
$team |= CAT_PLAYER;
$obj->defaultWhere = "(category=$team OR category2=$team OR category3=$team) AND type<>0 AND";
$obj->addCol = "num,numEquip2,numEquip3,category,category2,category3";
$obj->tableId = "team";
$obj->build();

$team = $team & 0xFFFF | CAT_COACH;
$tup = jmysql_fetch_assoc(jmysql_query("select num, name, firstName, ifnull(phP,phF) as phP, ifnull(emailP,emailF) as emailP,visibility from members where type<>0 and (category=$team or category2=$team or category3=$team)"));
echo "<h3 style=color:black;margin-left:100px>Coach</h3>" . nl;
echo "<div style='width:600px;margin-left:20px;border:1px solid black'>";
echo "<table id=edit style=border:0;font-size:1.5em;color:black;margin-left:auto;margin-right:auto><tr>";
if (file_exists($_SERVER["DOCUMENT_ROOT"] . "/members/pictures/N_" . $tup["num"] . ".jpg"))
  echo "<td rowspan=3><img style=margin-right:20px src=/members/pictures/N_" . $tup["num"] . ".jpg height=100px></td>";
echo "<td>Nom :</td><td>" . $tup["name"] . ' ' . $tup["firstName"] . "</td></tr>";
if (!$tup["emailP"] || $tup["visibility"][0] == 'N')
  echo "<td></td></tr>";
else
  echo "<tr><td>Email :</td><td class=tabLink><a href='mailto:" . $tup["name"] . ' ' . $tup["firstName"] . "<" . $tup["emailP"] . ">'>" . $tup["emailP"] . "</a></td></tr>";
echo "<tr><td>Tél :</td><td>" . $tup["phP"] . "</td></tr></table></div>";
?>