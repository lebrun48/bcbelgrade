<?php include "head.php" ?>
<title>Valider prestations     
</title>  
</head>
<?php

$bOnlyRoot = true;
include "admin.php";
$last = time() - ($now->format('N') + 6) * 24 * 3600;
$last = new DateTime("@" . $last);
$last = $last->format("Y-m-d");
$res = jmysql_query("select date, num, upd_time,prest,remp0,remp1,remp2,remp3,remp4,remp5,remp6,com from prestations where date<='$last' and (nprest is null or status=0) order by date");
echo jmysql_error();
if (jmysql_num_rows($res)) {
  echo "<p>Liste des prestations non vérifiées</p>";
  while ($prest = jmysql_fetch_assoc($res)) {
    //dump($prest,"prest");
    $coach = jmysql_fetch_assoc(jmysql_query("select id,name,firstName,team from members,coach where members.num=" . $prest["num"] . " and coach.num=members.num"));
    //dump($coach, "coach");
    $dt = new DateTime($prest["date"]);
    $begin = $prest["date"];

    //Compute prestations     
    $teamList = explode("-", $coach["team"]);
    unset($nTotPrest);
    foreach ($teamList as $val) {

      if (empty($val))
        continue;
      //Count presences
      $nPres = array(0, 0, 0, 0, 0, 0, 0);
      $r = jmysql_query("select pres from presences where date='$begin' and team=$val and num in (select num from members where category=" . ($val | CAT_PLAYER) . " or category2=" . ($val | CAT_PLAYER) . " or category3=" . ($val | CAT_PLAYER) . ")");
      while ($row = jmysql_fetch_assoc($r)) {
        //dump($row, "presences");
        for ($i = 0; $i < 7; $i++)
          $nPres[$i] |= $row["pres"][$i];
      }
      //dump($nPres);
      for ($i = 0; $i < 7; $i++)
        $nTotPrest[$i] += (bool) $nPres[$i];
    }
    $t = $prest["prest"] . "       ";
    $nPrest = $nDep = 0;
    $remp = '';
    for ($i = 0; $i < 7; $i++) {
      $remp += strlen($prest["remp$i"]);
      if (!$prest["remp$i"])
        $t[$i] = ($n = $nTotPrest[$i]) ? $n : ' ';
      $nPrest += $t[$i];
      $nDep += intval($t[$i]) ? 2 : 0;
    }
    $bCom = $remp || $prest["com"];
    jmysql_query("update prestations set prest='$t', nPrest=$nPrest, nDep=$nDep" . ($bCom ? '' : ",status=-1") . " where num=" . $prest["num"] . " and date='$begin'");
    if (!$bCom)
      continue;
    $begin = mktime(12, 0, 0, $dt->format('m'), $dt->format('d'), $dt->format('Y'));
    echo "<li><a href=prest_Club.php?user=root" . (($t = $request["season"]) ? "&season=$t" : '') . "&id=" . $coach["id"] . "&begin=$begin> Semaine " . $prest["date"] . " de " . $coach["name"] . ' ' . $coach["firstName"] . " encodé le " . $prest["upd_time"] . "</a></li>\n";
  }
  echo "</ul></div>";
  exit();
}
echo "Pas de prestations non validées"
?>
