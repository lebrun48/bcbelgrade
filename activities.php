<?php
include "admin.php";

switch ($action = $request["action"]) {
  case "getList":
    header("Content-Type:text/html; charset=UTF-8", true);
    $ar = str_replace(",,", ",", str_replace("\n", ',', $request["numList"]));
    if ($ar[0] == ",")
      $ar = substr($ar, 1);
    if ($ar[strlen($ar) - 1] == ",")
      $ar = substr($ar, 0, -1);
    $r = jmysql_query("select num, concat(name,' ',firstName)as fName,category from members where num in ($ar) order by fName");
    while ($tup = jmysql_fetch_row($r))
      echo "<option id=$tup[0]>$tup[1] (" . getCategory($tup[2], true) . ")</option>";
    exit();

  case "actIns":
  case "actRem":
    header("Content-Type:text/html; charset=UTF-8", true);
    include "common/tableRowEdit.php";
    $ar = getActivities();
    //echo mb_convert_encoding(buildActivities($ar,$action,$request["row"]),"UTF-8","ISO-8859-1");
    echo buildActivities($ar, $action, $request["row"]);
    exit();

  case "search":
    header("Content-Type:text/html; charset=UTF-8", true);
    $ar = substr($request["selected"], 0, -1);
    $r = jmysql_query("select num,concat(name,' ',firstName),category from members where concat(name,' ',firstName) like '%" . $request["val"] . "%' and type<>0" . ($ar ? " and num not in ($ar)" : '') . " order by category,name");
    while ($tup = jmysql_fetch_row($r))
      echo "<option id=$tup[0]>$tup[1] (" . getCategory($tup[2], true) . ")</option>";
    exit();
}

//------------------------------------------------------------------------------------
include "common/tableTools.php";

//dump($request);
class Activity
{

  function __construct($num = null)
  {
    $this->name = '';
    $this->max = -1;
    $this->unicity = 0;
    $this->num = $num;
  }

}

//-----------------------------------------------------------------------------------------------
function buildActivities($act, $oper = null, $row = null)
{
  $line = "<span id=activities><fieldset><legend style=color:blue;font-weight:bold>Postes </legend>";
  if (!$act)
    $act[0] = new Activity;
  else
    switch ($oper) {
      case "actIns":
        insertEl($act, $row);
        foreach ($act as $v)
          $m = max($m, $v->num);
        $act[$row + 1] = new Activity($m + 2);
        break;

      case "actRem":
        remEl($act, $row);
        break;
    }
  $line .= "<table><tr><th>N°</th><th>Nom du poste</th><th>Nombre maximum</th><th>Nombre</th><th>Catégories</th></tr>";
  $obj = new TableRowEdit();
  foreach ($act as $k => $v) {
    $line .= "<tr><td>$v->num<input name=_val_act_num$k value=" . (isset($v->num) ? $v->num : $k) . " type=hidden></td><td>";
    $ft = array(50);
    $line .= $obj->applyValue("act_name$k", $ft, $act[$k]->name, true);
    $line .= "</td><td>";
    $ft = array(5);
    $line .= $obj->applyValue("act_max$k", $ft, $act[$k]->max, true);
    $line .= "</td><td>";
    $ft = array(5, '', "[checkbox(:1)]");
    $line .= $obj->applyValue("act_number$k", $ft, $act[$k]->number, true);
    $line .= "</td><td>";
    $ft = array(5, '', "[selectMultiple(Administration:0;Entraineur:1;Assistant:2;Joueur:4;Autres:8) size=2]");
    $line .= $obj->applyValue("act_categories$k", $ft, $act[$k]->categories, true);
    $line .= "</td><td><img onclick=add_remClick('actIns',$k) style=width:20px;height:20px;margin-left:15px src=add.png>" . ($k ? "<img onclick=add_remClick('actRem',$k) src=cross_delete.png style=width:20px;height:20px;margin-left:15px>" : '') . "</td><td>";
  }

  return $line . "</table></fieldset></span>";
}

//-----------------------------------------------------------------------------------
function getActivities()
{
  global $request;
  $i = 0;
  while (isset($request["_val_act_name$i"])) {
    $ar[$i]->num = $request["_val_act_num$i"];
    $ar[$i]->name = $request["_val_act_name$i"];
    $ar[$i]->max = $request["_val_act_max$i"];
    $ar[$i]->categories = serialize($request["_val_act_categories$i"]);
    $ar[$i]->number = $request["_val_act_number$i"];
    unset($request["_val_act_num$i"]);
    unset($request["_val_act_name$i"]);
    unset($request["_val_act_max$i"]);
    unset($request["_val_act_unicity$i"]);
    unset($request["_val_act_categories$i"]);
    unset($request["_val_act_number$i"]);
    $i++;
  }
  return $ar;
}

//-----------------------------------------------------------------------------------
function getInvited()
{
  global $request, $action;
  if ($action == 'save' && $key = $request["key"])
    $inv = unserialize(jmysql_result(jmysql_query("select invited from $key"), 0));
  $tok = strtok($request["_val_invited"], ",");
  while ($tok) {
    $inv->nums[$tok]->pres = false;
    $tok = strtok(",");
  }
  foreach ($inv->nums as $k => $v)
    if ($v->pres)
      unset($inv->nums[$k]);
    else
      $inv->nums[$k]->pres = true;
  return $inv;
}

//------------------------------------------------------------------------------------------
switch ($action) {
  case "edit":
    include "common/tableRowEdit.php";
    include "head.php";
    echo "<title>Editon d'activité</title>";
    include "tools.php";
    include "common/msgBox.php";
    ?>
    <style>
      .selection {width:300;height:150;border:1px solid black;font-size:small;font-family:calibri;overflow-y:auto;cursor:pointer}
    </style>
    <script>
      function add_remClick(act, n)
      {
        line = "action=" + act + "&row=" + n;
        $("[name^='_val_act_']").each(function () {
          if (!this.hasAttribute("type") || this.type != "checkbox" || this.checked)
            line += "&" + this.name + "=" + encodeURIComponent(this.value);
        });
        $.ajax({data: line, success: function (result) {
            //alert(result);
            $("#activities").html(result);
          }});
      }

      function listSel(n)
      {
        line = "action=search&val=" + encodeURIComponent($('#search').val()) + "&row=" + n + "&selected=";
        $("#selected").children().each(function () {
          line += this.id + ',';
        });
        $.ajax({data: line, success: function (result) {
            //alert(result);
            $("#select").html(result);
          }});
      }

      function arrowClick(dir)
      {
        orig = dir ? "#select" : "#selected";
        dest = document.getElementById(dir ? "selected" : "select");
        $(orig).children().each(function () {
          if (this.selected)
          {
            clone = this.cloneNode();
            clone.innerHTML = this.innerHTML;
            dest.insertBefore(clone, dest.firstChild);
            this.parentNode.removeChild(this);
          }
        });
      }

      function insertList() {
        msgBox("<textarea cols=10 rows=50 id=numList>", {title: "liste de N°", click: function () {
            $.get("activities.php<?php echo $basePath ?>", {action: "getList", numList: $("#numList").val()}, function (data) {
              $("#selected").html(data);
              msgBoxClose()
            })
          }});
      }

    </script>
    <?php
    echo "</head>";

    $col = array("owner"          => array(50, "Responsable", "[mand][select(#getOwner)]"),
        "title"          => array(50, "Titre", "[mand]"),
        "date"           => array(50, "Date de l'activité", "[mand][DateTime(0,+1)]"),
        "descr"          => array(200, "Description ou message d'accueil"),
        "activities"     => array(0, "Postes de l'activité"),
        "invited"        => array(0, "Participants"),
        "viewActiveOnly" => array(0, "Voir uniquement les actifs"),
        "groupNm"        => array(50, "Groupe")
    );

    class Edit extends TableRowEdit
    {

      //-------------------------------------------------------------------------------------------------
      function applyValue($key, $format, $row)
      {
        switch ($key) {
          case "activities":
            echo buildActivities(unserialize($row["activities"]));
            return;

          case "invited":
            $inv = unserialize($row[$key]);
            echo "Entre une partie du nom ou prénom: <input type=text id=search maxlength=10>";
            createAction(10, "Chercher", "listSel(" . $row["ri"] . ")");
            echo "<table><tr><td><select id=select class=selection multiple></select></td>";
            echo "<td><img src=arrow-right.jpg style=cursor:pointer;width:50;height:30px;margin-bottom:15px onclick=arrowClick(1)><br><img style=cursor:pointer;width:50;height:30px src=arrow-left.jpg onclick=arrowClick(0)></td>";
            echo "<td><select class=selection id=selected multiple>";
            if ($inv) {
              foreach ($inv->nums as $k => $v)
                $ar .= "$k,";
              $ar = substr($ar, 0, -1);
              $r = jmysql_query("select num, concat(name,' ',firstName)as fName,category from members where num in ($ar) order by fName");
              while ($tup = jmysql_fetch_row($r))
                echo "<option id=$tup[0]>$tup[1] (" . getCategory($tup[2], true) . ")</option>";
            }
            echo "</select></td></tr></table><input name=_val_invited type=hidden value=''>";
            createAction(-1, "Insérer liste", "insertList()");
            return;

          case "viewActiveOnly":
            echo "<input type=checkbox value=1" . ($row[$key] ? " checked" : '') . " name=_val_$key>";
            return;
        }
        TableRowEdit::applyValue($key, $format, $row);
      }

      //-------------------------------------------------------------------------------------------------
      function applySave()
      {
        ?>
        line='';
        $("#selected").children().each(function(){
        line+=this.id+',';
        });
        if (!line)
        {
        alert("Il doit y avoir au moins 1 invité!");
        return;
        }
        document.getElementsByName("_val_invited")[0].value=line;
        <?php
      }

    }

    $obj = new Edit();
    $obj->title = "Edition d'activité";
    $obj->getOwner = jmysql_query("select num, concat(name, ' ',firstName) from members where type and (category&0xFF0000=0 or category2&0xFF0000=0 or category3&0xFF0000=0) order by name,firstName");
    $obj->colDef = &$col;
    $obj->editRow();
    exit();

  case "insert":
    $request["key"] = "activities where";
  case "save":
    if (!$request["_val_viewActiveOnly"])
      $request["_val_viewActiveOnly"] = "=null";
    $request["_val_activities"] = serialize(getActivities());
    $request["_val_invited"] = serialize(getInvited());
  //dump($request["_val_activities"]);
}


//dump($request,"req");
//$seeRequest=true;
selectAction();

include "head.php";
echo "<title>Activités partagées</title></head><body>";


include "common/tableEdit.php";
include "tools.php";

$col = array("owner"     => array(-1, "Responsable"),
    "title"     => array(1, "Titre"),
    "date"      => array(1, "Date activité", '', "[dt]"),
    "descr"     => array(0, "Description", '', "[hide100]"),
    "groupNm"   => array(0, "Groupe"),
    "operation" => array(0, "Operation", '', '', '@'),
);

class Table extends TableEdit
{

//---------------------------------------------------------------------------------------------------
  function buildKey($row)
  {
    return "activities where ri=" . $row["ri"];
  }

//---------------------------------------------------------------------------------------------------
  function applyDisplay($key, $format, $row, $idxLine)
  {
    global $basePath1;
    $dispVal = null;
    switch ($key) {
      case "operation":
        echo "<td>";
        createButton(5, "Mails", "activityMail.php${basePath1}_fwd_activity=" . $row["ri"] . "\" target=\"_blank");
        createButton(5, "Test", "activityOp.php{$basePath1}activity=" . $row["ri"] . "\" target=\"_blank");
        echo "</td>";
        return;

      case "owner":
        return TableEdit::applyDisplay($key, $format, $row, $idxLine, jmysql_result(jmysql_query("select concat(name, ' ', firstName) from members where num=$row[$key]"), 0));
    }

    return TableEdit::applyDisplay($key, $format, $row, $idxLine);
  }

}

$obj = new Table;
$obj->colDef = &$col;
$obj->addCol = "ri";
$obj->tableName = "activities";
$obj->tableId = "members";
$obj->defaultWhere = !$bRoot ? "owner=" . $admin["num"] . " and" : '';
$obj->bHeader = true;
$obj->bInsertNew = true;
if (!$bRoot) {
  unset($col["owner"]);
  $obj->dftWhere = "owner=" . $admin["num"] . " and ";
}
$obj->build();
