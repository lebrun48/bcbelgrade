<?php
include "admin.php";
include "tableTools.php";
//dump($request,"request");
//echo "action=$action".NL;

selectAction();

if ($action == "edit") {
  include "tableRowEdit.php";
  include "head.php";

  $col = array("descr" => array(100, "Description"),
      "begin" => array(0, "Début"),
      "end"   => array(0, "Fin"),
  );

  class Holydays extends TableRowEdit
  {

    function applyValue(&$key, &$format, &$row)
    {
      switch ($key) {
        case "begin":
        case "end":
          $y = substr($row[$key], 0, 4);
          $m = substr($row[$key], 4, 2);
          $d = substr($row[$key], 6);

          echo "<input type=hidden name=_val_$key><select id=day_$key>";
          for ($i = 1; $i <= 31; $i++)
            echo "<option value=" . sprintf("%02d", $i) . ($i == $d ? " selected=selected>" : '>') . sprintf("%02d", $i) . '</option>';
          echo "</select><select id=month_$key>";
          global $months;
          for ($i = 1; $i <= 12; $i++)
            echo "<option value=" . sprintf("%02d", $i) . ($i == $m ? " selected=selected>" : '>') . $months[$i] . '</option>';
          echo "</select><select id=year_$key>";
          global $yearSeason;
          echo "<option value=$yearSeason" . ($y == $yearSeason ? " selected=selected>" : '>') . $yearSeason . '</option>';
          echo "<option value=" . ($yearSeason + 1) . ($y != $yearSeason ? " selected=selected>" : '>') . ($yearSeason + 1) . '</option>';
          echo "</select><b style=color:black> " . ($key == "begin" ? "00:00:00 " : " 23:59:59");
          return;

        default:
          return TableRowEdit::applyValue($key, $format, $row);
      }
    }

    function applySave()
    {
      ?>
      el=document.getElementsByName('_val_begin')[0].value=document.getElementById('year_begin').value+document.getElementById('month_begin').value+document.getElementById('day_begin').value;
      el=document.getElementsByName('_val_end')[0].value=document.getElementById('year_end').value+document.getElementById('month_end').value+document.getElementById('day_end').value;
      <?php
    }

  }

  $obj = new Holydays;
  $obj->title = "Edition des congés";
  $obj->colDef = &$col;
  $obj->editRow();
  exit();
}

include "tableEdit.php";

$col = array("header" => array(),
    "descr"  => array(20, "Description", "width:150"),
    "begin"  => array(0, "Début", "width:70"),
    "end"    => array(0, "Fin", "width:70"),
);
$visible = array("begin", "end", "descr");

include "head.php";
//table itself
echo '<title>Congés';
echo '</title></head><body onload=genericLoadEvent()>';

class Holydays extends TableEdit
{

//---------------------------------------------------------------------------------------------------
  public function applyDisplay(&$key, &$val, &$row, $idxLine)
  {
    switch ($key) {
      case "begin":
      case "end":
        global $sWeekDays;
        $dt = new DateTime(substr($row[$key], 0, 4) . '/' . substr($row[$key], 4, 2) . '/' . substr($row[$key], 6));
        return TableEdit::applyDisplay($key, $val, $row, $idxLine, $sWeekDays[$dt->format('w')] . ' ' . $dt->format('d/m/Y'));

      default:
        return TableEdit::applyDisplay($key, $val, $row, $idxLine);
    }
  }

}

$obj = new Holydays;
$obj->colDef = &$col;
$obj->visible = &$visible;
$obj->tableName = "holydays";

$obj->build();
?>
